# A Griko-Italian narrative corpus

Last updated: May 21, 2018

[link to the paper]()

This is a collection of narratives on the Griko language, accompanied with translations in Italian.

The corpus is also annotated with Part-Of-Speech (POS) tags -- the test set (stories 1-11) was annotated by linguists
and it has gold-standard annotations. The training set (stories 12-113) were annotated automatically, with a POS tagger
that should yield about a 93% accuracy.

A full description of the resource, the code, and the POS tagging experiments can be found in our paper (see below).

## Reference
The following paper should be cited in any publications that use this dataset:

Antonios Anastasopoulos, Marika Lekakou, Josep Quer, Eleni Zimianiti, Justin DeBenedetto, and David Chiang. 
*Part-of-speech tagging on an endangered language: a parallel Griko-Italian resource.* 
In Proc. COLING 2018. Santa Fe, New Mexico, USA.

bibtex info:

	@InProceedings{anastasopoulos+al:2018:COLING,
	  author = "Anastasopoulos, Antonios and Lekakou, Marika and Quer, Josep and Zimianiti, Eleni and DeBenedetto, Justin and Chiang, David",
	  title = "Part-of-Speech Tagging on an Endangered Language: a Parallel {Griko-Italian} Resource",
	  booktitle = "Proc. COLING",
	  year = "2018",
	  note = "To appear"
	}



## Questions

Please email Antonis Anastasopoulos (aanastas[at]nd[dot]edu).

## License

The narratives were originally published by Vito Domenico Palumbo in *Io’ mia fora’ - Fiabe e Racconti della Grecìa Salentina* (1998) and *Itela na su pò - Canti popolari della Grecìa Salentina* (1999) - Calimera (LE): Ghetonìa.

The stories were originally retrieved from [http://www.ciuricepedi.it/](http://www.ciuricepedi.it/) in January 2018, under the CC 4.0 License.

This work is licensed under a [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-nc-sa/4.0/).

