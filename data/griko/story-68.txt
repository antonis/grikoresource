Iche i mana tu a ' Pedro ce ìone pleo mbidiusa . Ensìgnase na pratisi .
Prai prai , èftase a paisi mea , epresentefti ambrò mia ' puteka ce ghiùrefse mia ' karitata : Àmone àpote , ipe o putekari , ka ' e sozo kai ti ' karitata .
Èbike strada c' epirte àpote .
Mapale epirte ' s addhon àntrepo , c' ipe : Kamu mia ' karitata .
Evò , ipe cio , esea se norizo , ka ise mia ghineka àscimi .
Evò ime stammena panta kalì .
Bah , ' sù elèone ka ise e mana tu a ' Pedro , ka ' en ise ghenomena mai kalò .
Motti icha , ipe cini , evò èkanna kalò .
Àmone àmone , ipe cio , ka ise e mana tu a ' Pedro , c' ekanì .
Epirte àpote , c " en iche mai tìpiti .
Epirte es t' addho paisi , epresentefti ambrò mia ' ftocheddha : Dommu tìpiti na fao .
Evò , ipe cini , ' en echo ; emàrefsa alio zumari ce tù' me fimmena na fsichrani : arte pu nsignò na fao , su dio alio .
Ensìgnase na fai , c' is èmine a spirì zumari : Na , ipe , fato .
Mò' fere poddhìn alio , ipe cini ; evò , ghi pina , ' e se mblevo makà pleo .
Tuon ìsosa , ce tuo sò' dika , ipe ftocheddha .
Evò ' e se mblevo pleo ghi ' pina pu vastò .
Prai mian addhi porta , ipe cini , ka torì a ' su dòkune tìpiti addho .
Epirte àpote , ensìgnase na votisi , c " e tis èdie tispo tìpiti .
Epirte ambrò ' s a spiti , c' ecessu iche ena pu mare zumari ce krea : Dommu mia ' karitata , ipe , ja fsichì tu ciurusu .
Ti fseri ka o ciùrimu e ' pesammeno ?
O fsero , ipe cini .
Èbike c' is èdike an òrio piatto : Fato olo .
Tò' fe olo .
Etelis a miuli krasi ?
Kae cio pu teli .
Epirte c' is èfere an òrio miuli krasì .
Tò' pie .
Estei kalà ?
Steo kalà , ma a ' mò' fernes an addho , tò' pinna puro .
Etelis alio sciotta as to krea , ka en olon alai , na pi ?
Sì , ipe cini .
Tò' pie olo .
Èbike strada , c' epirte àpote .
O zumari ce e sciotta u krea ensignàsane n' is ebattèfsone ta tàntara .
Eguike atto paisi , c' ensìgnase na pratisi . Motti iche kàonta a spirì strada , ensìgnase n' is ponisi e cilia .
Ohimmena , ipe cini , ti mu ponì e cilia !
Is irte na chesi c' echesti oli , ce kòmose olu tus fustianu . Skosi c' ensìgnase na pratisi .
Sekundu prai is koddhùsane i fustiani es to kolo . Ohimmena , ipe , att' echi pu mu koddhune i fustiani es to kolo !
Èbike ce fseputisi , ce ide olu tus fustianu chemmenu .
Epirte , c' ìvrike mia ' lakkuara gomai nerò .
Embike cini ecès to nerò , c' ensìgnase na plisì . Èbike tus fustianu , ce us èbline ; estannòsane , us èvale , c' ensìgnase na pratisi mapale .
Prai prai , epirte es ti ' Romi : Fio na pao ecì , ipe , ka ecì echi o pedimmu , o ' Pedro .
Epirte es ti ' Romi , embike es tin aglisia , c' epèsane ecessu .
O Kristò in èmbiefse i ' fsichì es ton anfierno .
Dopo cerò epèsane o Pedro , c' epirte es to ' Paradiso , ce tù' rte estennù attin mànatu : ' En echo kammia notizia as tin mànamu , ipe u Kristù .
E mànasu , Pedre , ipe o Kristò , estei es ton anfierno .
Ah , ipe as Pedro , ka puro i ' mànamu èmbiefse es ton anfierno ? ' En ènghize n' in guàlome apu ' cessu ston anfierno ?
Acce , Pedre , ce ròtati an èkame mai kalò es ti ' zoitti .
Epirte o Pedro : Mana !
Pedàimmu ! ipe cini .
Èkame mai kalò es ti ' zoissu ? mia ' forà manechì .
Ce ti èdike ? is ipe. mia ' forà èdika o ' spoglio tu krimbidiu .
Epirte o Pedro es to ' Kristò ce u tù' pe ka èdike o ' spoglio tu krimbidiu . Nàona pu stei kremammeno o ' spoglio pu èdike e mànasu .
Piatto ce pai c' i ' lei n' afsakkosì , c' i ' sirni defore .
Epirte o Pedro c' ipe : Fsakkosu ettù sto ' spoglio tu krimbidiu .
Efsakkosi ma dio chèria . E fsiché efsakkòsisa ecisimà .
Apàte , apàte , ipe cini , ka mea estè ce me sirni o pedimmu . Epianni , c' èdike botta n' i ' siri defore .
Apàte , apàte , ipe cini , o ' fsichò pu stèane brafsomeni es safti .
Motti ipe iu , efinni o ' spoglio , c' embike pleon akau .
Epirte as Pedro es to ' Màstera : Màstera , ipe , èfike o ' spoglio tu krimbidiu e mànamu , c' embike pleon akau es ton anfierno .
A ' tei na pa ' n' u ' dì , ankora estei ecì .
