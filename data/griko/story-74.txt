Io ' mia forà c' ion ena ' ria ce mia regina c' ìchane tria pedia .
Enan vrai o ciuri us fònase olu ce tri ce os ipe : Avri pornò ènna mu pite oli on ìpuno pu ìsesta janomena i ' nifta .
O pornò , tappu skòsisa , pirte o mea : Bongiornu , papà .
Bongiornu , fiju miu .
Ce t' hai sonnatu stanotte ?
M' aggiu sonnatu ca m' hai datu Napuli .
Te sia koncessu .
Depoi pirte o menzano : Bongiornu , papà .
Bongiornu , figliu miu .
Ce t' hai sonnatu stanotte ?
M' aggiu sonnatu ca m' hai datu Roma .
Te sia koncessu .
O kecci arte ' en vasta malizia , pirte ' mbro sto ' ciuri : Bongiornu , papà .
Bongiornu , figliu miu .
Ce t' hai sonnatu stanotte ?
Nienti .
Vattene , ipe o ciuri , tu no si figliu miu , si figliu de quarche guastasi .
On èbbie matsae ce on èguale atto palai .
Cino nsìgnase na klafsi ce prai ' mbro sti ' kampagna .
Ekàise kau ' s enan àrgulo ce tù' rte ìpuno .
Pirte enan bèkkion ecisimà ce tù' pe : Fsunna , fsunna , pedàimmu .
Ti kannis ettusimà ?
Ce cinon ipe : Ti kanno ? M' ègguale p' essu o ciùrimmu , c " e me teli ja pedìn dikottu pleo .
Depoi ipe cinon : Ghiatì s' èguale ?
Jatì iche ponta n' u po on ìpuno pu ida tunifta , ce evò ' en ida tìpoti ce tipo tù' pa .
Ce cinon ipe : Chronduna , jatì e ' tù' pe ?
Amo ce kame na su doi ampari pu troi ena ' stuppeddhi c' ìmiso tin emera .
Jùrise essu tuo ce pirte ' cì sto ' ciùrittu c' ipe : Pàpammu , ipe , fsechorisomme : evò ntropiàzamo na su po on ìpuno pu icha donta .
Ellae ' ttu na kùsome ti ides es ìpunu .
Ida ' s ìpunu ti mu doke t' ampari pu troi ena ' stuppeddhi c' ìmiso kannellinu .
Ce cinon ipe : Àremo is s' èmase na pis iu ; ' en i ' loja dikasu tua .
Meh , sia : piatto .
Pirte kau st' ampari cino : Amparàimmu , ' sù ènna me visisi .
Ce cinon ipe : Basta ti mu di na fao , penseo ' vò ja sena .
Cino èkame mia ' pruista kannellinu , èbbie ampari , ' nneke ce nsìgnase na pratisi .
Dopu iche kàmonta poddhì poddhì stra ' , èbbie c' ìvrike mian allakai krusì . C' ipe t' ampariu : I ' pianno ? ipe .
Ipe cino : A ' ti ' piai patei , ce an de ' ti ' piai puru .
Ce cino èbbie c' in èbbie depoi .
Nsìgnase na pratisi mapale , mian addhi forà . Pianni c' ìvrike mia ' sella krusì .
Pianni c' ipe : I ' pianno ? ipe . Ipe cino : A ' ti ' piai patei , ce an de ' ti ' piai puru .
Ce arte i ' pianno , ipe cino . C' in èbbie .
Nsìgnase na pratisi ce guike ' s emmia cità .
Pirte sto ' ria c' ipe : Me teli ja servituri ?
Cino ide ka e ' sperto c' ipe : Sì , se telo ja kambarieri . O ria tù' che pianta poddhìn agapi .
E servituri e addhi ìchane mbìdia ti on gapa poddhì .
Pianni ce mian emera spièfsane ' cé sti ' kàmbara c' ida ' tutti sellan e servituri , ce on ekusèfsane sto ' ria . C' ìpane : O Giuseppìnossu , cino p' u teli tosso kalòn esù , fseri ti ipe ?
Ipe ti fidete na piai ampari cinì sella .
O ' fònase o ria : Ah , Giuseppine , evò se gapò tosso ce ' sù eklìnnese pus emena ; jatì ' e mu leis es emena cippu os ipe tos addhò ?
Evò ? ipe cino , ka ' vò t' ipa ? ' En ipa tipo .
Esù ipe ti fidese na piai t' ampari ...
Echi mia ' sella oli krusì , c' ipe ti fidese na piai t' ampari cinì sella .
Cino ipe : Kakò mu tèlune , signòremu , ipe ; evò ' en efsero tipo .
Eh , ipe cino , o pa ' ce o pianni , ce andé pena de la vita .
Cino ipe : Allora ' sù teli na me chasi .
Oi se channo o deje , esù ènna pai na m' o piai .
Tuo ' kkateke kau ce pirte st' ampari : Mparàimmu , mparàimmu , ide ti mu kànnune ?
Ipa ' ti ' vò fidèome na piao ampari cinì sella pu vrìkamon , ipe .
Cino tù' pe : Stupide , evò sù' pa na mi ' fiki mai niftòn essu , ce tappu guenni , na klisi ce na mu doi to klidì es emena .
Arte , amon es to ' padrùnassu ce kame na su kami mia ' briglia oli krusì .
Presta o ria u tin èkame .
In èbbie tuo , èbbie utti sella pu iche vrìkonta , ' nneke panu st' ampari ce nsìgnase na pratisi .
Praise ja ena ' mina fissa ; èftase ' mbro ' s emmia muntagna . Ipe ampari : Meh , ' kkatea , ipe . ' Kkateke .
Ipe : De , stasu ' kkorto cippu su leo : de ti ènna diaune dòdeka ' mparia .
Ce ampìs ampì pai ena ' tsoppo . Maleppena torì ' sena tsumpei tris forès , ipe , an esù stes tris forè fidese n' u vali ti ' briglia , o pianni ce o kratenni citton ampari , an de ' menis esù ' cipanu .
Tuo ' nneke ' cipanu c' ide utta ' mparia pu diaìkane : dòdeka , eccena eccena .
Cino pu mpi io ' tsoppo .
Maleppena ide tuo nsìgnase na tsumpefsi .
Chiusti cino n' u vali i ' briglia ce ' e fidèato .
Sti ' pronì forà ' e fidefti , sti ' sekunda depoi u tin èvale .
Tò' vale i ' briglia , ce itt' ampari guike olo krusò .
Depoi ' kkateke kau st' ampàrittu c' ipe : Beh , o piàkamo tunìs forà , skappèfsamo .
Ipe cino : Basta ti kuis emena , esù ' en ei mai kakò , ipe cino . ' Nneke panu st' ampari , i ' sella tu tin vàlane cinù mpariu , ce nsignasa ' na pratìsune .
Prai prai , ce ftàsane sto paisi tu padrùnatu .
Arte pu us ide , o rian , atto largo : Vu t' en òrio citt' amparin !
Èmine stupito . Eh , brao Giuseppino !
Eh , brao . Poka fidefti n' o piai t' amparin , ipe .
Cinon ipe : Mi ' fatia tò' bbia : ei tri minu ti mankeo pu ' ttu .
Eh , brao Giuseppino ; eh , brao !
Evò cippu mu jurefsi su dio , arte pu mò' fere tutt' ampari .
Èbbie trisakosce lire ce u te ' doke ja rigalo .
Arte este kuttento m' utt' ampari ce u piace poddhì .
Mian emeran ipe : Giuseppine , ndisu ka ènnà' guome spasso .
Ipe tuo : Maestà , àimme sìmmeri , ka pao strakko ; ti ènnà' rto na kamo ?
Ipe cino : Ndisu ti guènnome ' nkarrotsa , makà ' llampede .
Ce minon , ipe cino , na pao na doko na fai ampàrimmu .
Ipe cino : Pai o servituri .
Pianni ce u doke ena ' stuppeddhi c' ìmiso kannellinu c' èmbiefse o ' servituri , on addho .
Tuon ' en èftase arte na pai kau na klisi i ' kambarattu . E servituri spièfsane ' cessu ce ida ' tin allakain arte .
C' ipan , ena mo addho : De , de ti echi ' ttossu ; ènn' on ekkusèfsome , n' o ' kàmome na chasì pu ' ttupanu .
Maleppena ritirefti o padruna , o ' fonasa ' ce u to tù' pane : Fseri t' ipe o Giuseppino ?
Ipe ka fidete na pai na piai i ' kiatera pu vnesi ' cittin allakai . Ah , ipe ti ' e ti ' pianni , ìpane , na mi ' tu ti ' piain esù .
O padrunan io ' paddhikari puru .
Tuo to ' fònase : Giuseppine , ellae ' ttu na su po .
Ti kumandei , ' ccellenza ? ipe .
Esù fseri ti se gapò tosso ce ' sù ènna me tradefsi puru !
Ipe ka fidese na piai ti ' kiatera pu vnesi ' cittin allakai , c " e ti ' pianni na mi ' sù ' piao ' vò .
Evò se gapò tosso ce ' sù mu kannis tuttes aziune .
Evò ' en ime pimena tipo , evò ' en efsero tipo , kakò mu tèlune .
Ka pu ènna pao n' in briko ?
Evò ' en efsero kammia noa .
Esù tù' pe : ènna pai n' i ' piai , ce an de ' ti ' piai , pena de la vita .
Presta tuo nsìgnase na klafsi , ce ' kkateke kau st' ampari . Ipe : Amparàimmu , torì ti èndesa ?
Lei o padruna ka ipa ' vò ti fidèome na pao na piao ti ' kiatera pu vnesi ' cittin allakai , c' evò ' en efsero tipo .
C' ipe na pao n' i ' piao , andé pena de la vita .
Ipe cino : Esù ' e kuis emena ce ndenni panta utta guàita .
Jatì ' en èglise ti ' kambarassu ?
Ipe cino : ' E m' èkame manku na ' kkateo nà' rto na su doko na fain esena .
Meh , ipe , amo ce jùratu mia ' sarma mèlina .
U doke itti sarma ti ' mèlina , i ' fòrtose panu st' ampari , c' ipe n' u doi ena ' chrono cerò ; ce ipe : an diaì o chrono c " en en ghiurimmeno , ti ' en ghiurizi pleo , ti cinos e ' pesammeno . ' Nneke panu st' ampari ce nsìgnase na pratisi .
Èftase mes enan daso , c' ìvrike tosse furmìkule male male male , ce pian ole pesammene u fain .
Èbbie i ' mèlina ce os tin doke oli n' i ' fane .
Poi mapale , prai prai , c' eftasa ' ti ' tàlassa . Ipe tuo : Arte ' en brìskome pleo stra ' na pratìsome , oli tàlassan brìskome .
Ipe tuon : Amparàimmu , putten ènna diaume arte ?
Nerò ce nerò torume ' ttu .
Ce cino : Min ei kammia paura ; kratestu fermon es emena , ce penseo ' vò na diaò pu ' cé sti ' tàlassa .
Dopu kàmane in ìmisi stra ' pu mes ti ' tàlassa , vrika ' ti ' mamma Serena : is ichan endèsonta ola ta maddhia ' s emmia pèntuma c' este kremammeni ' cì .
Tuon ipe : ' Kkatea , Giuseppino , ce piatti ta maddhia pu ' cì sti ' pèntuma , ftecheddha , ti ei tosso cerò pu patei .
In eskàppefse pu ' cì ce cini ipe : Oh , pu na stasì panta kalò , pedàimmu ; tappu eis ebbesogno pus emena , fonasomme ti ' vò se visò .
Mbarkefsan oli ti ' tàlassa ce guikan defore depoi .
Vrìkane ena ' largo ; me ' citto largon iche frabekaon ena ' palain òrio .
Rifìskefse lio t' ampari ; u doke na fai ; nsignasa ' na votisu ' na vriku ' ti ' porta pus tutto palai .
Vota vota , vota vota , c " en briska ' pùpei porta .
Sulamente panu panu iche mia ' fenestra kèccia kèccia , a stentu na chorisi enan àntrepo na diaì pu ' cessu . Arte este poddhì panu : Pos kànnome ?
Ettù ìsele mia ' skala na ' nnèome .
Pos kànnome ?
Ipe ampari : Evò sozo tsumpefsi tris forés , ipe ; an esù sozis etsikkostìs ecì stes tris foré , andé mènome ' ttu c " e sòzome jurisi pleon essu , ipe .
Dopu mbennis ecessu , de ti vriski mia ' giòveni ce ste plonni kau sta fta veli , ce de ka se pianni mo kalò ; esù mi ' ti ' milisi mai mai , ka andé jurizi fse pedra màrmara .
Giuseppine , akkorto : mi ' tis pivi de ' kalòn de ' fiakko , sia ka ise muton ènna kami . 'Nneke panu st' ampari ce doke o pronò sarto , c " e fidefti na mini sti ' fenestreddha ; doke on addho , o ' sekundo , ce manku fidefti .
Oh , Giuseppine , ipe ampari , enan addho ènna doko ; an de ' fideftì ìmesta chameni oli ce diu .
Doke o ' tertso ce tò' ndese mia chera ' cé sti ' fenestreddha .
Sirti ce mbike .
Mbike c' ide utti giòveni kau sta fta veli , mia ' giòveni òria , ce ste c' èblonne .
Ìkuse lillì rebatto cini ce osson efsùnnise , c' ipe : Ahimé , ce sonnu felice !
Pos èkame , pos èkame nà' mbin ettossu ? ipe .
Ce cino citto .
Cini nsìgnase na pi : Oh , signorìnomu , t' ise kaluddhi ; posson ei ti se meno nà' rti ; àremo posson estràkkefse na ftasin ettù de ' ?
Pos èkame nà' mbi sentsa porta ?
Ellae , jatì ' e mu milì ?
Pemmu putten ise , is ise , peon e ' to paìsissu ?
Topu ide ti ' e ti ' milì , i ' botìsane , nsìgnase n' u pi loja : Guastasi , lazzaruna , ladro !
Esù irte na me pari ; ma ' e ti ' torì na me pari , ce ndìnnato .
I ' rtomeni tossin addhi kàglio pi ' sena , c " e m' i ' permena , arte ' sù , enan villanàccio , enan ascimuna ènna me pari ?
Stasu mea ti me perni !
Endisi tui arte ce cino ìsele n' i ' piai n' i ' pari .
In efsìkkose ferma tuo , ìvrike mia ' skalan ecessu stesso c' in enneke panu citti fenestreddha , ce tui lèonta loja .
Ipe amparai : Krai , krai , min gheftì danno .
I ' piàkane sadia sadia c' in ekkatèkane .
In èvale panu st' ampari , ' nneke cino ce nsignasa ' na pratìsune mian addhi forà .
Prai prai , prai prai , c' icha ' kàmonta in ìmisi stra ' mes ti ' tàlassa , ce tui lèonta loja sti ' stra ' . Èguale o ' diamanton atton dàftilo ce o ' pègliase ' cé sti ' tàlassa ja ràggia .
Diaikan atti ' tàlassa ce mbikan mes to daso mapale .
Èbbie us passantu atto sfòndilo , us espètsefse , ce us entortìgliefse ' cé sto daso .
Prai prai , prai prai , mapale ce ftàsane sti ' cità pu icha ' na pane .
Dopu i ' torusa ' tutti giòveni : Oh , t' en òria !
Ka pu in ìvrike tutti giòveni tosson òria ?
Tuo , ka ìfsere arte ka iche diaonta ena ' chrono , tuso padruna iche ' nnèonta panu sti ' làmmia , na di a ' tus eskuperei .
Us eskupèrefse atto largo c' ipe : Oh , ti en òria perdiu ! ' En ìfsere pos ekkatenni ' s fuddha .
Tuo sumpone ti u kanni diu ' kkoglienze arte pu ftazi , ce ' kkateke kau sto ' portuna . Dopu in ide atto kùkkio , nsìgnase na tramassi ji ' charà , tuo , ka ion òria .
Tui in ekkateka ' pu ' cipanu .
Tuo pirte n' i ' doi i ' chera : Fio pu ' ttu , ipe cini , ce ' nneke panu sto palai . ' Nneke ' cipanu ce klisi ' cessu mia ' kàmbara pu mpi , ce ' en ènifte tinò .
I ' fonasan arte : Ti figures i ' tue na stasì panta ' ttossu klimmeni ?
Ei tosso cerò ti pame votonta na se vrìkome !
Ipe tui : Fseri poan guenno ?
Tappu mu fernu ' to ' diamanto pu vàstone ston dàftilo .
Èbbie c' ipe tuo : Nghizi na pai , Giuseppine , n' is feri to ' diamanto .
Ce ' vò pu ènn' on briko ?
Cini o ' pègliase tappu stèamo emmés ti ' tàlassa .
Eh , nghizi na pais , ipe cino , ce andé o chìrusu .
Tuo ' kkateke klèonta kau st' ampari : Mparàimmu , mparàimmu , mapale manku na rifiskèfsome ' e mas fìnnune . ' E s' o tù' pa ' vò ti pateis ? ipe ampari , ce ènna patefsis ankora .
Meh , sia , faccia Diu . ' Nnea na pame na dume a ' to ' vrìkome .
Nsìgnase na pratisi c' èftase sti ' tàlassa .
Nsìgnase na fonasi : Mamma Serena , mamma Serena , arte eo besogno pus esena na me visisi .
Ipe tui : Ti teli , ti teli , pedàimmu ? ipe .
Ti telo ? ipe cino : Na mu vriki ton diamanto pu bèjase isi signura tappu diaìkamo pu mes ti ' tàlassa .
Presta cini doke ena ' fiskio ce sianòstisan ola ta fsària : Votìsete presta , dete an brìkete enan diamanto ecé sti ' tàlassa .
Ida ' mian àkula ' mperiale petonta ce on basta sto lemò .
U ton doke cinù ce pirte n' u to ' pari depoi u padrùnatu .
Maleppena èftase : Brao , Giuseppine , brao , Giuseppine .
Pìrtane sti ' signura na paru ' ton diamanto ; skaràssefse lio ti ' porta cini ce on èbbie , c' èglise mian addhi forà ti ' porta .
Cini ipa ' mapale : Mapale klinni ?
Jatì ' e nifti na se dume armenu pos ise ?
Ipe cini : Fseri poa nifto ? Tapu mu fèrnete olu tus passantu pu bèjasa me ' citto dason , ipe .
Ènghise na pai mian addhi forà o Giuseppino ecì sto daso .
Mbenni mes to daso , nsìgnase na fonasi : Regina os furmìkulo , dela visisomme !
Cini ìkuse : Ti teli , pedàimmu , ti teli ?
Na mu vrikis , ipe cino , olu cittus passantu pu bèjase isi signura motti pu diaìkamo pu ' ttossu .
Kulumòstisan oles e furmìkule , c' ipe cini : Votìsete , ka ènna mu vrìkete us passantus , ipe .
Nsignasa ' na votisune ce vrikan oles ena ' kokkon ghiana .
U tu ' dokan olu cinìs , us èvale ' cessu ' s ena ' chartì ce us pire .
Maleppena èftase , arte , cheresti o padrùnattu : Brao , brao , ipe o padrùnattu , arte torume mi guenni n' in dume .
Pìrtane mpi sti ' porta : Signorina , nifse na su dòkome us passantu .
Cini kàlefse i ' chera ce us èbbie c' èglise mian addhi forà .
C' ipe : Ti figures i ' tue ?
Mapale klinni ?
Ti skusan ènna vrikin addhi ?
Ipe tui : Fseri poa se pianno ' sena ? Dopu itto svergognao pu m' èfere pu ' cì , on baddhis ecé ' s ena ' furno ce o ' kanni na kaì .
Fònase ena ' furnari : Vale lumera , ce dopu on eftazi nà ' ' fermo fermo me fonazi .
O Giuseppino ekateke kau na doi na fai ampariu .
Dopu efe ampari , ipe : Dommu na pio .
U doke na pi c' èbbie efse menze nerò ; pu mai na pi tosso nerò citto ampari !
Pianni , tappu èftase o ' furno o furnari , fonasa ' to Giuseppino . Ipe : Beh , tuos ene o ùrtimo diskorso pu kànnome oli ce diu ; ènna se beliso ' cé sto ' furno ti andé ' vò ' e ti ' pianno cini pu èferes esù .
Achà , puru tuo pu panu , ce su ime janomena tosso kalò ! ipe cino .
Ipe : Giakka ti peseno ' vò , ènna pesani puru ampàrimmu ma ' mena , ipe ; pronà belite ampari , depoi belite ' mena na kaume oli isa .
Us pelìsane olu ce diu ce us klìsane ' cé sto ' furno .
Ampari , maleppena o pelìsane ' cessu , nsìgnase na fserasi olo citto nerò pu iche pìonta . Ce iun depoi èsbise oli ti ' lumera .
Tui arte pirta ' na spiefsu ' na du ' m' i ' pesammeni ce u ' brika ' pleon òriu pu tappu u ' balan ecessu .
Arte is to tù' pa ti ' signura , ce ipe cini : Torì po ' guike òrios o Giuseppino ?
Ius ènna kami puru ' sù , ti arte pu guennis atto ' furno guenni pleon òrio , ce toa stefanònnome , ti ' vò se telo pleon òrio pi ti ise .
Vala ' lumera ce vala ' to padruna ecé sto ' furno .
Pirta ' na spièfsune ce iche kaonta .
Guike e signura pu ' cé stes kàmbare c' ipe : Giuseppine , esù ise patuta , c' esù ènna godefsi ; emì ènna piastùmesta oli ce diu .
Stefanòsisa oli ce diu , kàmane invitu , ce stèune ngrazie de diu , ce ste godèune puru àrtena .
