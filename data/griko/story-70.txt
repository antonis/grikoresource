Io ' mia forà ce ion ena ' ria ce mia rigina ; ce ichan ena ' pedì ; ce telusa ' n' o rmàsune c " e tu piace kammia kiatera .
Iche ena ' palain duka , c' este derimpettu cino tu ria .
Mian attes foré effaccefti sti ' barkonà o pedì tu ria ce ' cessus enan ghiardino iche mia ' kiatera poddhìn òria ; ce isi kiatera iche enan vialin olo koppe , m' ena sikkiuddhi c' es pòtize ole .
Cino , torontai , u piàcefse poddhì cisi kiatera : " Ah " , ipe tuo , " pu n' ìfsera pos kui ! " .
Mian emera eguike e serva c' ipe : Signora Katerini !
Komanda , ipe cini . " Vu " , ipe cino , " signura Katerini kui " .
T' addho pornovo èbbie ce pirte sti ' barkonà cino , c' in ide tuo mapale iu sverta ce ipe : Bongiornu signuria , signora Caterini ipe .
Centu e mill' anni , cavajeri miu !
Comu te stae lu tou petrusinu ?
Quante fujazze nce basilikoi ? ipe tuo .
Ipe cini : Quante stelle nce a ncelu , e pedre a via , tante mina lu fiume di Giordanu .
Èbbie ce pirte mapale tuo .
Quai pornò ce quai vrai i ' tori tutti kiatera .
Èbbie c' is ton èmbiefse lèonta ; ce cini ipe deje : " Ti cino ' en e ' skarpa ja mena " .
Èbbie c' in èfike stei cino .
O vrai , arte pu klìsane ole tes porte , epai tuo m' ena marteddhai c' is klanni ole cittes koppe .
Tui skosi o pornòn amprimu ce es ide .
Skonni olu tu ' servituru , ole te ' serve ; iche tes addhes koppe preparae ; egomonna ' tes addhes koppe , pianna ' te ' stesse kiante c' e ' baddhan ecessu . Mira kaman iu ce mira skupìsane .
Nsomma tui èkame sia ka ' en iche tipo , ' e chòrise ja tipo .
O pornò skosi c' es ide tuo : " Ah , ti ghineka ! " ipe tuo . Epirte : Bongiornu signuria , signura Katerini .
Centu e mill' anni , cavajeri miu .
Comu te stae lu tou petrusinu ?
Quante fujazze nce basilikoi ?
Quante stelle nce an celu e pedre a via , tante mina lu fiume di Giordanu .
Èbbie ce pirte tui mapale .
Tuon èbbie ce sekùndefse , c' is to tò' mbiefse lèonta mi o ' teli .
Tui èbbie c' ipe : Sine .
Arte ipe tuo : Pos ènna diskorrèfsome ? ipe .
Cittu , scemo , ka penseo ' vò , ipe cini .
Nordinei mia ' tàola cini makrea ; e barkonae stea ' pacceffrunte o vrain , dopu plosan oli , fsikkonni utti tàola c' in baddhi sti ' barkonatti ce sti ' barkonattu .
Èbbie cini u tò' kame simai ce pirte stra ' stra ' pu ' cì . Ce o palai io ' fse diu piani arte .
Èbbie ce pirte essu cinivi . Tappu diskorrèfsane , beh , arte na doku ' tosse strinte de manu na pai cino .
Dapu este nà' mbi pròbbio sto palàitu , èsire i ' tàola ce on èkame na pesi . Ecimpivi iche chrondà ce chrondava !
Itti nifta èkanne o brao chioni puru . Èpese c' èmine ' cimpì cino oli ti ' nifta .
O pornò skòsisa ce pirta ' ce on brìkane ' cimpì . Arte , pensa , o pedì tu ria n' on brìkune ' cimpivi !
Pirta ' ce o ' piaka ' ce on bàlane sto gratti , na parun olu tu ' mèdeku .
Oli lèane ti ' en e ' kammia pleo . Cino tis iche i ' pena cinivi : " Ja kalòn dikommu , na peso vovo ! " .
Ti pense arte ti o trade cini ? Iche ena ' tio mèdeko tui ; isa isa iche èrtonta atti ' Nàpuli pu iche pianta provilègio : Esù , u tù' pe u tìuti , ènna mu dokis ola ta rucha pu ìbbies endimeno , ce na mu kamin diu kartelle na mi ' kamun de ' dannon de ' ùtulo .
Endìnnete tui ; ' nnenni panu ' s enan ampari ce nsìgnase na passiefsi olo to paisi tui .
O tìotti i ' pènsefse ce nsìgnase na jelasi .
Pirta ' ce u to vàlane st' aftì tu ria ka iche ena ' mèdeko pu ste c' èrkete atti ' Nàpuli .
Tui presta n' o ' fonàsune .
Èbbie ce ipe cino ka iche na ' nnei m' olo t' ampari ecianu .
Tapu ' nneke ' cianu : Beh , ipe , ènnà' guete oli oli pu ' ttossu .
Eminan oli ce diu e ' citti kàmbara .
Nsìgnase n' o ' tantefsi olo tui : Oh , figliu miu , ipe tui , serà c' ai vutu quarche cascata utra li grossi ? ipe .
Beh , piae tuttes kartelle , ce pie mia ' quai pornò .
Dapu ftàzune fto meres , ipe , se skònnune ce pai ce ' ffaccese lion e ' citti barkonàn , ipe .
Sine ! ipe tuo , ftechuddhi .
Ettuna cini , sti ' zoì , vasta mia ' cinta oli fse krusafi . T' en òria isi cinta ! ipe tuo .
I ' telis ? ipe .
Io ' kalòn ! ipe tuo .
Beh , ipe , ce fseri poa su ' n dio ? ipe , arte pu is filì to ' kolo ti ' mùlammu , ipe .
Ka is to ' filò , ipe cino , ti mu kanni ?
Epianni i ' cinta ce u tin doke .
Èbbie ce kateke cini pu ' cianu .
Stes eftò , cino , èbbie ce nfaccefti e ' citti barkonà ; ma pleo koscigna n' o ' kratèsune .
Èbbie cino , mapale , ipe : Bongiornu signuria , signura Katerini .
Centu e mill' anni , cavajeri miu .
Comu te stae lu tou petrusinu ?
Quante fujazze nce basilikoi ?
Quante stelle nce an celu e pedre a via , tante mina lu fiume di Giordanu . Èbbie c' ipe tuo : Ce bellu tremulizzu de graste ! ipe tuo .
Che bella cascata ' ntra li grossi ! ipe tui .
Che bella vasata nculu alla mula !
Tuo , tappu tù' pe iu , quai ti u tù' rte ce sirti ' mpivi .
Nsìgnase na piai pena tuo mapale , ce jùrise ampì mapale .
Dopu stasi mapale kamposso cerovo , nsìgnase na pai pleo kaluddhi ce stasi kalò tuon depoi .
Tappu stasi kalò , èbbie c' is to tò' mbiefse lèonta mapale .
Èbbie ce rmàstisan depoi . Stàsisa kalì kuttenti , kalì kunsulai .
Spìccefse o kunto .
