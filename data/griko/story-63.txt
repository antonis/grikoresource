Io ' mia forà c' ion ena ' ria ce mia regina c' ichan ena ' pedì ce telusa ' n' o rmàsune .
Iche mian embròs èssuto ' s ena ' palai c' ìone mia kiatera signuru .
Dunque , ipe o pedì , dakké ka ' sì tèlete na me rmàsete , mbiefsetèisto lèonta cinìs kiatera .
Èbbie o ciuri ce fònase ena ' konsijeri ce u tù' pe na pai n' u pi u ciuruti mi telu ' na kamu ' to matrimogno .
Èbbie ce pirte iso konsijeri , ce u tù' pe mi teli na kamu ' to matrimogno .
Erespundei e mana ce o ciuri u korasiu ce ipe : Cini ènna telisi , ka ' mì ma deka chèria , ka e ' pedì riu ce ' mì ' e ton esfuggèome mai .
Fonasa ' ti ' kiatera c' is to tù' pane , ce cini ipe ka ' e to teli mai mai in eternu .
Epirte ce u to tù' pe u ria . O ria tu dispiàcefse sentsa fina ka ' en iche telìsonta nà' i utton disonoro .
Tutto pedì tu ria , arte , èmine èmine , c " en ìtele n' armastivi .
Depoi e mana ce o ciuri o kostringea ' n' armastivi , ka cino manechòn ìchane , ce u lèane : Arte , pedàimmu , andén ermastì na kamis esù redi , ' s tinon ènna fìkome to rucho ?
Epianni o pedì c' ìvrike mian addhi . Tunì tis tin basta dakkameni , ka ìsele n' is tin eskuntiasi .
Èftase ora pu iche n' armastivi c' ichan epprontèfsonta otikaneve .
Depoi is ipe i ' mana : Arte , mamma , ipe , dekké cini ' e mas tèlise na ' pparentefsi , ipe , i ' telo sti ' tàola , ipe .
Gnorsì , ipe e mana .
Fonasan ena ' konsijeri na pai n' is to pi u ciuruti .
Epianni tu to tù' pane , ce fonasa ' ti ' kiatera , c' e kiatera ipe : Gnorsì .
Epianni , e kiatera , ftiazi mia ' kàscia c' èvale tris àbetu , ola ta guàita ta kalà .
Pirta ' mi ' karrotsa c' i ' piàkane . Eftàsane sto ' sponsalizio , ermàstisa .
Efane ce vàlane ballu .
Epianni , topu spiccefsa ' tu ' ballu , usi kiatera fònase o korasi .
Enifti itti kàscia ce i ' difti ittus tris àbitu tunù korasiu . Utto korasi io ' fseni .
Respundei utto korasi c' ipe : Oh , ti àbeti ! ipe : pu tus ìvrike tutus ?
Makari ka echo ' vò àbetus , ipe , ' en eftazu ' tutu . Arte icha ' na pa ' na kamu ' te ' bìsite .
Beh , ipe tui , us teli n' u ' bali ?
Sì , ipe tui . Andén , ipe cini , ' vò su kanno disonoron esena arte pu tu ' baddho . ena fs' uttus àbetu , o lavoron ìone : tuttu nu campu de fiori , kundu i ' primavera arte pu fiurèune .
O addhon àbeton ìone kundu i ' tàlassa , ole te ' ratse t' afsariu pu echi .
Ce o addhos ìone kundu on anghera arte pu stei olo stiddhao .
Beh , ipe tui , mentre ti ' sù te ' na vali tuttus àbetus , ipe , telo na mu kamis enan addho piacirin esù : ' e m' o kannis ? ipe .
Cippu telis , ipe tui .
Arte , ipe cini , ecé citti kàmbara ei o gràttimu , ipe , ce ' cèssuna echi o dikossu .
Arte ' vò telo pus esena , ipe , tunifta pu ènna plòsome , esù na plosis es to gràttimu , tù' pe u korasiu , ce ' vò na ploso sto dikossu .
Sine , ipe tui , ftecheddha .
Epianni c' èftase o vrai .
Ce ddhassa ' to gratti ; kundu iche na pai sto gratti tu korasiu tuo , pirte ecì sto gratti cinìs kiatera , sumpone ka ei itti kiatera .
Tuon ecì citto gratti pirte skuscetao , sumpone t' iche itti kiateran ecessu .
Epirte cino ecì citto gratti alio risoluto !
Arte pu is èkame cippu tèlise èguale a fsalìdia c' is èkofse a maddhia . Èftase o pornò ce skòsisa : tui ftecheddha , pirte ' cì citti kiatera ce nsìgnase na klafsi .
Ipe tui : Cittu , scema , pu penseis ! ipe .
Arte se ftiazo ' vò na mi ' chorisi tipo .
Èbbie is èlise a maddhia ce nsìgnase n' is ta ftiasi , n' is ta koddhisi .
Èbbie ce irte o àndratti , o ria , ce ipe o ria : Beh , ndisìtesta , ti ènna pame na kàmome te ' bìsite .
Epirte stin ghinèkattu ce cini este kurria .
Tui tis iche ponta : A ' se rotisi , mi ' pi ti echi .
Tuo nsìgnase n' i ' rotisi : Ti echi , ' en estei kuttenta ?
Ti echi ? ' En echo tipo , ipe tui .
Èbbie c' in èndise m' utton àbeto ; ndìsisan ole ce diu ce guika ' na pane stus passèggiu na kamu ' te ' bìsite .
Dopu ritirèftisa , epianni c' i ' fonazi mapale tui , c' i ' difti on addhon àbeto . Ipe tui : Ane ti ' sù kami kundu tunifta , naonnei o addhon àbeto ce on vaddhis avri ' sù .
Sì , ipe tui . Èftase mapale addhi nifta ce ddhàssane .
Èbbie ce pirte cino e ' citto gratti .
Epianni , arte pu is èkame cippu tèlise , mapale , eguaddhi o macheri c' is kofti a vizia . Tui skosi o pornò , skòsisa ; epirte ' cì cini klèonta .
Ipe cini : Cittu , scema , k' arte se ftiazo ' vòvo , ipe ; ce akkorta , a ' se rotisi mi ' pi ti echis , ipe .
Pianni lio koddha , macinei ce is ta koddhava .
Is ta kòddhise ; ma e kankarena prai pu ' cessu . . Depoi pianni c' in èftiase tui , in èftiase depoi .
Endìsisa ce pìrtane stus passeggiu .
Arte pu sianòstisan attus passeggiu , i ' fònase c' is èdifse on addhon àbeto : Oh , ti en òrion ! ipe tui .
Beh , ipe tui , a ' teli n' on valis avri , ipe , arte mian ghià tunifta ene .
Sine , ipe tui .
Èftase e ora u plosi , ce cino pirte sto gratti , sekundu iche kàmonta stes addhe foré .
Dopu is èkame cippu tèlise , eguaddhi o macheri ce is kofti i ' mitti ce a ftia .
Epianni ce skòsisa to pornò .
Ce pirte sti ' kumpagnissa klèonta : Oh , ti mò' kame , ipe tui , arte ti ènna kamo ?
Cittu , scema , ipe tui ; èbbie kamposso cirivi , ce a kàmane ole ce diu , i ' mitti ce a ftia .
Èftase addhi mera arte , pu iche n' i ' konsignefsi , pu icha ' na pa n' i ' piane .
Tossu ringraziamentun arte na pa n' i ' pàrune .
Nsignasa ' na filistune , nsomma . Dopu iche na tarassi , fsikkonni usi kiatera ce ipe , u tù' pe cinuvu : Io bella men de vinni , più bella men de vau .
Tie vane a mujereta e falli lu schiau . Ah , pu na se kafsi e fotia ! ipe cino .
Cini pirte èssuti . E jinèkatu èsire mian addhin emera ce depoi pèsane : ka e kankarena prai pu ' cessu .
Dopu tu pèsane , stàsisa kamposson e ' lutto ; èbbie c' is èmbiefse mapale cinì mi o teli .
Sì , ipe cini ; arte ' nkora se telo . K' arte su fìane oles e ragge pu mu vasta .
Ermàstisa ma cini , stàsisa kalì kuttenti , ce kalì kunsulai .
A ' pai na dì steu ' kajon àrtena pu mai .
