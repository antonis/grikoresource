Iche diu dèrfia , ce isa diu dèrfia konvivea ' nomeni ; allora , stèonta nomeni , kàmane ena ' pedì .
Tui , na min dòkune skàndalo sto paisi ka stea ' lio ' lontana atto paisi , stean defore , ' e telusa ' na masosì itto pedai o vàlane ' s emmia kanistreddha , o fasciòsane ce on entorniscetsa ' pramatsia ; iche mia ' tàlassa , ena ' fiumo ecisimà , ce o vàlaneall' onde de lu mare , a ' to piai tispo , n' o sarvetsi , ce tù' cha ' vàlonta ena ' bijetto ka ele : " Il mio nome è Gregorio " , a ' ti ' piai tispo .
Allora , fìnnome pu ' ttùs tutu ; tui o fìkane itto pedai ce pirtan èssuto ; però mina ' lio nascosti na du ' mi ' vrìskete tispo n' o piai .
Allora iche ena ' peskaturi , ide mia ' kanistreddha ka ìbbie ngalla , ipe : Kàspita , ettù echi ena ' fenòmeno , kané prama ka ' e pai .
Allora kùkkietse mi ' barca ce ide ka iche i ' kanistreddha m' ena pedai : Na , na , ipe , arte o pianno itto pedai , ka ' en icha ' makà pedia .
Pirte o pire i ' ghinèkattu , is to pire ; ipe : Maria , ìvrika ena ' pedai sti ' tàlassa , iu ' c' ìunna , na ; arte o nastènnome , o kratènnome , emì pedia ' en èchome , ce o kratènnome ja ma , itto pedì .
Sì sì , sì si , tui cheresti poddhì .
Allora itto pedai o criscètsane ; ntsìgnase na jettì mea , o pètsane stu stùdiu , ce iso giovanotto tèlise na jettì patera .
Allora , dopu iche jettonta patera , sti ' Romi icha ' na kàmune ena ' papa cinùrio , icha ' na kàmune , ce iso giovanotto epassegge mo ' libro ka èkanne es preghièrettu , passegge lontana atti ' Romi , ' en este ... este defore atti ' Romi .
Allora toa ìone usanza ka icha ' na petàsune mia ' kolomba , ecì ka pose isi kolomba icha ' na kamu ' to ' papa , icha ' na kàmune .
Allora , isi kolomba , dopu i ' petàsane , chasi ; e kardinali ka icha ' na kamu ' to ' papa stean oli ecì riuniti na du ' pu posei , invece isi kolomba èbbie addhi stra ' , tenimentu .
Allora ntsignasa ' n' i ' kulusìsune ; piàkane e ' vetture , a pràmata , ntsignasa ' n' i ' kulusisu ' na du ' pu fermei .
Allora , dopu pratisa ' kamposso , ìdane itto ' patera ka ste passegge ce isi kolomba epòsetse panu sti ' ciofali tunù patera .
Ebbé , kukkiètsane , ìpane : Allora esù ènna se kàmome papa , jatì ste kànnome o ' papa sti ' Romi : ecì ca ferme e kolomba ... fèrmetse es esena , ce esù ènna jettì papa .
Ius ene o destìnommu , ipe , iu ...
Cheresti tuo , de ' ?
O ' pìrane sti ' Romi , kuntetsa ' to ' fatto os kardinalo : Ettù ' citto patera efèrmetse e kolomba .
Ce pos kui ?
Evò kuo Gregorio .
Allora : Tinon ise pedì ?
Fonasa ' tu genitoru ce o ' kuntetsa ' to ' fatto , ce tui , e mari genitori , ìpane : Pedìn dikomma ' en ene , ma os ipa ' o ' fatto : itto pedai on brìkamo ' s emmia kanistreddha ecé sti ' tàlassa ce emì o criscètsamo ...
Allora tuo pirte sto ' Vatikano , o ' kama ' papa ; ce toa isi leggi , dopu kanna ' to ' papa cinùrio , dìane ena ' bando , ena ' cino , is pu kommettei , is pu e ' kommettuta kàtare na pai sto ' papa na tsemoloisi , ka us etsemoloà ka èrkutte perdonai e pekkati .
Allora isi aderfì m' itton aderfò kusa ' citto prama ; ìpane : Pame ? ka stea ' panta mi ' koscienza sporca : Pame na tsemoloìsome .
Allora , mentre piaka ' na tsemoloìsune , tui arte icha ' na pu ' to ' fatto , ka kàmane ena ' pedai ce o ' bbandunètsane sti ' tàlassa .
Allora tuo , iso Gregorio , ìtsere o ' fatto tu genitoru ; dopu jetti mea , os tù' pe : Esù ' en ise pròbbio pedìn dikomma , però iu ' c' ìunna .
Allora , sti ' konfessiuna , tui ipa ' tin amartia , ce tuo skobretti , tuo nòise ka ìsane e genitori , però , in confessione , ' en ipe tipo .
Tui , isa ' dèrfia , pirtan èssuto ce tuo este panta m' itto ' cino ka iche e genitori ka isan diu dèrfia , ka tuo io ' pedì citta diu dèrfia .
Allora tuo io ' poddhì kalò , ìone , iso papa ka ìone ; allora , dopu pèsane ngrazie de diu , èstase e ora ka èkame poddhùs chronu , o ' kaman ajo , o ' kàmane , jatì io ' kalò , ce o fonasa ' san Gregoriu papa .
Ce iu spiccei depoi iso fatto .
