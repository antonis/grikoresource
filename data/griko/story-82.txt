Io ' mia forà ce ìone ena ' signoro ce mia signura , ce icha ' mia ' kiateran òria , ce i ' kratusa ' kara : este panu ' s ena ' palai manechèddhati manechèddhati , c' ìbbie e serva c' is èperne na fai ; mai ' kkàtenne cini .
Mia ' forà o ciùritti iche na pai ' s ena ' paisi larga , ka io ' fera . Pirte sti ' kiatera c' ipe : ' sù pemmu ti telis , ipe .
Respùndefse cini c' ipe : Evò telo mia ' pupa na stasì ma mena ; c' ènn' i ' feris isa ma mena ; c' ènna milisi , ipe cini .
Depoi is tin èfere , c' èblonne ma cini .
Mia ' forà icha ' na pane ' s ena ' paisi larga , fore regnu , e kiatera ce oli , ' cessu sti ' karrotsa . Tèlise na pari i ' pupa cini : in èvale pànuti ' cessu sti ' karrotsa , ce tèlise n' i ' pari .
Depoi is èpese pu panu : iche tri nifte ka pratusa ' , c' is èpese pu panu . Cini ' kkateke sti ' botta ce pirte n' in briki , krifà atti mana .
Ce pùpeti in ìvrike . Ce cini tin iche vrìkonta ena ' pedì signuru , utti pupa , c' in iche vàlonta ' s emmia kàmbara , c' este manechèddhati .
Nsìgnase na pratisi ce pùpeti in ìvrike .
Depoi is ipe mia kristianì : Panu sto ' tale palai stei e pùpasu , ipe . Ce cini ' nneke ecipanu , c' ipe : Vrìkato makà i ' pùpamu ? ipe .
C' itto pedì pu in iche vrìkonta iche pesànonta ce on icha ' pianta e stiare .
Depoi , dopu on icha ' pianta e stiare , e signura ipe : Teli na mini ma mena ' ttuanu ja kiateran dikimmu ? ka ' en iche tinò , diu serves iche .
Depoi ipe e padruna : Evò pao stin aglisia , ce ' sù stasu ma tue ; ce den , ipe , a ' su pu ' tipo , pèmmuto : voniu ! ipe .
Depoi respùndefsan e serve c' ìpane : Arte pu èrkete e padruna , fseri ti ènn' is pi ?
Pestis , ipa ' : Ma ' sù me gapà kundu gapa o pedissu ? arte cine e ' nvìdia ìone .
Dopu irte depoi , is to tù' pe .
Ce cini ipe : Amo , amo pu ' ttù , ti ' vò se gapò kajon esena pi o pedimmu , ipe . 'Kkateke cini ce nsìgnase na pratisi . ' Ffrùntefse mia ' kantina ce mbike : ce ' cikau iche o pedì pu ste o tròane e stiare .
Osson ìgue ena ' sùngulon ecikau : c' isan e stiare pu ste ce tròane o pedì .
Cini ' ffaccefti pus emmia fenestreddha ce ide itto pedì pu ste ce o tròane e stiare .
Ìfsere isi kiatera ti io ' pedì cinì , c' èmbiefse lèonta sti ' mana cinù n' is embiefsi ena ' stenò na kami itto porteddhai pleo largo , ka ' cì ' en iche porta .
Is to fera ' to stenò , èkame o porteddhai pleo largo ce mbike ecessu . Ìvrike mia ' koppiddha mon aguento sti ' pronì kàmbara tis kantina , ce nsìgnase n' o lifsi cini m' itton aguento .
Depoi ide ka ste c' èrkatto e stiare ka este ce on èlife , ide ka ste c' èrkatto e stiare ce guike pu ' cessu .
Depoi cini mapale este defore , pelì ena ' lisarai ce guènnune mapale , ce guènnune ' ttumbrò . Depoi cino , pleo ton èlife , pleo ghènaton io .
Depoi jetti io pròbbio : Ah , ipe , pu nà' is panta kalòn ! ipe . Arte , ipe , ' mì ènna piastùmesta oli ce diu , ipe .
Cini respùndefse c' ipe : De ' , ipe , jatì ' vò ènna pao votonta na vriko i ' pùpammu , ipe .
Cino ìfsere ka i ' pupa in iche cino stesso , ma ' e tis ipe tipo , jatì cino ìfsere ti cini iche nà' rti , ka is pupa i ' basta poddhìn agapi .
Ce cini u tù' che ponta cinù : Fseri poa se pianno ?
Mottan vrisko tiì pùpammu .
Depoi ' ffruntei enan bèkkio ce o ' rotà : Ise domena makà ti ' pùpammu ?
C' iso vèkkio ion bèkkio paleo , ìfsere otikané cino .
Respùndefse cino c' ipe : Àmone sto ' tale palai ka in echi , i ' doke olu tus andiritsu cino .
Ma cino este ' nteso ka ènnà' rti cini . Cini ion òria sentsa fina .
Depoi cini este pu ' cé stin addhi kàmbara ce ' ffaccèato ce tori i ' pupa ' ffacciata sto jalì .
Ce cini este pu defore ce in guste .
Ce cino puru in guste , ma ' en ìfsere ti echi cini .
Ce cino tis ele cinì : Oh , che si bella !
Considera la padruna quantu era bella ! Dopu ipe iu diu tris foré , pianni ce mbenni cini .
Respùndefse e pupa c' ipe : Na , e padrùnamu !
Cino ipe : Evò in icha vrìkonta i ' pùpasu , torì ?
Ce ' sù ipe ti tappu vriski ti ' pùpassu piannis emena . Arte spìccefse .
Respùndefse cini c' ipe : A ' piain emena , ènna piai puru i ' pùpammu , ipe .
Respùndefse cino c' ipe : I ' pupa i ' kratènnome sa ' kiateran dikimman , ipe .
Depoi piaka ' ce rmàstisa , kalì kuttenti , kalì kunsulai , kajon àrtena pi mai .
