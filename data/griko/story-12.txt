Io ' mia forà c' io ' mia ' mana c' iche tria pedia .
O pronò pirte c " en ghiùrise pleo .
Èbbie c' èsire tuo o menzani : Mamma , aderfommu pirte : arte ' en ènna pao ' vò n' on vriko ?
Ce ti kànnome : ena ' c' enan diu ? ipe tui .
Deje , mamma , mi ' pensa ' cì ; ti ' vò pao ce jurizo .
Deje !
Vloisomme ti ènna pao .
Epirte .
Pirte tuo , ena ' c' enan diu , c " en ghiurisa ' pleo .
Fìnnome pu cinu pu ' en ghiurisa ' pleo ; piànnome atto keccin arte .
Epirte : Mamma , ecì pu pirta ' t' adèrfiamu telo na pao ' vòvo .
Auh , ti mu kanni pedàimmu !
Arte pais esù ce spicceo .
Mi ' pensa ' cì , mamma , ti ' vò jurizo .
Èbbie i ' strava tuo ce pirte na pai na vriki adèrfiatu . Oh !
Epirte , èmine ena ' mina , èmine diu ...
E mana , mini mini , èchase e ' sperantse pus ola ce tria .
O pedìn arte nsìgnase na pratisi , na di an ìvriske a ' derfia ta mala .
Sto pratisi pu èkanne ìvrike enan daso c' ipe : Mbenno ' ttossu o ' en embenno ?
Èkame ena ' koràggio mea mea c' embike .
Mbike ' cì sto daso ; arte pu toristi ' céssu , tuo nsìgnase na faristì ti este mes te ' makke .
Tappu pràise posson ena ' mili , epianni ce pai ce vriski ena ' liuna , mian àkula ce mia ' furmìkula pu ste kontraferèatto na meràsune enan vui fsofimmeno . Èbbie c' ipe tuo : Bongiornu !
Bongiornu ! Ce diaike .
Dopu iche kàmonta diu tris passu o fonàsane ; c' ipe tuo pu ìone o liuna : Dela ettù , kàmemas ena ' piaciri : merasomma lio tutto vui , ti ' mì , ipe , ' en eftiazomesta .
Beh , ' sù pu ise àkula , su dio a stèata , ti vastà to ' pitso ce a kaserni ; esù pu ise o liuna , su dio i ' muddhika ; ce ' sù pu ise e furmìkula , su dio i ' ciofali ka sozi embi c' ègui pu ' céssu na fai otikané pus pa ' tripi .
Estete kuttenti ? ipe tuo .
Sì ! Kajo pi mas èkames ' e soggeste , ipe o liuna , na mas kami .
Stasiste kalì .
Stasu kalò .
Ce pirte .
Tappu iche kàmonta fta fto passus , piaka ' ce o fonasa ' mapale .
Ipe tuo : Ti tèlete ? Elae , ipe o liuna , mi ' faristu !
Tuo farìato , tappu o fònase .
Mi ' faristu , ipe o liuna . Dopu jùrise tuo , ipe : Emìs ' en ìchamo tipo na su dòkome , ipe o liuna .
Evò , ipe cino , ' e telo tipo , sa ' ringrazieo , ipe , c " e telo tipoi .
De ' , ipe o liuna , sentsa tipo ' e se fìnnome .
Evò su dio : motta teli na jurisi liuna , lei : " lione me fazzu " .
Ce ' vò , ipe e àkula , puru o stesso : tappu vrìskese ' s kanèn abesogno lei : " aquila me fazzu " .
Evò o stesso , ipe e furmikuleddha .
Meh , stasiste kalì .
Stasu kalò .
Ce pirte .
Tappu làrghefse posson ena ' mili pu cinu : Ènna dume , ipe , an en jalìssio cippu mù' pe uso liuna ! Èbbie c' ipe : Leone me fazzu .
Ce jetti liuna .
Uh , t' ime àscimon ! ipe . Cristianu me farò , ka ' vò ime àscimo sentsa finan , ipe .
Beh , ipe tuo , ènna jettò furmìkula : furmìcula me fazzu ! Jetti furmìkula .
Ka ' vò ime poddhì keccia ! Fio fio ! ' E telo pleo .
Epianni ce jùrise kristianò . Cristianu me fazzu .
Beh , ènna dume mapale : aquila me fazzu , ipe .
Jùrise àkula ce pètase . Guike defore atto daso ce èftase ena ' paisi .
Kui e ' males kampane pu ndalùsane a mortòrio . Eftazi , èbbie tuo ce kàlefse kau , c' ipe : Cristianu mi farò .
Jetti kristianò ce nsìgnase na pratisi .
Ìvrike mian bèkkia c' ipe : ' E mu lei jatì ndalù ' tuse kampane tosson a mortòrio ?
Ti e ' , pedaimmu ? ipe . Èchase i ' kiatera , u piaka ' ti ' kiatera tu ria .
Dopu ipe : Ce fseri makà pu i ' piran ? ipe tuo .
Eh , pedaimmu , ipe cini , ka ti fsero ? !
Prai pleon ambrò ce vriski mia ' pleon bèkkia oi ena ' pleon bèkkio pi ' mena ce su fseri doi noa .
Prai prai , prai prai tuo , ce pai ce vriski enan bekkiaruddhi . Mu lei tuse kampane jatì ndalù ' tosson a mortorio ? ipe o stesso .
U klefsa ' mia ' kiatera u ria , ipe , c " en efsèrome de ' noan de ' vèkkian , ipe .
Guike pus tutto paisi ce vresi st' addho . Ce mapale kùatto to stesson e kampane .
Ìvrike mia ' kristianì : Mu leis , ipe , pu i ' pìrane utti kiatera , pu in espatrièfsane ?
Dommu noan , ipe .
Ah , pedaimmu , ipe cini , madonna t' i ' pìrane ' cìrtea sti ' tàlassa .
Tuo pianni c' ipe : Aquila me fazzu !
Èbbie volo ce pètase .
Epètase ce nsìgnase na ngirefsi ' mbrò sti ' tàlassa .
Petonta petonta , pai ce vriski ena ' palai stamesa is tàlassa .
Mpòggefse panu sto kornici tu paladiu c' èmine . " Evò ènna do , ipe tuo , m' in ei ' ttossu " .
Dalli e dalli , pai ce torì utti gioveni . " Evò ènna kamo de tuttu na kalefso " , ipe tuo ; fs' àkula panta este . Motti pu este panu ' s tutto ticho tuo , pu nkora iche na kalefsi , etorì o ' Nanni Orku pu este c' èguenne defore na kacciefsi .
Ipe tuo : Oh , katsas , ipe , ce pos kanno na kalefso ? Doppu tuo pirte larga pu ' cisimà , epianni ce kàlefse mes tutto ghiardino tuo t' iche ghiardino ce nsìgnase na piai es patte na di putten ènnà' mbi ' ttù ' s tui .
Proppi nà' rti o Nanni Orku , pianni tuo ce mbenni sti ' kàmbara pu este cini : iche jettonta àntrepo arte tuo .
Epianni ce mbike tuo motti cini este tisa mes to spiti , este c' èkanne serviziu .
Guike tuo ce in efsìkkose pu ' mpì . Pappo , pappo ! ntrikefti na fonasi tui .
Cino , pu ìone ora pu iche na sianosì atti ' kàccia , pai ' cì cini pu fònase : Ti praman e ' , kiaterèddhamu ? ipe .
Malepena fònase cini , cinos ipe : Furmìcula me fazzu ! Jetti furmìkula c' is embike sto ' petto .
C' is este sto ' petto tuo , ce cittu ' sù ce citta ' vò .
Èbbie ce nsìgnase na votisi o Nanni Orku : Kiaterèddhamu , ettù ' e sozi ennei tispon ettuanu !
Tuon vòtise vòtise c " en ìvrike tipo : Kiaterèddhamu , evò ' ttù ' en vrisko tìpoti .
Mi ' faristu , ipe , ti ' ttupanu ' e sozi ' nnei tispo , ti an ennèune us tro . Èbbie i ' strava : Kiaterèddhamu , stasu skuscetata ti ' vò pao .
Tuo mapale , motti pu skandàgliefse ti tuo iche ftàsonta kau , èbbie c' ipe tuo : Cristianu mi farò , c' in efsìkkose pu ' mpì ; c' is efsìkkose o ' lemò c' is to ' kràise : Citta , ipe , posson ei ti pateo ja sena . Pappo , pappo ! fònase cini mapale .
Ce o pappo pianni ce kui mapale .
Cino pianni ce jènete mapale furmìkula : " Ah , ti mò' kame ! Mapale èkame na jurisi " , ipe ecé safto .
Èbbie cino ce jùrise ; èbbie c' èkame o stesso ; votise : Ma ' vò ' e s' o tò ' ' pimena ti ' ttupanu ' e sozi ' nnei tispo ti ' vò us trovo ? ipe .
Èbbie ce pirte : Mi ' me fonasi pleo ! ipe .
Ce pirte .
Dopu pirte , èbbie tuo mapale c' ipe : Cristianu mi farò .
Èbbie c' is efsìkkose o ' lemò : Cittu , cittu , posson ei ti me kanni na patefso !
Vu ! ipe cini , ce po ' bresis ettupanu ?
Ah , ja ' sena vresi ettupanu . Ka emasa ti s' èglefse uso Nanni Orku ; se glefsa ' ce se fèrane ' ttù ; ce ' vò tò' masa , ce irta na dume a ' se soso piai na se paro .
Mai , kané cerò me sozi pari , ipe cini . Ma pròbbio , pos èkame na ' nneis ettupanu ? ipe cini cinù .
Pos èkama ?
Ènna di pos èkama ?
Lione me fazzu ! ce jetti liuna .
Vu , t' is ' àscimo , ipe cini ; jettu , jettu kristianò !
Ce liuna jetti nà' rtis ettù ? ipe cini .
De ' , ipe cino , arte torì .
Oh , benes , èbbie ce jetti furmìkula ; jetti furmìkula : Oh , jettu kristianò , ti arte ise poddhì kèccia ce se patume , ipe cini .
Èbbie , amés tutto mentre , kùstisa patimae . Vu vu , ste c' èrkete o pappos , ipe cini ; presta , klisu ! ipe .
Toa stesso iche jettonta kristianò ce poi ènghise na jettì furmìkula n' is embi sto ' petto ; ce tù' fsere cini . 'Nneke o pappo mapale : E ' domena tipo pleo , kiaterèddhamu ?
Deje , pàppomu , ' en e ' domena tipo pleo .
Èbbie i ' strava , doppu efe , èbbie o pappo ; kateke ce pirte .
Èbbie c' ipe , dopu este ste ' skale : Kiaterèddhamu , mi ' faristivi , stasu skuscetata ce mi ' pensa pupei .
Prai , ekatea ' sù pu ' ttupanu , ipe cini , ce jettu kristianò , ti ènna fais , ipe .
Èbbie , on èkame na fai , on èkame na pi , o ' kòrdose .
Plòsane . Stin ora pu skandàglie nà' rti o pappo : Aska , òriommu , ti art' èrkete cino ce mas troi olu ce diu , na !
Èbbie c' ipe : Esù ènna kami addhi mian , ipe tuo ; esù ènna stasì prikì ; tapu èrkete cino , cino se rotà : " Ti echi , kiaterèddhamu , ti stei prikì ? " .
Esù u respundei ce lei : " Esteo prikì ka , arte pu pesenis esù , evò meno manechimmu ' ttupanu " .
Ce u leis ola tutta loja , ipe o paddhikarin arte . Beh ' en en ora pleo , jatì arte pale ftazi o pappo .
Èbbie ce jetti mapale furmìkula tuo .
Èbbie ce ' nneke tuo , ce mapale in ìvrike pleo prikì : Ma kiaterèddhamu , jatì stei prikì ?
Jatì ' sù peseni ce ' vò meno manechimu !
Auh , kiaterèddhamu ! Evò sù' pa ka ' e peseno mai .
Efe , èbbie ce ekateke ce pirte .
Jetti kristianò cino . Beh , prai , prai na fai , ti cino kateke , ipe .
Dunque èbbie i ' stra ' doppu efe ; e ora èftase pu iche nà' rti o pappo .
Fseri ti ènn' u pis ? ipe tuo ; ènn' u pi : poa peseni ?
Dopu su pi otikané , esù pianni ce stei citta .
Vale skupò cippu su lei ce kraita stannù .
Fìnnome pus tuo ce piànnome atto ' pappo , ti èftase ora .
Epirte . Ipe : Kiaterèddhamu , pos ise stammeni ?
E ' domena tinò pleo ?
Deje , ma steo prikì ti arte pu ' sù peseni , evò meno manechimu mes ti ' tàlassa .
Ellae ' ttù ma ' mena , rispùndefse o pappo , kanòscio larga larga ' rtea sti ' tàlassa : ecé citti tàlassa echi ena ' palai frabekao ; ecessu citto palai echi mian ursa , cini ursa vastà mian aquila , ènn' in esfàfsune , cini vastà tria ' guà , itta ' guà ènna ma skiattarìsune sto frontili ' ccena ' ccena : toa peseno ' vò .
Torì , kiaterèddhamu ?
Stasu skuscetata , ti ' vò ' e peseno makata .
Efe ce pirte o pappo .
Tui , tapu pirte , u ta ta ipe tunù pu jetti kristianò . Tappu u ta tà' le , este prikì tui ti u ta tà' le tunù u paddhikari : Ah , ipe , amo ce kame utto praman arte !
Ka ti e ' pu sù' pe ?
Mù' pe iu c' iu . Ma utto prama soggeste arte pu ipe iu cino .
Ich ! ipe cino , ka tuo e ' tosso prama fàcelo !
Respùndefse cini : Daveru lei ?
Ka pleon daverun ' e soggesten , ipe cino .
Tui arte , uttin addhi virtù pu iche tuo , fse aquila , ' e tù' fsere tui . Sì ? respùndefse cini , eis addhe virtù ?
Kame kame na dume ti ene !
Aquila me fazzu , ipe cino , ce jetti aquila .
Vu vu , ti cherèome , ipe cini ; vu ti cherèome !
Meh , meh , allora ìmesta certi ti pame pu ' ttupanu ?
Sì , sì , mi ' pensa ' cì , ipe cino .
Èbbie i ' strava pale , fane ce pìane . Beh , arte tris emere cerò ènna mu dois , ipe o paddhikari ; an es tutte tris emeres ' en ime ' rtomeno , na mi ' me mini pleo , ti ' vò irta sfammeno attin ursa .
Èbbie tuo : Stasu kalì , ce pensa ' s emena tappu ' e me torivi .
Licenziefti , ipe : Aquila mi farò ! Is pètase pu ' mbrò ce pirte .
Pirte panu sto kornici tu paladiu c' èkame i ' spia tuo , na di mi tò' rkete fatta na di an esfafsi in ursa : c' este d' àquila . Kanòscio sìmmeri , kanòscion avri , all' ùrtimu èbbie c' in ide uttin ursa .
In ide pu stei , epelisti tuo ecessu . Belisti ecessu sto palai ce jetti liuna .
Nsignà na fsikkosì e ursa ce o liuna ; èbbie c' in èsfafse . Dopu in èsfafse , is èbbie ittin àquila pu ' céssu c' in èsfafse c' is èbbie utta tria ' guà ; èbbie ena ' ghià ranfa sta diu poja , c' ena sto ' pitso ce pètase .
Tappu pirte ' cì , cini este panu sti ' làmmia ce kanoni fissa ; maleppena on ide tui , is irte i ' fsichì sti ' mitti : Oh , ipe , grazia de diu , ste c' èrkete o ' nnamuràomu ! ipe . Maleppena èftase , pensa ' sù arte , mbike ' cé sto ' paradiso cini arte ji ' charava .
U doke na fai ce ntrikefti na rotisi : Ka pos èkame , pos èkame na feri tutta ' guà ?
Fsero ' vò posso pàtefsa n' is kamo i ' spia cinìs ursa !
Dunque efe , èbbie ce jetti furmikuleddha , ti oramai èftase o cerò .
Me ' citto momento , ekusti o pappo ce ipe : Prai nìfsemu , ipe , kiaterèddhamu , ti ' e sozo na nifso o ' portuna .
Vu vu , furtùnamu , ipe tui ; ka poka , pai ce me finni ?
Utta tria ' guà , tui , a vasta sto ' petto .
Pah ! tu skiattàrise o pronò sto frontili .
Ohimé ! ipe tuo , ime pesammeno .
Èbbie ce u tò' sire t' addho , ' mpì cino t' addho ce pèsane .
Piàkane , u tò' kofse i ' ciofali , o soma o pègliase sti ' tàlassa c' i ' ciofali ti ' krèmase ' cì sto palai , es ena ' ravdì tin èvale .
Beh , pos kànnome arte ? ipe tui . Evò on èsfafsa cino , ipe ; pensa ' sù arte pos ènna kàmome na pame pu ' ttù , ipe cini .
Ipe cino : Arte ' nnenno panu citti làmmia ce a ' toriso kané bastimento u kanno simai mo makkaluri nà' rti ' tturtea .
Èbbie ' nneke panu sti ' làmmia cini ce nsìgnase na kami simai , ka ide o ' bastimento .
Kànnonta simai , ekkùkkefse . Ekkùkkefse c' ipe : Ti teli ?
Cini : Mu kannis ena ' piacirin ? ipe cini .
Ti telis ? ipe o kapitano .
Ènna mas kami to piaciri na mas pari pu ' ttupanu .
Gnorsì , ipe cino .
Su dio tosso ce mas perni ; mas perni puru àrtena .
Motti teli , ipe .
Sianosa ' ' tikaneve ce vàlane cipanu sto ' bastimento .
Piaka ' ce pìrtane ce e ciofali u pappu èmine kremammeni .
Pìrtane stamesa i ' tàlassa : itto pedì on efsìkkose e mamma Serena .
Èmine òria cini ! Èbbie ce ipe cini , to korasi : Vu vu ! tuo mas ton èbbie ce mas ton èvale kau sti ' tàlassa mia vèkkia , ipe .
Respùndefse o kapitano c' ipe : Beh , prai na pame .
Ipe cini : ' En èrkome makà , ipe cini , ti cino me libèrefse pu ' cìanu .
Ce dunque pos kànnome ? Menome ' ttusimà ?
Sì , ' ttusumuddhia ; kame na mini o ' bastimento ti ènnà' rti àndrammu , ipe cini . ' Ttusumùddhia meno ' vò ; ce ' sù àmone ' cì citto paisi ce rota pos ènna kàmome na guàlome pu kau sto nerò tuo .
Àmone .
Pirte , fatsamu san e ' Martignana , ce ròtise c' ipe : E Serena mas èbbie quai pedì ce tò' vale kau sti ' tàlassa ; anì bèkkia o kùntefse , ti teli ?
Ipe cini : Ti teli ?
Teli poddhù sonu , ka cini en gapimeni attu ' sonu .
Tuo ius èkame . Ìvrike u ' malu sonu ce us pire ' cisimà citto punto pu iche cino .
Nsignasa ' na ndalisun arte oli tui soni pu pire . E vèkkia pu iche panta na pari u ' sonu ìbbie puru ma tutu .
Tappu is endalùsane , èbbie c' effaccefti cini e Serena . Beh , ipe tui , beh , cittìsete , min endalìsete pleo , ipe e vèkkia , ' ttusumuddhia stei .
Ipe tui , e Serena : Ah , vèkkia maledetta , esù iso ' pu èkame na mussèfsune e soni ? ipe .
Tappu ' ffaccei cino pu kratì kau sto nerò , toa kordonni sonu , ipe .
Efsikkonni mapale ce nsignasa ' na ndalìsune : Meh , ndalìsete , ipe e vèkkia .
Èbbie on effàccefse ìmiso . Respùndefse cino , tappu on effàccefse : Kàmete na ndalisi , iu forsi me ' ffaccei ros ta kònata ce ' vò petò .
Na nkurtèfsome o ' kunto , tappu on enfàccefse ros ta kònata , èbbie ce pètase tuo . Es to bastimento ' en embike pleo , pètase , pirte ' mbrò .
Ftàsane stin agra is tàlassa ce katekan oli . Iche posson enan addho ìmiso mili stra ' c' i ' kàmane allampedi ji ' charà .
Ftàsane sto paisi : Vu ! ntrikèftisa , ste c' èrkete e kiatera u ria : vresi !
Ce ntrikèftisa na ndalìsune e kampane arte all' armi , makà pleo mortorio . O jeno : Vu vu , irte e kiatera u ria ; is efseri pu este !
I ' pìrane sto palai , nsignasa ' na preparefsu ' tossa mala pràmata .
Pai , èbbie i ' strava , doppu fane , doppu pìane , iche enan ambrosti ce on addho pu ' cì , cino pu in èffie pu ' cìvi , st' addho mero , dekoste . Èbbie i ' stra ' c' ipe : Arte ti teli , kiaterèddhamu , na rmastìs ? ipe o ciuri .
Ce tùonna o teli , pu stei ' ttusumà ' mbrossu ?
Na mu stasì kalò , ipe cini , ' e to telo makà tuo .
Tuo ka scendei fse ria , kundu ìmesta ' mì , c " e to teli ?
O mu di tuo pu m' èffie pus ta chèria u Nanni Orku andé n' armastò ' e telo .
Ce meh , kiaterèddhamu , tuo s' èffie pus ta chèria u Nanni Orku , tuo piao .
Èbbie , ' cì citto stesso fai pu stèane , stefanòsane ce stàsisa kalì kuttenti ce kunsulai .
Is pu te ' n' u ' di , na pai sti ' Napuli ka us torì .
