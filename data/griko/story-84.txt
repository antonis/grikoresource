Mia ' forà iche ena ' ' s Vernula ce rmasti .
Dopu diu tris emere dopu iche rmastonta eskosi ena ' pornovo na pai na feri enan biàggio lachri .
Satti pu este c' èguenne atti ' porta , o ' fònase e jinèkatu ce tù' pe : Emena ma tino me finnis arte ti ' sù pai ?
Cinon bòtise c' is ipe : Ka mino cu ci sia sia .
Tàrasse ce pirte tuo . Cini kàise ' mbrò sti ' porta c' èmine na diaì uso ci sia sia , n' o ' fonasi na stasì ma safti .
Jaike ena pu ìbbie pulonta alai , pu ìone o Arkàngelo tu Signorina , pu ìbbie fonàzonta : Ci vole ogliu !
On ìkuse tui ce o ' ròtise : Ce si ' , lu sia sia ?
Tuo is ipe : Gnorsì , iu suntu lu sia sia .
Ce poka , ipe tui , dela ce stasu ma mena , jatì ius ipe o àndrammu , na stasò mo' sia sia .
Mbike uso Arkàngelo , kaisa ' ce stea ' nomeni sti ' kàmbara .
Doppu lio èftase o àndratti ce us ìvrike olu ce diu : Ah , ipe , utta guàita kanni ? Va bene !
Mìnete ti arte sas eftiazo ' vòvo olu ce diu , ipe cino . Èklise i ' porta , vòtise , cino pu ion o àntrepo , ston Arkàngelo ce tù' pe : Beh , ti telis kajo ?
Na se paro sti ' Roka vèkkia es to ' nomo ce depoi na jurisi manechossu , oi teli na se sfafso ?
O Arkàngelo ipe : Telo kajo na me pari sti ' Roka vèkkia sto ' nomo ce depoi na juriso manechommu pi na me sfafsi .
In ghinèkattu in èfike ' cé sti ' kàmbara , tuo to ' fòrtose sto ' nomo ce o ' pire sti ' Roka vèkkia .
On èfike ' civi ce on èfike ce tù' pe : Arte jùriso manechossu , arte dopu pao ' vòvo , ti ius chànnese , mes tutte makke ce forsi ' en ghiurizi pleo .
Tuo ìfsere i ' stra ' , tàrasse ce pirte sto paisi amprimu .
Èftase stin ghinèkattu , mbike essu ce is ipe tunivi : Ti teli na su kamo : mia ' paura , oi teli na se sfafso ?
E jineka jàddhefse kajo n' is kami mia ' paura .
In èglise ' cessu mia ' kàmbara c' in èfike kamposso .
Depoi pirte cittu cittu ' mpì sti ' porta ce is èkame : Guessa ! sumponèonta cino ti is embenni e paura ' cé sta stèata ce ti peseni celì celì .
Ma cinì tipo ' e tis èkame .
Ecì stèonta , ìkuse o messerin Arkàngelo pu ìbbie fonàzonta : Ci ole ogliu ? mes ti ' strava .
Iche jurìsonta atton biaggio cino puru , irte passu passu . ' Ffaccefti tuo ce on ide .
C' ipe : Pu na kaì pu on èkame ! Fs' alio ' en eftazi proppi mena !
Ce i ' ghinèkamu , ipe , seleste manku is èkame makà senso usi paura pu is èkama .
Iche stasonta kajo sfafsontàus olu ce diu .
Emivi , ipe , mìnkiu ma lèune ce minkiuni ìmesta .
Ce ' en ei pleo .
