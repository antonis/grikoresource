Ìone mia forà ce ìsane diu adèrfia ce ìchane o ' negozio .
Ena pus tutta adèrfia eskosi ena ' pornò ce pirte sti ' puteka ce in ìvrike fseputimeni , sentsa tìpoti , ce ìone aderfottu stesso pu on iche klèfsonta .
Tuo , doppu ite iu , ensìgnase na kami kàtare ce èffie sti ' desperaziuna .
Prai prai , ce èftase mia ' lokanda fore paisi , embike ecessu ce iche dòdeka signuru ce o ' patruna , ce stèane oli nfilerai ce ste tròane .
Tuo èbike ce ipe : Doketemmu tìpoti na fao , ka ' en ime prata tìpoti .
Meh , ìpane usi signuri .
Ekàise ma cinu ce ìpane isi signuri : Po ' vrìskese ' s tutta merei , ce ' tturtea ' en èrkete manku tispo ?
Eh , ipe cino , m' èguale e desperaziuna , ce vresi ' s tutta merei .
Ce pemma , pemma , ìpane tui , e dòdeka mini ti kànnune ?
Kànnune pràmata àscima , de ' ?
Sas eskuajèune ji ' fsichra de ' ? tù' pa ' tunù .
Eh , ipe tuo , ti ènna kàmune , ftechùddhia : tipo , embrò stin annata pu feto .
Ti lèune mo marti ?
Nsomma , ìpane oli ti lèune .
Ti ènna pune ?
Ka ' en e ' janomena tosse tristarie .
Dunque , ipa ' cini , i ' kalì ?
Gnorsì .
De , ìpane , de ka ' mì ìmesta e dòdeka mini .
Amo ce na mi ' to pi tinò ce ' mì su dìome utti tabakkera gomai tabakko : mirìzese ce kumandei cippu teli esù .
U ti ' dòkane ce pirte èssutu .
Cippu ìtele , tò' rkato .
Ejetti signoro mea .
Èftase ena ' cerò pu o addho aderfò o ' glefsa ' cino otikanene , c' èmine nudu e crudu ce pirte ston aderfò pu pirte ecì ce ipe : Pos ejetti rikko esù ?
Epirta es quai lokanda ce iche signuru ce mu doka ' lire poddhè .
Epianni tuo , èvale i ' stra ' kau , prai prai , ce èftase ecì : embike ecessu ce ipe ti pai sfamao na fai .
Èbike ce u dòkane na fai , depoi ìpane cini : Ti lèune attu ' minu , kakò o kalò ?
Atsu !
Pu nà ' ' fotia n' us kafsi !
Kalò pu n' os e ' jestimao , àscimi janomeni , tristi , pu n' o ' doi !
Insomma ipe poddhì kakò .
Tusi dòdeka mini on efsikkòsane ce o kordalìsane kalò kalò , on esfàfsane ce o ' fìkane vìttimo c " e jùrise pleo .
E jineka èmine èmine ce pirte ston aderfò ce tù' pe : Àndramu nkora ènnà' rti .
Ce ipe cino : S' on esfàfsane ecì pu ' en e ' ' rtomeno .
E jineka ensìgnase na klafsi ce ipe o aderfò : Meh , mino ti evò ' en echo jineka , ce se pianno esena .
Epiàstisa , stàsisa kalì kuttenti ce kalì kunsulai : àrtena esteu ' kajo pi mai .
