Io ' mia forà c' isan diu maghi ; tui diu maghi fruntèftisan oli ce diu ; ce ena ' ròtise on addho : Pu pai , kumpare ?
Pao sti ' fera , kumpare , na piao enan amparai .
Ce ' vò puru steo m' itto pensierin , ipe o addho .
Doppu cerò fsechorìstisa : Stasu kalò , kumpare .
Stasu kalò .
Ce poa toriomesta ?
Ka ti fsero ?
Soggeste na toristùmesta , soggeste na mi ' toristùmesta : kundu piai .
Èbbie , dopu tosso ' cerò , ena ' mina , vrèsisa tui mapale : Oh , kumpare , ipe , posson iche ti ' e se toron !
Ce ' vò puru ' sena .
Ipe : Èkames affaru sti ' fera ?
Èkama , ipe cino .
Ti èbbie ?
Enan ampari .
Esù ti èbbie ?
Ena ' ciùccio .
Ce ddhàssome , kumpare ? ipe tuo .
Ddhàssome , ipe cino .
Esù ti vastas ettò sta visàccia ?
Vastò kàssima , ipe cino ; Ce ' sù ti vastà ?
Matafsi .
Ce ddhàssome , ipe ena fse cinu .
Ddhàssome .
Cino u doke ampari cinù , ce cino u doke o ' ciùccio ce ddhàssane .
Ena pirte ' s emmia merea ce addho stin addhi ; cinon èbbie ampari ce u addhù u doke o ' ciùcciotu .
Dopu larghèfsane , ipe tuo : Evò ènna do ti echi ecè sta visàccia , ena fse tutu . Èbbie kanònise , èbbie c' ìvrike àlegan ecessu .
Ahu , ipe tuo , evò u tù' pa ti vastò kàssima , ma cino puru in èndese ti vriski fiddha verza ' cessu .
Fìnnome pus tuo , piànnome atton addho , c' ipe : Ènna dume ti ei ' ttossu ' s tutta visàccia , jatì ddhàssane visàccia , ' tikanene . Vòtise ' cessu , pirte c' ìvrike fiddha verza .
Epianni c' ipe tuo : Beh , ipe , evò u kòddhisa ti vastò matafsi ce cino mu kòddhise ti vastà kàssima . ' E pirte kalà d' enan d' addho .
Tui arte enan iche panta ' s emmia merea , tappu ddhàssane , c' ena chasi stin addhi .
Dopu cerò , vrèsisa pale , ce dòsisa c' ipe tuo : Ah , kumpare , ' sù me mbrògliefse ma ' vò se mbrògliefsa proppi ' sena .
Jatì , kumpare , ipes iu ?
Jatì ?
Ka ' sù me mbrògliefse t ' iche kàssima ce ' vò su kòddhisa t' iche ' cessu matafsi ce intantu iche àlega .
Ce su pos tin ìsele ? ipe : Esù ' e mù' pe tartea ce ' vò nemmenu sù' pa mia ' kalìn , ipe cino .
Tui utta ftinà ta pulìsane ; cino pùlise o ' ciùccio , ce o addho pùlise ampari .
Beh , ipe , ' e to ' pùlise to ' ciùccio ?
Ce ' vò puru o pùlisa t' ampari .
Piànnome ce pame rtea sti ' Napuli , ipe .
Pame , ipe cino , ce pu pame ?
Pame rtea sti ' Napuli .
Ce ti pame na kàmome ? ipe ena fse cinu .
Pame ce glèttome , ipe ena fse cinu .
Piaka ' ti ' stra ' ce pirta ' tui ; pìrtane ce ftàsane ecì sto ' kugno , ecì pu kugnea ' ta turnìscia tu ria , ce ipe : Kumpare , arte ti e ' pu ènna kàmome ' mìs ? ipe ; stèome ìunna ce ' e kànnome kammia . ' En ènghize nà' mbis ettòssuna na piai kampossi moneta ?
Ipe cino : Emba ' sù pu ise ' nnorimmeno ' tturtea , ti ' vò ' en efsero , ipe tuo , ka io ' Luppioti .
E ' kàglio nà' mbis esù .
Mi ' me kami nà' mbo , ti ' vò meno ' cessu a ' pao nà' mbo .
Ti pauran echi , ipe uso Napuletano u Luppiotu , jin ghinèkasu ?
Min ei paura , penseo ' vò jin ghinèkasu .
Kànnome ' vò ti meno ' ttossu , evò erkome piammeno , e jinèkamu meni iu , ti e ' pu ènna kamo ? ipe .
Evò su leo : mi ' pensa pùpei , t' i ' dio na fai ce na pi , in bastò ' vò , ipe , emba ' sù liberamente .
Èbbie o ' persuàdefse ce mbike .
Embike , èbbie èbbie moneta , ci ' pu tèlise èbbie fòrtose .
Doppu fòrtose : Sìreme , kumpare , ti e ' supèrkia , ipe .
On èsire . On èsire , pirtan essu ce o rceròsane fse nifta stesso .
Dopu pirtan essu arte , e jineka cheresti ti ide oli citti somma tos turnìscio .
Doppu ftiasa ' mia ' tàola , fane , pìane ce plòsane .
In addhi nifta pale kama ' ti ' stessa .
O pornovo e guardie kanonìsane ' cisimùddhia ce ida ' to simai ti mankeu ' turnìscia .
Piàkane ce vàlane misko ' cessu sti ' fossa pu iche a turnìscia : Is pu tà ' ' biammena , ènna jurisi addhi mia ' foràn , ìpane e surdai .
Ohò , èbbie èftase e nifta ce pirta ' tui madapale .
Ipe uso Leccesi tu Napuletani : Arte amba ' sù arte vrai , sa ' pu to tò' le e kardia .
Vu , ka ti difikortà kanni ?
Amba ce mi ' pensa ' civi .
Èbbie ce mbike .
Mbike ce nsìgnase : Kumpare , ehi , sìreme , aggiu miscatu !
Misca oru argentu e rame !
Mìskefsa .
Ce jatì mìskefse ?
Echi koddha ce kòddhisa ' cessu , ipe all' ùrtimu .
Beh , allora su kaleo ' nfsarti : destu ti se sirno .
Èbbie kàlefse anfsarti , on èdese ce nsìgnase na siri . Kumpare , jettu lafrò , evò ' e se sozo na se siro .
Kòddhisa , ' e me sirni !
Poka , fseri ti kànnome ? ipe tuo , o panu : Mentre ti kòddhise , ipe , ' en ei ti kami .
Su belò mia ' sciàbula , kofti ti ' ciofali ; proppi n' i ' kofsi ndesei sto scinì ti ' vò i ' sirno .
Ius , ipe , fìnnome ' ttossu o soma ce i ' ciofali i ' sirno ce ' e se norìzune .
Ius èkame tuo .
Èbbie cino èsire i ' ciofali pu ' cessu , èbbie c' in èkofse ; in èkofse min ennoristivi , ce cinos èmine ' cessu .
Èffie depoi ce pirte cino , sentsa turnìscia ce chasi pu ' cì .
O pornò pirte cino pu ìone o padruna tos turnìscio , pu iche o ' kugno .
Dunefti cino , epirte ce fònase i ' truppa .
Èffie e truppa ce nsignasa ' : Vu , vu , ti spettàkulo pu ene ' ttù ! ' Cessu echi ena ' sentsa ciofali ce ' cisimà ' en iche tinò !
Nsìgnase na fi o mean gheno : Delaste na dite ti spettakulo !
Ti praman ene ?
Kanonìsete ' ttossu , ti ' ttossu echi ena ' sentsa ciofali c " en ennorìzete is ene .
Fsèrete ti kàmete ? Sireteo pu ' cessu , ti forsi on ennorìzome .
O ' sira ' pu ' cessu , ce nsignasa ' n' o ' kanonisu ' , n' on ennorìsune .
Tispo ton ennòrise , ti u manke e ciofali .
Oh , dopu o ' sìrane pu ' cessu , ti pensèfsane ?
On bàddhome panu mia ' bara , ce o ' fìnnome stamesa i ' mesi .
O pronò spidi pu torume ti klei , ecì e ' simai ti andra cinìs ene .
Ius kama ' tui : on vala ' panu sti ' bara , ce nsignasan arte e guardie na pa ' botonta na dune speo spidi ene pu klèune .
Fìnnome pus tutus arte ce piànnome pu cinon arte .
Cino ' en iche milìsonta makà i ' ghineka nkora , in iche mbrojèfsonta .
Tappu ide ola utta guàita , pu on bàlane sti ' bara , ènghise cino , ce èffie essu ce is ipe cinì : Attenta , ipe , na mi ' klafsi , ti àndrasu èndese tuo ce tuon , ipe .
E guardia ìbbie votonta .
Tappu ìkuse iu , tui nsìgnase na klafsi figureftu ' sù ! ce na kami foné ce tsumpu .
Tappu este na ftasi e guardia ' cisimà , is èkanne tuo : Cittu , cittu , t' ìmesta pesammeni !
Ti pensei tuo ? Pianni ena ' kapasuna pu panu sti ' ciminean alai tuutùffiti ! c' is to pègliase ' cimesa .
Tui ' e pense st' alai pu is pèglia , pense ston andra , ston accesso . pu is iche ponta cino .
Pùffiti , ce ftàzune e guardie ' cisimà citto spidi , ce vàlane ' ftivi : Beh , ipe , ' ttossu ene .
Ftàsane ' cessu ce mbìkane . Mbìkane : Ti praman ene , nunna ?
Maleziosamente tuo pianni ce belìete ' mbrò c' ipe : Na , ' nneke e mùscia c' is pègliase alai pu panu sti ' ciminea ce tui ste ce klei , ipe .
Ce ste ce klei m' utton accesson ? ipe e guardia .
I ' leo ' vò na stasì citta ti is perno t' addho !
Tui , maleziosamente , guika ' pu ' cessu , guìkane atti ' porta , piaka ' ce siggillefsa ' citti ' porta .
Tuo pu ene o mago dunefti ; arte nsìgnase na pai sigillèonta porte , oli ti ' stra ' sigìllefse mi ' pian kostruziuna .
E guardie to pornovo , mapale na pume , nsignasa ' na pan botonta : ettù sigillo , ettù sigillo : Pos e ' tui ?
Ettù ' e piànnome i ' kostruziuna pu este cini jineka pu este c' egle pu ìchamo suspettèfsonta .
Ise guardie ti pensèfsane ?
On vàddhome panu sti ' bara ce o pèrnome stin aglisia afse nifta ce vàddhome spia : is pu pai na klafsi empì sti ' porta , simai ti cini ene .
Pirte cino c' ipe : Attenta , de ti cini o ' pìrane ' cì stin aglisia : mi ' pa ' ce kustì .
Tui nsìgnase na kami derrassu , tappu is ipe iu , ti ìsele na pai n' on di , na pai n' on di .
Cittu , ce mi ' pensa ' cì ; a ' teli fai , a ' teli pi , cippu teli ei pus emena .
Motti pu ftàsane ìunna e diu , tui ìsele nà' gui na pai .
Tuos ' e tin èfike : A ' pai na sistì , su klanno a stèata !
Ma , pemmu , ti e ' pu su mankei ?
Ci ' pu su mankefsi , pèmmuto .
Teli turnìscia , su dio turnìscia .
Prai , turnìcia !
Èbbie c' i ' pire sto kulumi : Deje , ' e telo ' ttù , telo on àndrammu . ' E ti ' persuade : Dunque ti e ' pu teli ?
Telo na pao n' on do .
Esù ' e sozi pai pùpeti , andé pame ' galera , oh !
Èbbie na fi tui , èbbie c' i ' kopànise .
Tui nsìgnase na klafsi .
O jeno èvaddhe aftì : Ti kanni tui ?
I ' persuàdefse : stasi citta .
Fìnnome pu ' ttù arte ce piànnome pu ' cì sto ' pesammeno pale .
Ti pensèfsane e guardie ? Soggeste na min bresì is e ' pu èkame utto furto tunifta ?
Pensèfsane ìunna : O ' piànnome ce on bàddhome panu sti ' bara ; is pu pai ce o ' piai apu ' cì , ei simai ti cini ìone pu èkame o ' furto , cinìs io ' pu is poni o mai .
Tuo min abelitatu pirte ce on èbbie .
On èbbie ce pirte on èchose ' mpì mia ' fossa .
Tui o pornò , motti pu eskòsisa , dunèftisa ti ' e ton echi : o ti us pire ìpuno , o pos ìone , cino tos tò' bbie . O pornò tui pìrtane : Ohimmena , o kadavaru ' e ton ei makata !
Nsignasa ' na pan botonta , motti dunèftisa tui . Ce pos e ' tui , ce pu o ' pira ' tutto ' pesammeno ?
Vota e kilivota , pai ce vastusan ena ' sciddho ma cinu .
Uso sciddho pai ce o ' ventei ' cì pu este , bèntefse ce ntrikefti na fsechosi uso sciddho .
Pianni pìrtane ' cisimùddhia e guardie , ce pai ce o sciddho iche fsechòsonta pu fènaton ena ' poda .
Iu depoi , doka ' mia ' fonì ce kùkkiefse o mean gheno .
Kùkkiefse o jeno ecisimùddhia , piaka ' ce on guala ' pu ' cikau . Tuos arte u stèane siftì e kazzi , ka tò' mase ti on brika ' cino : " Ah , sòrtamu , arte on brìskune " , ipe cino , " ka tui klei ! " Dopu èmase tuo ti on brikan , èffie ce nsìgnase na kanonisi ma cinu .
Ti pensefsa ' tui stessi , e guardie ?
Na pian tutto ' pesammeno addhi mia ' forà pu ' cikau , n' on vàlune stamesa i ' mesi mapale . Ste diu ore tu niftù , is pu diaenni pu ' cisimà , simai ti cini e ' pu èkame o ' micìdio .
Ius kàmane : o ' fìkane stamesa i ' mesi , ce o jeno olo retiro , sianòsisa oli èssuto .
E guardie depoi stèane attente na dune pa ' ce pai tispo .
Tuo ti kanni ?
Motti pu ìsane e diu ore , pu ' en iche manku fsichì , pirte , fingefti de passeggeri , èbbie fta panettu , ena ' piretto krasì , a prepàrefse tua èssutu ce tà' fike .
Epirte ' s ena ' kumento mòneku , ce ipe : Kametemme piaciri , doketemmu fta tònike ' ffittae . ' E te ' sòzome dois , ipa ' cini .
Ka ' vò sas tes ferno , ipe cino .
Ma ' di mia ' lira ti ' mia , ka iu s' e ' dìome , andé ' e s' e ' dìome makà .
Esì ius tèlete , na sa ' doko fta lire ?
Evò sas te ' dio .
Èguale tuo , o ' doke fta lire cinòs , èbbie es eftà tòneke ce èffie pu ' cì .
Èbbie o ' piretto , èbbie us panettu , ce es èvale kau sto ' kappotto .
Pirte ste ' guardie , mbike ' cikau cino c' ipe : Ahu , ipe , ime ena ' passeggeri , c' ènna mu kàmete piaciri na me kàmete na ploso ' ttù arte vrain , ipe .
Epirte c' ipe tuo : Vastò quai fsomì ce quai piretto krasì .
Tèlete na fate lio ?
Ipa ' cini : Ka ' mì èome famena .
Ce arte piate lio pu ' ttù , ammenu na fate lio .
Piaka ' ce dakkàsane sto ' panetto proi proi .
Meh , ipe , arte pìete ena ' bikkeri krasìn , ipe , ka pìnnome ce plònnome .
Quai tòssonna o mea pu ' cinu èkanne na ' ffacceftì mi sia ti o ' piau ' ce o ' paru ' pu ' cì .
Tuo essu safto jela : " Mena pìete ce fate ' ttuna ; ka ti mu pensete cino " . Pìnnonta ce tronta ecessu iche o ' nùbbio èbbie i ' stra ' ce stendefsan oli ce fta , de botta .
Èbbie tuo , èguale itte ' tòneke , èbbie ce os èvale alò mian ghiana , roffulìzonta cini .
Ti kanni tuo ? Pianni , us finni plònnonta , o ' fortonni c' èffie .
On èbbie ce on ellàrghefse atto paisi .
Jùrise cino ce pirte c' ìvrike na di ti lèune . O pornò , tappu fsunnisa ' cini : Na na , ipe o pronò pu fsùnnise , ' vò pao ndimeno fse mònekon , ipe .
Pirte na pai na torisi us addhu ce oli fse mòneku : Fsunnìsete , ipe , ti oli fse mòneku pame ndimeni .
Tappu fsunnisa ' , torìstisa oli fse mòneku tui , guika ' pu ' cessu , piaka ' ti ' stra ' ce fìane . Kanonisa ' c " en ida ' pleo to ' pesammeno .
Ti ènna kàmome arte ? Arte pu pame sto ' ria ti ènna pume ?
Ti pame ndimenu fse mòneku ce ti mas piaka ' to ' pesammeno ?
Tappu pìrtane sto ' ria , tuo , motti pu tus ide , nsìgnase na jelasi : Ti pràmata i ' tua ? ipe .
O jeno pu ' mpì us kanoni c' ìbbie jelonta . Tui kalefsa ' to ' muso kau ce pia ' trèchonta .
Respùndefse o ria c' ipe : Nsomma ' en efsèrete is io ' tuo pu on èbbie ?
U tù' pane : Diaike enan iu c' iu , pu vasta ena ' piretto krasì , ce u tù' pane otikanè .
O ria arte jela .
Epirta ' tui .
Pensefsa ' na pan botonta pale n' on briku ' tuo .
O jeno c' e guardie pian botonta arte ce on brìkane ' mpì ' s ena ' ticho : arte pensa ' sù pos ìone vromerò !
Nàonna pu stei ! ìpane .
Trèchete , ipe e guardia , vrìkete mia ' stotsa kascia , feretei , ipe .
Fìane , piaka ' mia ' stotsa kàscia ce on bàlane ' cessu .
O ' piàkane ce o ' pìrane sto ' ria : On brìkato ?
Ton brìkamo .
Pu este ? ' mpì ' s ena ' ticho .
Arte fsèrete ti ènna kàmete ?
Nvece t' ìsaston eftava , ènna ìsesta dekatèssari fse guardia ; ti cinon ènn' on vàlete stamesa i ' mesi .
O ' pìrane stamesa i ' mesi ce o ' fìkane .
O ' doke òrdinon o ria na mi ' tos pari makà o ìpuno .
Tui ti kàmane ?
Stèane panta de guardia tui dekatèssari .
Fìnnome pu ' cinu pu stean de guardia ce piànnome pu ' cì cinon arte .
Ti kanni cino ?
Us ide tuttu , ti echi o mean gheno , tarassi ce pai e ' mia ' massaria . Pirte e ' mia ' massaria c' ipe : Massari , ènna mu kamis ena ' piaciri : ènna mu doi dekafse jatsu mian j' arte vrai , ti ' vò su dio on affitto , na !
Su dio dekafse dukau , mian j' arte vrai .
Den , ipe cino , mi ' mò' chun àdeko , ipe , c' ènna doko kunto tu padruna .
Mi ' pensa ' cì , ipe cino ; motta pu cini sò' chun àdeko , evò ime responsàbelo na s' us ekkutefso .
Iu ' lei ?
Ce piattu pokka .
Pianni uttu magliau , tuo ; iche panta sto ' fra Bicenzi ce tù' che dòkonta triantadiu kandile ; pianni ittes kandile , pianni us magliau ce on baddhi mian ghià cèrato .
Os èvale diu on ena panu sta cèrata ce nsìgnase na ta mbiefsi ce cinos ampivi .
E guardia pu este ' cisimuddhia , tappu us eskupèrefse atto largo , tuos ' e fènato makà , cinon ìbbie ' mpìs ampì me ' cinu motti pu e guardie skuperefsan ola utta pràmata : Ohimmena ! ìpane : Dunque ste c' èrkutte na piau ' ti ' fsichì tunù pu stei mes ti ' mesi .
Pame ti i ' ta demogna tua , fèome ! Piaka ' ti ' stra ' ce fìane ' s karrera pu ' cì ce o ' fika ' manechottu o ' pesammeno .
Motti pu tui ' larghèfsane , tuo ti kanni ?
Pianni o ' pesammeno , o ' fortonni , vaddhi u ' magliaus ambrò , ce fèonta .
Motti pu èftase ' s ena ' punto tuo , pu ìfsere ti ' e ton brisku ' pleo tutto ' pesammeno , o ' pègliase , pirte sto ' massari , u pire u ' magliau , on ekkùtefse c' èffie pu ' cì .
Èbbie ce pirte èssutu . Este èssutu cino tranquillo c " e pense pùpeti pleo .
Piànnome pus tutus pu fìane ce pìrtane sto ' ria pesammeni is paura .
Tappu us ide cino spammentusus olu tutu : Ti praman ene , ti praman ene ?
O pesammeno pu este mes ti ' mesi irta ' tossa demogna n' o ' piàune .
Emì toronta , signore rìamu , tale spettàkulo , fìamo pu ' cì ce o ' fìkamo .
Ion ena ' spramento mea , toronta tossa demogna , u lea ' tu ria arte .
Ahu , ipe tuo , pu e ' vuta oli tuttin abelitàva ! Fìkete stei , mi ' pate pùpei pleo .
Ènna dume arte is e ' tuo pu e ' vuta olo tutto koraggio n' o ' piai , n' o ' pelisi , n' o ' pai pèrnonta , ipe .
Tui arte , toronta iu to ' spettàkulo : Emì , ipa ' cini , ' e pame pleo na kàmome i ' guardia , os pai is pu teli !
Den den ! ipe tuo , kàmete , kàmete o ' fàttossa ; mi ' pate pùpeti pleo .
Pate sto ' fàttossa , citti ce mutto !
Èbbie depoi uso ria c' èkame na vandièfsune ' s olo to ' regno , ce ipe ti ènna kami mia ' tàola c' ènna mbitefsi tòssunna passio cità ce passio paisi .
Rtea citti stra ' pu ichan bàlonta o ' sicillo , embìtefse kampossu .
Èftase ' cì cino arte , tuo pu us ìbbie mbitèonta ce tù' pe : Esù teli nà' rti ti ènna jettì mia taola ?
Doppu siànose kampossu , èbbie tuo ce pirte sto palai tu ria c' ipe : De ti ' vò mbìtefsa tòssunna .
Beh , i ' superki , ipe cino .
Me ' cinus arte iche na vresì cino . Tuo ti kanni ?
Pianni ce pirte ' s tutti kummaran arte , c' ipe : Po ' lei , pao ' vò ?
Pu ? ipe cini . ' S tutti ' tàola pu ènna kami o ria ; mbìtefse poddhù na pame na fame .
Ti fsero ' vò arte ?
Pao o ' e pao ?
Mi ' pais , ipe cini : àremo ti ènna jettivi !
Eh , ipe cino , oi ti me sfàzune oi guenno sti ' libertava !
Tuos ìone o paddhikarin arte , cino pu tò' kofse i ' ciofali ion armammeno .
Pirte tuo .
Tappu mbikan oli ' cipanu sto palai , jenon arte , pensa ' sù t' iche ! estean oli ìunna ' s filari kundu motti ènna sirtì mia ' kausa , kaimmeni .
Arte ntrikèftisa na pa ' piatti ste ' banke ftiammene .
Tunù u tò' kanne tria tria o kolo ti ' en ìfsere pu ènnà' gui e facenda : " Ah , sòrtamu , ti pirta na kamo nà' rto ! " , ipe tuo .
Doppu arte fane , pensa ' sù possus piattus iche fse pa ' qualità ! na mi ' tin bastàfsome makrea , dopu spiccèfsane fse fai , èbbie ce guike tuo pu ene o ria .
Kàise sto ' trono ma mia ' kurunan es ti ' chera . Ensìgnase tuo : Esì ènna mu pite arte , milìsete libera e franka , is io ' pus esà pu iche utto ' kuràggio ce uttin abelità na pai pronà pronà na pai ' cì pu kugnèutte a turnìscia ...
Ce ipe otikané , olu tus puntu .
Tuo arte ele : " O leo o ' e leo ? " U tò' kanne tu tu ecessu sti ' fsichì . Tunù arte u tò' kanne tria ce tèssara .
Pensa ' sù pu tu stèane e kazzi tunù . Beh , ipe tuo ; eskosi ce èbbie i ' kurunan es ti chera ce ipe : Esì fariesesta na pite , ipe , ma ' vò sas epprumetteo , arte nsìgnase na tsumpefsi mapale e fsichì , sas epprumetteo i ' kurunan , ipe .
Toa tu tò' nifse pleo poddhivi ammai . Arte tuo ' en ìfsere ti ènna kami : este ma dekapente pensèria , ' en ìfsere ti ènna kami .
Èbbie o ria ce ambrostu vasta mia ' kiatera , iche fèronta ma safto ; èbbie i ' stran , ipe tuo : Sas epprumetteo i ' kuruna ce i ' kiatera , is pu iche uttin abelitava , ipe .
Satti pu ìkuse iu cino , jetti tiso : Evò imon , ipe ; toa tu guike o fimò .
Ce allora , ipe , ti ison esù , tui ene e kuruna ce tuin ene e kiatera , piattin ipe , na sò ' ' koncessa .
Èmine òria e kummara depoi !
Rmàstisa , kama ' tosse ' feste , ce stàsisa kuttenti ce kunsulai .
Is pu te ' n' u ' di na pai sti ' Napuli ka us torì .
O kunto ' e pai pleo , ena ' turnisi emmereteo .
