Io ' mia forà ce ìone ena ' liko ce mia alipuna ; ce stea ' tui ìunna pu ' e kanna ' kammia .
Mian attes foré , ipe o liko tis alipuna : 'En ènghize na piàme ena ' korafi n' o spìrome ?
O pìannome , ipe e alipuna . Piaka ' citto korafi .
O vrai kama ' tokontratto . O pornò skòsisa ce pìrtane .
Ftàsane sto korafi ce nsignasa ' na skàfsune .
Tui , e signura , stràkkefse , e alipuna . Evò , ipe cini , likùddhimu , stràkkefsa .
Evò pao ce kratò i ' serra c' esù skafse .
Ce arte pai ce me finnis ? ipe cino .
Ma motti pu e serra ste ce petti , evò pao c' i ' kratò ipe cini .
Tuos èbbianne ce polema sto ' fàttottu ce piste ti e ' lipuna ste ce kratì i ' serra .
Tui ti èkanne ? Ìbbie votonta ; ecì pu ìvriske fai , eddhe .
Doppu ìbbie tui ce kòrdonne tra tra , pu ìbbie ' cessu ste ' massarie doppu èguenne pu iche na pai ' cì cino , èvaddhe mia ' rikotta panu sti ' ciofali , ìbbie ' mbrò cino : Ah , ti èndesa sìmmeri , ' cì pu krao ' ti ' serra !
Diake ena ' pekurari , mò' sire mia ' botta ravdì ce mò' guale a mialà .
Prai polema , ti ènna spiccèfsome o korafi , èkanne o likuddhi .
Respùndefse e alipuna : Evò su leo : prai na pame essu , ti ' e resisteo pleo ; i vasta i ' pantsan gomai .
Nsomma èbbie tui ce pirtan essu .
Skùriase .
Es ti ' stra ' tappu pìane : Vu ! ' E fidèome na kamo i ' stran , ipe e alipuna . 'En ènghize na me piai lion akkanci ? tù' pe u liku .
Ah , ipe cino , ka ' vò telo piammenos evò !
Ce umme , ce deje , ce piamme ... In èbbie ' kkanci .
Dopu este pànutu , tui e alipuna , èbbie ce ntrikefti na kantalisi : Nana , nana , lu malatu porta la sana !
Ce cino ' e fela , ti ìbbie strakko : Ah , karogna , ipe o liko , allora kannis iu jatì pai kordomeni ! ?
C' ipe ti ' e sozi !
Èbbie c' in eskiòppefse sto ' lakko .
Beh , stasu ' ttù , ti ' vò pao essu , ipe . Vu , ti mò' kame !
Ka ' sù me ruìnefse pleo poddhì , ipe e alipuna .
Nsomma , na mi tin bastàfsome poddhì makrea , pirtan essu .
Ftiasa ' na fane ce cini ' e telise makà na fai : ìbbie kordomeni .
Minkiala , prai fae ! 'E telo , ka ' sù me skuàjefse ; pos ènnà' rto na fao ?
Stasu , ti tro ' vò .
Piaka ' ce plòsane , e alipuna ce o liko .
O pornò fsemèrose . Meh , prai ce aska ce pame ' ttofsu !
Amon esù , pu efes eftè sto vrai .
Ènnà' rti puru ' sù .
Evò èrkome ; ma , an èrkome , i ' serra kratovo .
Eh , tappu ftàsane ' cì , èkame i ' stessa tevni .
Na mi ' tin bastàfsome makrea , èkame itti tevni mian addomà , e alipuna u liku .
Nsomma , èftase pu o terìsane , utto krisari .
O pìrane st' aloni , o lonìsane , o kulumosa ' ce o fìkane .
Beh , is ipe is alipuna o liko , epame na piame itto krisari ?
Umme , umme , ipe cini . Kànnome is pu ftazi proi to pianni ?
Va bene , ipe o liko , is pu ftazi proi to pianni .
Èbbie i ' stra ' tui ce nsìgnase na pratisi . ena ' punton ampì , tappu tui icha ' kàmonta o ' patto , icha ' pàronta o quartuddhin ecianu ; ce o liko iche vàlonta ena ' sciddhon ecessu ce on iche sciopàsonta mo ' sakko . Ftàsane .
Tappu ftàsane ' cisimà , tui arte , pu este pleon ambrò , ìbbie cherùmeni ti ftazi ce fsikkonni to krisari . Kumpare likuddhi , evò èftasa .
Ce piatto , ipe cino .
Chiusti sto quartuddhi tui , pu este sciopammeno .
Pirte n' o fsesciopasi , na fi o ' sakko . Fèonta o sciddho !
In efsìkkose atto sfòndilo c' in errebbàttefse .
Meh , pia ' o krisarin arte , ipe o liko .
Nkora ste ce rebbatte . Tò' bbie o liko , pu tù' che polemìsonta , ce èmine olo kuttento .
