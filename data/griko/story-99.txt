Io ' mia forà ce ìone mia signura ce ena ' signuro ce ìchane ena ' servituri ce tù' fsere ola a sekreti u signuru .
Mia attes forè , epirte e ' Luppiu o signuro ; proppi na pai tù' pe u servituri : Strìja a ' mparia kalà .
Ecì pu est ce a strije , epianni ce ' kkateke kau e signura , ecì sto ' servituri , ce u tù' pe : ' Nkora ste kanni pulizia ?
Ce skertsèane . Respùndefse e signura ce ipe : Pistei ka se gapà àndrammu ?
Respùndefse o servituri ce ipe : Ka egapà pleon emena ka ' sena .
Stasu mea , ipe cini , ' e se gapà makata pròbbio , ' e se gapà .
Ipe o servituri : Ka ènna di posso ' e se gapà makata ?
Ka evò u fsero ola a sekreti !
Ce nsìgnase e signura : Ce pemmu , ce pemmu !
Èftase ka on bòtise n' is pi .
Dopu is ipe , ekàlefse o ' muso e signura .
Èbbie ce ipe o servo i ' signura : Na mi ' me guali , ti me sfazi a ' to pi ti ' vò sù' pa ola utta loja .
Èftase o cerò ka irte o patruna ce in ìvrike prikì , utti ' signura , ce ipe o andra i ' ghineka : Jatì stei prikì ?
Esù , rispùndefse tui , ti tù' pe u Ntoni ?
Ti tù' pa ?
Evò ' en efsero tipo .
Ka cino mù' pe tuo ce tùonna .
O patruna ekateke kau sto ' servituri ce u tù' pe : Stiase o ' ciùccio ce ampari ka ènna pame ' s ena ' topo .
O patruna enneke st' ampari ce o servituri sto ' ciùccio ce pratùsane ce ftàsane ena ' feo dikottu .
Patruna , ' nkora pratume ?
Ecessu ènnà' mbome .
O patruna evota ' s emmia merea ce stin addhi ce tori jeno .
Dopu eftàsane ' s emmia massaria larga ka ' en iche tinò , o patruna belisti att' ampari ce ' kkateke , ce o servituri puru ekkateke , ce nsìgnase na kanonisi pukané ; ce iche tria picciugna panu ' s ena ' kornici ce stèane nfilerata ; o patruna : Tua ' e me ' kkusèune .
Èguale o spasì c' èghire n' on esfafsi .
Erespùndefse o servituri : Ghiatì me sfazi ?
Kame o stavrò ka ènna se sfafso .
Ipe o servituri : ' E pisteo , tutta tria picciugna i ' destemogna .
Ce on èsfafse , o ' servituri .
O patruna èbbie o ' ciùccio ce ampari ce pirte sti ' ghinèkattu ce ipe : O servituri arte èrkete .
Ce jaike mian emera .
Es tin addhi o signoro osso ìgue st' aftìatu : Giustizia , giustizia !
O signuro ja poddhés emere panta ìgue st' aftìatu : Giustizia , giustizia ! mia ' attes foré , ipe e signura : Panta ellamentese ?
Ah , ipe cino , èsfafsa o ' Ntoni .
Ce arte , ipe cini , enghizi na pai na fsemoloisi .
Eh , kaggià ka me nghizi na pao , ka evò fissa echo ena ' marteddhi es t' aftia .
Ce ipe cini : Àmone sto ' tale kumento , echi ena ' patera ce se fsemoloà tuo . Iu èkame .
Èbbie o ' fònase o ' patera ka ste fsemoloa ce ipe : Pàtremu , evò èsfafsa ena .
Ce jatì ?
Jatì cino me ' kkùsefse sti ' ghinèkamu .
Meh , petàimmu , àmone , àmone sto ' ghiardìnosu ce su guenni ena ' spirito ce ' sù o ' rotà pu stei e giustizia ce cino su rispundei .
Epirte sti ' ghinèkattu ce ipe : Efsemolòise ?
Efsemolòisa .
Arte evò epao empì sto ' ghiardino ce na min erti tispo , ka ènna kamo ena ' ' ffaro .
Gnorsì .
U kumpàrefse enan àntrepo ce ìone o spirito u servituri pu èsfafse , ce nsìgnase na fonasi o spirito : Giustizia , giustizia , giustizia !
Respùndefse uso signoro ce ipe : Pu i ' teli , i ' giustizia ?
Stes Indie .
Umme , i ' kanno .
Epianni uso signoro ce ghiùrise sto ' padre ce ipe : Ti èkame ?
Tuo ce tuo .
Ipe o padre : ' Sù arte nghizi na pai sti ' spiàggia ce na mini na di pu stei usi giustizia .
Epianni ce guike mia ' varka , epianni o kapitano ce on èbbie : Teli nà' rti ma ' mà ka mas kanni o ' servo ?
Sine , ipe o signuro .
Epianni ce pìrtane .
Siri siri siri , epianni ce ipe o kapitano u marinari : Kala o ' kiumbo .
Ekàlefse ce iche akatò àntrepu nerò .
Pu stèome àrtena ?
Sto nerò os Indio .
Arte nsìgnase na penteftì uso signuro .
Epianni ce ' kkatèkane stes Indie , ce ipe o kapitano , ipe u signuru : Amo ce piàone mia ' ciofali arnai .
Epirte ce in èbbie ce ste c' èrkato sti ' barka ce on ìdane e surdai ce ìpane ti vasta mia ' ciofali arnì . ' En ene arnì : de ka tui ene e ciofali u Ntoni .
Epianni ce vrèsisa a tria picciugna ghià destemogna ce o ' piàkane karcerao .
