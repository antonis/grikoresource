c' erano una volta un marito e una moglie che vivevano in povertà e pensate ! erano così sventurati che il pover' uomo lo chiamavano Miseria . egli faceva il mezzano : recava notizie , lettere , di tutto .
una volta era partito per un viaggio e aveva lasciato la moglie incinta . tardò a far ritorno .
intanto , passato il tempo , la moglie partorì e , non avendo altro per coprire il figlio , l' avvolse in un suo vestito .
quando il marito rientrò e vide che la moglie aveva partorito e teneva il bimbo in quelle condizioni , disse tra sé : " non mi resta che uscire in cerca di fortuna " .
così fece : si mise in cammino , dicendo alla moglie che andava a trovare i testimoni per il battesimo .
cammina cammina , incontrò un vecchio : Miseria ! questi disse .
anche tu mi conosci ?
certo che ti conosco , riprese il vecchio : cosa fai da queste parti ?
cosa faccio ? mia moglie ha partorito ed io sto cercando i testimoni .
vuoi che lo faccia io ?
e tu chi sei ?
sono Gesù Cristo .
allora non ti voglio , perché tu non fai le cose giuste ; se le facessi giuste , non permetteresti che uno abbia enormi ricchezze e mia moglie neppure uno straccio per avvolgere il figlio .
rispose così e riprese il cammino .
cammina cammina , incontrò san Giusto : Miseria , dove vai da queste parti ?
dove vado ? sicché anche tu mi conosci .
certo che ti conosco ; vai girando per tutto il mondo !
ma ora cosa fai da queste parti ?
cosa faccio ? mia moglie ha partorito e sto cercando i testimoni .
e il santo : vuoi che faccia io da compare ?
e tu chi sei ?
chi sono ? san Giusto .
Ah , il nome hai di giusto , ma giusto non sei ; altrimenti non permetteresti che uno abbia masserie e mia moglie che ha partorito neppure uno straccio per avvolgere il figlio .
rispose così e riprese il cammino .
cammina cammina , incontrò una donna : compare , dove vai da queste parti ?
dove vado ? vado in cerca di testimoni che facciano le cose giuste .
allora , non puoi trovare nessuno che faccia le cose giuste meglio di me .
e tu chi sei ? chi sono ?
la morte , che porta tutti via , poveri e ricchi .
sì , disse Miseria , dunque voglio te per comare .
però , dovresti fare in modo di procurarci almeno le fasce per avvolgere il bambino e un povero lettuccio per poggiarvelo quando lo battezzeremo .
d' accordo , lei rispose , fa ' come ti dico e non preoccuparti .
allora la morte disse : compare Miseria , sappi che a Napoli la figlia del re sta in fin di vita ; tu vestiti da medico e va ' ; una volta giunto alle porte di Napoli , troverai le guardie di finanza ; presentati da loro e chiedi che ti procurino un' uniforme ; gliela pagherai al ritorno ; poi indossala per bene , da ' quel che spetta e recati al palazzo del re ; sali ed entra nella camera della figlia ; lì troverai me : se mi vedrai ai piedi del letto , vorrà dire che la ragazza vivrà ; invece , se mi vedrai al capo , morirà .
fa ' come ti ho detto e non sbaglierai .
va bene , rispose Miseria .
si mise in cammino . cammina cammina , arrivò alla porta di Napoli ; c' erano le guardie di finanza ; si rivolse ad una di loro e disse : dovresti procurarmi un' uniforme in prestito ; al mio ritorno ti pagherò il dovuto ; io sono un medico famoso , ma durante il viaggio mi hanno derubato del denaro e dei vestiti , lasciandomi senza niente .
lo fecero entrare e scegliere l' uniforme .
egli si vestì per bene da medico e andò al palazzo del re ; vide il re affacciato al balcone e domandò : maestà , posso salire sul tuo palazzo per curare tua figlia ?
certo , sali ; son venuti già tanti ; ora prova anche tu .
salì sul palazzo , entrò nella camera della figlia e vide la comare ai piedi del letto .
rimase qualche ora , poi si recò dal re e gli disse : tua figlia vivrà ; lascia passare due giorni e sarà guarita .
bene , rispose il re , se tu guarirai mia figlia , ti farò ricco dalla testa ai piedi .
quali medicine occorrono ?
portami un pestello , schiaccerò delle medicine che ho con me e la farò guarire .
la ragazza guarì , poi s' abbigliò con cura e andò dal padre ; questi , appena la vide , l' abbracciò e fu pieno di gioia ; fece preparare un banchetto al quale invitò tutti i signori , e tra questi anche Miseria .
maestà , perdonami , disse allora Miseria , se mangerò più degli altri ; non tocco cibo da tre giorni e ho una gran fame .
mangia e non darti pensiero , rispose il re ; nessuno avrà da ridire .
finito il banchetto , il re lo chiamò in disparte e per ricompensa gli diede l' equivalente di ciò che avrebbe speso , se la figlia fosse morta , per tomba , catafalco , bara , abito , preti e funzioni ; e in più quel che valeva la figlia da viva ; insomma , lo caricò tanto che non riusciva a camminare .
si congedò poi dal re e partì ; giunse alla porta di Napoli ; c' erano le guardie cui doveva consegnare il vestito ; mise fuori un bigliettone e glielo regalò : chissà quanto ha ricevuto dal re , disse la guardia , se a me ha dato tanto !
davvero ! rispose l' altro .
così Miseria se ne tornò a casa ricco .
trovò la moglie rannicchiata in un angolo della casa : sei ancora qui ?
nessuno ti ha aiutata ? disse : io invece ho trovato fortuna .
la moglie , nel vederlo così ben vestito , non lo riconobbe , credette fosse un signore e rispose : vattene , signore mio , non prenderti gioco di me ; tu devi esser sazio , io invece non mangio da otto giorni ; se vuoi farmi la carità , dammi pure qualcosa , altrimenti vattene e lasciami vivere i pochi giorni che mi restano .
ma io sono tuo marito , egli disse , non mi riconosci più ?
la povera donna non si capacitava in nessun modo .
egli allora le raccontò la storia dall' inizio alla fine e le mostrò il denaro che aveva ricevuto ; la moglie guardò tutti quei soldi e alla fine riconobbe il marito .
questi cominciò a comprar beni d' ogni genere : masserie e case di campagna , un palazzo che arredò con gusto , fasce per il bambino ; insomma non gli mancò più niente .
poi invitò la comare e fu celebrato il battesimo ; finita la festa , si salutarono : Stammi bene , compare !
Stammi bene , comare !
ma la comare tornò indietro e disse : compare , la prossima settimana verrò a prenderti per mostrarti la mia casa . va bene , rispose il compare .
così , otto giorni dopo , lei venne e portò il compare a casa sua ; si mise a mostrargli le stanze e a farlo girare di qua e di là . entrarono infine in una stanza dove c' erano torce d' ogni genere : grandi , piccole , di ogni nazionalità ; alcune erano appena accese , altre avevano una grande fiamma , altre stavano per spegnersi ; insomma , ogni tipo di torce .
gliene mostrò una tutt' avvolta nel fuoco : Oh , come arde ! disse Miseria : chissà quando si consumerà !
ne avrà ancora per molto !
perché non farle ardere tutte allo stesso modo ?
perché ? rispose la morte : perché esse sone le anime di tutti gli uomini della terra : qualcuna è in fin di vita e sta per spegnersi ; altre hanno ancora da vivere cinque o sei anni e sono a metà ; ad altre ne restano ancora venti , e ardono meglio ; così va la faccenda .
Miseria vide una torcia ben accesa e disse : chissà di chi è questa torcia che arde così bene !
vuoi sapere di chi è ? rispose la morte : è di tua moglie ; lei ne ha ancora da vivere !
Oh , fortunata lei ! esclamò Miseria : beata lei !
poi vide una che stava per spegnersi : questa si spegnerà tra poco ; chissà di chi è !
dimmelo , chiese Miseria .
e la comare : non te lo dirò ; ti dispiacerebbe .
dimmelo , non mi dispiacerà . vuoi proprio che te lo dica ?
te lo sto chiedendo !
questa , disse la morte , è la tua anima .
tu hai ancora da vivere per dieci giorni soltanto . povero me !
lasciami vivere almeno per dieci anni per godere la ricchezza che mi hai procurato .
no , rispose la morte : non ti ricordi più di quando cercavi qualcuno che facesse le cose giuste ?
Ahimé , ti prego , lasciami un po ' ! se non altro per ricompensarmi d' averti voluta per comare .
no , io sono la morte che non rispetta nessuno . mi fermo a chiunque capiti .
dammi un' ultima speranza , un vincolo ; assolto il vincolo che mi darai , morirò .
la morte disse : allora , recita il Credo ; quando avrai finito di recitarlo , morirai .
Miseria cominciò a recitare il Credo ; arrivava a metà e non andava più avanti .
la morte gli rammentava : compare , inizia il Credo .
egli ricominciava , arrivava a un terzo e di nuovo smetteva . insomma , passarono diversi anni ; sempre cominciava e mai finiva ; non diceva più il rosario perché non gli capitasse di recitare il Credo .
la morte lo lasciò vivere per quel che poteva , poi andò nel giardino e sistemò dodici colonne e sulle colonne scrisse il Credo .
la mattina Miseria uscì in giardino e vide le dodici colonne con le scritte ; andò a leggere quelle scritte e alla fine s' accorse ch' era il Credo . allora comparve la comare e gli disse : hai vissuto un po ' , un anno , quanto hai voluto ; ora ti tocca morire ; hai detto il Credo ; va ' a salutare tua moglie perché è giunta l' ora .
Miseria si recò dalla moglie e la salutò ; il giorno dopo morì .
venne la comare , se lo caricò e lo seppellì ; lei stessa celebrò le funzioni e il povero Miseria finì ; aveva cercato tanto di non recitare il Credo , ma alla fine c' era cascato lo stesso .
