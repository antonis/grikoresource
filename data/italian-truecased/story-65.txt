c' erano una volta un re e una regina ; la regina non faceva mai figli , mentre una loro serva ne faceva uno ogni anno ; sicché la regina era fuori di sé perché non riusciva ad avere un bambino .
passava sempre da lì un mendicante , cui lei dava un tozzo di pane , un pezzo di formaggio , o qualsiasi altra cosa .
un giorno questo mendicante , che usava ricevere l' elemosina da lei , si presentò e chiese la carità : che carità e carità ! disse la donna : non voglio fare più la carità .
perché non vuoi far più la carità ? domandò il vecchietto : raccontami , forse posso aiutarti .
ho una serva , lei rispose , che ogni anno fa un figlio ; io invece non ne ho nessuno .
allora il mendicante disse : vai nel giardino dell' Orca , che è pieno di prezzemolo , e riempitene un sacchetto ; poi torna a casa e mangialo .
attenta : se l' Orca ha gli occhi aperti , sta dormendo profondamente ; se ha gli occhi chiusi , è sveglia .
la regina chiamò la serva e andarono .
fece entrare la serva nel giardino per raccogliere il prezzemolo ; l' Orca aveva gli occhi aperti , perciò riempirono il sacchetto e tornarono a casa .
subito la regina restò incinta .
la mattina seguente , appena spuntò l' alba , andò di nuovo le era stato detto di continuare per tre giorni e vide un' altra volta l' Orca con gli occhi aperti .
tornata a casa , riprese a mangiare il prezzemolo .
l' Orca s' accorse del prezzemolo che mancava e disse tra sé : " mi nasconderò dietro questo tronco ; chiunque verrà , so io come fargliela pagare ! " il giorno dopo arrivarono le due donne ; mentre stavano raccogliendo il prezzemolo , ed erano proprio al centro del giardino , l' Orca uscì da dietro l' albero di fico e disse alla signora : fermati lì e sta ' a sentire , tu che ti sei mangiata tutto il mio prezzemolo ! e continuò : spogliati nuda !
quando restò solamente con il busto , la regina supplicò : non mangiarmi ; quando avrò partorito e la bambina avrà dieci anni , la darò a te .
se è così , vestiti , e andate via .
la regina si rivestì e tornarono a casa .
partorì , dunque , e diede alla luce una bambina con la luna sulla fronte e la chiamò Prezzemolina .
quando aveva due anni , la mandò dalla maestra .
la bambina doveva necessariamente passare dalla strada dell' Orca ; non c' erano altre strade .
l' Orca la vide e la chiamò : vieni , vieni , ti devo parlare .
quando tornerai a casa , dì a tua madre che sono rimasti solo otto anni , poi mi dovrà consegnare ciò che mi ha promesso .
la bambina andò dalla madre e riferì quanto le aveva detto l' Orca : tra otto anni doveva consegnare quel che aveva promesso .
la madre scoppiò in lacrime .
ed ecco che mancavano solo tre giorni dalla data della consegna .
la ragazza andò di nuovo dalla maestra ; l' Orca la vide e disse : dì a tua madre che fra tre giorni mi deve consegnare ciò che mi ha promesso .
la ragazza riferì tutto alla madre : rispondi che ti sei dimenticata di dirmelo , disse la madre .
e l' Orca , il giorno dopo : l' hai detto a tua madre ?
me ne sono dimenticata .
allora ti farò due nodi al fazzoletto , così non ti dimenticherai ; dille che è rimasto solo un altro giorno .
andò a riferire di nuovo alla madre , e questa rispose : dì all' Orca di prenderselo da sola .
e piangevano , la madre e il padre .
la ragazza disse all' Orca : mi ha detto che te lo puoi prendere da sola .
allora l' Orca la afferrò : vieni con me , dunque !
l' Orca aveva un figlio che si chamava Bonfiore .
un giorno l' Orca disse alla ragazza : io sto andando in chiesa .
vedi questa cantina piena di letame ?
quando tornerò dalla chiesa devo trovarla linda come uno specchio . lei andò in chiesa e la povera ragazza si mise a piangere vicino alla cantina .
le si avvicinò Bonfiore : perché piangi ?
non piangere .
cosa vuoi ?
tua madre mi ha ordinato di togliere tutto il letame dalla cantina .
per questo piangi ? non preoccuparti .
il giovane diede un colpo con una bacchetta e la cantina brillò come una farmacia .
poi Bonfiore andò in piazza , affinché la madre non pensasse ch' era stato lui ; aveva pure avvertito la ragazza che sua madre , una volta tornata , avrebbe detto : questa non è roba tua , ma di Bonfiore .
e allora avrebbe dovuto rispondere : son tre giorni che non lo vedo ; non so neppure com' è fatto .
l' Orca tornò e chiese : hai pulito la cantina ?
certo , vieni a vedere come ho pulito . questa non è roba tua , ma di Bonfiore .
non lo vedo da tanto tempo ! per questa volta , pazienza , disse infine l' Orca .
spuntò di nuovo l' alba e l' Orca disse : io sto andando in chiesa ; tu devi farmi trovare questo pozzo come uno specchio ; doveva ripulirlo , tirar fuori la terra e le pietre che c' erano dentro .
sì , lei rispose , e ricominciò a piangere vicino al pozzo .
s' avvicinò Bonfiore : zitta , perché piangi ?
ora lo faccio prosciugare io !
diede un altro colpo di bacchetta e dentro fu tutto pulito e lustro .
tornò l' Orca : l' hai ripulito ?
l' ho ripulito . son dovuta andare anche dall' Abbondanza , qui vicino , per farmi prestare quattro giare per il trasporto .
questa non è roba tua , disse l' Orca , ma di Bonfiore .
non lo vedo da tanto tempo !
invece avevano fatto l' amore , tutt' e due : si volevano bene .
intanto la madre aveva confidato a Bonfiore : non c' è verso : non possiamo ucciderla noi , questa ragazza , per mangiarcela .
così il pomeriggio l' Orca andò dalla sorella e le disse : ti manderò una ragazza che si chiama Prezzemolina ; tu dille : che bella ragazza , che bella ragazza !
aspetta , ti regalerò delle mandorle !
nel frattempo vatti a limare i denti ; poi l' ammazzi , te ne mangi metà e l' altra metà la lasci per me .
va bene , lei rispose . allora l' Orca ordinò alla ragazza : vai da mia sorella e fatti dare il lievito .
non le indicò neppure la strada .
la poveretta s' avvio , e piangeva perché non sapeva da che parte andare ; in piazza incontrò Bonfiore , che le domandò : che cos' hai ?
che cos' hai ?
tua madre mi ha ordinato di andare da tua zia a prendere il lievito e non mi ha detto né la strada né altro .
questa è la strada , le disse Bonfiore ; poi aggiunse : incontrerai due serpenti che si azzuffano ; tu tira loro una pietra e falli smettere .
dopo incontrerai tre cani ; eccoti tre pagnotte , gettane una ciascuno e non ti toccheranno .
ti imbatterai ancora in una fornaia , che sta pulendo il forno con le mammelle ; tu dalle un fazzoletto per asciugarsi il sudore .
poi ti si presenteranno tre mendicanti ; spidocchiali un poco e ti lasceranno andare .
vedrai infine un portone che sbatte ; fermalo con queste pietre d' argento .
una volta arrivata , la zia ti darà il figlio in braccio ; tu gettalo e cerca il lievito dietro la porta ; poi lei andrà a limarsi i denti e tu scappa .
la ragazza s' incamminò .
incontrò due serpenti che si azzuffavano : gettò loro una pietra ed essi s' allontanarono .
di nuovo , cammina cammina , incontrò tre cani : gettò loro una pagnotta ciascuno e poté continuare , perché i cani si lanciarono al pane .
poi s' imbatté nella fornaia e le diede il fazzoletto per asciugarsi il sudore .
le si presentarono allora tre mendicanti ; li spidocchiò e la lasciarono andare .
vide il portone e mise una pietra per parte .
salì finalmente sul palazzo della zia , entrò nella stanza : Oh , che bella ragazza ! lei cominciò a dire , che bella ragazza !
siediti , siediti , cosa vuoi ?
tua sorella ha bisogno del lievito .
aspetta , ti vado a prendere delle mandorle ; tienimi intanto il bambino .
la ragazza , invece , cercò il lievito dietro la porta , lo prese , gettò il bambino e scappò .
la zia tornò e vide il bambino per terra e lei che correva .
scese allora dal palazzo per raggiungerla : portone , disse , prendimi quella ragazza e chiudila dentro .
lei è passata una sola volta e mi ha fermato ; tu sei passata tante volte e non lo hai mai fatto ; adesso , corri !
incontrò i tre mendicanti : mendicanti , accattoni , fermate la ragazza , la devo rosicchiare .
lei è passata una volta e ci ha spidocchiati , tu sei passata tante volte e non lo hai fatto mai ; adesso , corri !
ed ecco la fornaia : fornaia , fornaia , che inforni e sforni , tienimi la ragazza .
lei è passata una volta e mi ha dato il fazzoletto per asciugarmi , tu sei passata tante volte e non lo hai fatto mai .
incontrò i cani : cani , fermatemi la ragazza , la devo mangiare .
lei è passata una volta e ci ha dato una pagnotta ciascuno , tu passi tante volte e non fai che sputarci addosso ; corri , ora !
incontrò i serpenti : serpenti , fermatemi la ragazza , la devo mangiare .
lei è passata una volta , essi risposero , e ci ha fatto smettere d' azzuffarci ; tu sei passata tante volte ed hai solo sputato .
la ragazza arrivò dall' Orca : sei tornata ? chiese l' Orca .
certo , son tornata .
allora la madre disse a Bonfiore : con questa non si cava niente !
ma facciamo un' altra prova : riempi mezza pentola d' olio e mezza d' acqua ; poi mettila sul fuoco e , quando bolle per bene , la getteremo dentro .
io ora vado a raccogliere una fascina di legna per cucinarla .
ma il giovane fece un incantesimo alla scopa , per farla parlare , poi anche alla sedia , al tavolo , alla pentola , al letto ; fece in modo che ogni cosa parlasse e potesse rispondere .
poi , Bonfiore e la ragazza , chiusero la stanza e scapparono .
venne l' Orca e si mise a bussare dietro la porta : Bonfiore , aprimi , vado carica .
aspetta , devo mettere altra legna sotto la pentola , altrimenti non bolle , rispose la pentola .
Bonfiore , aprimi !
aspetta , devo aggiustare il letto .
Bonfiore , te lo dico con le buone , aprimi !
aspetta , devo apparecchiare tavola .
aprimi , sto scoppiando .
aspetta , devo pulire il focolare .
ogni cosa diede la sua risposta .
l' Orca gettò per terra la fascina , sfondò la porta ed entrò ; ma non trovò nessuno : Ah , me l' hanno fatta ! esclamò .
i due correvano ed avevano entrambi tre oggetti : una nocciolina , una mandorla e una noce .
correvano a più non posso , ma l' Orca li inseguiva ; a un tratto se la videro dietro e il giovane disse : mia madre , mia madre !
ora cosa facciamo , cosa gettiamo ?
gettarono la nocciolina e comparve una cappella ; essi si trasformarono in sagrestano e sagrestana .
l' Orca chiese loro : avete visto passare un uomo e una donna ?
no , non è ora di messa .
vi ho chiesto se avete visto un uomo e una donna .
non è ora di confessioni .
Beh , sarà meglio tornarmene a casa !
quando arrivò a casa , disse tra sé : " Ah , erano loro !
devo correre a raggiungerli ! " cominciò a inseguirli di nuovo ; essi la videro e gettarono la mandorla ; comparve un giardino , e diventarono uno giardiniere , l' altra giardiniera .
sopraggiunse l' Orca e domandò : avete visto passare un uomo e una donna ?
adesso non è tempo di lattughe . strano , non riesco a farmi capire con nessuno !
meglio andarmene .
una volta a casa : " Ah , erano loro !
devo correre a raggiungerli ! " si mise a correre ; il figlio la vide : ahi , di nuovo mia madre ; stavolta siamo finiti !
gettarono la noce ; comparve il mare ed essi si trasformarono in pesci .
l' Orca cercava di afferrarli con la mano ; li prendeva , ma le sfuggivano : non è per me , disse ; meglio tornarmene a casa .
tornò .
i due uscirono dal mare : adesso non c' è più pericolo , disse Bonfiore , mia madre non verrà più .
poi continuò : tu non vorresti andare un po ' da tua madre ?
certo che lo vorrei .
però devi stare attenta a non farti baciare da tua madre , altrimenti io morirò .
bene , vai ; ti aspetto qui .
diede un colpo di bacchetta e la ragazza si trovò a casa sua , da sua madre ; la madre le venne incontro per baciarla , ma lei la fermò : non toccarmi , altrimenti non potrò più sposarmi e non tornerò : il mio fidanzato morirà .
arrivò la sera e andarono a dormire .
la madre s' avvicinò a guardare la figlia ; dormiva , e la baciò .
subito la ragazza dimenticò completamente il giovane .
la mattina la madre le ricordò : devi andare a sposarti con Bonfiore .
chi te l' ha detto ?
io non ne so niente .
il giovane allora disse tra sé : " Ahimé , la madre l' ha baciata ! " lei sposò un altro .
arrivò il momento del banchetto .
a Bonfiore intanto era giunta notizia che lei s' era sposata e , verso sera , andò : volete che vi prepari un dolce per il banchetto ? domandò .
sì , sì , risposero in coro i signori .
lo fecero salire ed egli preparò il dolce .
Compose due pupazzi , un uomo e una donna ; li mise uno di fronte all' altro : erano Bonfiore e Prezzemolina .
la ragazza non riconobbe il giovane ; egli invece la riconobbe e fece l' incantesimo che i due pupazzi parlassero .
allora Bonfiore disse : prima di mangiare , fate un po ' di silenzio e ascoltate quello che i pupazzi hanno da dire .
cominciò il primo , Bonfiore : ricordi quando mia madre ti ha ordinato di levare tutto il letame dalla cantina ed io ti ho salvata dalla morte ?
e con questo ? rispose l' altro pupazzo , Prezzemolina .
aspetta , aspetta ! e riprese : ricordi quando mia madre ti ha ordinato di pulire il pozzo e l' ho fatto io per te ?
e con questo ?
aspetta , aspetta !
ricordi quando mia madre ti ha mandata da sua sorella per farti divorare ed io ti ho fatta scappare ?
e con questo ?
aspetta , aspetta !
ricordi quando ci siamo trasformati in sagrestano e sagrestana ?
e con questo ?
aspetta , aspetta !
ricordi quando ci siamo trasformati in giardiniere e giardiniera ?
e con questo ?
aspetta , aspetta !
ricordi quando ci siamo trasformati in pesci e mia madre si bagnò tutta per prenderci ?
sentito tutto ciò , la ragazza alla fine esclamò : sei tu dunque Bonfiore !
subito scombinò il matrimonio , lasciò uno e prese l' altro . ed è finito !
il racconto è terminato ed io un soldo ho meritato .
