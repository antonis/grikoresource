c' era una volta una madre che aveva un figlio , chiamato Antonio lo Scemo .
un giorno doveva fare il pane e mandò il figlio al mulino per prendere la farina .
questi , quando s' avviò , non prese nulla da mettere , per strada , sopra la cesta .
così arrivò al mulino , caricò la cesta di farina , ma fuori c' era una forte tramontana e la portò tutta via .
giunto a casa , poveretto , non aveva più nulla .
la madre lo sgridò : cosa ci guadagni a sgridarmi ? egli disse : zitta , io so chi mi ha preso la farina ; è stata la tramontana ed io andrò a trovarla .
s' incamminò .
fece parecchia strada e arrivò ad un convento ; s' era fatta sera e si fermò lì a dormire .
la mattina dopo s' alzò e partì .
cammina cammina , senza mai fermarsi , trovò una pianura , e nel bel mezzo della campagna c' era una stanzetta ; andò a bussarvi alla porta : toc , toc !
chi è ?
sono Antonio lo Scemo .
alzati e aprimi .
che cosa vuoi ?
alzati , perché quando entro ti devo uccidere .
tu devi restituirmi la mia farina , continuò , altrimenti ti uccido . come ?
io ti ho dato la cesta piena fino all' orlo e devo restituirti la farina ?
io ti ho preso la farina ? non ne so niente .
tu devi restituirmi la farina , altrimenti ti uccido .
allora , fa ' come ti dico , lei rispose : eccoti questo tovagliolo ; quando vorrai qualcosa , subito ti si presenterà .
Antonio lo Scemo prese il tovagliolo e se ne uscì : " devo vedere se è vero " , disse più tardi .
si sistemò per mangiare : tovagliolo , apparecchia tavola ! ordinò .
poi riprese la sua strada , arrivò al convento e vi alloggiò .
la sera , si sedette a tavola , poi mise fuori il tovagliolo e disse : tovagliolo , apparecchia tavola !
cominciò a ordinare ciò che voleva e si trovò tutto bell' e pronto .
i monaci adocchiarono il tovagliolo e , quand' egli andò a dormire , glielo rubarono e gliene misero in tasca un altro .
la mattina s' alzò e se ne tornò tranquillamente a casa .
andò dalla madre , mise fuori il tovagliolo e ordinò : tovagliolo , apparecchia tavola !
ma il tovagliolo non apparecchiò un bel nulla .
Antonio lo Scemo s' arrabbiò e tornò un' altra volta dalla tramontana : questa volta mi hai ingannato , le disse , ma ora non m' ingannerai più ; tieniti il tuo tovagliolo !
ti ammazzo e così non ti prenderai più gioco di me .
zitto , lei rispose , eccoti queste forbici ; qualsiasi cosa taglierai , taglierai oro .
appena uscì , non vedeva l' ora di provare e tagliò un pezzo di pantaloni .
poi riprese la sua strada .
cammina cammina , arrivò di nuovo dai monaci e vi alloggiò per la notte .
la sera , cominciarono a divertirsi : Portatemi degli stracci .
i monaci non gliene portavano ; allora egli saltò addosso a uno di loro e gli tagliò un pezzo di tonaca .
non appena si resero conto , essi non sapevano più cosa portare per farla diventare oro .
quando il ragazzo andò a dormire , gli rubarono le forbici e gli misero in tasca un altro paio .
la mattina seguente Antonio lo Scemo s' alzò e se ne tornò a casa .
andò allegro dalla madre : mamma , portami degli stracci , portami degli stracci !
la poveretta non sapeva cosa dargli : che cosa ti devo portare ?
egli s' affannava : non mi vuoi ascoltare ?
aspetta .
s' avvicinò al letto e tagliò un pezzo di coperta , ma restò col pezzo di coperta in mano ; non diventò affatto d' oro .
ora la madre cominciò ad alzare la voce , ad andare su tutte le furie , a inviperirsi .
alla fine egli disse : Beh , stavolta la devo proprio ammazzare , venisse anche sua madre a supplicarmi !
andò e ricominciò a bussare : toc , toc !
chi è ?
Antonio lo Scemo .
di nuovo ; che cosa hai fatto ?
cosa hai fatto tu ! non mi hai ingannato ancora ?
Riprenditi le tue forbici .
io voglio proprio ammazzarti , altrimenti non sarò contento .
zitto , lei rispose , eccoti quest' asino ; sappi che , quando vorrai denari , basterà che tu dica : asino mio , caca denari ! e lui lo farà .
il ragazzo prese l' asino e se ne andò .
appena fuori , disse : asino mio , caca denari !
subito vennero fuori i denari .
s' accertò e riprese la sua strada .
giunse un' altra volta dai monaci .
portò l' asino nel bel mezzo della stanza e cominciò : Beh , avanti , asino mio , caca denari !
visto che davvero quello cacava denari , lo fecero portare dentro .
l' asino era bianco ; gli fecero fare denari a volontà , poi andarono a dormire .
i monaci trafugarono quell' asino e al suo posto ne misero un altro , ugualmente bianco .
la mattina dopo , il ragazzo , tutto tranquillo , si preparò , prese l' asino e se ne tornò .
andò dalla madre : mamma , stendi un lenzuolo per terra , sbrigati !
cosa devi fare col lenzuolo ?
ti ho detto di stendere un lenzuolo per terra .
a furia di ripeterlo , la convinse a stendere il lenzuolo : avanti , asino mio , caca denari !
il povero asino , invece di cacar denari , fece i suoi bravi escrementi .
la madre cominciò a urlare : malandrino , farabutto , cosa mi hai combinato !
raccolse il lenzuolo , poveretta , e lo ficcò nell' acqua .
Antonio lo Scemo riprese l' asino e disse : Beh , stavolta è sicuro che la farò a pezzi , venga pure chissà chi !
andò : alzati e aprimi .
Riprenditi l' asino , perché io non lo voglio .
voglio solo ammazzarti .
cosa ti ho fatto ? lei chiese : ti ho dato tante cose !
sì , me le hai date ; ma io ti ammazzo lo stesso .
aspetta , prima di uccidermi , fammi dire una parola ; rispondi a questa domanda : quando torni , passi per caso da un convento ?
sì , egli rispose .
e allora i monaci ti hanno rubato tutto .
davvero ? certamente .
comunque , eccoti adesso questi bastoni ; sappi che , quando tu dirai : bastoni miei , menate giù colpi ! essi si metteranno a picchiare .
il ragazzo uscì di lì .
non vedeva l' ora di provare : bastoni miei , menate giù colpi !
ora cominciò a buscarsi bastonate !
basta , basta , basta !
afferrò i bastoni e questi smisero di dar colpi .
arrivò dai monaci : uno , due , tre , contò : e gli altri dove stanno ?
non ci sono .
andate a chiamarli .
abbi pazienza , non ci sono .
no , dovete esser tutti .
le altre volte c' eravate tutti ; anche stavolta dovete esserci .
andarono a chiamarli e vennero tutti ; gli si disposero intorno : Beh , disse allora , bastoni miei , menate giù colpi ; e datene senza tregua !
i bastoni si misero a picchiare , a dar colpi a più non posso .
dopo che li ebbe bastonati a volontà : Beh , basta ! disse .
li aveva tutti sfracassati ; uno era finito a esalar l' anima sotto l' altare ; ne morirono due .
così ben conciati , li lasciò , prese gli oggetti che gli erano stati rubati e se ne tornò a casa .
tornò a casa e disse alla madre : mamma , apparecchia tavola .
la madre , poveretta , apparecchiò ; egli allora mise fuori il tovagliolo e ordinò da mangiare .
si sedettero e mangiarono , e vissero felici e contenti ; mai stati meglio di così !
