c' erano una volta una madre e tre figlie , e vicino viveva una zia fata .
la madre non aveva più pane da dare alle ragazze e una notte pensava come fare per portarle via da casa .
la zia , che abitava proprio accanto , ascoltava tutti i piani della madre : all' alba , architettava costei , sarebbe andata in un bosco e avrebbe portato le figlie affinché vi si smarrissero .
la piccola stava a sentire ; la mattina s' alzò e andò a riferire alla zia .
la zia le diede allora un gomitolo di spago e disse : quando andrete con vostra madre nel bosco , tu rimani dietro e svolgi piano piano il gomitolo .
la madre portò le figlie nel bosco e le abbandonò .
poi se ne tornò a casa .
le ragazze seguirono lo spago e riuscirono a trovare la strada ; andarono a casa .
la madre , nel vederle , disse : siete venute di nuovo ?
certo , mamma , tu te ne sei andata e ci hai lasciate lì , rispose la piccola .
la sera si misero a letto e la madre rinnovò il suo proposito : domani devo portarle in un bosco più lontano , diceva a sé stessa , ma la figlia piccola era sveglia e ascoltava .
la mattina s' alzò e corse un' altra volta dalla zia : la mamma ha detto che vuole portarci in un bosco più lontano .
prendi questa farina , lei rispose , tu cammina dietro e spargila per strada .
le portò in un bosco più lontano ; la piccola restava dietro e gettava la farina .
la madre abbandonò le ragazze e tornò , ma quelle , seguendo la traccia della farina , ritrovarono la strada di casa .
la sera si misero di nuovo a letto e la madre diceva : devo portarle in un bosco più lontano .
la mattina dopo la piccola s' alzò e andò ancora dalla zia : la mamma ha detto che vuole portarci in un bosco più lontano .
la zia rispose : ascolta , voi siete della brave ragazze ; troverete nel bosco un palazzo , vedrete delle grate e vi salirete su .
va bene , disse la piccola .
la madre si diresse di nuovo con le figlie verso un bosco , lo raggiunse , lasciò lì le ragazze e se ne tornò .
le tre sorelle videro il palazzo , vi salirono e trovarono un Orco e un' Orchessa : da dove venite ? essi chiesero .
avevamo una madre che non ci dava più da mangiare , rispose la piccola , non aveva più pane per noi ; allora ci ha portate nel bosco ; noi abbiamo visto questo palazzo e siamo salite .
avete fatto bene , disse l' Orchessa , a venire qui .
cominciarono a dar loro da mangiare , per farle ingrassare , perché erano molto magre , e farsene poi un bel boccone .
dopo parecchio tempo , la sorella più piccola disse : e ' ormai tanto tempo che stiamo qui e non abbiamo fatto ancora una focaccia .
tu la sapresti fare ? chiese l' Orco .
certamente , rispose lei .
così le portò la padella , le fornì l' occorrente e la ragazza preparò la focaccia : va ' a mettere molte fascine nel forno , disse poi ; la focaccia ha bisogno di molto calore per cuocersi .
l' Orco accese il fuoco e lei portò la padella : sali e spingi la padella più giù , fece la ragazza .
l' Orco salì sullo scanno e subito lei gli diede uno spintone e lo fece finire nel forno .
allora , sempre la sorella più giovane , si recò dall' Orchessa : s' è alzato il nonno ? chiese l' Orchessa .
sì , lei rispose , sta badando alla padella che è nel forno .
poi continuò : ho già trascorso qui tanto tempo e non ho ancora pettinato l' Orchessa .
le sciolse dunque i capelli e si mise a spidocchiarla ; colse al volo l' occasione , prese lo spillo che teneva appuntato al vestito , glielo ficcò in testa e la fece stramazzare .
le tre sorelle presero allora l' Orchessa , la sollevarono e la gettarono in una fossa ; la coprirono di terra e di pietre e tornarono nel palazzo .
entrarono nelle stanze e trovarono oggetti preziosi d' ogni tipo .
la piccola trovò un piccolo scrigno dove c' erano degli abiti di seta e d' oro ; lo ripose e lo tenne nascosto dalle sorelle .
un giorno , il figlio del re d' un paese vicino fece un invito : chiunque volesse ballare e divertirsi poteva recarsi alla reggia .
le sorelle dissero alla più piccola : tu rimani qui , noi andiamo a partecipare al ballo del re .
la lasciarono sola e se ne andarono . il ballo ebbe inizio .
torniamo dalla ragazza che era rimasta a casa .
la zia fata le mandò un cavallo con le staffe d' oro , le briglie d' oro , la sella d' oro , tutto d' oro .
il cavallo arrivò al palazzo e le si presentò davanti ; lei capì che l' aveva mandato la zia e s' affrettò a vestirsi . poi montò sul cavallo , andò alla festa , fece un giro di ballo e scappò via .
tornò sul palazzo , si svestì , spedì indietro il cavallo e si mise di nuovo vicino al fuoco .
tornarono le sorelle maggiori .
la piccola domandò : avete visto una signora su un cavallo intervenire alla festa ?
Mah , non importa ! se ne andarono a dormire .
spuntò il giorno seguente ; quando arrivò la sera , le sorelle partirono di nuovo .
la zia mandò un' altra volta il cavallo ; lei lo vide , si vestì , montò su e si recò alla festa ; fece un altro giro di ballo e ad un tratto perdette lo scarpino .
la ragazza per la fretta lo lasciò e scappò via .
spedì il cavallo e si rimise al focolare .
intanto dal re cercavano di scoprire a chi appartenesse lo scarpino , ma non ci riuscirono .
quando le sorelle tornarono a casa , raccontarono : stasera è venuta una signora con un vestito bellissimo , le è caduto lo scarpino ed hanno chiesto a tutte per sapere di chi fosse , ma la padrona non s' è trovata .
non m' importa , disse la piccola . se ne andarono a dormire .
spuntò di nuovo l' altro giorno , arrivò la sera e le sorelle partirono per il ballo . la zia mandò ancora il cavallo alla ragazza ; lei lo vide , si vestì , montò su , ed aveva un solo scarpino ad un piede .
i partecipanti al ballo erano tutti attenti e a braccia aperte per afferrarla appena fosse entrata .
lei fece un giro di ballo e subito la presero : Proviamo se lo scarpino va bene a lei .
glielo infilarono e le stava a perfezione .
voglio lei per moglie , disse allora il figlio del re .
si sposarono , felici e contenti .
e , se non credi e vuoi andarli a cercare , sappi che ancora lì stanno ad aspettare .
il racconto è terminato e io un soldo ho meritato .
