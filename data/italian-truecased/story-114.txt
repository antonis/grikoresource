c' era una volta un giovane che andava in giro .
aveva preso con sé il cavallo , tre cani e s' era messo in cammino .
cammina cammina , cammina cammina , giunse a Roma .
appena arrivò a Roma , sentì suonare le campane a morto .
chiese a una donna : che cosa è successo ? perché le campane , da quando sono entrato a Roma , continuano a suonare a morto ?
eh , ragazzo mio , suonano perché appena fuori città , qui vicino , c' è una villa con un drago e questo drago ogni giorno deve mangiare una ragazza . suonano a morto già da stamattina .
davvero ? disse il giovane .
allora egli andò , aprì il cancello ed entrò ; c' era lì una cappella ; disse : Beh , signorina , e tu cosa fai ?
sto pregando , lei rispose , perché alle dodici il drago mi mangerà ; sono costretta a farmi divorare .
per forza ?
per forza .
allora il giovane disse : va bene .
tolse la spada dal fodero , la impugnò e cominciò a tagliare le teste al drago .
appena ne tagliò una , il cane l' afferrò e la portò lontano ; ma , come la portò via , la testa subito tornò indietro .
allora il giovane staccò al drago l' altra testa e disse al cane : Spezzaferri , prendila e portala lontano !
il cane l' afferrò e la portò lontano .
riprese a combattere , a tagliare al drago l' altra testa , e quello faceva : uh , uh , uh !
staccò la testa e disse all' altro cane : lupo , prendi questa testa e portala lontano !
ricominciò di nuovo a combattere ; c' era l' altro cane : Fioravante , prendi la testa e portala lontano !
la portò lontano .
il giovane combatteva e i cani portavano lontano le teste .
alla fine tagliò al drago tutt' e tre le teste e le portò lontano .
dopo averle portate via , disse : signorina , sei libera , puoi andare dove vuoi , puoi tornartene a casa ; le teste non si riattaccheranno più ; l' animale , il drago non c' è più , l' ho ucciso .
dimmi chi sei .
no , no , rispose il giovane , non ce n' è bisogno .
andò via .
quand' era per strada , pensò : " però , quelle teste ...
se qualcuno passerà e le prenderà , che farà ? dirà che è stato lui a uccidere il drago " .
allora tornò , tagliò le lingue a tutte le teste , le mise in una borsa e le portò con sé .
il giorno dopo il re fece bandire : chiunque avesse ucciso l' animale , il drago , aveva il diritto di sposare sua figlia .
s' era trovato a passare uno stacciaio , che riparava le giare , aveva raccolto quelle teste , le aveva messe in un sacco e se l' era portate dietro .
quando sentì che chi aveva ucciso il drago avrebbe sposato la figlia del re , egli si presentò e disse : ecco , ho ucciso io l' animale ; qui ci sono le teste .
bravo , bravo , hai fatto bene !
Sicche tu vuoi sposare mia figlia ?
perché no ? rispose : la sposo .
lo vestirono per bene , dunque , lo prepararono , tutto ben pulito , per le nozze .
ora , immagina un po ' ! c' erano tanti invitati : principi , nobili ...
quando stavano per celebrare il matrimonio , quel giovane si trovò a passare un' altra volta da Roma e domandò : cosa c' è qui ? c' è per caso qualche festa , oggi ?
certo . si sta sposando la figlia del re ; un tale ha ucciso il drago ed ora sposa la principessa .
Ah , disse il giovane , bravo !
si diresse allora lì e vide un gran movimento ; bussò per salire sopra , ma c' era l' ordine di non lasciar entrare nessuno .
egli aveva con sé un cagnolino , bianco ; esso passò tra le guardie e riuscì a salire . andò dritto dalla principessa il cane conosceva la ragazza , e anche lei lo conosceva , perché , mentre il giovane uccideva il drago , quel cane lo aiutava nell' impresa .
allora lei corse dal padre : papà , guarda : qui c' è il cane che ha aiutato il principe a uccidere il drago ; io però non lo conosco , non so chi sia .
davvero ? disse il re .
certo .
giù c' è un tale , continuò il re , che non fanno entrare .
comandò allora che lo facessero salire , e il giovane entrò nel palazzo .
quando questi salì su c' erano tutti i principi , i duchi , i conti che mangiavano e banchettavano disse : Permettete che vi dica una parola ?
perché no ?
come mai state facendo tanti festeggiamenti ? chiese allora : avete controllato quelle teste ? continuò : c' è qualche animale che non ha la lingua ?
no , risposero : non c' è nessun animale senza lingua ; tutti gli animali devono avere la lingua , gli uomini e tutti .
fatemi dunque un favore , disse : controllate quell' animale ; guardate se ha le lingue .
aprirono allora una testa , e non aveva lingua ; aprirono l' altra , neppure ; aprirono l' altra , neppure ...
le aprirono tutt' e sette : nessuna aveva la lingua .
e dov' è la lingua di quest' animale , del drago ? chiese . nessuno sapeva rispondere , naturalmente .
volete vedere dove sono le lingue ? disse allora .
le prese e le mise fuori dalla borsa : sono queste le lingue del drago .
rimasero tutti di stucco , il re per primo .
la principessa riconobbe il giovane e disse : papà , è lui il giovane che ha ucciso il drago ; ed anche il cagnolino è lo stesso che veniva di tanto in tanto , quand' egli tagliava le teste , a leccarmi le gambe e poi scappava ; proprio questo cagnolino bianco .
davvero ? disse il re : va bene , dunque .
allora ordinò subito che l' altro fosse arrestato , portato in piazza , lontano , e bruciato , poiché aveva cercato di ingannarli .
e così finì : la ragazza sposò il principe che aveva ucciso il drago .
