c' era una volta un mercante che aveva tre figlie ; erano belle tutt' e tre , ma la piccola era così graziosa da non avere uguali , sembrava la dea Venere .
un giorno il padre le chiamò e disse : io devo partire per un paese lontano , e al mio ritorno voglio trovarvi così come vi ho lasciate ; non devo aver preoccupazioni , altrimenti non me ne potrò andare .
va ' pure tranquillo , risposero le figlie , non darti pensiero per noi ; come ci lasci , così ci troverai .
allora , chiedetemi cosa volete che vi porti , esponetemi il vostro desiderio .
io voglio un abito di seta rossa , disse la maggiore .
io un abito si seta azzurra , aggiunse la seconda .
per me sai cosa voglio ? disse la più piccola : che tu porti il mio saluto al Gran Visir ; mi basta questo ; e fa ' un buon viaggio .
dunque il padre s' accommiatò e se ne partì contento , vedendo che le sue ragazze erano così giudiziose ; s' imbarcò e le figlie restarono a casa .
cominciò ad andar per mare .
cammina cammina , raggiunse una città ; fece ormeggiare il bastimento e andò a informarsi , giacché non lo sapeva , se fosse arrivato nella città del Gran Visir .
ma nessuno lì sapeva dargli notizie del Gran Visir .
partì di nuovo e raggiunse un' altra città .
poi navigò ancora per molto tempo e di nuovo si fermò ; cominciò a chiedere del Gran Visir , se fosse la città del Gran Visir , ma neppure qui gli seppero dire qualcosa ; nessuno lo aveva sentito nominare .
prese il largo e continuò il viaggio .
arrivò in una nuova città , scese un' altra volta e si mise a chiedere ; finalmente risposero che quella era la città del Gran Visir .
cercò e gli indicarono il palazzo ; l' informatore però aggiunse : ma tu vorresti parlare con il Gran Visir ?
Dubito proprio che possa riuscirci .
il mercante si fece coraggio e salì sul palazzo .
mentre saliva , incontrò un giovane e lo riverì .
cosa desideri ? chiese il giovane .
io cerco il Gran Visir , rispose : è vostra signoria ?
no , il Gran Visir è mio fratello .
io vorrei parlare con lui in persona . Beh , se vuoi parlar con lui ...
egli fa una sola apparizione durante la giornata ; esce da quella porta per entrare in quell' altra .
se ti trovi lì in quell' istante , lo vedrai .
farò come mi dici , rispose il mercante .
si fece indicare l' ora e si sedette ad aspettare .
arrivò l' ora che gli era stata indicata .
appena lo vide venir fuori , s' avvicinò ; si gettò ai suoi piedi e disse : una preghiera !
cosa desideri ?
solo una preghiera !
e qual è la tua richiesta ?
io ho una figlia , rispose allora il mercante , che ti manda il suo saluto ; lei vorrebbe soltanto vederti .
egli cercò nella tasca , tirò fuori uno spillo d' oro e disse : ecco , dalle questo ; se mi vuole vedere , lo metta nella bacinella con l' acqua , guardi dentro e mi vedrà .
mentre usciva , il mercante incontrò il giovane col quale aveva parlato la mattina , il fratello del Gran Visir : cosa ti detto ? domandò .
gli ho portato i saluti di mia figlia .
e cosa ti ha risposto ?
mi ha dato questo spillo da portarle .
fai un' altra cosa , disse allora l' altro : aspettalo domani alla stessa ora e digli che a tua figlia lo spillo non basta ; che lo guarda e piange ; ti dia qualche altra cosa .
così fece ; aspettò di nuovo alla stessa ora e quello passò : cosa desideri ?
cosa desidero ?
a mia figlia lo spillo non basta ; quando lo guarda piange .
il Gran Visir cercò nella tasca e tirò fuori un fazzoletto : ecco , con questo si asciughi le lacrime !
il padre lo raccolse e s' avviò per andar via .
di nuovo incontrò il fratello del Visir : cosa hai fatto ? questi chiese .
mi ha dato un fazzoletto perché si asciughi le lacrime .
aspetta un altro giorno , disse ancora il fratello del Visir e digli che a tua figlia non bastano le cose che ti ha dato .
il poveretto si strinse nelle spalle e restò .
arrivò l' ora e passò il Visir : mia figlia , quando vede queste cose , si dispera , riferì il mercante , secondo il suggerimento ricevuto .
il Visir cercò nella tasca , tirò fuori un coltello e disse : Portaglielo ; quando si dispera , dille che si uccida . mentre usciva , rivide il fratello : che cosa hai fatto ?
mi ha dato questo .
adesso va bene ; va ' pure per la tua strada .
il mercante concluse allora gli affari per i quali era venuto e se ne tornò a casa .
quando le figlie videro arrivare il bastimento del padre , si precipitarono ; appena sceso , gli saltarono addosso per abbracciarlo e fargli festa .
questi fu pieno di gioia nel ritrovarle così come le aveva lasciate .
chiamò allora la maggiore e le consegnò il suo regalo ; poi fece lo stesso con la seconda .
le sorelle presero i loro doni e cominciarono a farsi beffe della più piccola : guarda , guarda che bei regali ! tu invece a mani vuote : arrangiati !
il padre chiamò poi la piccola e le disse : Beh , ecco i doni per te ! e le raccontò ogni cosa .
la ragazza ritirò il regalo e non vedeva l' ora di restar sola e prender la bacinella senza esser vista . non appena le fu possibile , corse in giardino con una bacinella , si nascose sotto un arancio , gettò dentro lo spillo e sotto l' acqua le comparve il Visir .
dopo averlo visto , la poveretta non ebbe più pace e il giorno seguente andò a parlare al padre : padre mio , vorrei del denaro , qualunque somma tu mi possa dare : devo partire per trovare il Gran Visir .
il padre non volela lasciarla e , insieme con le sorelle , cercò tra le lacrime di convincerla .
cosa dite ? devo partire , lei ripeteva .
il padre tergiversava , ma la figlia insisteva ; alla fine , vedendola così decisa , le diede quattromila ducati e la lasciò andare .
lei si recò in riva al mare e attese di scorgere un bastimento per imbarcarsi .
dopo qualche tempo ne scorse uno , fece segno col fazzoletto e l' imbarcazione s' accostò : cosa vuoi ? le chiesero .
voglio imbarcarmi .
dovrebbe partire il bastimento solo per te ?
occorre molto denaro .
ho quattromila ducati : finché basteranno , navigheremo ; poi mi avviserete ed io sbarcherò .
Salparono dunque e cominciarono a navigare .
quando finì il denaro , i marinai le dissero : non possiamo andare più avanti ; i soldi son finiti .
non c' è qualche città nelle vicinanze , un paese dove io possa scendere ? lei domandò : come potrei fare ?
proprio allora si profilò una città in lontananza : fatemi sbarcare lì e vendetemi , disse ; però non meno di quattromila ducati .
Sbarcarono in quella città e si misero a gridare : chi vuole una schiava , chi vuole una schiava !
c' era nelle vicinanze il palazzo del re e molte damigelle stavano affacciate al balcone : Oh , com' è bella !
prendiamola , maestà , prendiamola , è molto bella !
la fecero salire ; al re e alla regina piacque e decisero di comprarla .
Pagarono i quattromila ducati , i marinai partirono e lei restò con loro .
Vivendo sul palazzo del re , la ragazza non notava altro che tristezza e musi lunghi ; non sembrava affatto un palazzo reale .
un giorno si trovò con la regina e le chiese : vorrei farti una domanda ; questo vostro palazzo non sembra proprio un palazzo reale ; non c' è alcun divertimento , sembra perennemente in lutto .
la regina rispose : ragazza mia , meglio non sapere .
perché ?
io ho avuto un solo figlio , disse la regina , e adesso ha ventun anni , però sta sempre a letto , dorme e non si sveglia mai . non mangia e non beve , eppure sembra sereno ; a guardarlo , non si crederebbe ; sempre nel sonno più profondo , di notte e di giorno .
infatti , per quanto lo stuzzicassero , il giovane non si svegliava mai ; sempre steso nel letto .
la ragazza domandò : fammi vedere tuo figlio .
l' accompagnò per mostrarglielo ; appena lo vide , la ragazza restò di stucco : aveva il volto d' un rosso così chiaro !
si mise a punzecchiarlo , ma non ci fu alcuna reazione .
si rivolse allora alla regina : sai cosa devi fare ?
fai portare il mio letto dietro la sua porta .
presero il letto e glielo portarono ; sicché qui stava il figlio del re e dietro la porta c' era il letto della ragazza .
la sera lei si coricò .
quand' erano le cinque e tutti s' erano ormai addormentati e non si sentiva più nulla , ecco calarsi una scimmia e un orso ; entrarono , la scimmia toccò il giovane con una bacchetta e lo svegliò .
poi ordinò all' orso : orso , apparecchia !
non appena la tavola fu imbandita , con ogni sorta di pietanze , il giovane e la scimmia cominciarono a mangiare .
finito il banchetto , dormirono ; dopo aver dormito un paio d' ore , la scimmia chiese : orso , che ore sono ?
e l' orso rispose : galli non cantano , campane non suonano e cani non abbaiano : vuol dire che possiamo restare ancora .
la ragazza se ne stava dietro la porta e spiava tutta la scena .
dopo un po ' la scimmia si svegliò di nuovo e ripeté la domanda : adesso dobbiamo alzarci , ché l' ora è vicina , rispose stavolta l' orso .
la scimmia si alzò e diede un colpo di bacchetta al giovane , che di nuovo cadde come morto , da non poter essere svegliato in nessun modo .
la mattina seguente la ragazza lasciò il suo posto e corse dalla regina : sapete cosa occorre fare ? disse .
cosa ?
magari ci fosse un rimedio .
ascoltate : dovete avvertire la guardiana delle galline affinché , quando saranno le sette della notte , crei scompiglio nel pollaio in modo che le galline si mettano a cantare ; ancora , le chiese siano avvertite che alle sette della notte suonino tutte le campane e , alla stessa ora , quanti cani ci sono sul palazzo , vengano tutti prontamente aizzati ad abbaiare .
arrivò l' ora che tutti dormivano e si presentò di nuovo la scimmia ; toccò il giovane con la bacchetta e lo svegliò ; poi ordinò all' orso di apparecchiare , mangiarono e si misero di nuovo a letto .
dormirono un po ' , poi la scimmia chiese : orso , che ore sono ?
galli non cantano , campane non suonano e cani non abbaiano : vuol dire che possiamo restare ancora .
allora s' addormentarono ancora .
quando arrivarono le sette , successe il pandemonio ; la scimmia si svegliò di soprassalto e sentì i galli che cantavano , le campane , i cani .
scappò via e dimenticò di colpire il giovane con la bacchetta , sicché questi si ritrovò in piedi e cominciò a passaggiare per tutto il palazzo .
quando s' alzarono il padre e la madre e lo videro in giro per le stanze , non si trattennero dalla gioia ; corsero da lei : Oh , figlia mia , chi ti ha mandata da noi ?
avevamo tentato ogni espediente senza alcun successo .
adesso io ti incoronerò , il padre incoronerà suo figlio e vi sposerete .
lei si schermì : vi ringrazio per la vostra bontà , ma io non sono destinata per vostro figlio ; per lui dovete trovare la figlia d' un re .
noi preferiamo te , perché tu gli hai ridato la vita , disse il padre .
io voglio soltanto la mia libertà .
vedendo allora la sua determinazione , la lasciarono libera : che tu sia benedetta ! dissero .
la ragazza uscì dal palazzo e se ne andò .
cammina cammina , arrivò in riva al mare ; restò lì finché non scorse un bastimento ; cominciò a far segni col fazzoletto e l' imbarcazione s' accostò : cosa vuoi ? le chiesero .
voglio andare oltre , se mi potete portare .
dovrebbe partire il bastimento solo per te ?
occorre molto denaro .
ho quattromila ducati ; finché basteranno , navigheremo ; poi mi avviserete ed io sbarcherò .
raggiunsero un' altra città ; le dissero : il denaro è finito , non possiamo andare più avanti .
Lasciatemi allora in città , lei rispose .
entrarono nella città e si misero a gridare : chi vuole una schiava ?
chi vuole una schiava ?
si trovò a passare un vecchio , che viveva solo con la moglie ; andò questi da lei e le propose : c' è una bella ragazza in vendita , non la si potrebbe comprare ?
la donna prima si oppose , poi , visto che il marito insisteva , acconsentì : se tu la vuoi , prendiamola ; forse ci sarà utile ; noi non abbiamo nessuno .
il vecchio tornò , pagò i quattromila ducati e prese la ragazza con sé .
la moglie del vecchio , ogni sera , dava del sonnifero al marito e lo faceva addormentare ; il poveretto , quand' era ancora a tavola , sprofondava nel sonno , sicché la moglie e la ragazza dovevano spogliarlo e metterlo a letto .
dopo averlo sistemato , la moglie usciva e si ritirava al mattino .
la ragazza , che s' era accorta d' ogni cosa , un giorno propose al vecchio : nonno , così lo chiamava , questa sera , quando tua moglie ti verserà il vino , fa ' finta di bere e rovescialo invece sul petto ; poi accasciati come se dormissi .
figlia mia , farò quanto mi suggerisci , egli rispose .
arrivò la sera , la vecchia apparecchiò e preparò il sonnifero .
il marito portò il bicchiere alla bocca , finse di bere e invece si rovesciò addosso il contenuto ; poi simulò d' essersi addormentato e piombò come morto ; allora fu preso e messo a letto .
la moglie fece come ogni sera : scomparve da casa .
quando la vide uscire , la ragazza tornò dal vecchio : vedi , nonno , disse , con tutto quel sonnifero che lei ti dà continuamente finiresti per morire .
vieni qui , la chiamò ; egli aveva un tesoro e lo mostrò alla serva : questo è oro , questo argento e questo rame ; carica quanto ne vuoi .
io , nonno , non voglio niente ; solo la mia libertà .
io avrei voluto tenerti con me , e tutto questo sarebbe stato tuo .
comunque , rispetto la tua volontà ; sei libera .
lei s' incamminò , raggiunse la riva del mare e , come le volte precedenti , fece accostare il bastimento e salì a bordo .
quando sbarcò nella nuova città , s' informò : che città è questa ?
la città del Gran Visir , risposero .
allora lei s' addentrò e si mise a cercare il palazzo del Visir .
lumen de lumine ! le indicarono il palazzo e vi si diresse .
trovò una vecchia nelle vicinanze e restò da lei : vorrei che tu mi cercassi il modo per entrare nel palazzo del Gran Visir , chiese la ragazza .
lì , rispose la vecchia , si può salire solamente quando vanno tutti per la festa da ballo ; allora ti potrò accompagnare ; è l' unica occasione in cui si può accedere liberamente .
così , quando vide che si preparavano gli strumenti , l' avvertì : adesso è il momento , disse .
la ragazza , che di sopra indossava un vestito molto semplice , aveva di sotto un abito elegantissimo , il più bello ; la sera tardi s' acconciò e si fece portare dalla vecchia sul palazzo .
appena salì , bella d' abito e più bella d' aspetto , tutti si voltarono a guardarla , sbalorditi .
anche il Gran Visir le aveva messo gli occhi addosso ; era davvero d' una gran bellezza , non c' era nessuna che reggesse il confronto .
quando poi la invitarono a ballare , ancor di più tutti quanti restarono soggiogati .
terminato il ballo , il Gran Visir la invitò con un cenno a sederglisi accanto ; poi le si accostò all' orecchio , perché gli altri non sentissero , e domandò : dimmi chi sei e da dove vieni .
non mi conosci ? lei rispose .
come posso conoscerti ?
non ti ho mai vista .
lei allora gli mostrò lo spillo d' oro : questo sì , lo conosco , disse il Visir .
certo , per vederti dovevo metterlo nella bacinella ...
e poi , dopo averti visto , mio padre ti disse che avrei pianto , e tu gli desti il fazzoletto perché asciugassi le lacrime ; te ne ricordi ?
mostrò allora il coltello : e quando mio padre ti disse che mi sarei disperata , gli hai consegnato il coltello perché mi uccidessi .
dopo aver ascoltato le sue parole , e poi il racconto di tutte le sue peripezie , non è il caso che ci mettiamo a ripeterle il Gran Visir disse : ora non te ne andrai più da qui !
passò la notte e , quando la mattina s' alzarono , il Visir dichiarò : voglio sposarti .
anch' io non cerco altro ; ho tanto sofferto !
si sposarono e lei rimase col Gran Visir .
questi aveva nel suo palazzo una camera con un grande specchio , e l' apriva una volta l' anno .
quando fu il tempo , andò ad aprire e guardò nello specchio ; si mise a ridere .
vorrei proprio sapere perché ridi , chiese la moglie ; chissà cosa hai visto dentro .
vuoi che te lo dica ?
certo che voglio .
tua sorella maggiore oggi si sposa , disse il Visir : si stanno preparando per andare in chiesa .
Ah , potessi esserci anch' io !
vuoi andarci ?
sicuro , e immediatamente !
se vuoi andare , entra in quella stanza , indossa l' abito più bello , prendi un dono per tua sorella , qualcosa di oro da regalarle , poi va ' lì vicino : soffierà il vento e ti ci porterà .
arriverai e troverai tua sorella che sta andando in chiesa .
però fa ' attenzione : quando tornerà a casa , non sederti a tavola per mangiare ; congedati e avvicinati al portone , perché soffierà di nuovo il vento e ti riporterà qui .
non fermarti a mangiare ; se lo facessi , non mi troveresti più .
farò esattamente ciò che mi hai detto , lei rispose .
soffiò il vento e si trovò dalla sorella .
non appena le sorelle la videro apparire in quel modo , si precipitarono ad abbracciarla ; poi s' avviarono insieme alla chiesa .
al ritorno , volevano farla restare per il pranzo , ma lei rifiutò : non posso fermarmi , devo andare .
allora si salutarono ; lei attese presso il portone ; il vento soffiò e la riportò a casa .
il marito si rallegrò .
arrivò l' anno seguente .
si sposò la seconda sorella e ricominciò la stessa storia .
senza stare a raccontare tutti i dettagli , basterà dire che andò e se ne tornò sana e salva .
la volta successiva ci saranno problemi !
passò l' anno e il Visir s' avvicinò allo specchio ; guardò e subito scoppiò in lacrime .
la moglie , che gli stava vicino , lo vide piangere : dimmi cos' hai , tu stai piangendo , domandò .
le altre volte te l' ho detto , egli rispose , ma stavolta non te lo dirò .
lei lo scongiurò : devi dirmelo !
dovette rivelarlo per forza : tuo padre è morto , disse , e lo stanno chiudendo nella bara .
Ahimé , fammi andare come le altre volte !
ci vuoi andare davvero ?
se ci vai , devi fare quello che ti dico , altrimenti non mi troverai più .
la moglie disse : non preoccuparti ; le volte scorse non ti ho obbedito ?
anche stavolta lo farò .
va ' allora nella stanza dove ci sono gli abiti da lutto tutti di seta e indossane uno .
lei entrò nella stanza , s' abbigliò e tornò dal marito : adesso sai cosa fare , le disse ; quando arriverai , le tue sorelle si staranno struggendo dal dolore ; tu ti presenterai , bacerai tuo padre e poi andrai a sederti discosta da tutti ; mentre starai seduta , un oggetto d' oro ti colpirà sulla guancia : quello è il segnale del ritorno ; alzati e torna immediatamente .
lei disse di sì , andò sul palazzo e si trovò dalle sorelle : Ah , sorella ! queste dissero , non appena la videro di nuovo .
lei baciò il padre e poi andò a sedersi .
allora le sorelle cominciarono a dirle : sorella , vieni a piangere tuo padre ; che figlia ingrata !
continuarono a biasimarla .
a un tratto avvertì l' oggetto d' oro che la colpiva per farla andar via ; allora lei esclamò : Batti , orologio , finché vuoi ; io non posso più trattenermi dall' abbracciare mio padre !
s' alzò e si mise a piangere accanto alla bara .
dopo che arrivarono i preti e portarono via il morto , lei rimase un po ' con le sorelle , poi le salutò e attese al solito posto per essere trasportata , ma non venne né vento né altro ; restò sola , poveretta .
Vedendosi abbandonata , senza dir nulla alle sorelle , lei s' incamminò ; ed era pure incinta , povera donna !
cammina cammina , senza mangiare né bere , senza riparo né di giorno né di notte , giunse in una campagna scura , dove non compariva né cielo né terra e vi si addentrò .
cammina cammina , incontrò un carrettiere , il quale , nel vederla così bella e così ben vestita , restò sbalordito ; le si avvicinò : cosa fai per queste pianure , dove non arrivano neppure gli animali ?
lei rispose : meglio che tu non conosca la mia sventura .
fu tanto il dispiacere , tanta la compassione che l' uomo provò , che le disse : sai cosa devi fare ? sali sul carretto e ti porterò a casa mia .
la poveretta , che non aveva altra salvezza , salì e andò col carrettiere .
questi era benestante , ma non aveva figli ; viveva solo con la moglie .
non appena lei vide la ragazza : chi è questa donna che ti sei portato dietro , a che cosa ti serve ? cominciò ad alzar la voce e a rimbrottare ; non la voleva , era gelosa .
il marito le rispose : abbi compassione di lei ; non hai nessuno a casa con te ; basterà darle ospitalità ; ti sbrigherà anche le faccende .
la moglie non voleva saperne , per nessuna ragione .
a furia di insistere e dire che le avrebbero sistemato un lettuccio nella stalla , alla fine il marito la persuase e così le approntarono un giaciglio alla buona .
passa oggi , passa domani , passa dopodomani , alla giovane vennero le doglie .
il carrettiere si trovò a entrare nella stalla delle bestie e sentì che si lamentava tutta sola ; non c' era nessuno ad aiutarla .
corse dalla moglie : Oh , Maria , va ' ad aiutare quella poveretta !
non è pure lei una creatura di Dio ? disse .
allora alla donna si intenerì un po ' il cuore , ché non era cattiva ; fece venire l' ostetrica e l' aiutarono .
nacque un bambino .
dopo il parto , la rimisero nel letto , ognuna andò per le sue faccende e la giovane rimase di nuovo sola .
lasciamo ora da lei e torniamo dal Gran Visir .
questi sapeva ogni cosa , era a conoscenza di tutte le sofferenze della moglie ; così , quando lei fu vinta dal sonno e s' addormentò , andò a trovarla ; arrivò , prese il bambino , gli tolse le fasce , vi mise dentro un oggetto d' oro e sparì aveva le sette arti e mezza , lui !
la mattina seguente , il piccolo s' era messo a piangere ; lo sentì la moglie del carrettiere ed entrò ; provò compassione e prese in braccio il bambino , per permettere alla donna di sbrigare le sue cose ; cominciò a levargli le fasce e cadde per terra l' oggetto d' oro facendo risplendere tutto il pavimento .
la giovane era occupata e non notò nulla ; sicché la moglie del carrettiere si piegò e se lo mise in tasca .
dopo aver restituito il bambino , lei andò ad osservare per bene l' oggetto e capì che si trattava d' una cosa preziosa .
allora sì che cominciò ad ammazzare galline e preparare brodini per la giovane !
quando il Visir s' accorse che le premure diminuivano , tornò e fece la stessa cosa : mise un altro oggetto d' oro nelle fasce del bambino .
il piccolo pianse di nuovo ; la moglie del carrettiere lo prese , gli tolse le fasce , l' oggetto d' oro cadde per terra e lei ricominciò a portar da mangiare .
bisogna ora sapere che nelle vicinanze c' era il palazzo di un re ed anche la regina aveva avuto le doglie e aveva partorito .
la moglie del carrettiere si trovava lì quando nacque il bambino e udì il re che diceva : adesso ci vorrebbe una nutrice .
se volete , disse allora lei , nella mia rimessa c' è una donna che di latte ne ha in abbondanza ; basterà per tutt' e due .
falla venire qui , rispose il re . la giovane si recò da loro e risultò gradita .
le affidarono il bambino da allevare e lei se li portò entrambi nella stalla ; a volte stava lì , a volte nella casa del re .
ora i bambini , succhiando e mangiando , s' erano fatti grandicelli ; camminavano già da soli , e il figlio del re si mostrava sottomesso nei confronti del figlio del Visir .
una volta che giocavano insieme , il figlio del re s' era messo a piangere .
il re s' accorse che il figlio piangeva e cominciò a inveire contro la madre e il povero bambino : bastardo , chissà da dove vieni fuori !
il Gran Visir , che sapeva ogni cosa , provò dispiacere e si presentò ; prese suo figlio tra le braccia : questo è mio figlio , non un bastardo ! disse .
afferrò il figlio del re per i capelli , gli diede uno strattone , lo gettò per terra e l' uccise ; poi prese il figlio e la moglie e scomparve con loro .
li portò nel suo palazzo e vissero felici e contenti .
disse alla moglie : hai visto cosa ti è capitato per non aver dato ascolto alle mie parole ?
poi vissero sempre insieme ...
e così finisce la storia .
