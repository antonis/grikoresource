un tale di Vernole si sposò .
qualche giorno dopo il matrimonio , s' alzò una mattina per andare a raccogliere della verdura selvatica .
stava giusto sulla soglia e la moglie lo chiamò : e io con chi rimango adesso che te ne vai ? chiese .
ma resta con chiunque sia ! rispose , e se ne andò .
lei si sedette davanti alla porta di casa e aspettò che passasse chiunque sia per invitarlo a star con lei .
passò un venditore di olio , un certo Arcangelo della Signorina , che gridava : olio , chi vuole olio !
lo fermò e gli domandò : tu sei chiunque sia ?
Sissignore , rispose , sono chiunque sia .
e allora , lei disse , vieni a stare con me ; così mi ha ordinato mio marito , di stare con chiunque sia .
Arcangelo entrò e s' accomodò in camera con lei .
dopo un po ' tornò il marito e li trovò insieme : Ah , è così , fai anche di queste cose ! gridò ; bene , restare pure lì ; ora v' accomodo io tutt' e due .
chiuse la porta , poi si rivolse all' uomo , ad Arcangelo , e chiese : cosa preferisci ?
che ti porti a Roca vecchia sulle spalle e ti abbandoni lì o che ti ammazzi ?
piuttosto che essere ammazzato , rispose Arcangelo , preferisco esser portato sulle spalle fino a Roca vecchia e tornarmene da solo .
l' uomo lasciò la moglie in casa , caricò Arcangelo sulle spalle e lo portò a Roca vecchia .
l' abbandonò lì e disse : tornatene pure da solo , se ce la fai , quando me ne sarò andato ! ti perderai certo per le macchie e non troverai più la strada .
egli invece conosceva la strada ; s' incamminò e se ne venne subito al paese .
l' altro intanto era tornato dalla moglie , era entrato in casa : cosa vuoi che ti faccia ? domandò : metterti spavento o ucciderti ?
la moglie preferì lo spavento .
la chiuse perciò in una stanza e la lasciò lì .
dopo un poco , andò zitto zitto dietro la porta e fece : Uuh ! pensando che la paura le sarebbe penetrata nelle ossa e piano piano l' avrebbe fatta morire .
invece non le provocò un bel nulla .
mentre s' affaccendava in tale impresa , udì messer Arcangelo che gridava per la strada : olio , chi vuole olio !
anch' egli , infatti , passo passo , era rientrato dal viaggio .
l' altro s' affacciò , lo vide e disse : gli caschi un fulmine , per poco non arriva prima di me !
e forse , aggiunse , neppure mia moglie s' è lasciata turbare dallo spavento che le ho messo .
sarebbe stato meglio ammazzarli subito , tutt' e due !
e ' vero , concluse , ci chiamano minchioni , noi di Vernole , e minchioni siamo per davvero .
e così è finita la storia .
