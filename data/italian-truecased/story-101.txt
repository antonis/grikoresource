c' era una volta un marchese che aveva un solo figlio . il padre non voleva far prendere al ragazzo il vizio delle donne , e gli diede un maestro che lo istruisse a stare alla larga dalle donne ; non lo faceva mai scendere dal palazzo ; sempre chiuso lì per non veder gente e guastarsi .
se ne stava così nascosto che neppure gli abitanti del paese lo conoscevano .
dopo averlo educato in questo modo , il figlio era ormai sui diciassette anni il padre si confidò con la moglie : forse l' educazione che stiamo dando a nostro figlio non ci è di gran vantaggio , dal momento che , siccome finiremo entrambi col morire , il ragazzo resterà solo , senza essersi sposato , ed ecco che i nostri averi finiranno in mano d' estranei ; dunque sarà meglio fargli conoscere il mondo e spingerlo a sistemarsi ; così resteranno ai figli .
furono d' accordo .
il ragazzo non aveva più bisogno del maestro , era istruito abbastanza .
decisero di farlo scendere ; però nessuno era in grado di persuaderlo ad andare tra la gente ; si vergognava , non era abituato .
sicché il padre si mise a dirgli : figlio mio , non ti conviene star qui chiuso , devi avere relazioni con la gente .
il figlio gli rispose : che relazioni devo avere con la gente ? non capiva cosa il padre volesse dire .
e il padre : vorrei che ti sistemassi , che avessi rapporti con le donne ; non ti conviene restar così .
il figlio rispose di nuovo : da quando ero piccolo mi hai insegnato a non star dietro alle donne ; dunque io seguo i tuoi consigli .
d' accordo , disse il padre , l' educazione che ti ho dato va bene ; però adesso , se moriamo , la proprietà resterà ad estranei .
noi vogliamo che tu abbia una sistemazione .
allora il padre , visto che il figlio non si convinceva , incaricò un suo nipote , che era allegro e aveva amicizia con tutte le ragazze , affinché l' addestrasse lui , gli fornisse qualche occasione .
cominciò a raccomandargli che lo portasse in giro : fa ' di tutto , diceva , per invogliarlo .
e ' sufficiente che gli piaccia una ragazza , purché garbata ; non importa se non ha alcuna ricchezza , ne abbiamo noi per lei .
insomma , voleva mettere alla prova le intenzioni del figlio .
il nipote rispose : zio , non darti pensiero ; non verrà con me ?
gli farò venire io il gusto per le donne , non preoccuparti .
così questo nipote salì sul palazzo converrà ora dare un nome al figlio ; chiamiamolo Camillo e andò da Camillo .
gli disse : cugino , facciamo due passi di là ; ci sono delle belle ragazze e voglio fartele vedere ; vedrai poi come ti piacerà stare con le donne : è uno spasso !
e cominciò a raccontar chiacchiere , per convincerlo ad andare .
allora l' altro rispose : caro cugino , a me i tuoi discorsi non interessano ; perché mi stai a parlar di donne ?
il cugino , nel vederlo così indifferente , s' impegnò a far altri discorsi , ma senza convincerlo ; scese perciò dal padre e glielo riferì .
senza portarla per le lunghe , la prima volta non lo persuase ; la seconda volta , però , che era stato ancor di più sollecitato dal padre , riuscì a portarlo in giro per la città .
cominciarono a passeggiare , e gli diceva : guarda com' è bella quella ragazza , e com' è simpatica quell' altra !
non ti piace nessuna ?
no , l' altro rispondeva , non mi piace nessuna .
devi vedere com' è bello scherzare con le ragazze ! riprendeva , e giù a dar lezioni .
insomma , la prima volta che uscirono , non riuscì a fargli piacere nessuna .
Ritornarono e il padre , il marchese , corse a informarsi : caro zio , rispose il nipote , per il momento non gli è piaciuta nessuna ragazza .
appena gli propongo qualcosa , mi dice : io devo seguire gli ordini del mio maestro .
il padre restò deluso dalla notizia : non preoccuparti , continuò il nipote ; io farò il possibile ; domani lo porterò di nuovo a passeggiare ; gli mostrerò due nuove strade ; non preoccuparti .
arrivò il giorno successivo e lo portò per due nuove strade ; la gente si voltava a guardarlo ; non lo aveva mai visto , e lo seguiva .
tutte le ragazze , dai balconi , sentivano la gente e si sporgevano ; erano tutte ben vestite , agghindate ; lo guardavano e il cugino lo presentava .
tra tante ragazze , finalmente s' invaghì di una ; gli piacque molto .
il cugino gli chiese : non ti è piaciuta nessuna di queste ragazze ?
ne abbiamo viste tante ! egli rispose : vedi quelle tre sul balcone ?
la ragazza che sta in mezzo mi è piaciuta molto .
erano tre sorelle . quanto a lei , ti vorrà sicuramente , disse l' altro ; forse , in confronto a te , è più bella ; ma ricchezze non ne ha .
tuo padre mi ha detto che è sufficiente che ti piaccia , la ricchezza non importa ; se ti piace , possiedi abbastanza anche per lei .
tornarono a casa dal padre di Camillo : finalmente ci siamo , disse il nipote : a tuo figlio piace una ragazza . il padre si rallegrò .
allora il cugino e Camillo si recarono dalla fidanzata ; le ragazze lo vedevano dal balcone e non riuscivano a crederci ; sicché , non appena il figlio del marchese salì sul loro palazzo , gioirono e si misero a far grandi accoglienze .
il cugino chiamò in disparte la preferita e la informò : siamo venuti qui per sapere se tu vuoi far l' amore con questo giovane , disse .
non potrei cercar di meglio , lei rispose ; fu d' accordo all' istante .
Combinarono il fidanzamento ed eran tutti contenti .
quando il giovane andava a far visita , veniva sempre col cugino ; poi prese confidenza e ci andava da solo .
quand' era col cugino , diceva qualche parola ; quando andava solo , non parlava affatto : aveva soggezione , si vergognava .
la cosa però non piaceva alla fidanzata , abituata con un altro che le ripeteva di continuo : io muoio per te , non ho più pace , e cose del genere .
allora lei cominciò a dire che era uno sciocco , che non sapeva stare con la gente ; fece venire il cugino e disse : dì a tuo cugino di non passar più da me ; è uno stupido e non lo voglio .
sì , sì , hai ragione , rispose il cugino , anche a me è parso un po ' stupido . insomma , i due se la intendevano .
in conclusione , lo mandò via , non lo volle più , screditandolo davanti a tutta la città .
il padre si struggeva , perché il suo unico figlio era stupido ; così il suo ammaestramento , a non star dietro alle donne , si rivelò controproducente e lo deluse .
il povero giovane , intanto , nel vedersi cacciato di casa , e per giunta da una inferiore a lui , restò amareggiato .
si presentò allora dal padre e gli disse : papà , dammi la santa benedizione , perché devo andare in giro per il mondo .
il padre non voleva lasciarlo , ma egli non l' ascoltò e partì : o mi lasci o no , io me ne vado .
se ne andò .
si sistemò a Venezia e diventò orefice ; comprava oro e lo rivendeva , insomma .
poi , dopo parecchio tempo , si fece crescere la barba per non essere riconosciuto , prese con sé un servo e tornò dalla vecchia fidanzata .
vendeva gioielli , e lei s' affacciò e lo chiamò ; disse che voleva un anello ed egli cominciò a mostrarglieli .
c' era un anello che le piacque molto ; costava cento ducati e lei volle provarlo : te lo regalo , egli disse .
perché ?
non sei in obbligo .
se non ho obbligo , ho devozione .
allora gli rivolsero dei complimenti , poi si salutarono ed egli partì .
quando tornerai ? gli chiesero .
tra un mese , rispose , e se ne andò .
si procurò allora un anello di duecento ducati e venne un' altra volta .
la ragazza , per via del regalo ricevuto , quando lo rivide , lo trattò con familiarità e lo invitò a mangiare .
egli le mostrò l' anello di duecento ducati e glielo regalò ; lei voleva pagarlo , ma il giovane non accettò .
per non stare a ripetere lo stesso discorso , andò quattro volte e le regalò altrettanti anelli : uno di cento ducati , l' altro di duecento , l' altro di trecento , e l' ultimo di quattrocento .
l' ultima volta , mangiarono insieme lei aveva preso ad amarlo e possiamo dire che andarono pure a letto .
allora lei si mise a dire : dimmi quando tornerai ; io non posso vivere senza di te .
tornerò quando tu lo vorrai ; neppure io so vivere senza di te .
si salutarono e s' accordarono : tornerò tra otto giorni .
ma non tornò più .
lei ogni giorno s' affacciava al balcone , ma non lo vedeva mai e piangeva .
passarono tre mesi e , visto che non tornava , cominciò a dire che voleva sposarsi col suo primo fidanzato , quello che aveva lasciato per Camillo .
nel frattempo Camillo era tornato dalla sua famiglia .
ora , venivano distribuiti i biglietti d' invito per il matrimonio , e l' invito fu portato anche a lui ; era il capo del paese e non potevano non invitarlo .
la cosa anzi destava piacere : Beh , ci prenderemo gioco di lui , dicevano , ritenendolo stupido .
giunse l' ora e il padre e il figlio si presentarono al banchetto che stava per cominciare ; quello sempre zitto , non parlava mai .
gli dicevano : don Camillo , hai portato qualche bella novità da lontano ?
lo deridevano , ed egli si mostrava indifferente .
mangiarono , ballarono , poi cominciarono a dire indovinelli .
gli facevano : tu non dici niente ?
che cosa devo dire ?
non so .
tutti lo canzonavano , ed egli se ne stava impassibile ; anche il cugino lo scherniva .
il padre si addolorava nel vedere che il figlio veniva deriso e non sapeva reagire .
a un tratto egli disse : un momento , devo dirvi anch' io un indovinello ; però nessuno si deve offendere . prese in mano un revolver e s' alzò in piedi : nessuno si dovrà offendere per quel che ho da dire ; se qualcuno si offenderà , gli sparerò addosso .
e cominciò : io sono andato a fare un viaggio a Roma , come voi ben sapete ; vi racconterò cosa mi è capitato durante il viaggio .
lungo la strada , sono entrato in una campagna ed ho visto una quaglia ; mi sono tanto intestardito a volerla prendere che ho comprato un cane da cento ducati , una pistola e sono andato deciso verso la quaglia .
ho sparato , il cane s' è lanciato , ma la quaglia era scomparsa : non l' avevo colpita .
ancor più ostinato , ho comprato di nuovo un cane di duecento ducati e ho fatto la stessa cosa ; ho sparato , ma la quaglia è volata via e il cane è scomparso .
per quattro volte la stessa storia , sempre più testardo .
la quaglia si perdeva sempre .
alla fine , quando il cane era costato quattrocento ducati , l' ho colpita , la quaglia .
se non credete che l' ho colpita , chiedetelo a lei , che ne sa qualcosa . allora mise fuori il fazzoletto e glielo lanciò in faccia .
la gente restò di stucco : dunque , così va la faccenda !
chi se la vuol prendere , ora , la prenda !
e a lui tutti dicevano : bravo , bravo !
il padre si rallegrò .
andarono via .
poi si frapposero delle brave persone e cominciarono a dirgli : ora , cosa ci vuoi fare ? tu stesso ci sei stato !
alla fine la prese .
e vissero felici e contenti .
e chi non crede e vuol verificare sappia che stanno lì ancora ad aspettare .
