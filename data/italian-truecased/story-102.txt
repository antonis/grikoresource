c' erano una volta due bricconi ; s' incontrarono un giorno e uno chiese all' altro : dove vai , compare ?
vado alla fiera , compare , a comprare un cavalluccio .
anch' io ho la stessa intenzione , disse l' altro .
dopo un po ' si separarono : Statti bene , compare !
Statti bene .
quando ci rivedremo ?
che ne so ?
può darsi che ci rivediamo , può darsi che non ci rivediamo ; come capita .
dopo diverso tempo , circa un mese , si incontrarono di nuovo : Oh , compare , da quanto non ti vedevo !
anch' io è da tanto che non vedevo te .
hai fatto buoni affari alla fiera ?
li ho fatti .
che cosa hai comprato ?
un cavallo .
e tu cosa hai comprato ?
un asino .
vuoi che ce li scambiamo ?
d' accordo .
tu cos' hai nella bisaccia ?
cuoio . e tu ?
seta .
Scambiamoceli , dunque , disse uno di loro .
Scambiamoceli .
uno diede il cavallo , e l' altro diede l' asino : se li scambiarono .
poi il primo s' avviò da un lato ; il secondo dall' altro .
quando s' erano allontanati , uno di loro disse tra sé : " voglio vedere che cosa c' è nella bisaccia " .
guardò e ci trovò alghe : " Ah , pensò , io gli ho detto che avevo cuoio , e anche lui avrà la sorpresa di trovare foglie di cavolo " .
lasciamo questo e andiamo all' altro : " voglio vedere che cosa c' è in queste bisacce " , disse tra sé , poiché avevano scambiato bisacce e tutto il resto .
guardò dentro e trovò foglie di cavolo : " Beh , pensò , io gli ho dato a intendere di avere seta , e lui di avere cuoio : non è andata bene a nessuno dei due " .
ora i due , dopo aver fatto lo scambio , s' erano diretti uno da una parte , uno dall' altra .
dopo molto tempo s' incontrarono di nuovo ; si videro e uno disse : Ah , compare , tu mi hai imbrogliato , ma io l' ho fatto prima di te .
perché , compare , dici questo ?
perché ?
tu mi hai detto di portare cuoio e io di avere seta ; invece c' erano alghe .
e tu cosa avresti voluto ? tu mi hai ingannato , ma neppure io ti ho detto la verità .
gli animali , intanto , se li erano venduti : uno aveva venduto l' asino e l' altro il cavallo .
Beh , disse il primo , tu l' asino non te lo sei venduto ?
e io mi son venduto il cavallo .
ora andiamocene verso Napoli .
andiamo , ma dove ?
verso Napoli .
e a far cosa ? andiamo a rubare .
si misero in viaggio e andarono .
arrivarono al conio , dove venivano fabbricate le monete del re : compare , disse uno , che cosa facciamo qui ? ce ne stiamo così senza far niente .
non sarebbe bene che entrassi dentro e prendessi un po ' di soldi ? entraci tu , che sei esperto del luogo , rispose l' altro , che era leccese ; io non lo conosco .
invece sarebbe meglio che entrassi tu .
non far entrare me ; ci resterei dentro , se entrassi .
per che cosa hai paura , chiese il napoletano al leccese , per tua moglie ?
non temere , penserò io a tua moglie .
poniamo che rimanga dentro : mi prenderanno , e mia moglie resterà sola ; cosa potrò fare ?
ti dico di non pensarci ; le darò da mangiare e da bere , la manterrò io . entra e non preoccuparti .
lo convinse e quello entrò .
entrò e si caricò di monete , ne prese a volontà .
dopo essersi caricato , disse : tirami fuori , compare , è sufficiente .
lo tirò su , andarono a casa e la notte stessa svuotarono il bottino .
ora la moglie , quando tornarono a casa , si rallegrò nel vedere tutto quel denaro .
prepararono allora una buona tavola , mangiarono , bevvero e andarono a dormire .
la notte seguente fecero di nuovo la stessa cosa .
intanto , la mattina , le guardie avevano osservato tutt' intorno e s' erano accorte che mancava del denaro .
misero allora della colla nella fossa dove c' erano le monete : chi ha rubato tornerà un' altra volta , dissero i soldati .
e infatti arrivò la notte e i due andarono di nuovo .
il leccese disse al napoletano : questa sera entraci tu , quasi avesse il presentimento .
Oh , che difficoltà c' è ?
entra e non pensarci .
entrò .
non appena scese , cominciò : compare , tirami , mi sono attaccato !
attacca oro , argento e rame !
mi sono attaccato !
e perché ti sei attaccato ?
c' è colla e sono rimasto incollato dentro , spiegò infine .
Beh , allora ti getto la corda ; legati e ti tirerò su .
calò la corda , lo legò e cominciò a tirare : compare , fatti leggero , non riesco a tirarti .
non mi tirerai più , sono incollato !
allora , sai cosa bisogna fare ? disse quello che stava sopra : dal momento che sei incollato , non c' è via d' uscita .
ti getterò una sciabola ; tu tagliati la testa . prima di tagliarla , legala alla corda e io la tirerò fuori .
così lasceremo dentro il corpo , io porterò via la testa e non ti riconosceranno .
l' altro così fece .
e così tirò fuori la testa da lì ; l' altro se l' era tagliata per non farsi riconoscere e il corpo era rimasto dentro .
poi , senza soldi , andò via ; se la squagliò di là .
la mattina venne il padrone delle monete , colui che aveva il conio .
s' accorse e andò a chiamare i soldati .
i soldati si precipitarono : Oh , che stranezza è questa ? dentro ci sta uno senza testa e vicino non c' è nessuno .
cominciò ad accorrere molta gente : venite a vedere lo spettacolo !
che cosa c' è ?
guardate dentro ; c' è uno senza testa e non si capisce chi è .
fate una cosa ; tiratelo fuori e forse lo riconosceremo .
lo tirarono fuori , cominciarono a osservarlo , ma non lo riconobbero .
nessuno riuscì a identificarlo , perché gli mancava la testa .
allora , dopo averlo tolto da lì , cosa escogitarono ?
lo mettiamo in una bara e lo lasciamo in piazza .
se in qualche casa si sentirà piangere una donna , significherà che si tratta di suo marito .
così fecero : lo sistemarono su di una bara e le guardie si misero a girare per vedere in quale casa si piangeva .
lasciamo intanto questi e torniamo al ladro .
egli non aveva ancora detto niente alla moglie dell' amico , l' aveva ingannata .
ma , quando vide quel che accadeva , che l' avevano messo nella bara , dovette svelarle tutto ; corse a casa e le disse : attenta , non piangere , ché a tuo marito è capitato questo e quest' altro .
le guardie erano in giro .
la donna , appena udì la storia , figuratevi un po ' ! cominciò a piangere , a gridare e dimenarsi .
quando le guardie stavano per arrivare lì vicino , quello le fece : zitta , zitta , o siamo morti !
ebbe allora un' idea : afferrò un orciuolo d' olio ch' era sulla mensola del camino e patapùffete ! lo gettò per terra .
lei non badava all' olio che era caduto , pensava al marito , alla storia ch' egli le aveva raccontato .
ed ecco arrivare le guardie nelle vicinanze della casa ; origliarono : e ' qui , dissero .
entrarono : che cosa è successo , signora ?
Furbescamente l' uomo si precipitò e rispose : ecco , è salito il gatto e le ha gettato l' olio dal camino , e lei sta piangendo .
e piange con tanto strazio ? disse la guardia .
io le dico di non piangere , che le porterò l' altro .
le guardie , astutamente , se ne andarono , uscirono fuori e misero un segno sulla porta .
ma quello , il ladro , se ne accorse e subito si mise a mettere segni sulle porte ; segnò tutta la strada , in modo che non potessero più raccapezzarsi .
la mattina dopo , infatti , le guardie cominciarono a cercare : qui un segno , qui un altro segno : com' è possibile ?
così non riusciremo a sapere dov' era la donna che piangeva e che avevamo sospettato !
cosa escogitarono allora le guardie ?
lo mettiamo nella bara , dissero , lo portiamo la notte in chiesa e facciamo spiare ; chi andrà a piangere dietro la porta sarà l' indiziata .
egli andò e le disse : attenta , l' hanno portato in chiesa ; non farti sentire .
ma la donna , appena lo seppe , si mise a far baccano ; voleva andare a vederlo , voleva andarci assolutamente .
zitta , non pensarci neppure ! ti darò io da mangiare e da bere , avrai tutto da me .
arrivarono le due e lei voleva uscire per andare in chiesa .
l' uomo non glielo permise : se ti muovi , ti romperò le ossa !
ma , dimmi , cosa ti manca ?
qualunque cosa tu voglia , fammelo sapere .
vuoi soldi ? te li do .
eccoti i soldi !
la portò al mucchio di monete : no , non voglio soldi , voglio mio marito ! non riusciva a convincerla : dunque , cosa vuoi ?
voglio andare a vederlo .
tu non puoi andare da nessuna parte , altrimenti finiremo in galera , hai capito ?
lei cercò di scappare e l' uomo la bastonò .
ricominciò a piangere .
la gente stava ad ascoltare : che cosa fa costei ?
alla fine la persuase e stette zitta .
torniamo ora un' altra volta dal morto .
alle guardie venne un' idea : possibile , dicevano , che non si riesca a trovare chi è l' autore del furto di questa notte ?
mettiamo il cadavere nella bara , pensarono ; la donna che andrà a prenderlo da lì è la moglie ; è lei a provare dolore .
il ladro , con la sua abilità , andò e lo prese .
lo portò via e lo seppellì in una fossa .
le guardie , appena si svegliarono la mattina , si accorsero che il cadavere non c' era ; o che si fossero addormentate , o chissà come , l' avevano trafugato : Ahimé , non c' è più il cadavere ! dissero , quando la mattina andarono a vedere .
cominciarono a cercarlo , dopo essersene accorti : com' è possibile ?
dove l' avranno portato , questo morto ?
gira e rigira , avevano un cane con loro il cane annusò dov' era e si mise o scavare .
le guardie si avvicinarono e intanto il cane aveva scavato abbastanza da scoprire un piede .
Sparsero la voce e venne molta gente ; lo tirarono fuori .
ora , per il ladro , cominciava a mettersi male la faccenda ; seppe che il cadavere era stato ritrovato : " Oh , povero me , pensò , adesso mi troveranno ; lei continua a piangere ! " saputo del ritrovamento , egli andò a guardare insieme con gli altri .
cosa pensarono , di nuovo , le guardie ?
pensarono di prendere un' altra volta il cadavere dalla fossa e rimetterlo in piazza : chi gli fosse passato vicino alle due di notte sarebbe stato considerato responsabile dell' omicidio .
così fecero ; lo lasciarono in piazza e la gente si ritirò tutta ; ognuno andò a chiudersi in casa sua .
le guardie stavano con gli occhi aperti a vedere se passava qualcuno .
il ladro cosa fece ?
quando furono le due di notte e non c' era più anima viva , si finse viandante e andò . nel frattempo s' era procurato sette pagnotte , un fiasco di vino e aveva lasciato tutto a casa .
poi era andato in un convento di monaci e aveva detto : fatemi un favore , datemi sette tonache in prestito .
non possiamo dartele .
ma io ve le riporterò . Dacci una lira per tonaca : così te le diamo , altrimenti non te le diamo .
volete che vi dia sette lire ?
ve le do .
mise fuori il denaro , diede loro le sette lire , prese le sette tonache e andò via .
prese allora il fiasco , le pagnotte e mise tutto sotto il cappotto .
venne dritto dalle guardie , entrò giù e disse : sono un viandante ; fatemi un favore , fatemi dormire qui per questa notte .
poi aggiunse : ho con me del pane e un fiasco di vino .
volete mangiare un po ' ?
noi abbiamo già mangiato .
Prendetene un pochino , un assaggio almeno .
cominciarono col dar di morso al pane .
Beh , ora bevete un bicchiere di vino , disse il viandante : beviamo e ci mettiamo a dormire .
di tanto in tanto il capo delle guardie s' affacciava a controllare che non andassero a prendere e portar via il cadavere .
il ladro dentro di sé se la rideva : " mangiate , presto , e bevete ; non pensate a lui ! " Bevendo e mangiando dentro c' era il sonnifero tutti e sette caddero di colpo addormentati .
il ladro tirò fuori le tonache e ne mise una ciascuno a tutti loro , che già russavano .
li lasciò dormire , si caricò il cadavere e scappò ; lo portò lontano dal paese .
poi tornò e cercò di sapere cosa si dicesse .
la mattina , intanto , le guardie si svegliarono : guarda , guarda , disse il primo che si svegliò , son vestito da monaco !
andò a trovare gli altri ed eran tutti vestiti da monaci : Svegliatevi , disse , siamo tutti vestiti da monaci !
si svegliarono , si videro così conciati , uscirono da lì e si precipitarono fuori ; guardarono e il morto non c' era più : adesso che cosa dobbiamo fare ?
cosa diremo quando andremo dal re ?
che siamo vestiti da monaci e ci hanno preso il morto ?
quando arrivarono dal re , questi , nel vederli , cominciò a ridere : che cosa avete combinato ? domandò .
anche la gente , prima , li guardava da dietro e rideva ; i poveretti , con la faccia nascosta , s' erano messi a correre .
disse dunque il re : insomma , non sapete chi è stato a prendere il cadavere ?
e ' passato un tale così e così , spiegarono , che aveva un fiasco di vino ... e raccontarono ogni cosa .
il re se la rideva .
se ne andarono .
pensarono di andare in giro un' altra volta per trovare il cadavere .
la gente e le guardie cercavano di qua e di là e alla fine lo trovarono dietro un muretto : pensate ormai come puzzava !
eccolo dov' è ! dissero .
correte , disse una guardia , cercate una cassa e portatela .
corsero a prendere una cassa e lo misero dentro .
lo presero e lo portarono dal re : lo avete trovato ?
lo abbiamo trovato .
dove stava ? dietro un muro .
ora sapete cosa dovete fare ?
invece di essere in sette a far la guardia , dovete essere in quattordici ; e il cadavere portatelo in piazza .
lo portarono in piazza e lo lasciarono .
il re ordinò che nessuno si addormentasse .
ed essi cosa fecero ?
stavano continuamente tutti e quattordici di guardia .
Lasciamoli far la guardia e torniamo ora dal ladro .
cosa escogitò ?
egli aveva visto che erano in tanti ed era andato in una masseria : massaro , disse , devi farmi un favore ; devi darmi per questa sera sedici caproni , e io ti pagherò l' affitto .
ecco , ti do sedici ducati per una sera .
attento , che non abbian danno , disse l' altro : devo dar conto al padrone .
non preoccuparti ; dovesse succedere loro qualcosa , io ne sono responsabile e te li pagherò .
così dici ?
prendili , dunque .
prese i caproni . intanto era andato da frate Vincenzo e s' era fatto dare trentadue candele .
prese le candele e le legò , una per una , alle corna dei caproni .
mise dunque due candele sulle corna di ogni caprone , mandò avanti gli animali ed egli andò dietro . la guardia che era più vicina , e poi tutte le altre , a veder da lontano tante luci , il ladro non si vedeva , perché stava dietro , confuso tra i caproni si misero a dire : poveri noi !
stanno venendo a prendere l' anima del morto che sta in piazza ; scappiamo , questi sono i diavoli , scappiamo !
se la diedero a gambe e lasciarono il morto da solo .
quando le guardie s' allontanarono , egli prese il morto , se lo caricò , fece andare avanti i caproni e se la svignò .
arrivato a un punto dove sapeva non l' avrebbero più trovato , gettò il cadavere ; poi si recò dal massaro , gli restituì i caproni , lo pagò e se ne andò .
tornò a casa e se ne stette lì tranquillo , senza pensare più a niente .
torniamo da quelli che erano scappati ed erano corsi dal re morti di paura .
il re li vide così sconvolti e domandò : cosa è successo ?
cosa è successo ? sono venuti tanti diavoli a prendere il morto che stava in piazza .
noi , maestà , a tale vista , siamo scappati via e l' abbiamo lasciato .
era spaventoso vedere tanti diavoli ! ripetevano al re .
Oh , chissà chi è ad avere tanta destrezza ! egli disse : lasciate perdere , non andate più da nessuna parte .
vedremo chi è ad aver avuto tanto coraggio da prendere il morto , portarlo in giro , gettarlo .
le guardie , a queste parole , dissero : noi non andiamo più a far la guardia ; ci vada chi vuole !
no , no , tornate pure alle vostre faccende , non andrete più da nessuna parte .
fate il vostro lavoro e acqua in bocca !
in seguito il re fece bandire per tutto il regno d' aver organizzato un grande banchetto e invitava gente d' ogni città e d' ogni paese .
da quella strada dove era stato messo il segno sulla porta fu invitata molta gente .
il messo che faceva gli inviti venne anche dal ladro e disse : tu vuoi venire al banchetto che si sta preparando ?
poi , dopo aver invitato parecchi , tornò al palazzo del re e disse : ne ho invitati tanti .
bene , rispose il re , bastano .
ora , tra tutti gli invitati , doveva esserci anche il ladro .
questi allora andò dalla comare e chiese : che ne dici , vado anch' io ? dove ? lei disse .
al banchetto che il re ha organizzato ; ci ha invitati in moltissimi per andare a mangiare .
e io che ne so ?
vado o non vado ?
non ci andare ; chissà cosa succederà !
Beh , o mi uccideranno o resterò libero .
egli era scapolo ; l' altro , cui aveva tagliato la testa , invece era sposato .
andò dunque .
quando entrarono tutti sul palazzo c' era tantissima gente , figuratevi un po ' ! si disposero in una lunga fila , seduti , come quando si deve svolgere un processo .
cominciarono a esser serviti tanti piatti sulle tavole imbandite .
al poveretto bruciava il sedere , perché non sapeva come sarebbe andata a finire : " Ahimé , cosa ho fatto a venir qui ! " diceva tra sé .
finito di mangiare , immaginate quante portate , d' ogni qualità ! per non tirarla ancora per le lunghe , si presentò il re .
si sedette sul trono , con una corona in mano , e cominciò : voi dovete dirmi , parlando in tutta libertà , chi di voi ha avuto il coraggio e l' abilità di andare prima di tutto là dove si coniano le monete ...
ed espose ogni cosa , punto per punto .
il ladro diceva dentro di sé : " lo dico o non lo dico ? " e sentiva il cuore battergli forte e le gambe tremargli dentro i calzoni .
Beh , disse infine il re , alzandosi e tenendo la corona in mano : voi avete paura di confessare , ma io vi prometto ... al ladro il cuore ricominciò a saltare vi prometto la corona .
il poveretto sgranò gli occhi ; non sapeva cosa fare ; cento pensieri gli si affollavano nella mente , non riusciva a decidere .
il re aveva a fianco una ragazza , l' aveva portata con sé ; ricominciò : prometto la corona e mia figlia a colui che ha avuto tanta destrezza .
a sentir ciò egli s' alzò in piedi : sono stato io ! disse .
gli era uscito il fiato , finalmente . se sei stato tu , disse il re , eccoti allora la corona e anche mia figlia : ti sia concessa .
restò delusa , la comare !
si sposarono , dunque ; fecero tanti festeggiamenti , e vissero felici e contenti .
chi non crede e vuol verificare vada a Napoli e li potrà trovare .
il racconto è terminato e un tornese ho meritato .
