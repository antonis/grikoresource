una volta la madre di san Pietro , donna molto avida , si mise in cammino .
cammina cammina , raggiunse un grande paese ; si presentò ad una bottega e chiese la carità : vattene via , disse il padrone , non posso fare elemosina .
lei si voltò e andò via .
si presentò di nuovo ad un' altra persona : fammi la carità , disse .
io ti conosco , tu sei una donna cattiva , fu la risposta .
sono stata sempre buona .
Beh , dicono che sei la madre di san Pietro e non hai fatto mai del bene .
quando mi è stato possibile , del bene l' ho fatto .
vattene , vattene ; sei la madre di san Pietro , e questo basta .
se ne andò di là , ma non riceveva mai nulla .
cambiò allora paese e si rivolse ad una povera donna : dammi qualcosa da mangiare .
io , rispose la donna , non ho nulla ; ho preparato un po ' di minestra e sto apettando che si raffreddi ; quando comincio a mangiare , te ne faccio parte .
si versò da mangiare e le restò un mestolo : eccoti , disse , mangia .
mi hai dato molto poco , fece lei ; io , per la fame , non riesco neppure a vederti .
tanto ho potuto , e tanto ti ho dato , disse la poveretta .
ma io non ci vedo per la fame !
vai più avanti , rispose allora la donna , forse potranno darti di più .
ripartì e si mise a cercare , ma nessuno le dava niente .
s' avvicinò ad una casa dove stavano cucinando minestra con la carne : fammi la carità , chiese , per la buon' anima di tuo padre .
come fai a sapere che mio padre è morto ?
lo so , rispose .
le diede un bel piatto di minestra : puoi mangiartela tutta .
se la mangiò tutta .
vuoi un bicchiere di vino ?
fa ' come vuoi .
le offrì un bel bicchiere di vino .
lo bevve .
sei soddisfatta ?
soddisfatta , ma se me ne portassi un altro , pure lo berrei .
vuoi del brodo di carne da sorseggiare ?
e ' tutto grasso .
certo , disse .
lo bevve tutto .
uscì per strada e s' incamminò .
la minestra e il brodo cominciarono a scombussolarle l' intestino .
arrivò fuori dal paese e piano piano s' allontanò .
aveva percorso appena un breve tratto e le venne il mal di pancia : " Ahimé , pensò , che mal di pancia ! " non riuscì a trattenersi e se la fece addosso , riempiendosi il vestito .
riprese a camminare , ma , come si muoveva , le si appiccicava il vestito alla schiena : " Diamine !
ora mi si incolla il vestito al sedere " .
si spogliò e vide che gli abiti erano tutti insozzati .
cercò una pozzanghera piena d' acqua , si ficcò dentro e si pulì ; lavò anche i vestiti , li lasciò asciugare , li indossò e si rimise in viaggio .
cammina cammina , si diresse verso Roma : " sarà meglio che vada lì , pensò ; ci sta mio figlio , Pietro " .
arrivò a Roma , entrò nella chiesa e lì morì .
il Signore mandò la sua anima nell' inferno .
qualche tempo dopo , morì anche Pietro e andò in paradiso ; si ricordò della madre e disse al Signore : non ho notizie di mia madre .
tua madre , Pietro , rispose il Signore , sta nell' inferno .
Oh , Signore , anche mia madre dovevi mandare nell' inferno ! non si potrebbe portarla via di là ?
va ' , Pietro , e chiedile se nella sua vita ha mai fatto un' azione buona .
Pietro andò : mamma !
figlio mio !
hai fatto mai del bene in vita tua ? soltanto una volta .
e che cosa hai fatto ? una volta ho dato una spoglia di cipolla .
Pietro tornò dal Signore e riferì che sua madre una volta aveva dato la spoglia di una cipolla : ecco , disse allora il Signore , dov' è appesa la spoglia che ha dato tua madre .
prendila , dille di afferrarsi e tirala su .
Pietro si recò di nuovo da lei : Aggrappati alla spoglia della cipolla .
lei si aggrappò con tutt' e due le mani .
le altre anime s' attaccarono anch' esse tutt' intorno : via , via , lei disse , è me che sta tirando mio figlio .
diede uno strappo per farsi tirar fuori : andate via ! ripeté alle anime che le si erano aggrappate .
nel dir questo , lasciò la spoglia e cadde ancora più giù .
Pietro tornò dal Maestro : maestro , disse , ha lasciato la spoglia ed è finita ancor più giù nell' inferno .
or , se non credi che il fatto stia così , va ' a vedere : lei sta ancora lì .
