c' erano una volta un padre e una madre che avevano tre figli .
erano molto poveri ; il padre era invalido , era sempre malato , e non avevano niente da mangiare .
quando i figli diventarono grandicelli , il più grande disse : padre , dammi la santa benedizione ; io voglio andar via e cercare un lavoro ; farò il garzone da qualche parte , guadagnerò qualcosa e lo porterò ; altrimenti , qui , come finiremo ?
Moriremo tutti di fame .
Ah , figliolo , risposero il padre e la madre , ora ci lasci ? e se ti succederà una disgrazia ?
gli volevano bene , e la madre si mise a piangere e a dire : resta qui , non andare ; possibile che il Signore non provveda a noi ?
ma egli non volle sentir ragione : vado , disse , datemi la santa benedizione .
e se ne andò .
dopo aver camminato parecchio , incontrò una signora vestita di nero che gli chiese : dove vai , ragazzo mio , tutto solo ?
vado a cercar lavoro come garzone , per guadagnare qualche soldo da portare a casa ; non abbiamo più niente e mio padre è malato .
vuoi venire con me allora ?
perché no ? vengo .
non ti spaventare , lei continuò , ho da chiederti solo un favore ; me lo farai ?
te lo farò .
sappi , però , lei disse , che devi fare esattamente tutto quel che ti dirò , altrimenti te ne pentirai .
farò come mi dirai .
vieni dunque .
lo portò a casa , gli diede una cavalla , gli consegnò una lettera e disse : prendi questa cavalla e va ' dove lei ti porterà ; tu non preoccuparti , lasciala camminare ; lei conosce la strada .
arriverai ad un grande lago ; tu non toccare la cavalla , non farla voltare , lasciala andare avanti ; con un salto essa l' attraverserà ; tu non aver paura .
dopo il lago troverai il palazzo di mia sorella e le darai la lettera .
mi porterai la risposta , ti pagherò e andrai per la tua strada .
d' accordo , egli rispose .
salì e partì : attento , gli ripeté lei da dietro , non tirare le briglie alla cavalla ; lasciala andare dove vuole . non la toccherò , egli rispose , e se ne andò .
cammina cammina , in mezzo alla macchia , tutto il tragitto era coperto dalla macchia , da non poterci passare finché non raggiunse il lago .
quando vide il lago , così grande , il giovane si spaventò : " dovrei lasciarle attraversare il lago ? pensò no , no , meglio andarmene via ; tornerò a riportare la lettera alla signora , e tanti saluti a lei e ai suoi soldi " .
fece voltare la cavalla e tornò .
Ah , non ci sei riuscito , lei disse , hai avuto paura ! non t' avevo detto di non toccare la cavalla , ché t' avrebbe portato lei ?
Beh , non fa niente ; scendi e va ' di là e gli mostrò una stanza : lì stanno i soldi , prendine quanti ne vuoi e vattene per la tua strada .
egli entrò , un po ' vergognoso , nella stanza ; si caricò di soldi a più non posso , riempì le tasche , il berretto , la camicia , le scarpe che s' era levato ; si caricò come un' ape e se ne andò .
lungo la strada pensò : " sarà meglio non tornare a casa ; andrò in un paese dove non mi conoscono e farò il signore " .
così fece , non si preoccupò più né del padre , né della madre , né di nessun altro ; andò a Maglie e costruì un bel palazzo ; comprò un bel cane e restò lì .
lasciamo questo e veniamo agli altri fratelli .
quando passò molto tempo ed egli non tornava , il mezzano disse al padre che voleva andare a trovare il fratello e guadagnar qualcosa per campare , ché stavano in miseria .
il padre e la madre ripresero a dire : cosa fai ? e ad affliggersi , per il timore di perdere l' altro ; poi , però , riflettendo sulle loro condizioni , diedero la benedizione anche a lui e lo lasciarono partire .
partì e , come suo fratello , incontrò allo stesso punto la signora che gli chiese dove stesse andando e gli propose d' andar come garzone da lei .
egli rispose di sì .
la signora ripeté a lui quel che aveva detto all' altro : voleva affidargli un solo incarico da portare a termine .
gli spiegò come comportarsi con la cavalla ; tutto allo stesso modo .
egli prese la cavalla e partì , tutto contento che si sarebbe subito sbrigato e avrebbe portato a casa qualcosa da mangiare ; non badava neppure che la cavalla passava tra rovi e querceti .
essa intanto procedeva per la sua strada come niente fosse , non provava alcun fastidio , nonostante la vegetazione d' infittisse sempre di più man mano che avanzavano .
arrivarono di nuovo al lago ; quando il giovane lo vide così grande , ebbe un sussulto ; restò un po ' soprappensiero , ma poi ebbe paura e tornò indietro anche lui .
la signora disse ciò che aveva detto all' altro , gli fece prendere denaro a volontà e lo fece andar via .
lungo la strada , a sentirsi padrone di tanto denaro , anch' egli non pensò più ai suoi , né alla miseria e alla fame che aveva lasciato a casa .
venne anche a lui il desiderio di viver da signore e si diresse a Maglie ; costruì un bel palazzo , prese un cane come aveva fatto il fratello e rimase lì .
torniamo un' altra volta a quelli che erano rimasti a casa e che ora stavano sulle spine ; i figli non tornavano ed erano arrivati proprio allo stremo : Beh , disse un giorno il più piccolo , i miei fratelli non sono più tornati ; chissà cosa gli è capitato !
ora , padre , devo andare io ; datemi la santa benedizione , e vedremo se Dio ci darà una mano .
il padre e la madre allora si misero a piangere : figliolo , ci lasci soli adesso ?
se ti capiterà qualche disgrazia ?
che aiuto avremo se resteremo senza più nessuno ?
e come faremo ?
volete che resti qui e che moriamo tutti di fame ?
vado a vedere se stavolta la fortuna ci aiuterà .
la madre e il padre non ne volevano sentir parlare ; poi , vedendo la sua determinazione , e considerando che ormai stavano per morire di fame , lo lasciarono andare , benedicendolo e pregando il Signore e la Madonna affinché non avesse a subire sventure .
partì e , dopo parecchio , incontrò la signora che gli domandò cosa facesse .
rispose , e allora lei gli propose di mettersi al suo servizio .
disse di sì .
lei spiegò ogni cosa come aveva fatto con gli altri : le serviva un giorno solamente , per compiere un viaggio ; poi lo avrebbe lasciato tornare a casa : d' accordo , signora , rispose il giovane , come tu desideri ; quel che mi dirai , io farò .
la donna lo portò a casa sua , gli diede la cavalla , gli disse come comportarsi con l' animale e lo mandò .
egli s' incamminò , attraversò la macchia , giunse al lago e non toccò la cavalla .
questa , con un gran salto , lo superò volando .
arrivò , dopo un po ' , al palazzo dove doveva consegnare la lettera ; vi trovò un' altra signora vestita di nero come la prima ; le diede la lettera e chiese se voleva dargli la risposta : no , lei disse , riferisci solo che non è giunta l' ora .
come tu comandi .
salì di nuovo sulla cavalla , si mise in strada e tornò .
sulla via del ritorno , il poveretto non riusciva a raccapezzarsi : era tutto cambiato ; non c' era più macchia , non c' erano querce né rovi , non c' era il lago ; nulla di tutto questo .
la cavalla andava per una strada bella , larga e pianeggiante ; da un lato e dall' altro c' erano rose e fiori bellissimi da vedere .
cammina cammina , in quel paradiso , era proprio un paradiso ! il giovane s' attardava ad ammirare tante bellezze ; era rimasto sconcertato .
era perplesso , non sapeva cosa pensare e si fermava a guardare da una parte e dall' altra .
cammina cammina , arrivò davanti ad un pero altissimo , carico di pere meravigliose ti veniva il desiderio solo a guardarle !
sotto l' albero c' era tanta gente che cercava di raccogliere le pere : chi spezzava con le mani quelle che riusciva a raggiungere , chi le colpiva con i bastoni , chi lanciava pietre per farle cadere ; però , tutte le pere che cadevano , non le toccavano . " guarda un po ' questi ! egli disse allora tra sé , non comprendendo il significato di ciò che facevano , gettare a terra le pere e non toccarle " .
andò più avanti e vide un altro pero ; al tronco erano legati due grossi cani e mordevano la catena per romperla e scappare .
il ragazzo rimaneva di stucco a osservare tutto ciò .
fece ancora parecchia strada e arrivò dalla signora .
lei gli venne incontro : ora va ' pure in quella stanza e prendi tutto il denaro che vuoi ; sta lì .
che risposta ti è stata data ?
ha detto che ti saluta e , quanto a ciò che hai scritto sulla lettera , non è giunta l' ora , egli rispose , poi aggiunse : padrona , io non voglio denaro ; mi bastano poche monete per comprare del pane e della pasta da mangiare con mio padre .
fa ' come credi , lei disse , e gli diede quattro monete , ma se vuoi denaro , vallo a prendere , sta lì ; per me è lo stesso .
no , mi bastano quattro monete .
Stammi bene .
Stammi bene , figliolo .
egli se ne andò .
tornò a casa , dal padre e dalla madre ; prepararono e mangiarono tutti contenti ; poco tempo dopo , tutt' a un tratto , morirono .
la Madonna era lei la signora della lettera e di tutto il resto aveva mandato due angeli a prendere le loro anime e portarle dritte in paradiso .
( il pero rappresentava il mondo e le pere gli uomini ; tutta la gente che era sotto erano i diavoli .
e i cani erano i fratelli , che avevano amato più i cani che loro padre ) .
