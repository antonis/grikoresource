C' erano una volta due bricconi ; s' incontrarono un giorno e uno chiese all' altro : Dove vai , compare ?
Vado alla fiera , compare , a comprare un cavalluccio .
Anch' io ho la stessa intenzione , disse l' altro .
Dopo un po ' si separarono : Statti bene , compare !
Statti bene .
Quando ci rivedremo ?
Che ne so ?
Può darsi che ci rivediamo , può darsi che non ci rivediamo ; come capita .
Dopo diverso tempo , circa un mese , si incontrarono di nuovo : Oh , compare , da quanto non ti vedevo !
Anch' io è da tanto che non vedevo te .
Hai fatto buoni affari alla fiera ?
Li ho fatti .
Che cosa hai comprato ?
Un cavallo .
E tu cosa hai comprato ?
Un asino .
Vuoi che ce li scambiamo ?
D' accordo .
Tu cos' hai nella bisaccia ?
Cuoio . E tu ?
Seta .
Scambiamoceli , dunque , disse uno di loro .
Scambiamoceli .
Uno diede il cavallo , e l' altro diede l' asino : se li scambiarono .
Poi il primo s' avviò da un lato ; il secondo dall' altro .
Quando s' erano allontanati , uno di loro disse tra sé : " Voglio vedere che cosa c' è nella bisaccia " .
Guardò e ci trovò alghe : " Ah , pensò , io gli ho detto che avevo cuoio , e anche lui avrà la sorpresa di trovare foglie di cavolo " .
Lasciamo questo e andiamo all' altro : " Voglio vedere che cosa c' è in queste bisacce " , disse tra sé , poiché avevano scambiato bisacce e tutto il resto .
Guardò dentro e trovò foglie di cavolo : " Beh , pensò , io gli ho dato a intendere di avere seta , e lui di avere cuoio : non è andata bene a nessuno dei due " .
Ora i due , dopo aver fatto lo scambio , s' erano diretti uno da una parte , uno dall' altra .
Dopo molto tempo s' incontrarono di nuovo ; si videro e uno disse : Ah , compare , tu mi hai imbrogliato , ma io l' ho fatto prima di te .
Perché , compare , dici questo ?
Perché ?
Tu mi hai detto di portare cuoio e io di avere seta ; invece c' erano alghe .
E tu cosa avresti voluto ? Tu mi hai ingannato , ma neppure io ti ho detto la verità .
Gli animali , intanto , se li erano venduti : uno aveva venduto l' asino e l' altro il cavallo .
Beh , disse il primo , tu l' asino non te lo sei venduto ?
E io mi son venduto il cavallo .
Ora andiamocene verso Napoli .
Andiamo , ma dove ?
Verso Napoli .
E a far cosa ? Andiamo a rubare .
Si misero in viaggio e andarono .
Arrivarono al conio , dove venivano fabbricate le monete del re : Compare , disse uno , che cosa facciamo qui ? Ce ne stiamo così senza far niente .
Non sarebbe bene che entrassi dentro e prendessi un po ' di soldi ? Entraci tu , che sei esperto del luogo , rispose l' altro , che era leccese ; io non lo conosco .
Invece sarebbe meglio che entrassi tu .
Non far entrare me ; ci resterei dentro , se entrassi .
Per che cosa hai paura , chiese il napoletano al leccese , per tua moglie ?
Non temere , penserò io a tua moglie .
Poniamo che rimanga dentro : mi prenderanno , e mia moglie resterà sola ; cosa potrò fare ?
Ti dico di non pensarci ; le darò da mangiare e da bere , la manterrò io . Entra e non preoccuparti .
Lo convinse e quello entrò .
Entrò e si caricò di monete , ne prese a volontà .
Dopo essersi caricato , disse : Tirami fuori , compare , è sufficiente .
Lo tirò su , andarono a casa e la notte stessa svuotarono il bottino .
Ora la moglie , quando tornarono a casa , si rallegrò nel vedere tutto quel denaro .
Prepararono allora una buona tavola , mangiarono , bevvero e andarono a dormire .
La notte seguente fecero di nuovo la stessa cosa .
Intanto , la mattina , le guardie avevano osservato tutt' intorno e s' erano accorte che mancava del denaro .
Misero allora della colla nella fossa dove c' erano le monete : Chi ha rubato tornerà un' altra volta , dissero i soldati .
E infatti arrivò la notte e i due andarono di nuovo .
Il leccese disse al napoletano : Questa sera entraci tu , quasi avesse il presentimento .
Oh , che difficoltà c' è ?
Entra e non pensarci .
Entrò .
Non appena scese , cominciò : Compare , tirami , mi sono attaccato !
Attacca oro , argento e rame !
Mi sono attaccato !
E perché ti sei attaccato ?
C' è colla e sono rimasto incollato dentro , spiegò infine .
Beh , allora ti getto la corda ; legati e ti tirerò su .
Calò la corda , lo legò e cominciò a tirare : Compare , fatti leggero , non riesco a tirarti .
Non mi tirerai più , sono incollato !
Allora , sai cosa bisogna fare ? disse quello che stava sopra : dal momento che sei incollato , non c' è via d' uscita .
Ti getterò una sciabola ; tu tagliati la testa . Prima di tagliarla , legala alla corda e io la tirerò fuori .
Così lasceremo dentro il corpo , io porterò via la testa e non ti riconosceranno .
L' altro così fece .
E così tirò fuori la testa da lì ; l' altro se l' era tagliata per non farsi riconoscere e il corpo era rimasto dentro .
Poi , senza soldi , andò via ; se la squagliò di là .
La mattina venne il padrone delle monete , colui che aveva il conio .
S' accorse e andò a chiamare i soldati .
I soldati si precipitarono : Oh , che stranezza è questa ? Dentro ci sta uno senza testa e vicino non c' è nessuno .
Cominciò ad accorrere molta gente : Venite a vedere lo spettacolo !
Che cosa c' è ?
Guardate dentro ; c' è uno senza testa e non si capisce chi è .
Fate una cosa ; tiratelo fuori e forse lo riconosceremo .
Lo tirarono fuori , cominciarono a osservarlo , ma non lo riconobbero .
Nessuno riuscì a identificarlo , perché gli mancava la testa .
Allora , dopo averlo tolto da lì , cosa escogitarono ?
Lo mettiamo in una bara e lo lasciamo in piazza .
Se in qualche casa si sentirà piangere una donna , significherà che si tratta di suo marito .
Così fecero : lo sistemarono su di una bara e le guardie si misero a girare per vedere in quale casa si piangeva .
Lasciamo intanto questi e torniamo al ladro .
Egli non aveva ancora detto niente alla moglie dell' amico , l' aveva ingannata .
Ma , quando vide quel che accadeva , che l' avevano messo nella bara , dovette svelarle tutto ; corse a casa e le disse : Attenta , non piangere , ché a tuo marito è capitato questo e quest' altro .
Le guardie erano in giro .
La donna , appena udì la storia , figuratevi un po ' ! cominciò a piangere , a gridare e dimenarsi .
Quando le guardie stavano per arrivare lì vicino , quello le fece : Zitta , zitta , o siamo morti !
Ebbe allora un' idea : afferrò un orciuolo d' olio ch' era sulla mensola del camino e patapùffete ! lo gettò per terra .
Lei non badava all' olio che era caduto , pensava al marito , alla storia ch' egli le aveva raccontato .
Ed ecco arrivare le guardie nelle vicinanze della casa ; origliarono : E ' qui , dissero .
Entrarono : Che cosa è successo , signora ?
Furbescamente l' uomo si precipitò e rispose : Ecco , è salito il gatto e le ha gettato l' olio dal camino , e lei sta piangendo .
E piange con tanto strazio ? disse la guardia .
Io le dico di non piangere , che le porterò l' altro .
Le guardie , astutamente , se ne andarono , uscirono fuori e misero un segno sulla porta .
Ma quello , il ladro , se ne accorse e subito si mise a mettere segni sulle porte ; segnò tutta la strada , in modo che non potessero più raccapezzarsi .
La mattina dopo , infatti , le guardie cominciarono a cercare : qui un segno , qui un altro segno : Com' è possibile ?
Così non riusciremo a sapere dov' era la donna che piangeva e che avevamo sospettato !
Cosa escogitarono allora le guardie ?
Lo mettiamo nella bara , dissero , lo portiamo la notte in chiesa e facciamo spiare ; chi andrà a piangere dietro la porta sarà l' indiziata .
Egli andò e le disse : Attenta , l' hanno portato in chiesa ; non farti sentire .
Ma la donna , appena lo seppe , si mise a far baccano ; voleva andare a vederlo , voleva andarci assolutamente .
Zitta , non pensarci neppure ! Ti darò io da mangiare e da bere , avrai tutto da me .
Arrivarono le due e lei voleva uscire per andare in chiesa .
L' uomo non glielo permise : Se ti muovi , ti romperò le ossa !
Ma , dimmi , cosa ti manca ?
Qualunque cosa tu voglia , fammelo sapere .
Vuoi soldi ? Te li do .
Eccoti i soldi !
La portò al mucchio di monete : No , non voglio soldi , voglio mio marito ! Non riusciva a convincerla : Dunque , cosa vuoi ?
Voglio andare a vederlo .
Tu non puoi andare da nessuna parte , altrimenti finiremo in galera , hai capito ?
Lei cercò di scappare e l' uomo la bastonò .
Ricominciò a piangere .
La gente stava ad ascoltare : Che cosa fa costei ?
Alla fine la persuase e stette zitta .
Torniamo ora un' altra volta dal morto .
Alle guardie venne un' idea : Possibile , dicevano , che non si riesca a trovare chi è l' autore del furto di questa notte ?
Mettiamo il cadavere nella bara , pensarono ; la donna che andrà a prenderlo da lì è la moglie ; è lei a provare dolore .
Il ladro , con la sua abilità , andò e lo prese .
Lo portò via e lo seppellì in una fossa .
Le guardie , appena si svegliarono la mattina , si accorsero che il cadavere non c' era ; o che si fossero addormentate , o chissà come , l' avevano trafugato : Ahimé , non c' è più il cadavere ! dissero , quando la mattina andarono a vedere .
Cominciarono a cercarlo , dopo essersene accorti : Com' è possibile ?
Dove l' avranno portato , questo morto ?
Gira e rigira , avevano un cane con loro il cane annusò dov' era e si mise o scavare .
Le guardie si avvicinarono e intanto il cane aveva scavato abbastanza da scoprire un piede .
Sparsero la voce e venne molta gente ; lo tirarono fuori .
Ora , per il ladro , cominciava a mettersi male la faccenda ; seppe che il cadavere era stato ritrovato : " Oh , povero me , pensò , adesso mi troveranno ; lei continua a piangere ! " Saputo del ritrovamento , egli andò a guardare insieme con gli altri .
Cosa pensarono , di nuovo , le guardie ?
Pensarono di prendere un' altra volta il cadavere dalla fossa e rimetterlo in piazza : chi gli fosse passato vicino alle due di notte sarebbe stato considerato responsabile dell' omicidio .
Così fecero ; lo lasciarono in piazza e la gente si ritirò tutta ; ognuno andò a chiudersi in casa sua .
Le guardie stavano con gli occhi aperti a vedere se passava qualcuno .
Il ladro cosa fece ?
Quando furono le due di notte e non c' era più anima viva , si finse viandante e andò . Nel frattempo s' era procurato sette pagnotte , un fiasco di vino e aveva lasciato tutto a casa .
Poi era andato in un convento di monaci e aveva detto : Fatemi un favore , datemi sette tonache in prestito .
Non possiamo dartele .
Ma io ve le riporterò . Dacci una lira per tonaca : così te le diamo , altrimenti non te le diamo .
Volete che vi dia sette lire ?
Ve le do .
Mise fuori il denaro , diede loro le sette lire , prese le sette tonache e andò via .
Prese allora il fiasco , le pagnotte e mise tutto sotto il cappotto .
Venne dritto dalle guardie , entrò giù e disse : Sono un viandante ; fatemi un favore , fatemi dormire qui per questa notte .
Poi aggiunse : Ho con me del pane e un fiasco di vino .
Volete mangiare un po ' ?
Noi abbiamo già mangiato .
Prendetene un pochino , un assaggio almeno .
Cominciarono col dar di morso al pane .
Beh , ora bevete un bicchiere di vino , disse il viandante : beviamo e ci mettiamo a dormire .
Di tanto in tanto il capo delle guardie s' affacciava a controllare che non andassero a prendere e portar via il cadavere .
Il ladro dentro di sé se la rideva : " Mangiate , presto , e bevete ; non pensate a lui ! " Bevendo e mangiando dentro c' era il sonnifero tutti e sette caddero di colpo addormentati .
Il ladro tirò fuori le tonache e ne mise una ciascuno a tutti loro , che già russavano .
Li lasciò dormire , si caricò il cadavere e scappò ; lo portò lontano dal paese .
Poi tornò e cercò di sapere cosa si dicesse .
La mattina , intanto , le guardie si svegliarono : Guarda , guarda , disse il primo che si svegliò , son vestito da monaco !
Andò a trovare gli altri ed eran tutti vestiti da monaci : Svegliatevi , disse , siamo tutti vestiti da monaci !
Si svegliarono , si videro così conciati , uscirono da lì e si precipitarono fuori ; guardarono e il morto non c' era più : Adesso che cosa dobbiamo fare ?
Cosa diremo quando andremo dal re ?
Che siamo vestiti da monaci e ci hanno preso il morto ?
Quando arrivarono dal re , questi , nel vederli , cominciò a ridere : Che cosa avete combinato ? domandò .
Anche la gente , prima , li guardava da dietro e rideva ; i poveretti , con la faccia nascosta , s' erano messi a correre .
Disse dunque il re : Insomma , non sapete chi è stato a prendere il cadavere ?
E ' passato un tale così e così , spiegarono , che aveva un fiasco di vino ... e raccontarono ogni cosa .
Il re se la rideva .
Se ne andarono .
Pensarono di andare in giro un' altra volta per trovare il cadavere .
La gente e le guardie cercavano di qua e di là e alla fine lo trovarono dietro un muretto : pensate ormai come puzzava !
Eccolo dov' è ! dissero .
Correte , disse una guardia , cercate una cassa e portatela .
Corsero a prendere una cassa e lo misero dentro .
Lo presero e lo portarono dal re : Lo avete trovato ?
Lo abbiamo trovato .
Dove stava ? Dietro un muro .
Ora sapete cosa dovete fare ?
Invece di essere in sette a far la guardia , dovete essere in quattordici ; e il cadavere portatelo in piazza .
Lo portarono in piazza e lo lasciarono .
Il re ordinò che nessuno si addormentasse .
Ed essi cosa fecero ?
Stavano continuamente tutti e quattordici di guardia .
Lasciamoli far la guardia e torniamo ora dal ladro .
Cosa escogitò ?
Egli aveva visto che erano in tanti ed era andato in una masseria : Massaro , disse , devi farmi un favore ; devi darmi per questa sera sedici caproni , e io ti pagherò l' affitto .
Ecco , ti do sedici ducati per una sera .
Attento , che non abbian danno , disse l' altro : devo dar conto al padrone .
Non preoccuparti ; dovesse succedere loro qualcosa , io ne sono responsabile e te li pagherò .
Così dici ?
Prendili , dunque .
Prese i caproni . Intanto era andato da frate Vincenzo e s' era fatto dare trentadue candele .
Prese le candele e le legò , una per una , alle corna dei caproni .
Mise dunque due candele sulle corna di ogni caprone , mandò avanti gli animali ed egli andò dietro . La guardia che era più vicina , e poi tutte le altre , a veder da lontano tante luci , il ladro non si vedeva , perché stava dietro , confuso tra i caproni si misero a dire : Poveri noi !
Stanno venendo a prendere l' anima del morto che sta in piazza ; scappiamo , questi sono i diavoli , scappiamo !
Se la diedero a gambe e lasciarono il morto da solo .
Quando le guardie s' allontanarono , egli prese il morto , se lo caricò , fece andare avanti i caproni e se la svignò .
Arrivato a un punto dove sapeva non l' avrebbero più trovato , gettò il cadavere ; poi si recò dal massaro , gli restituì i caproni , lo pagò e se ne andò .
Tornò a casa e se ne stette lì tranquillo , senza pensare più a niente .
Torniamo da quelli che erano scappati ed erano corsi dal re morti di paura .
Il re li vide così sconvolti e domandò : Cosa è successo ?
Cosa è successo ? Sono venuti tanti diavoli a prendere il morto che stava in piazza .
Noi , maestà , a tale vista , siamo scappati via e l' abbiamo lasciato .
Era spaventoso vedere tanti diavoli ! ripetevano al re .
Oh , chissà chi è ad avere tanta destrezza ! egli disse : lasciate perdere , non andate più da nessuna parte .
Vedremo chi è ad aver avuto tanto coraggio da prendere il morto , portarlo in giro , gettarlo .
Le guardie , a queste parole , dissero : Noi non andiamo più a far la guardia ; ci vada chi vuole !
No , no , tornate pure alle vostre faccende , non andrete più da nessuna parte .
Fate il vostro lavoro e acqua in bocca !
In seguito il re fece bandire per tutto il regno d' aver organizzato un grande banchetto e invitava gente d' ogni città e d' ogni paese .
Da quella strada dove era stato messo il segno sulla porta fu invitata molta gente .
Il messo che faceva gli inviti venne anche dal ladro e disse : Tu vuoi venire al banchetto che si sta preparando ?
Poi , dopo aver invitato parecchi , tornò al palazzo del re e disse : Ne ho invitati tanti .
Bene , rispose il re , bastano .
Ora , tra tutti gli invitati , doveva esserci anche il ladro .
Questi allora andò dalla comare e chiese : Che ne dici , vado anch' io ? Dove ? lei disse .
Al banchetto che il re ha organizzato ; ci ha invitati in moltissimi per andare a mangiare .
E io che ne so ?
Vado o non vado ?
Non ci andare ; chissà cosa succederà !
Beh , o mi uccideranno o resterò libero .
Egli era scapolo ; l' altro , cui aveva tagliato la testa , invece era sposato .
Andò dunque .
Quando entrarono tutti sul palazzo c' era tantissima gente , figuratevi un po ' ! si disposero in una lunga fila , seduti , come quando si deve svolgere un processo .
Cominciarono a esser serviti tanti piatti sulle tavole imbandite .
Al poveretto bruciava il sedere , perché non sapeva come sarebbe andata a finire : " Ahimé , cosa ho fatto a venir qui ! " diceva tra sé .
Finito di mangiare , immaginate quante portate , d' ogni qualità ! per non tirarla ancora per le lunghe , si presentò il re .
Si sedette sul trono , con una corona in mano , e cominciò : Voi dovete dirmi , parlando in tutta libertà , chi di voi ha avuto il coraggio e l' abilità di andare prima di tutto là dove si coniano le monete ...
Ed espose ogni cosa , punto per punto .
Il ladro diceva dentro di sé : " Lo dico o non lo dico ? " E sentiva il cuore battergli forte e le gambe tremargli dentro i calzoni .
Beh , disse infine il re , alzandosi e tenendo la corona in mano : voi avete paura di confessare , ma io vi prometto ... al ladro il cuore ricominciò a saltare vi prometto la corona .
Il poveretto sgranò gli occhi ; non sapeva cosa fare ; cento pensieri gli si affollavano nella mente , non riusciva a decidere .
Il re aveva a fianco una ragazza , l' aveva portata con sé ; ricominciò : Prometto la corona e mia figlia a colui che ha avuto tanta destrezza .
A sentir ciò egli s' alzò in piedi : Sono stato io ! disse .
Gli era uscito il fiato , finalmente . Se sei stato tu , disse il re , eccoti allora la corona e anche mia figlia : ti sia concessa .
Restò delusa , la comare !
Si sposarono , dunque ; fecero tanti festeggiamenti , e vissero felici e contenti .
Chi non crede e vuol verificare vada a Napoli e li potrà trovare .
Il racconto è terminato e un tornese ho meritato .
