C' era una volta una madre che aveva tre figli .
Il primo partì e non tornò più .
Si presentò il secondo : Mamma , mio fratello se n' è andato ; non dovrei partire anch' io per cercarlo ?
E cosa facciamo ? Uno più uno due ?
No , mamma , non preoccuparti ; io vado e torno .
No !
Dammi la benedizione , perché devo partire .
Partì .
Uno più uno due , non tornò neanche lui .
Lasciamo stare questi che non tornarono e parliamo del fratello minore .
Si rivolse alla madre : Mamma , voglio andare dove sono andati i miei fratelli .
Ah , figlio mio , cosa mi vuoi fare !
Se parti anche tu , per me è finita .
Non preoccuparti , mamma , io tornerò .
Si mise in strada e andò a cercare i suoi fratelli .
Passò un mese , passarono due .
La madre , aspetta oggi e aspetta domani , alla fine perdette le speranze per tutti e tre .
Il ragazzo s' era incamminato per cercare i fratelli maggiori .
Cammina cammina , trovò un bosco : " Entro o non entro ? " si chiedeva .
Si fece coraggio ed entrò .
Una volta dentro , si guardò attorno e cominciò ad aver paura ; stava in una macchia .
Aveva camminato per circa un miglio quando s' imbatté in un leone , un' aquila e una formica che litigavano su come spartirsi il cadavere di un bue : Buongiorno ! disse il ragazzo .
Buongiorno , e passò oltre .
Aveva fatto due o tre passi e lo chiamarono : Vieni qui , disse il leone ; facci un favore , dividi il bue tra di noi ; noi non riusciamo ad accordarci .
Bene : a te che sei aquila , do le ossa , perché hai il becco e puoi ripulirle ; a te che sei leone do la polpa ; a te che sei formica do la testa , perché puoi entrare , uscire e mangiare tutto infilandoti in ogni fessura .
Siete soddisfatti ? chiese .
Sì , rispose il leone , non potevi dividere meglio di come hai fatto .
Statevi bene .
Statti bene .
E se ne andò .
Aveva fatto pochi passi e lo chiamarono di nuovo : Cosa volete ? domandò .
Vieni , non aver paura , rispose il leone .
Ma lui sì che ne aveva , di paura !
Non aver paura , ripeté il leone , e quando il ragazzo ritornò , disse : Noi non avevamo niente da darti .
Io non voglio niente ; vi ringrazio , ma non voglio niente .
No , riprese il leone , non ti lasciamo andar via senza niente .
Io , da parte mia , ti concedo di poter diventare leone ; quando lo vorrai , basterà che tu dica : " Mi faccio leone " .
Ed io , disse l' aquila , di diventare aquila ; quando ne avrai bisogno , dirai : " Mi faccio aquila " .
Ed io la stessa cosa , disse la formica .
Beh , statevi bene .
Statti bene .
E se ne andò .
Si allontanò da loro per circa un miglio e pensò : " Devo vedere se è vero quello che mi ha detto il leone " , e disse : Mi faccio leone .

Subito diventò leone : " Come son brutto ! " pensò , poi disse : Mi faccio uomo , se no son brutto d' accidenti .
Ma ora voglio diventare formica : Mi faccio formica !
Diventò formica : Adesso son troppo piccolo ; no , no , non voglio più : Mi faccio uomo !
Ridiventò uomo : Beh , vediamo ancora : Mi faccio aquila !
Diventò aquila e volò .
Uscì dal bosco e raggiunse un paese .
Udì le campane che suonavano a morto ; allora s' abbassò : Mi faccio uomo !
Si trasformò in uomo e cominciò a camminare .
Incontrò una vecchia : Mi spieghi perché le campane suonano a morto ?
Perché , figlio mio , il re ha perduto la figlia ; gliel' hanno rapita .
Sai dove l' hanno portata ?
Eh , figlio mio , che ne so ?
Va ' più avanti e troverai una più vecchia o uno più vecchio di me e ti darà notizie .
Cammina cammina , incontrò un vecchietto : Mi sai dire perché queste campane suonano a morto ? chiese di nuovo .
Hanno rapito la figlia del re e non abbiamo nessuna notizia .
Lasciò quel paese e raggiunse un altro ; di nuovo s' udivano le campane a morto .
Incontrò una donna : Mi sai dire dove hanno portato la ragazza , dov' è finita ?
Dimmi qualcosa .
Eh , figlio mio , forse l' hanno portata verso il mare .
Egli disse allora : Mi faccio aquila !
S' alzò in aria e volò .
Volando cominciò a volteggiare sul mare ; poi continuò a volare e vide un palazzo in mezzo al mare .
Si fermò sul cornicione del palazzo e vi rimase : " Devo vedere se sta qui " , pensò .
Aspetta aspetta , alla fine vide la ragazza : " Devo trovare il modo di scendere " , disse , ed era ancora aquila .
Prima di scendere , quand' era ancora sul muro , vide l' Orco che usciva per andare a caccia : " Caspita ! pensò , come faccio a scendere ? " Ma non appena l' Orco s' allontanò da lì , subito si calò nel giardino e cominciò a riflettere su come entrare da lei .
Così , prima che tornasse l' Orco , riuscì ad entrare nella stanza dov' era la ragazza ; nel frattempo , s' era fatto uomo .
Entrò mentre lei sfaccendava per la casa ; le si accostò e l' afferrò alle spalle : Nonno , nonno ! lei si mise a gridare .
Era l' ora che l' Orco tornava dalla caccia e , al grido di lei , subito accorse : Cosa è successo , figlia mia ?
Il giovane invece , appena sentì gridare , disse : Mi faccio formica !
Diventò formica e le si ficcò nel seno ; zitta tu e zitto anch' io , se ne stette nascosto lì .
L' Orco si mise a cercare : Figlia mia , quassù non può arrivare nessuno !
Non trovò niente , infatti : Figlia mia , io qui non trovo niente .
Ma tu non devi aver paura ; qui non può salire nessuno ; se sale qualcuno , lo divoro .
Statti ben tranquilla , figliola ; io vado via .
Il giovane di nuovo , appena calcolò che l' Orco era già fuori , disse : Mi faccio uomo !
L' afferrò un' altra volta alle spalle , le portò la mano alla bocca e gliela tenne chiusa : Zitta , le disse , è tanto che soffro per causa tua .
Nonno , nonno ! gridò ancora lei .
L' Orco udì e il giovane ridiventò formica : " Cosa mi ha combinato ! pensava , l' ha fatto tornare " .
L' Orco venne e fece come prima ; cercò , poi disse : Te l' ho già detto che quassù non viene nessuno ; se vengono , io li divoro .
Adesso non mi chiamare più .
E se ne andò .
Sparito l' Orco , il giovane disse : Mi faccio uomo !
Le tappò allora la bocca : Zitta , ti prego , quant' è che mi fai penare !
Oh ! esclamò lei , e tu come fai a trovarti qui ?
Son venuto per te : ho saputo che ti ha rapita l' Orco e ti ha portata quassù ; non appena ne ho avuto notizia , son venuto per cercare di riportarti indietro .
Mai potrai riportarmi , proprio mai , rispose la ragazza , e aggiunse : ma tu , dimmi , come hai fatto a salire fin qui ?
Come ho fatto ?
Vuoi vedere come ho fatto ?
Mi faccio leone ! e diventò leone .
Ah , come sei brutto , lei disse , dai , ridiventa uomo .
E tu ti sei trasformato in leone per venire qui ?
No , egli rispose , adesso vedrai .
Ed eccolo diventare formica . Ah , no , ridiventa uomo ; così sei troppo piccolo e ti si può schiacciare .
In quell' istante s' udirono dei passi : Ahimé , sta arrivando l' Orco ; presto , nasconditi !
Era appena ridiventato uomo e dovette farsi un' altra volta formica ed entrarle nel petto ; e lei lo sapeva bene ! Salì di nuovo l' Orco : Hai veduto qualcosa ancora ?
No , nonno , non ho visto più niente .
L' Orco mangiò , poi scese e se ne andò .
Quand' era sulle scale , disse : Non aver paura , figlia mia , sta ' tranquilla e non pensarci .
La ragazza allora si rivolse al giovane : Su , vieni fuori , trasformati in uomo ; è ora di mangiare .
Gli diede da mangiare e da bere finché non fu sazio , poi andarono a dormire .
Quando lei s' accorse che era l' ora del rientro dell' Orco , disse : Alzati , bello mio ; altrimenti arriva l' Orco e ci divora tutt' e due .
Tu dovresti fare una cosa , le propose il giovane : devi fingere d' esser triste ; al suo rientro egli ti domanderà : Che cos' hai , figlia mia , perché sei triste ?
Tu gli risponderai : Sono triste perché , quando tu sarai morto , io resterò quassù tutta sola .
Digli queste parole , disse dunque il giovane : Ma adesso non c' è tempo da perdere ; sta per arrivare l' Orco .
Ridiventò formica .
Giunse l' Orco e la trovò molto triste : Figlia mia , perché sei così triste ?
Perché quando tu morirai io resterò sola .
Oh , figlia mia , ti ho detto che io non morirò mai .
Mangiò , bevve , poi scese e se ne andò .
Il giovane si rifece uomo : Vieni a mangiare , lei disse ; l' Orco se n' è andato .
Mangiò , poi giunse di nuovo l' ora del rientro dell' Orco : Sai stavolta cosa devi chiedere ?
Devi chiedergli : In che modo tu potresti morire ?
Dopo che ti risponderà , non chiedergli più nulla .
Fa ' attenzione alle sue parole e tienile ben in mente .
Torniamo all' Orco .
Giunse l' ora del ritorno ; entrò e chiese : Figlia mia , come sei stata ?
Hai visto qualcosa ?
No , però sono triste , perché quando tu morirai resterò sola in mezzo al mare .
Vieni qui con me , disse allora l' Orco , e guarda lontano lontano verso il mare ; di là c' è costruito un palazzo ; dentro il palazzo c' è un' orsa ; l' orsa ha un' aquila ; dovrebbero ammazzare l' aquila , prendere le tre uova che l' aquila custodisce e rompermele ad una ad una in fronte ; solo allora io morirei .
Vedi , figlia mia ?
Dunque sta ' tranquilla che io non morirò .
Poi l' Orco mangiò e andò via .
Non appena se ne fu andato , lei riferì tutto al giovane , che intanto era ridiventato uomo ; ma , mentre gli parlava , era triste : Come si possono superare tante prove ! ? disse .
Cosa ti ha detto ? egli domandò .
Questo e quest' altro ; ma si tratta di cose impossibili .
Oh , disse il giovane , invece sono cose semplici !
Ne sei certo ?
Certissimo .
Lei non conosceva ancora la sua capacità di diventare aquila e domandò : Possiedi anche altre capacità ?
Mostrami , mostrami !
Mi faccio aquila , egli disse allora , e si fece aquila .
Come son contenta , come son contenta !
Dunque è sicuro che ce ne andremo via da qui !
Sì , sì , non preoccuparti .
Mangiarono e bevvero un' altra volta : Ora devi concedermi tre giorni di tempo , disse il giovane ; se dopo tre giorni non sarò tornato , non mi aspettare più ; vorrà dire che sono stato divorato dall' orsa .
Statti bene e pensami quando non ci sarò .
Si congedò ; disse : Mi faccio aquila ! le volò davanti e partì .
Arrivò sul cornicione del palazzo e stava in guardia per cogliere l' occasione propizia e ammazzare l' orsa ; era sempre nelle vesti di aquila .
Passa un giorno , passano due , alla fine intravide l' orsa , scoprì dov' era e si lanciò dentro .
Appena fu nel palazzo si trasformò in leone ; cominciò a combattere contro l' orsa e la uccise ; poi le ghermì l' aquila , uccise anche questa e prese le tre uova ; ne afferrò uno per zampa , l' altro se lo mise nel becco e volò .
Quando tornò , la ragazza stava sulla terrazza fissa a guardare ; appena lo vide , le saltò il cuore in gola : " Sia ringraziato il cielo , pensò , il mio uomo sta tornando ! " Il giovane arrivò provate a immaginare , lei era in paradiso dalla gioia !
Gli preparò da mangiare e si mise a chiedere : Come hai fatto , come ci sei riuscito a portare le uova ?
So io che pena a far la spia a quell' orsa !
Egli poi mangiò , bevve e si trasformò in formica , perché era ormai ora .
In quell' istante s' udì l' Orco : Aprimi , figlia mia , disse , io non ce la faccio ad aprire il portone .
Oh , povera me ; dunque te ne vai e mi lasci sola ?
Lei aveva nascosto le tre uova nel petto .
Pah ! e gli schiacciò il primo sulla fronte .
Ahimé , egli disse , sono morto !
Subito gli schiacciò l' altro , poi l' ultimo , e l' Orco morì .
Gli tagliarono allora la testa : gettarono il corpo in mare e la testa l' appesero sul muro del palazzo , attaccata a un bastone .
E adesso come si fa ? chiese la ragazza : Io ho ucciso l' Orco ; adesso tocca a te trovare come andar via da qui .
E lui : Salirò sulla terrazza e appena vedrò un bastimento gli farò segno col fazzoletto perché si avvicini .
Salì sulla terrazza , vide un bastimento e cominciò a far segni .
La nave s' accorse e s' accostò : Cosa volete ?
Vogliamo un favore .
Cosa ? domandò il capitano .
Devi portarci via da qui .
D' accordo .
Ti paghiamo e ci porti via , anche subito .
Quando volete .
Raccolsero ogni cosa e caricarono il bastimento .
Poi partirono e la testa dell' Orco restò appesa .
Giunsero in altomare e qui il giovane fu preso da Mamma Serena .
La povera ragazza restò di sasso : Ahimé , una vecchia l' ha acciuffato e l' ha portato in fondo al mare !
Ma noi dobbiamo andarcene , disse il capitano .
Non posso venire ; egli mi ha liberata dall' Orco .
E cosa facciamo , restiamo qui ?
Sì , aspettiamo ; fai fermare il bastimento ; devo aspettare che torni mio marito , lei disse , e aggiunse : io mi fermo qui ; tu vai in paese e chiedi come fare per liberare il giovane che sta in fondo al mare .
Fa ' presto .
Egli arrivò poniamo a Martignano e chiese ad una vecchia : Mamma Serena ha preso un giovane e l' ha portato in fondo al mare . Cosa si può fare ?
Cosa si può fare ?
Occorrono molti suoni ; lei va matta per i suoni .
Il capitano così fece : cercò tanti suonatori e li portò proprio nel punto dov' era stato preso il giovane .
Si misero tutti quanti a suonare ; anche la vecchia che aveva dato il suggerimento stava con loro .
Mentre suonavano , s' affacciò mamma Serena : Smettete subito di suonare , ordinò la vecchia ; lei sta qui vicino .
Vecchia maledetta , disse allora mamma Serena , sei stata tu a far smettere i suoni !
Quando porterai su il giovane che trattieni nel fondo , ascolterai di nuovo suonare , lei rispose .
Ripresero a suonare : Suonate , disse la vecchia .
Ed ecco che Mamma Serena fece sporgere il giovane per metà : Suonate più forte , egli disse , forse mi farà sporgere fino alle ginocchia ed io potrò volare .
Per farla breve , non appena lo fece sporgere fino alle ginocchia , il giovane volò ; non entrò più nel bastimento , andò avanti volando .
Raggiunsero la riva del mare e scesero tutti ; mancava appena mezzo miglio di strada e lo fecero a piedi per la gioia .
Arrivarono in paese : Oh , è tornata la figlia del re ! cominciarono a dire .
Si misero a suonare le campane a festa , non più a morto : E ' tornata la figlia del re ! diceva la gente : chissà dov' è stata !
L' accompagnarono al palazzo e cominciarono grandi festeggiamenti : mangiarono e bevvero .
Al fianco di lei c' erano , da un lato , il giovane che l' aveva salvata , dall' altro , un principe ; le chiese allora il re : Adesso , figlia mia , ti vorrai sposare .
Vuoi prendere il principe che hai al tuo fianco ?
Oh , proprio no ; non voglio lui ! Cosa ?
Non vuoi un principe di sangue reale come noi ?
O mi farai sposare il giovane che mi ha liberata dall' Orco , o non mi sposerò affatto .
Sia fatto come tu vuoi ; chi ti ha liberata dall' Orco sia tuo sposo .
Così , in quello stesso banchetto , si sposarono e vissero felici e contenti .
E chi non crede e vuole controllare vada a Napoli e li potrà trovare .
