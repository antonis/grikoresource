C' era una volta una signora ; si era sposata , ma dopo tanti anni che viveva col marito non aveva ancora avuto un figlio .
Fece un voto a san Giacomo di Galizia affinché le concedesse di mettere al mondo un bambino : fosse pure colpito da un fulmine , quando avrebbe compiuto i dodici anni , e incenerito .
La donna restò incinta , partorì e le nacque un bel bambino .
Il piccolo cresceva , giorno dopo giorno ; fu mandato a scuola , e col passar del tempo superò anche il maestro .
S' avvicinava il tempo del voto fatto dalla madre e lei , quando vedeva il figlio tornare a casa dalla scuola , si metteva a piangere .
Il ragazzo non s' accorgeva del pianto della madre , ma un giorno la scoprì .
Uscì per andare a scuola e non le parlò , perché piangeva .
Tornò , entrò a casa , e lei di nuovo a piangere .
Andò a raccontarlo al padre : Appena entro a casa , la mamma mi guarda e comincia a piangere , come se vedesse il diavolo .
L' uomo chiamò la moglie e le chiese : Vorrei che mi spiegassi perché mai quando vedi nostro figlio è come se vedessi il demonio .
Devi dirmi perché piangi .
Tu ricordi , lei disse , che non riuscivamo ad avere figli ; doveva esserci qualche ostacolo ; io feci voto a san Giacomo di Galizia , pur di rimanere incinta e dare al mondo un bambino ; a dodici anni , dissi , fosse colpito da un fulmine e incenerito .
Tu hai fatto questo ? ! esclamò il marito .
Poi chiamò il figlio : Vieni qui ; ecco perché tua madre piange : è per te .
Lei non aveva figli e ha fatto un voto a san Giacomo di Galizia , pur di avere un bambino : a dodici anni , cadesse pure un fulmine e ti incenerisse .
Lei ha fatto questo ? domandò il figlio .
Il padre allora disse : Eccoti un cavallo , e questo è un sacchetto di monete ; andrai da san Giacomo di Galizia e dirai al padre guardiano : Il sacchetto di monete e pure il cavallo sono destinati a una messa cantata per amor mio .
Prendi anche queste tre mele fuori stagione e mettitele in tasca .
Eccoti infine una spazzola ed un foglio di carta .
Ricordati : quando arriverai in città , tirerai dritto alla taverna ; ti verrà incontro un giovane e ti dirà : Caro compagno , quant' è che non ti vedo !
Anch' io , risponderai .
Vi metterete a mangiare .
Alla fine , tira fuori la mela , dividila in due parti , una più piccola e una più grande , e proponi all' amico : Prendi la tua parte .
Se prenderà la più piccola , si tratta di un amico fedele ; se prenderà la più grande , si tratta di un furfante .
Il figlio così fece .
L' amico scelse la parte più grande . " Questo è un furfante " , pensò allora il giovane ; poi disse al compagno : Va ' pure a dormire ; però domattina alzati presto , vieni a chiamarmi e partiremo insieme di buon' ora .
L' altro tornò a casa e confidò alla moglie : Domani ci sarà da guadagnare .
Ho incontrato un giovane con un bel cavallo ed un sacchetto di monete ; domattina andrò a svegliarlo , l' ammazzerò e gli prenderò cavallo e denaro .
L' altro invece era montato a cavallo ed aveva cavalcato tutta la notte .
Quando il compagno cominciò a gridare per svegliarlo , la padrona domandò : Non mi dici chi cerchi ?
Va ' a dire a quel giovane di alzarsi , ché ormai è tardi .
Hai sbagliato strada ; quello è partito da qui ieri sera .
L' ingannato tornò dalla moglie : Beh , com' è andata ?
Io son furbo , ma lui è stato più furbo di me ; è andato via ieri sera .
Cammina cammina per tutta la notte , il giovane raggiunse un' altra città .
Si diresse alla taverna ; gli venne incontro un garzone , gli tolse il cavallo , lo portò nella stalla e lo accudì ; poi si rivolse al viaggiatore : Oh , caro compagno , dove sei diretto ?
A san Giacomo di Galizia .
Per adesso andiamo a mangiare .
Che cosa c' è di pronto ? chiese .
C' è di tutto .
Allora portateci due piatti : uno di tagliatelle ed uno di pesce , e naturalmente un litro di vino . Consumarono tutto .
Allora il giovane cacciò di tasca una mela , la divise in due parti , una più grande ed una più piccola ; invitò l' altro : Compagno prendi la tua parte .
Prese la più grande . " Anche il secondo è un furfante , pensò tra sé , ha adocchiato il cavallo e il sacchetto del denaro " .
Poi si rivolse a lui : Va ' pure a dormire ; domattina vieni a chiamarmi all' alba e partiremo col fresco .
Il garzone rientrò a casa e confidò alla moglie : Domattina ci sarà un buon boccone : lo ucciderò e gli prenderò il sacchetto di denaro ed anche il cavallo .
Dopo qualche ora andò già a bussare .
La taverniera domandò : Chi bussa ?
Fa ' svegliare quel giovane , ché è tardi .
T' è scappata l' occasione ! E ' partito da ieri sera .
Tornò a casa e riferì alla moglie : Io son furbo , ma lui è stato più furbo di me .
Il giovane viaggiò tutta la notte e il giorno successivo ; la sera finalmente giunse in città .
S' avvicinò il garzone : Oh , caro compagno , non ti vedevo da molto tempo !
Anch' io .
Prese il cavallo , lo portò nella stalla , lo accudì , poi si recarono dalla cantiniera : Che cosa c' è di cucinato ?
Quello che volete .
Portaci allora due piatti , uno di carne e uno di pesce , e naturalmente un litro di vino .
Poi tirò fuori la mela , fece due parti , una più grande ed una più piccola : Compagno , prendi pure la tua parte .
L' altro prese la più piccola , e da quella si ritagliò ancora una parte .
Il giovane disse : Ora andiamo a dormire ; domattina ci alzeremo di buon' ora e partiremo col fresco .
Si alzarono e si misero in cammino : Tu dove sei diretto ? chiese il giovane .
Io ho fatto un voto a san Giacomo di Galizia : vado a far celebrare una messa cantata .
E tu , che voto hai fatto ?
Se te lo dico , mi abbandonerai e te ne andrai via . Non farò mai una cosa del genere .
Allora , eccoti questa carta , questa spazzola e questo sacchetto di denaro : va ' dal convento di san Giacomo di Galizia e regala il cavallo e il denaro .
In quel momento stava sul cavallo ; scoppiò un fulmine e lo incenerì .
Il compagno rimase stordito .
Ma , dopo un istante , scese da cavallo , prese la spazzola e la carta , raccolse tutta quella cenere e la portò al convento ; chiamò il padre guardiano e gli consegnò il cavallo e il denaro : Tu dovrai celebrare le funzioni per questa cenere .
Il padre guardiano fece venire un altro monaco ; quando arrivò , gli ordinò : Prendi il cavallo e il sacchetto di denaro e portali nella stalla .
Il monaco legò il cavallo e ripose il denaro in una cassa che chiuse a chiave .
Il giovane allora se ne andò via .
Aveva percorso appena un tratto di strada , quando vide il cavallo correre veloce , nitrendo e tirandosi dietro il sacchetto del denaro legato alla coda .
L' afferrò e lo riportò al convento ; chiamò il padre guardiano : Il cavallo ve l' ho lasciato perché voi celebriate le funzioni per quella cenere raccolta nella carta , disse .
Il padre domandò al monaco : Dov' è il cavallo ?
Nella stalla .
E il sacchetto del denaro ?
L' ho riposto in una cassetta e ho conservato tutto nella cassa più grande , che ho chiuso con due chiavi .
Com' è possibile , se il sacchetto è legato alla coda del cavallo ?
Io , il cavallo , l' ho legato con doppia catena .
Allora il padre guardiano si rivolse al giovane : Ascolta , figliolo , segui le mie raccomandazioni : arriverai sul luogo della disgrazia , dov' è scoppiato il fulmine che ti ha ucciso il compagno e lo troverai sano e salvo , poggiato al muretto ; prenditi cura di lui , ti prego , e riportarlo a casa .
Quanto a te , ci rivedremo ; se non oggi , sarà domani ; siamo qui ad aspettarti .
Il compagno rimase poi parecchio tempo a casa del giovane miracolato , il quale intanto aveva preso moglie e aveva avuto due figli .
Stando lì , s' era ammalato e il suo corpo si era riempito di piaghe .
L' amico gli stava sempre accanto .
Un giorno si presentò un vecchio : Fammi la carità , per amore di san Giacomo di Galizia , disse ; poi chiese : chi sta ammalato ?
Il mio compagno ; si è riempito di piaghe .
Vuoi che guarisca ?
Prendi i figli che tua moglie ha partorito , tutt' e due , uccidili , raccogli il sangue in una bacinella e col sangue ungi il corpo del tuo amico ; man mano che lo ungerai , egli guarirà .
La moglie era andata a messa .
Al ritorno , trovò quel giovane sano come un pesce .
Ma il marito era angosciato , perché aveva ucciso i due figli .
La donna entrò in casa , aprì la porta e vide i due bambini saltare sul letto ; chiamò il marito : Vieni a vedere come saltano sul letto i tuoi figli !
Lasciami in pace ! egli rispose .
Ma è vero !
Andò a guardare e li vide saltellare tutt' e due : Ah , disse , quel vecchio ha fatto bene a dirmi di ucciderli e di ungere col loro sangue il mio compagno , ché sarebbe guarito come un pesce !
Il giorno dopo il vecchio si ripresentò : Come sta il tuo amico ? domandò , è guarito ?
Sì , rispose l' altro .
Vuoi sapere chi sono ?
Io sono san Giacomo di Galizia .
Adesso ti prego di prenderlo e accompagnarlo a casa .
E ' sufficiente il bene che è stato fatto .
