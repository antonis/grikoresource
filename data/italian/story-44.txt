C' erano una volta cento soldati che stavano al servizio d' un re .
Cinque di loro non eran contenti .
Allora il caporale disse : Potremmo svignarcela .
Presero un cavallo ciascuno e fuggirono .
Cammina cammina , arrivarono in una campagna ; trovarono una capanna piena d' erba e di paglia e ci misero i cavalli ; essi cercarono verdura selvatica da mangiare .
Uno di loro salì sopra un albero a raccogliere rami per il fuoco e da lì scorse in lontananza una luce rossa : Andiamoci , disse il capo .
Vi si diressero e trovarono un palazzo tutto rosso ; salirono su e videro cinque piatti , e dentro i piatti c' erano maccheroni rossi , carne rossa e in mezzo una mela rossa .
Poi entrarono nell' altra stanza e c' erano due letti rossi : Ora restiamo a dormire qui , disse il capo : quattro si mettono nel letto grande ed io in quello piccolo .
Dopo aver dormito , s' alzarono e s' allontanarono . Strada facendo , uno di loro che usava ubriacarsi e che non era facile al sonno disse al caporale : Vorrei raccontarti una cosa , anche se tu non mi crederai : questa notte , mentre dormivamo , s' è udito a un tratto un suono d' orologio e subito dopo dei passi ; una ragazza si è avvicinata a te e ha detto : Alzati , caporale , è tempo che mi aiuti !
Alzati , caporale , è tempo che mi aiuti ! Sta ' zitto , bugiardo ! rispose il caporale .
Non gli credettero . Cammina cammina , si ritrovarono nella stessa campagna ; mangiarono verdura selvatica , perché ormai avevano fame , e misero i cavalli nella capanna .
Il caporale disse al soldato di prima : Sali sull' albero , forse vedrai qualche altra luce .
Il soldato salì e vide una luce azzurra . Andiamoci , fece il caporale .
S' incamminarono di nuovo e stavolta il palazzo era tutto azzurro ; salirono e trovarono la tavola imbandita : cinque piatti azzurri , pieni di carne azzurra , maccheroni azzurri , tutto azzurro , ed una mela azzurra nel mezzo .
Mangiarono , poi uno di loro domandò : Mangiamo anche la mela ?
Ci fosse una per ciascuno , la mangeremmo ; ma , poiché è una sola , meglio lasciarla ; non si sa mai .
Entrarono nell' altra stanza e trovarono un letto grande ed uno piccolo , azzurri .
Il caporale disse : Mettiamoci a dormire : io nel letto piccolo , voi quattro nel grande .
Dopo aver dormito , ripresero il cavallo e s' allontanarono . Cammina cammina , arrivarono in una campagna simile alla prima .
Il soldato ubriaco riferì di nuovo : Caporale , è venuta un' altra volta la ragazza , quand' era l' una esatta , vestita tutta d' azzurro , e ha parlato per un quarto d' ora con te .
Giunti nella campagna , il capo disse al soldato : Sali sull' albero e guarda se c' è qualche altra luce .
Salì e vide una luce nera come il diavolo : Andiamoci , fece il caporale .
S' incamminarono e trovarono un palazzo tutto nero .
Salirono e c' erano cinque piatti neri apparecchiati e una mela nera nel mezzo .
Mangiarono , poi uno di loro domandò : Mangiamo anche la mela ?
Ci fosse una per ciascuno , la mangeremmo ; ma , poiché è una sola , meglio lasciarla , disse il caporale ; poi , rivolto al soldato ubriacone : Bevi pure tutto il vino che c' è , disse , forse riuscirai a prender sonno !
Entrarono nell' altra stanza e trovarono due letti neri , uno grande e uno piccolo , e dormirono .
Quella notte il caporale non s' addormentò ; alle sette suonò l' orologio ; s' udirono passi e a un tratto egli vide una ragazza vestita di nero che s' avvicinò e disse : Alzati , è ora che mi aiuti !
Il caporale balzò : Cosa vuoi ?
Cosa vuoi ? Cosa voglio ?
Che tu vada ad ascoltare tre messe per me ; quando lo farai , io potrò tornare da mio padre ; per il momento sto a cuocere nell' olio tutta viva , perché le streghe mi hanno fatto un incantesimo . Io sono la figlia del re presso cui tu prestavi servizio ; se tu ascolterai tre messe , io potrò tornare da lui .
Attento , però : mentre ascolterai la messa , verrà una vecchia e ti porterà un paniere di frutta e ti dirà : Prendi , prendi !
Tu non lo toccare , altrimenti finirai come me a cuocere nell' olio . Invece rispondi : Se mi date dell' acqua , l' accetto , perché ho sete ; ma frutta non ne voglio .
Egli così fece . E le vecchie gli dicevano : Acqua non te ne diamo ; se vuoi frutta , eccola !
E gli gettavano acqua addosso .
Il mattino seguente , la vecchia ricominciò : Prendi ! Prendi !
Acqua me ne date ?
No , acqua niente ! Insomma , ascoltò le tre messe , e le vecchie ripetevano sempre lo stesso ritornello , ma lui non si lasciò piegare .
Dopo aver ascoltato le tre messe , andò a dormire .
S' alzò la mattina , aprì la credenza e trovò cinque sacchetti di monete ; distribuì uno per ciascuno ai soldati e disse : Ora andatevene , io resterò qui .
Essi partirono e il caporale rimase .
Allora la ragazza gli diede tre fazzoletti , tutti uguali ; dopo averglieli dati , se ne volò via e lo lasciò solo .
Così il giovane dovette mettersi a cercare il paese di lei ; e questo paese si chiamava Spagna ; però , invece di prendere la direzione della Spagna , s' avviò verso la direzione opposta .
Cammina cammina , cammina cammina , s' imbatté in una capannuccia ; s' accostò alla porta e cominciò a dire : Eccola , eccola , la bella casa !
Chi è ? disse il vecchio da dentro .
Aprimi , son cristiano come te .
Che segno mi dai ?
Il segno della santa croce .
Il vecchio aprì : Sai darmi notizie della Spagna ? Notizie io ?
Come potrei ?
Sono stato sempre qui chiuso a far l' eremita ! Tuttavia , aspetta : chiamerò gli uccelli , sui quali il Signore mi ha dato il comando , e chiederò se sanno qualcosa .
Con un fischio chiamò a raccolta tutti gli uccellini e chiese loro : Sapete darmi notizie della Spagna ? Spagna ?
Spagna ?
Mai sentita nominare ; ma noi stiamo sempre qui vicino . Va ' più avanti ; c' è un altro fratello , e forse ti saprà dare notizie .
Cammina cammina , cammina cammina , trovò un' altra capannuccia ; s' accostò e cominciò a dire : Eccola , eccola , la bella casa !
Chi è ?
Aprimi , son cristiano come te .
Che segno mi dai ?
Il segno della santa croce .
Il vecchio aprì : Oh , figlio mio , disse , sei così bello e ti trovi da queste parti ?
Hai forbici lunghe e corte per tagliarmi le ciglia folte ?
Il giovane domandò : Sai darmi notizie della Spagna ?
Figlio mio , chiamerò tutti gli animali e chiederò loro .
Con un fischio fece venire leoni , lupe , lupi e altri animali : Sapete dar notizie della Spagna a questo giovane ? Spagna ?
Spagna ? Mai sentita nominare .
Vedi , figlio mio ? disse allora il vecchio : non l' hanno mai sentita nominare .
Ora , però , fai come ti dico : va ' più avanti , troverai mio padre e forse ti saprà dare notizie .
Cammina cammina , cammina cammina , raggiunse l' altra capannuccia . Eccola , eccola , la bella casa !
Chi è ? disse il vecchio da dentro .
Aprimi , son cristiano come te .
Che segno mi dai ?
Il segno della santa croce .
Il vecchio aprì : Oh , figlio mio , come sei bello !
Hai forbici lunghe e corte per tagliarmi le ciglia folte ?
Sai cosa voglio da te ?
Avere notizie della Spagna .
Ah , figliolo , aspetta , aspetta : chiamerò gli animali su cui il Signore mi ha dato il comando e chiederò loro .
Diede un fischio e accorsero animali d' ogni specie , ma risposero che della Spagna non avevano sentito neppure il nome : Vedi , figlio mio ?
Non sanno niente .
Ma , aspetta , chiamerò l' aquila : lei forse sa qualcosa .
Fischiò e arrivò l' aquila con due teste : Sai dar notizie della Spagna a questo giovane ?
Spagna ? rispose l' aquila : certo che che lo so .
Son venuta giusto da lì ; anzi col tuo fischio mi hai fatto perdere il banchetto di nozze della figlia del re .
Devi portarci subito questo giovane .
Questo giovane ? Non posso portarlo ; egli ha provviste per farmi mangiare ?
Glielo chiederemo .
Cosa posso fare ? chiese il giovane .
Casa fare ?
Uccidi il cavallo .
D' accordo .
Uccidiamolo , poi lo facciamo a pezzi e lo mettiamo nel sacco .
Così fecero : misero la carne nel sacco , il giovane salì con tutto il sacco sulle corna dell' aquila , e questa s' alzò in volo .
Mentre volava , si voltò una testa : Ho fame . Poi si voltò l' altra : Ho fame .
Continuò ad andare , e di nuovo : Ho fame .
E l' altra : Ho fame . A un certo punto il cibo finì .
Ho fame , disse una testa .
Il giovane non aveva più nulla : Se non hai da farmi mangiare , ti getterò in mare , disse l' aquila , e lo gettò .
Ma subito ci ripensò : Sarà meglio raccoglierti , altrimenti dall' eremita saranno botte .
Lo rimise sulla spalla .
Giunsero in Spagna e lo lasciò lì .
Il giovane s' accorse che stavano spalando la neve perché doveva sposarsi la figlia del re ch' egli aveva liberato .
Il mattino seguente vide passare le carrozze che portavano la giovane in chiesa .
Mentre passavano , egli salì sopra un tavolo e si mise a far segno con i tre fazzoletti .
La figlia del re li vide e svenne .
Le carrozze allora tornarono indietro . Quando furono a casa , la ragazza mandò una serva con tre fazzoletti uguali .
Disse alla serva : Guarda bene se i fazzoletti del giovane sono uguali a questi ; se sono uguali , fallo venire da me .
La serva andò , vide che i fazzoletti erano uguali e disse al giovane : Vieni con me , la regina vuole parlarti .
Egli si presentò al palazzo reale .
Intanto la figlia del re aveva detto a suo padre : E ' questo l' uomo che io voglio sposare , non l' altro , perché è lui che mi ha liberata dalle streghe .
Il re allora si tolse la corona e la diede al giovane ; gliela mise sul capo e furono celebrate le nozze .
