C' era una volta un giovane che andava in giro .
Aveva preso con sé il cavallo , tre cani e s' era messo in cammino .
Cammina cammina , cammina cammina , giunse a Roma .
Appena arrivò a Roma , sentì suonare le campane a morto .
Chiese a una donna : Che cosa è successo ? Perché le campane , da quando sono entrato a Roma , continuano a suonare a morto ?
Eh , ragazzo mio , suonano perché appena fuori città , qui vicino , c' è una villa con un drago e questo drago ogni giorno deve mangiare una ragazza . Suonano a morto già da stamattina .
Davvero ? disse il giovane .
Allora egli andò , aprì il cancello ed entrò ; c' era lì una cappella ; disse : Beh , signorina , e tu cosa fai ?
Sto pregando , lei rispose , perché alle dodici il drago mi mangerà ; sono costretta a farmi divorare .
Per forza ?
Per forza .
Allora il giovane disse : Va bene .
Tolse la spada dal fodero , la impugnò e cominciò a tagliare le teste al drago .
Appena ne tagliò una , il cane l' afferrò e la portò lontano ; ma , come la portò via , la testa subito tornò indietro .
Allora il giovane staccò al drago l' altra testa e disse al cane : Spezzaferri , prendila e portala lontano !
Il cane l' afferrò e la portò lontano .
Riprese a combattere , a tagliare al drago l' altra testa , e quello faceva : Uh , uh , uh !
Staccò la testa e disse all' altro cane : Lupo , prendi questa testa e portala lontano !
Ricominciò di nuovo a combattere ; c' era l' altro cane : Fioravante , prendi la testa e portala lontano !
La portò lontano .
Il giovane combatteva e i cani portavano lontano le teste .
Alla fine tagliò al drago tutt' e tre le teste e le portò lontano .
Dopo averle portate via , disse : Signorina , sei libera , puoi andare dove vuoi , puoi tornartene a casa ; le teste non si riattaccheranno più ; l' animale , il drago non c' è più , l' ho ucciso .
Dimmi chi sei .
No , no , rispose il giovane , non ce n' è bisogno .
Andò via .
Quand' era per strada , pensò : " Però , quelle teste ...
Se qualcuno passerà e le prenderà , che farà ? dirà che è stato lui a uccidere il drago " .
Allora tornò , tagliò le lingue a tutte le teste , le mise in una borsa e le portò con sé .
Il giorno dopo il re fece bandire : chiunque avesse ucciso l' animale , il drago , aveva il diritto di sposare sua figlia .
S' era trovato a passare uno stacciaio , che riparava le giare , aveva raccolto quelle teste , le aveva messe in un sacco e se l' era portate dietro .
Quando sentì che chi aveva ucciso il drago avrebbe sposato la figlia del re , egli si presentò e disse : Ecco , ho ucciso io l' animale ; qui ci sono le teste .
Bravo , bravo , hai fatto bene !
Sicche tu vuoi sposare mia figlia ?
Perché no ? rispose : la sposo .
Lo vestirono per bene , dunque , lo prepararono , tutto ben pulito , per le nozze .
Ora , immagina un po ' ! c' erano tanti invitati : principi , nobili ...
Quando stavano per celebrare il matrimonio , quel giovane si trovò a passare un' altra volta da Roma e domandò : Cosa c' è qui ? C' è per caso qualche festa , oggi ?
Certo . Si sta sposando la figlia del re ; un tale ha ucciso il drago ed ora sposa la principessa .
Ah , disse il giovane , bravo !
Si diresse allora lì e vide un gran movimento ; bussò per salire sopra , ma c' era l' ordine di non lasciar entrare nessuno .
Egli aveva con sé un cagnolino , bianco ; esso passò tra le guardie e riuscì a salire . Andò dritto dalla principessa il cane conosceva la ragazza , e anche lei lo conosceva , perché , mentre il giovane uccideva il drago , quel cane lo aiutava nell' impresa .
Allora lei corse dal padre : Papà , guarda : qui c' è il cane che ha aiutato il principe a uccidere il drago ; io però non lo conosco , non so chi sia .
Davvero ? disse il re .
Certo .
Giù c' è un tale , continuò il re , che non fanno entrare .
Comandò allora che lo facessero salire , e il giovane entrò nel palazzo .
Quando questi salì su c' erano tutti i principi , i duchi , i conti che mangiavano e banchettavano disse : Permettete che vi dica una parola ?
Perché no ?
Come mai state facendo tanti festeggiamenti ? chiese allora : avete controllato quelle teste ? continuò : c' è qualche animale che non ha la lingua ?
No , risposero : non c' è nessun animale senza lingua ; tutti gli animali devono avere la lingua , gli uomini e tutti .
Fatemi dunque un favore , disse : controllate quell' animale ; guardate se ha le lingue .
Aprirono allora una testa , e non aveva lingua ; aprirono l' altra , neppure ; aprirono l' altra , neppure ...
Le aprirono tutt' e sette : nessuna aveva la lingua .
E dov' è la lingua di quest' animale , del drago ? chiese . Nessuno sapeva rispondere , naturalmente .
Volete vedere dove sono le lingue ? disse allora .
Le prese e le mise fuori dalla borsa : Sono queste le lingue del drago .
Rimasero tutti di stucco , il re per primo .
La principessa riconobbe il giovane e disse : Papà , è lui il giovane che ha ucciso il drago ; ed anche il cagnolino è lo stesso che veniva di tanto in tanto , quand' egli tagliava le teste , a leccarmi le gambe e poi scappava ; proprio questo cagnolino bianco .
Davvero ? disse il re : va bene , dunque .
Allora ordinò subito che l' altro fosse arrestato , portato in piazza , lontano , e bruciato , poiché aveva cercato di ingannarli .
E così finì : la ragazza sposò il principe che aveva ucciso il drago .
