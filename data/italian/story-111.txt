C' era una volta un palazzo sul quale non saliva nessuno .
Stava in aperta campagna .
Chiunque vi andasse ci rimetteva la pelle , perché lì c' erano i diavoli .
Perciò nessuno ci andava più ; venivano tutti uccisi .
Un giorno maestro Achille , il più brutto di Calimera , disse : Stasera andrò io , col mio tavolino , a cucire scarpe .
Si mise a cucire scarpe e a un tratto sentì una voce che diceva : Cado !
E gli spegneva il lume . Egli aveva con sé i fiammiferi e rispondeva : Tu lo spegni e io lo accendo .
Si faceva coraggio .
Poi , dopo aver detto cinque o sei volte : Cado ! ecco venir giù una gamba .
Disse ancora : Cado ! e gettò l' altra . Poi gettò il tronco ; piano piano gettò tutto ; ad ogni parola veniva giù un pezzo .
Si formò un bel giovane ; cadde tutto a pezzi e si formò un bel giovane .
Disse : Ora non c' è più bisogno di uccidere . Tutti quelli che son venuti qui si sono spaventati ; son morti di paura , io non ho fatto niente .
Tu non hai avuto paura . La mattina dopo la gente diceva : Beh , andiamo a prendere maestro Achille ; son morti gli esperti , poteva non morire un balordo ?
Ma lui , quella mattina , faceva il gradasso : si lavava la faccia e s' asciugava con l' asciugamani ; e quelli andavano a prenderlo con la bara !
Appena lo videro , dissero : Caspita , non sei morto ?
Noi abbiamo portato la bara .
Fossi come gli altri !
Così se ne tornarono .
Gettò il tavolino dalla finestra , portò su la moglie e viveva da signore .
Dopo qualche tempo maestro Achille disse : Voglio andare un po ' a caccia .
Andò a caccia in un bosco .
Cominciò a venir brutto tempo ; diventò tutto scuro , come di notte , e il poveretto non sapeva dove andare . Rimase nel bosco .
Scorse un lumicino , vi si diresse e chiese alloggio in una casetta dove c' era un brigante : C' è un po ' d' alloggio ?
Sì , sì .
Il brigante lo fece entrare , poi mise dietro un macigno perché non potesse uscire più .
Maestro Achille disse : E io da dove uscirò ?
Ah , tu vorresti uscire ! Invece non uscirai più ; dovrai restare qui con me , rispose il brigante .
Lì dentro c' erano tanti uomini appesi : A cosa ti servono , quelli ?
A cosa servono ?
Li mangio .
Quando li avrò finiti , mangerò te .
Il brigante li uccideva e li appendeva per tenerli al fresco .
Il povero Achille cominciò a tremare di paura ; il macigno era grande e non riusciva a spostarlo ; l' altro invece era un gigante e lo poteva togliere .
Ma dopo un po ' pensò : " Non mi ha spaventato il primo , dovrebbe farlo il secondo ?
Cercherò una soluzione " .
Cosa escogitò ?
Il gigante aveva un solo occhio : Tu , gli disse dunque maestro Achille , hai un solo occhio ; se ne avessi due , chissà come saresti bello !
Qual è il tuo mestiere ? chiese il gigante .
Faccio il " guasta e aggiusta " ; aggiusto pure gli occhi e , dovessi sistemare il tuo , pure lo farei ; invece d' averne uno , te ne farei avere due .
Sei davvero capace di farlo ?
Certo che son capace .
Vieni qui , vedrai come ti sistemerò .
Gli legò con le funi i piedi , le mani , la testa e lo appese .
Poi accese il fuoco , arroventò lo spiedo e con quello gli bruciò l' occhio .
Il gigante allora cominciò a dar strattoni finché non cadde giù , strappò tutte le funi e cercò di prenderlo ; ma non riusciva a trovarlo perché era cieco .
Quello si metteva sotto le pecore e il gigante , per quanto frugasse , non lo trovava mai .
Dopo aver cercato per parecchio tempo , stanco e affranto dal dolore , cadde giù e s' addormentò .
Allora maestro Achille scorticò una pecora , l' arrostì e se la mangiò .
Poi s' alzò , prese la pelle della pecora e se la tenne .
Il gigante diceva tra sè : " Egli sta ancora qui , ma deve uscire !
Lo scoprirò quando dovrà uscire .
Le pecore sono sei .
Spostò il macigno e aprì le gambe davanti alla porta per far passare sotto le pecore .
Maestro Achille si mise sopra la pelle e uscì ; il gigante lo palpò , credette fosse una pecora e cominciò a contare : E una , e due , e tre ...
Contò fino a sette : Ah , disse , queste son sette !
Ed io sono stato il primo , disse quello da fuori .
Il gigante si disperava ; ma cosa poteva fare più ?
E poi ?
Poi non ho più visto che cosa hanno fatto .
E il racconto è finito .
