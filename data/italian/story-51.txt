C' erano una volta due comari che avevano una figlia ciascuna .
Un giorno se ne stavano al fresco e chiacchieravano : Facciamo un patto tra noi due , disse una : chi di noi muore prima prende la figlia dell' altra : se muoio io per prima , tu prendi mia figlia ; se muori tu , prenderò io tua figlia . Va bene , rispose l' altra ; insomma , fecero il patto e rimasero d' accordo così .
Non passò molto tempo e una delle due comari morì .
Quando stava in fin di vita , aveva chiamato l' altra donna e le aveva detto : Comare , ricordati del patto che abbiamo fatto ...
Sì , sì , prenderò io tua figlia , aveva risposto lei , ma ora pensa a star bene , non preoccuparti !
Due giorni dopo era morta .
La figlia andò a vivere presso la comare ; sicché la donna teneva con sé la figlia e l' altra ragazza .
Però , mentre trattava con ogni riguardo la figlia , non smetteva mai di far torti alla figliastra .
Aveva una masseria e mandava la povera ragazza a guardare le mucche ; la figlia invece se ne stava a casa e non faceva altro che mangiare e dormire ; arrivava la sera e la madre le diceva : Figlia mia , fa ' la piscia e va ' a dormire .
E lei pisciava e andava a letto .
Alla figlia della comare , invece , diede un rotolo ( * ) di canapa e le ordinò di andare alla masseria : Va ' a guardare le mucche e stasera la canapa dev' esser completata , altrimenti , quando verrò , per te sarà la fine .
La povera ragazza prese la canapa e s' avviò alla masseria . Appena giunse , le venne incontro un vecchio e le disse : Ragazza mia , mi spidocchi un po ' ?
Ti spidocchio , ma solo per poco , perché devo filare tutta questa canapa , altrimenti stasera la mia matrigna mi uccide .
Non preoccuparti , rispose il vecchio , te la filerò io la canapa .
Va bene .
Cominciò a spidocchiarlo e , mentre lo spidocchiava , il vecchio domandò : Cosa c' è sulla mia testa ?
Perle e gioielli .
Perle e gioielli sulla tua fronte , disse il vecchio .
Ora l' avreste dovuta vedere , col capo pieno di perle , il volto bello e ingioiellato , tutta piena d' oro !
Di nuovo lo spidocchiava e , alla domanda cosa avesse , rispondeva sempre perle e gioielli ; e lei sempre di più si riempiva d' oro .
Poi il vecchio prese la canapa , la mise sulle corna dei buoi , la filò e gliela restituì tutta filata .
La ragazza tornò a casa piena d' oro .
La matrigna , appena la vide risplendere da lontano e così bella , provò invidia , perché la figliastra era bella e la figlia invece brutta , e volle sapere come avesse fatto a diventare così bella .
La povera ragazza raccontò allora tutta la storia del vecchio , che in realtà era san Giuseppe .
La matrigna prese un altro rotolo di canapa e mandò alla masseria la figlia .
La ragazza s' avvio e , appena arrivò , le venne incontro il vecchio e le disse : Dove vai , ragazza mia ?
Dove ? A guardar le mucche alla masseria .
Mi spidocchi un po ' ?
No , non posso spidocchiarti ; devo filare la canapa che mi ha dato mia madre .
Te la filerò io , disse il vecchio .
Già , fece la ragazza brutta , e tu , vecchio come sei , sapresti filare ?
Certo che so farlo , rispose il vecchio .
Prese la canapa , la mise sulle corna dei buoi , la filò , la raccolse con l' aspo e gliela restituì .
Adesso mi spidocchi ?
Ti spidocchio , ma per poco ; devo tornare a casa perché la mamma mi aspetta .
Cominciò a spidocchiare il vecchio ; dopo un po ' egli chiese : Cosa c' è sulla mia testa ?
Cosa hai detto ? Non ho capito .
Cosa c' è sulla mia testa ? ripeté .
Cosa ? Pidocchi e lendini , rispose la ragazza .
Pidocchi e lendini sulla tua fronte .
Ora l' avreste dovuta vedere riempirsi di pidocchi e lendini : sulla faccia , sui vestiti , dappertutto .
Quando tornò a casa , la madre la vide così piena di pidocchi e di lendini e se la prese con la figliastra ; cominciò a sgridarla e , per dispetto , la mattina la svegliava al canto del gallo , e la sera la faceva restare a lavorare , alla luce della lucerna , fino al canto del gallo , e non la lasciava mai dormire .
Alla figlia diceva , quando suonava il vespro : Fa ' la piscia e va ' a dormire , e la figlia pisciava e andava a letto ; la figliastra invece doveva rimanere fino al canto del gallo .
Pensava , la matrigna , di fare un dispetto alla figliastra ; al contrario , le faceva un favore ; alla figlia invece , cui credeva di far del bene , faceva un danno .
Passò il tempo e le ragazze cominciarono a ricevere i fidanzati : la bella era corteggiata da un artigiano , la brutta da un contadino .
Dopo un po ' si sposarono . La ragazza abituata a lavorare fino al canto del gallo teneva la sua casa in ordine come una farmacia ; quella cui la madre diceva " fa ' la piscia e va ' a dormire " teneva la casa peggio d' una stalla per animali : faceva lì i suoi bisogni e , ad entrarci , bisognava sollevare il vestito .
La madre andò una volta dalla figliastra e vide la casa così ben sistemata da sembrare una farmacia ; andò poi dalla figlia e notò il disordine e la puzza .
Allora cominciò a riflettere e confrontare la casa delle due ragazze .
Un giorno disse alla figlia : Figlia mia , comprati un maiale , raccogli tutti gli avanzi in una vasca e conservali per nutrire il maiale .
La figlia capì che ci doveva mettere rifiuti , escrementi e porcherie , e sistemò la vasca dentro casa , sicché lì non si poteva più entrare ; c' era da morire per il fetore .
Nella casa della figliastra che vegliava fino al canto del gallo era invece così pulito da farti risanare .
Una volta c' era una festa al paese della madre , e lei volle invitare le figlie a casa sua per il pranzo .
Arrivò il giorno dell' invito ; la ragazza che vegliava fino al canto del gallo si mise in bell' ordine , montò sul cavallo e partì avanti ; l' altra " fa ' la piscia e va ' a dormire " prese un asino spellacchiato , vi salì tutta sbrendoli , piena di pidocchi e sporca , e si trascinava fiaccamente ; l' altra invece faceva risplendere dovunque passasse .
La madre s' affacciò a controllare se le figlie stessero arrivando e subito i suoi occhi s' illuminarono dello splendore della prima ; poi guardò dietro e scorse la figlia sull' asino spellacchiato .
Quando notò tanta differenza , concluse : Colei che vegliava fino al canto del gallo bella e fiera vedo arrivar sopra il cavallo ; l' altra invece " fa ' la piscia e va ' a dormire " utta sporca e trasandata vedo venire . Credevo di far del bene a mia figlia e , al contrario , la danneggiavo ; all' altra credevo di far del male e senza saperlo l' avvantaggiavo .
Poi si rivolse alla figlia e disse : Tu va ' pure a vivere tra le tue sporcizie , e alla figliastra : E tu nel tuo ordine e nella tua pulizia .
Così ognuna se ne tornò a casa .
La figlia ben presto morì : l' eccessiva sporcizia la fece ammalare a morire . La matrigna andò a vivere con la figliastra , e se ne stava , nella delizia e nell' ordine , a rammaricarsi del male che le aveva fatto e che s' era tramutato in bene , e del bene che aveva fatto alla figlia e che s' era tramutato in male .
