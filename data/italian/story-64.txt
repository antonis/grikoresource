C' erano una volta una madre , un padre e una figlia ; la madre era locandiera , e la figlia che aveva era molto bella ; lei però era più bella ancora ; insomma , erano tutt' e due bellissime .
La madre era gelosa della bellezza della figlia , sicché la portò sopra un palazzo e ve la chiuse , mentre lei , essendo locandiera , stava giù a ricevere i soldati e l' altra gente ; e a tutti gli uomini che passavano da lì per dormire domandava : Voi che venite da Bari , avete mai visto una donna più bella di me ?
No , rispondevano .
Poi salivano su per dormire .
Quando s' alzavano la mattina , di nuovo la locandiera ripeteva la stessa domanda .
Ed essi rispondevano : No , no , non abbiamo visto nessuna più bella di te .
Chiunque passasse , faceva sempre la stessa domanda .
Una volta si fermarono lì due giovani per dormire ; passarono dalla locandiera e lei domandò : Giovani che venite da paesi lontani , avete visto qualcuna più bella di me ?
No , risposero .
Arrivò la notte e dormirono nelle stanze di sopra . Uno s' addormentò subito , l' altro no .
Quest' ultimo allora pensò : " Perbacco , son venuto tante volte su questo palazzo e non ho mai girato per le stanze a vedere cosa c' è ! " Mentre così pensava , s' accorse d' una camera piena di luce : splendeva , la stanza , per la bellezza della ragazza che era dentro !
Sbirciò da una fessura e la vide : " Dio , com' è bella ! " disse .
Ne restò subito affascinato . " Per questo , pensò , la madre ci chiede se abbiamo visto qualcuna più bella di lei ; ha ragione a chiedercelo ! " Tutta la notte spiava dalla fessura e la guardava .
La mattina i due giovani si prepararono per partire .
Scesero dalla madre , la quale ripeteva la sua domanda ogni volta che scendevano o che venivano da fuori ; lei disse di nuovo : Cari giovani , avete visto qualcuna più bella di me ?
Sì , essi risposero . " Ah , lei pensò , l' hanno scoperta !
Era passato tanto tempo senza che nessuno la vedesse ! " Poi si rivolse al marito e disse : Adesso cosa facciamo ?
Io voglio farla sparire da qui .
La madre era gelosa .
Cosa possiamo fare ? disse il padre , il quale provava dispiacere per la figlia .
Facciamo così , propose la madre : diciamo che vogliamo uscire insieme per una passeggiata ; cuciniamo e ci mettiamo in carrozza per andar fuori a pranzare ; poi , quando arriveremo là dove vogliamo abbandonarla , fingiamo di aver dimenticato le forchette , la lasciamo e torniamo .
Va bene , rispose il padre .
Andarono a proporlo alla figlia , ma lei , poveretta , non aveva visto mai anima viva ; conosceva solo il padre e la madre ; sicché , quando le chiesero di uscire a passeggio , non voleva in nessun modo .
Riuscirono a ingannarla e a portarla fuori .
Giunti vicino a un bosco , la fecero scendere .
Apparecchiarono tavola , ci misero sopra tutto , poi il padre e la madre dissero che mancavano le forchette : Adesso , figlia mia , devi restare qui , disse la madre : noi andiamo a prendere le forchette e torniamo .
La ragazza non voleva , ma dovette restare e , quando i genitori partirono , non fece che piangere .
Mentre piangeva , vide nel bosco un branco di animali e avanti c' era un leone : Vieni da questa parte , disse il leone , e le fece un cenno con la zampa .
Poi s' accorse che gli altri animali s' erano lanciati verso di lei e di nuovo le indicò di spostarsi , di scappare , togliersi di là .
Allora lei corse fuori dal bosco .
Gli animali arrivarono vicino alla tavola imbandita e si fermarono .
La ragazza , con l' aiuto del leone , si ritrovò in una pianura .
Il leone la difendeva , voleva il suo bene e , appena l' ebbe messa in salvo , scomparve .
La poveretta continuò a piangere , su quella pianura .
Giunse la notte ; trovò un masso , vi si poggiò e s' addormentò .
Passarono due briganti e la videro dormire .
Uno disse : Com' è bella !
La prendiamo ? disse l' altro .
Prendiamola .
La presero , la portarono in un' altra pianura e , dopo aver camminato parecchio , la lasciarono .
La poveretta si ritrovò sola ; non vedeva che pianure , neppure un muricciolo , niente .
Si mise a camminare giorno e notte per quelle pianure ; camminava e piangeva .
La notte successiva continuava a non vedere niente , ma a un tratto s' imbatté in un palazzo : era il palazzo dei briganti .
Lei , che da tanto tempo ormai non toccava cibo , salì su e cominciò a mangiare a volontà .
Quando fu sazia , rigovernò , pulì la stanza , mise in ordine .
Poi , stanca , poggiò il capo sopra un mucchio di carboni e s' addormentò .
I briganti , al loro ritorno , videro il palazzo aperto e si precipitarono : pensavano ci fosse dentro un ladro .
Girarono in fretta per tutto il palazzo ; erano tredici briganti , più il capo , quattordici .
Cercarono il ladro dappertutto , na del ladro non c' era traccia .
Allora si sedettero e mangiarono .
Ora , dovevano cucinare per il giorno seguente , per quando sarebbero tornati .
Il cuoco andò a prendere i carboni per cucinare e trovò lei che dormiva .
Per la fretta di comunicare agli altri che aveva trovato il ladro , non guardò neppure se era maschio o donna .
Subito vennero i briganti : Uccidiamola , uccidiamola !
Lei è il ladro , dissero .
Ma il capo propose : Lasciamola qui ; non deve far altro che cucinare e tenere la casa in ordine .
Va bene , lasciamola , fecero gli altri .
La tennero con loro e le dissero : Tu rimani qui ; vogliamo che cucini , faccia le pulizie e tenga tutto in ordine .
Dopo diversi giorni , passò una vecchina a chiedere l' elemosina : Padrona , fammi la carità .
Io non posso dare niente di quello che c' è sul palazzo , rispose la ragazza ; ma poi pensò : " Le darò un po ' di pane ; pure io ho trovato aiuto quando sono arrivata qui " ; e allora disse : Ho solo un po ' di pane , lo vuoi ?
Prenderò ciò che mi dai , rispose la vecchia .
Le diede il pane e s' avviò per andarsene ; mentre scendeva le scale , la ragazza le chiese : Nonnina , mi pettineresti i capelli ?
Certo , rispose la vecchia , ordina e io ti servirò .
La pettinò , poi se ne tornò a casa .
Passò parecchio tempo e la madre della ragazza seppe che la vecchia era stata dalla figlia .
La fece chiamare e le disse : Tu sei stata da mia figlia , vero ?
Sì , sì , ci sono stata .
Ci potresti tornare ?
Mi vergogno ad andarci di nuovo , rispose la vecchia .
Vai a chiedere la carità , fatti dare qualcosa .
Lo farai ?
Va bene .
Quando ti avrà fatto l' elemosina , dille : Vuoi che ti pettini ?
Hai i capelli in disordine .
Poi , eccoti uno spillo : dopo che l' avrai pettinata , ficcale lo spillo in testa .
Va bene , disse la vecchia .
Andò e chiese l' elemosina .
La ragazza le diede un po ' di pane .
Quando stava per andarsene , la vecchia domandò : Vuoi che ti pettini ? Hai i capelli in disordine .
Pettinami .
Nel pettinarla , alla fine , le ficcò lo spillo ; si trattava di uno spillo stregato e la ragazza restò subito immobile , con gli occhi aperti , come se fosse viva e non morta .
Così come si trovava , così rimase : seduta sulla sedia in mezzo alla casa .
La vecchia la lasciò lì , prese tutto quel che le serviva e se ne andò .
La sera , prima che tornassero i briganti , passò di lì il figlio di un re sul suo cavallo , accompagnato da un servo .
Disse : Com' è bella quella ragazza !
Credi che sia morta o viva ?
E ' morta , rispose il servo .
Io dico che è viva .
Salgo su ?
No , disse il servo ; sarà meglio andar via ; la ragazza è certamente morta .
Il figlio del re salì e vide che era morta .
La prese con tutta la sedia , la caricò sul cavallo e la portò con sé .
La mise nella sua camera e , da allora , quella stanza , prima continuamente rigovernata e pulita dalle serve , restò chiusa .
Il giovane la teneva serrata e non permetteva a nessuno di metterci piede .
Dopo parecchio tempo , un re vicino fece chiamare il giovane , perché c' era una guerra .
Egli allora affidò le chiavi della sua stanza alla madre , raccomandandole di non aprirla mai , per nessun motivo .
Le serve presero alla padrona le chiavi nascoste sotto il cuscino : Diamine , dissero , dobbiamo vedere cosa c' è là dentro ch' è sempre chiuso !
Aprirono , rovistarono dappertutto e non trovarono niente .
Cercarono di nuovo : niente .
Il figlio del re aveva fatto una nicchia nella parete e aveva messo lì la ragazza morta .
Le serve osservarono la parete e videro uno sportello ; l' aprirono e trovarono la ragazza : Com' è bella ! esclamarono , subito affascinate .
Fatemela togliere da lì e pettinarla , disse una di loro .
La presero con tutta la sedia e si misero a ordinarle i capelli .
Mentre la pettinavano , saltò fuori lo spillo e lei immediatamente ritornò in vita .
Le serve per la gran paura corsero a ficcarsi nel letto e non si curarono neppure di chiudere la porta della stanza ; lasciarono aperto .
La ragazza restò nella stanza per tutta la notte , finché non spuntò l' alba .
La mattina , quando la padrona e le figlie s' alzarono , cominciarono a domandare alla sconosciuta : Tu chi sei ?
Come mai ti trovi qui ? Lei si mise a raccontare ogni cosa .
Allora le sorelle del giovane la presero con sé e la trattarono con ogni riguardo , come fosse una di loro .
Dopo molti giorni il fratello ritornò ; non appena arrivò in paese , vide da lontano la ragazza , che stava sul balcone .
Venne a casa e gli riferirono tutto per filo e per segno .
Allora volle prenderla in moglie .
Si sposarono .
Molti giorni dopo , il re d' un regno vicino lo fece chiamare di nuovo e dovette andar via .
Raccomandò la moglie alle sorelle , le quali la trattarono con ogni premura e con affetto .
Arrivò il momento che doveva partorire e il marito era ancora lontano .
Subito dopo il parto , le sorelle mandarono al fratello il messaggio : " Tua moglie ha partorito e ha dato alla luce due bellissimi bambini , più belli della madre " .
I messaggeri che portavano la notizia al marito dovettero far tappa presso la locandiera , la madre della ragazza .
La locandiera li conosceva e sapeva anche che essi avevano con sé la lettera della figlia .
Come faceva a saperlo ?
Chi lo sa !
Forse aveva le sette arti .
Allora lei prese la lettera , la lesse e la strappò .
Ne scrisse un' altra fingendo che erano le sorelle a scrivere e raccontò che la moglie aveva partorito e aveva fatto due pezzi di legno : decidesse il da farsi con questa storia ; esse non la volevano più con loro .
Il poveretto , nonostante tutto , rispose : Dite a mia moglie di non preoccuparsi e di non stare in pensiero : non fa niente .
Il messaggero passò di nuovo dalla locanda e vi alloggiò per la notte .
La locandiera di nuovo trafugò la lettera , la lesse e la strappò . Ne scrisse una nuova , dicendo che le tagliassero le mani e la cacciassero via , oppure che la portassero alla forca .
Le cognate , poverette , non volevano ucciderla .
Le tagliarono allora le mani , le misero un bambino su ogni braccio e la mandarono via .
I bambini cominciavano a dire le prime parole , e uno di loro chiese da bere .
Vide una vasca d' acqua e la donna accostò il piccolo per farlo bere , ma , mentre l' avvicinava , gli cadde dentro .
Cercò d' afferrarlo e le cadde l' altro .
La poveretta s' affannava per prendere i bambini e , mentre annaspava , le tornarono le mani come prima .
Tirò fuori i bambini e riprese la sua strada .
Cammina cammina , cammina cammina , incontrò un giardiniere ; era arrivata a Torino .
Cosa fai tu qui ? egli chiese .
Ah , se ti raccontassi la mia sventura !
Vuoi venirtene con me ? disse l' uomo : non devi far altro che occuparti della cucina e allevare questi bambini ; io andrò nel girdino e coltiverò qualche pianta .
S' accordò col giardiniere e vivevano insieme .
Passato molto tempo , il marito fece ritorno a casa .
Cercò la moglie e non la trovò .
Infuriato , voleva uccidere le sorelle : perché mai l' avevano mandata via ?
Ma le poverette non avevano fatto che eseguire gli ordini della lettera .
Sicché egli se ne stava sempre chiuso nella sua stanza .
Un giorno volle uscire e si ripropose di andare a Torino .
Stava per arrivarci e incontrò due bambini bellissimi : " Oh , come sono belli questi bambini ! pensò : e dire che dovrei averne due anch' io " .
Va ' a indovinare che erano i suoi !
Gli parvero così belli che comprò loro un regalo e glielo diede .
I bambini corsero a raccontarlo alla mamma : Mamma , dissero , è passato un signore e ci ha dato un regalo .
Ed egli aveva pure detto ai bambini che , quando sarebbe tornato dal paese dov' era diretto , gliene avrebbe portato un altro .
Anche questo riferirono alla madre .
Allora la madre disse ai figlioletti : Quando tornerà e vi darà il regalo , voi avvicinatevi e dite : Per l' amore che vi ha condotto qui , vogliamo dare un bacio al nostro papà . Poi vi gettate al collo e lo baciate .
I bambini così fecero .
Quando passò da lì , il signore portò loro un bel berretto : Venite , disse poi : vi porterò all' osteria e vi farò mangiare e bere .
Li portò giusto all' osteria della madre lei aveva un' osteria .
La madre lo vide e lo riconobbe : " Ahimé , mio marito ! " Apparecchiò una buona tavola e lo fece mangiare con i bambini .
Allora essi gli si gettarono al collo e lo baciarono : Cosa sono questi baci ? egli domandò : Perché mi baciate ?
Ah , rispose la moglie , tu non sai niente . Non mi riconosci più ?
Cominciarono tutt' e due a raccontarsi la loro storia , nei minimi particolari .
Poi presero la carrozza , fecero salire i figlioletti e partirono .
Il giardiniere restò solo nel giardino .
Essi vissero felici e contenti ; non erano mai stati meglio di così .
