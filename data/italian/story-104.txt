C' era una volta una madre che aveva tre figlie ; non avevano padre e vivevano sole con la madre in un bosco . Le due figlie più grandi erano molto belle , ma la piccola era terribilmente brutta ; era balbuziente , guercia , ce le aveva tutte .
Le sorelle maggiori la stuzzicavano dicendole ch' era brutta , la offendevano e la maltrattavano ; ripetevano di volerla gettare giù dal palazzo abitavano in un palazzo nel bosco .
Le sorelle tessevano sempre , a lei invece toccava filare la stoppa ; e cantavano lassù .
Un giorno passò il figlio del re , sentì cantare e s' avvicinò al palazzo ; andò vicino alla porta per bussare : Apritemi , disse , voglio vedere le ragazze che stanno quassù .
La madre rispose : Non puoi guardare le mie figlie , perché ce n' è una che , se l' aria la vedesse , se la porterebbe via .
Egli disse : Vorrei guardarla almeno da una fessura .
E la madre : Oggi non è possibile .
Vieni domattina e la vedrai .
Il giovane se ne andò contento a casa ; non vedeva l' ora che spuntasse l' alba per tornare nel bosco .
La mattina andò e le sentì cantare .
Bussò : Apritemi , la voglio vedere .
Guarda , rispose la madre , lì c' è una fessura ; le vedrai il dito , altrimenti l' aria se la porterà via e resterò senza figlia per causa tua .
Allora gli mostrarono il dito attraverso la fessura ; le sorelle maggiori , le belle , avevano preso la più piccola e l' avevano portata dietro la porta .
Alla poveretta , che stava sempre a torcere la stoppa e la lisca , era venuto fuori un bel dito bianco e rosso ; e misero nella fessura quel dito .
Il re lo vide : Sappiate , egli disse allora , che domani verrò per bandire che voglio sposarla ; non ho neppure bisogno di vederla : se il dito è così bello , chissà come dev' essere bella tutta !
Era stato ammaliato da quel dito bianco e rosso .
La ragazza diceva : Lasciatemi !
E le sorelle la tenevano lì : Zitta , stupida , sposerai il re .
Lasciatemi , mi getterà giù dal balcone !
Era balbuziente , la povera ragazza : Sposalo tu , diceva alla più grande , che sei bella .
Zitta , rispondevano , tu sei sciocca ; se lo sposerai , diventerai regina .
E poi mi getterà dal balcone , lei ripeteva .
Lasciamo costoro e andiamo ora dal giovane che era tornato dalla madre e s' era messo a raccontare d' aver trovato un fior di ragazza in un altro paese .
Egli era ancora piccolo di età e andò a chiedere al padre : Papà , devi farmi sposare ; ho trovato una ragazza molto bella .
E ' così bella che non vorrei perderla .
Va bene , rispose il padre , quando vuoi , fa ' bandire e sposati .
Era pure figlio unico .
Partì e venne nel bosco dove esse abitavano . Bussò alla porta , perché gli aprissero assolutamente : Io , diceva , voglio vederla in volto , voglio vedere che tipo è .
Era infatti indispettito : doverla avere per fidanzata e non sapere com' era !
No , no , rispose la madre , se vuoi mia figlia , vieni domenica mattina per bandire .
Possibile , egli disse , che io debba andarmene via senza averla vista , senza aver parlato un po ' con lei ?
Puoi parlare o piangere finché vorrai , disse la madre , tu mia figlia non la vedrai se prima non l' avrai sposata .
Beh , vi saluto , concluse il giovane , domenica mattina preparerò i documenti e farò bandire .
Bandisci , disse la madre .
Tornò in paese , preparò i documenti e bandì .
Bandì che avrebbe sposato quella ragazza , che si chiamava Addolorata .
Dopo aver bandito , la domenica , andò di nuovo dalle ragazze , dalla fidanzata : Io ho bandito , disse , sempre da dietro la porta .
Davvero ? Davvero ? si misero a fare le sorelle .
Sì , davvero .
Se ne rallegrarono .
Poi egli disse : Oggi , prima d' andarmene , voglio assolutamente vederla .
Tu non la vedrai mai , rispose la madre ; se vuoi venire , vieni , altrimenti non venire più .
Il giovane pensò : " Voglio proprio vedere come andrà a finire ! " Poi si congedò e disse : Io me ne vado , statevi bene ; sappiate che domani manderò il sarto a prenderle le misure per l' abito .
D' accordo , rispose la madre , ma devi dirci a che ora verrà .
Domani , verso il tardi .
Siate pronte e fatelo entrare , ché deve prender le misure di persona .
E aggiunse : Me ne vado , devo preparare tutto il resto .
Se ne andò .
La mattina seguente , sul tardi , mandò il sarto : Devi andare in quel bosco , gli disse : c' è un palazzo e sopra ci sono tre ragazze ; bussa e ti apriranno .
Questi arrivò e bussò : Chi sei ? domandò la madre .
Sono stato mandato dal vostro fidanzato ; devo prender le misure alla sposa per farle l' abito .
Passami il metro da questa fessura , disse la madre ; prenderò io le misure a mia figlia . Lei era tutta sciancata : aveva i piedi storti e la gobba .
Come ? rispose il sarto alla madre : io vengo , mandato dal re , e tu non mi fai prendere le misure a tua figlia ?
E la madre , da dentro : Dammi qui , prenderò io le misure .
Allora , invece di prender le misure alla sciancata , le prese all' altra .
Prese le misure e le diede da portare al sarto .
Il sarto ritornò dal re ; questi andò a procurarsi la stoffa e il vestito fu cucito .
Il giovane venne poi dalle ragazze , dalla fidanzata e disse : Sappiate che domenica prossima ci sposeremo .
Parlarono un po ' attraverso la porta , poi egli si rimise in strada e tornò al suo paese .
La domenica cominciò a invitare tutti i signori , perché si doveva sposare con quella ragazza .
La mattina fece venire dieci carrozze per andarla a prendere .
Andarono e bussarono ; avevano l' abito da far indossare alla sposa .
Datelo a me , disse la madre , la vestirò io .
Come ? fece lo sposo : senza che io entri a vederla ?
La madre chiuse tutte le porte e ripeté : Vestirò io mia figlia .
La vestì , anzi le gettò addosso a casaccio i vestiti : pareva una stracciona .
Ora , erano tutti lì per accompagnarla , ma la madre disse : Nessuno si avvicini alla porta ; prendo io mia figlia e la porto nella carrozza .
Il povero figlio del re esclamò : Possibile che debba portarla a casa senza poterla vedere ?
La madre mise la figlia nella carrozza : Beh , disse , qui dentro staremo io e mia figlia .
Arrivarono in paese .
Il padre dello sposo aspettava per avvicinarsi e dare la mano alla nuora : Andate via di qui , disse la madre , devo accompagnarla io stessa in una stanza , altrimenti , se l' aria la vede ...
Entrarono tutt' e due in una stanza .
Il re servì loro il rinfresco : Date tutto a me , disse la madre , lo porterò io a mia figlia .
Il figlio de re pensò : " Chissà che diavolo mi ha portato oggi quassù ! " Giunse mezzogiorno e volevano farla mangiare a tavola : Portate qui , mangeremo io e mia figlia , continuò a dire la madre .
Pazienza , le portarono da mangiare .
Il pomeriggio , poi , andavano i signori a far la visita e lei se ne stava tutta sola e non voleva che entrasse nessuno nella sua stanza .
Si fece buio e finalmente la madre prese a congedarsi dalla figlia ; le disse : Io me vado ; verrò domani con le tue sorelle a farti visita .
Mamma , voglio venire con te .
Zitta , figlia mia , adesso tu rimani col figlio del re ; tu sei stata così fortunata a prendere lui , la consolava la madre .
Stasera il re mi getterà dal balcone , non mi vorrà .
Beh , statevi bene , disse infine . Poi partì e se ne andò .
Il figlio del re non vedeva l' ora che la madre partisse per poter andare a vedere com' era la ragazza .
S' affrettò nella stanza ; appena entrò , scorse una specie di ammasso rannicchiato in un angolo che piangeva , poveretta : Vieni su e fatti vedere ; adesso non c' è più aria che ti possa divorare .
La fece venir fuori e la tirò verso l' altra stanza .
Appena la vide cominciò a imprecare : Carogna , cosa mi ha combinato tua madre !
Vieni , fatti buttare giù .
La prese in braccio e la gettò a terra dal balcone .
La poveretta cadde e restò tutta stordita .
Allora s' alzò , storta com' era , e s' incamminò per andare da Michelangelo .
Infatti lei chiedeva sempre alle sorelle : Perché voi siete belle ed io sono brutta ?
Perché noi siamo andate da Michelangelo , rispondevano .
Disse dunque : Michelangelo , voglio che tu mi faccia bella come hai fatto le mie sorelle .
Egli rispose : Stenditi qui ! c' era lì un gran tavolo dove tagliavano la carne Rocca , prepara una pentola d' acqua bollente .
La moglie mise a bollire l' acqua .
Michelangelo prese un boccale e lo riempì d' acqua bollente : Sta ' ferma ! disse .
Va bene , purché mi faccia bella .
Certo , egli rispose , e le gettò l' acqua addosso .
Ah ! si mise a gridare , quand' egli la bruciò .
E Michelangelo : Tien duro , vecchierella , se vuoi sembrare bella .
Lei rispose : Durare mi vedrai , se bella mi farai .
Certo che ti farò bella , egli disse .
Ed è finita .
La bruciò e la lasciò tutta raggrinzita sul tavolo .
