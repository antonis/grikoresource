io ' mia forà ce ìone ena ' liko ce mia alipuna ; ce stea ' tui ìunna pu ' e kanna ' kammia .
mian attes foré , ipe o liko tis alipuna : 'En ènghize na piàme ena ' korafi n' o spìrome ?
o pìannome , ipe e alipuna . piaka ' citto korafi .
o vrai kama ' tokontratto . o pornò skòsisa ce pìrtane .
ftàsane sto korafi ce nsignasa ' na skàfsune .
tui , e signura , stràkkefse , e alipuna . evò , ipe cini , likùddhimu , stràkkefsa .
evò pao ce kratò i ' serra c' esù skafse .
ce arte pai ce me finnis ? ipe cino .
ma motti pu e serra ste ce petti , evò pao c' i ' kratò ipe cini .
tuos èbbianne ce polema sto ' fàttottu ce piste ti e ' lipuna ste ce kratì i ' serra .
tui ti èkanne ? ìbbie votonta ; ecì pu ìvriske fai , eddhe .
doppu ìbbie tui ce kòrdonne tra tra , pu ìbbie ' cessu ste ' massarie doppu èguenne pu iche na pai ' cì cino , èvaddhe mia ' rikotta panu sti ' ciofali , ìbbie ' mbrò cino : Ah , ti èndesa sìmmeri , ' cì pu krao ' ti ' serra !
diake ena ' pekurari , mò' sire mia ' botta ravdì ce mò' guale a mialà .
prai polema , ti ènna spiccèfsome o korafi , èkanne o likuddhi .
respùndefse e alipuna : evò su leo : prai na pame essu , ti ' e resisteo pleo ; i vasta i ' pantsan gomai .
nsomma èbbie tui ce pirtan essu .
skùriase .
es ti ' stra ' tappu pìane : vu ! ' e fidèome na kamo i ' stran , ipe e alipuna . 'En ènghize na me piai lion akkanci ? tù' pe u liku .
Ah , ipe cino , ka ' vò telo piammenos evò !
ce umme , ce deje , ce piamme ... in èbbie ' kkanci .
dopu este pànutu , tui e alipuna , èbbie ce ntrikefti na kantalisi : nana , nana , lu malatu porta la sana !
ce cino ' e fela , ti ìbbie strakko : Ah , karogna , ipe o liko , allora kannis iu jatì pai kordomeni ! ?
c' ipe ti ' e sozi !
èbbie c' in eskiòppefse sto ' lakko .
beh , stasu ' ttù , ti ' vò pao essu , ipe . vu , ti mò' kame !
ka ' sù me ruìnefse pleo poddhì , ipe e alipuna .
nsomma , na mi tin bastàfsome poddhì makrea , pirtan essu .
ftiasa ' na fane ce cini ' e telise makà na fai : ìbbie kordomeni .
minkiala , prai fae ! 'E telo , ka ' sù me skuàjefse ; pos ènnà' rto na fao ?
stasu , ti tro ' vò .
piaka ' ce plòsane , e alipuna ce o liko .
o pornò fsemèrose . meh , prai ce aska ce pame ' ttofsu !
amon esù , pu efes eftè sto vrai .
ènnà' rti puru ' sù .
evò èrkome ; ma , an èrkome , i ' serra kratovo .
Eh , tappu ftàsane ' cì , èkame i ' stessa tevni .
na mi ' tin bastàfsome makrea , èkame itti tevni mian addomà , e alipuna u liku .
nsomma , èftase pu o terìsane , utto krisari .
o pìrane st' aloni , o lonìsane , o kulumosa ' ce o fìkane .
beh , is ipe is alipuna o liko , epame na piame itto krisari ?
umme , umme , ipe cini . kànnome is pu ftazi proi to pianni ?
va bene , ipe o liko , is pu ftazi proi to pianni .
èbbie i ' stra ' tui ce nsìgnase na pratisi . ena ' punton ampì , tappu tui icha ' kàmonta o ' patto , icha ' pàronta o quartuddhin ecianu ; ce o liko iche vàlonta ena ' sciddhon ecessu ce on iche sciopàsonta mo ' sakko . ftàsane .
tappu ftàsane ' cisimà , tui arte , pu este pleon ambrò , ìbbie cherùmeni ti ftazi ce fsikkonni to krisari . kumpare likuddhi , evò èftasa .
ce piatto , ipe cino .
chiusti sto quartuddhi tui , pu este sciopammeno .
pirte n' o fsesciopasi , na fi o ' sakko . fèonta o sciddho !
in efsìkkose atto sfòndilo c' in errebbàttefse .
meh , pia ' o krisarin arte , ipe o liko .
nkora ste ce rebbatte . tò' bbie o liko , pu tù' che polemìsonta , ce èmine olo kuttento .
