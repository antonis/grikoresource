io ' mia forà ce ìone o bonànima tu papa Giuanni . pianni ce ste c' ìbbie na pi lutria .
diaike attin Bicentsa , pu toa este ' mbrò stin aglisia , ce ide mia ' stotsa sarditsa . èbbie c' is ipe : Vicentsa , kràimmu utti sarditsa , ti depoi , tappu guenno atti ' lutria , ti ' pianno .
èbbie cini : Papa Giuvànnimu , o ènn' i ' piais ... ipe , ka tui e ' ' vuta poddhé poddhé cèrkite. ena ' kilon e ' cini , ipe .
ma ènna po lutria , ipe cino ; beh , fseri ti kame ?
fere , fere , in vaddho sti ' punga kau sti ' suttana .
in èvale sti ' punga pu kau , ce iche ena ' sciddho ce u mirìzato sti ' punga .
proppi nà' mbi stin aglisia , pianni ìbbie o sciddho : Prista a diàulu ! ipe cino .
ce cino panta o ' kulusa .
pirte sti ' sakristia na ndisì , ce o sciddho puru pirte ' cessu : ìsele anfortsa i ' sarditsa cino .
cino èbbie o ' kàleko ce pirte , ce o sciddho kulusa .
Prista fore ! ipe cino sadia sadia .
dopu ' nneke st' artari na pi lutria , cino este mo pensieri cinù sciddhu . pianni ...
fsèrete ti echi ena ' lò ' , motti leu ' lutria , pu o jutanto tu jerni o ' kàmiso na ' nnei ...
pianni , o jutanto tò' bbie o ' kamiso ; cino piste ti ene o sciddho pu tò' bianne i ' sarditsa .
Prista a diàolu ! ipe , ce u doke mia ' lattea , ce on èbbie sta dòntia . èbbie depoi e kristianì on eskosa ' pu ' ciumesa .
cino fonàzonta : mamma mia , mamma mia !
m' esfafse .
o maro patera vòtise olo spramentuso : ti ene , ti ene ?
esù fseri ti tò' kame . e mari kristianì , arte , sozan efseri ti ene ?
ènghise na kuntefsi o prama kundu pu ìone : iche ena ' sciddho ... Evò vasto quai sarditsa ce èrkato panta ...
pisto ti ìone o sciddho ...
ce depoi ?
ce depoi ?
o kunto ' e pai pleo , lio sarditsa mereteo .
Mah , ' e tin ìsela àrtena ...
