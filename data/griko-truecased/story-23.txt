io ' mia forà ce ìone mia ' mana ce tris kiatere ce icha ' mia ' tsia fata .
ce ' en iche pleo fsomi usi mana na doi os kiaterò .
pianni mia ' nifta epense pos iche na kami n' es guali pu essu ; ce e tsia este st' addho spiti ecisimùddhia , sta capetaja , ce ìgue otikané cippu ele ' mana . ce usi mana ipe ti toppu fsemeronni ènna pai ' s enan daso ce n' es kami na chasune .
e kèccia ìgue . skosi o pornò ce is to tù' pe i ' tsia .
pianni ce e tsia i ' doke enan guari sfìkomma ce ipe : tapu pate mi ' mànassa ' cé citto daso , esù pai pu ' mpì ce pai rendèonta ti ' kordeddha .
depoi pianni prai c' es èfike ' mbrò ' s enan daso , usi mana .
pianni tui ce jùrise essu .
tue , torusa ' to sfìkomma , ce jurisa ' mapale . epianni ce pirtan essu .
e mana , tappu es ide , ipe : mapale ìrtaton ? ipe .
ipe e kèccia : ka ' sù iche panta ce mas èfike ' cirtea , ipe .
pianni mapale ce plosa ' to vrai . e mana èkame on addhon diskorso : " Avripale ènna pao ' s enan daso pleo ' larga " diskorre manechèddhati , c' e kèccia este efsùnniti ce in ìgue .
o pornò skosi e kèccia , ce pirte sti ' tsia . ce ipe : ipe e mamma ti ènna mas pari ' s enan daso pleo larga .
na utti sìmulan , ipe , esù pai pu ' mpì ce i ' pai pelonta .
mapale pianni ce es pire ' s enan daso pleo ' larga ; tui ìbbie pu ' mpì , ce ìbbie belonta simuleddha . pianni es èfike ce jùrise e mana .
pianni tue kulusisa ' ti ' karrasciuleddha ce jurìsane . ftasan essu .
mapale èftase o vrai ce plòsane . e mana mapale ipe : ènn' es paro ' s enan daso pleo ' larga .
pianni mapale skosi e kiatera kèccia o pornò ce pirte sti ' tsia : ipe ka ènna mas pari ' s ena ' pleo ' larga dason , ipe .
ipe e tsia : dete ka ' sivi ìsesta kalé fìmmene ; ce dete ka torite ena ' palain , skuperete e ' grate ce ' nnènnete ' cipanu .
gnorsì , ipe e kèccia .
pianni pirte c' es èguale mapale ' s enan daso e mana . prai ce pìrtane ' ttù tutto daso .
e mana pianni ce jùrise . e tri kiatere skuperefsa ' tutto palai , ' nnèkane ' cipanu ce vrika ' mian Anniorka ce enan Anniorko .
c' ìpane : putten erkesesta ? ipa ' : ìchamo mia ' mana c " e mas èdie makà pleo na fame ; ' en iche makà pleo fsomì na fame , ce mas èfere ' s tutto dason .
emì skuperèfsamo tutto palai ce ' nnèkamon , ipe ' kiatera kèccia .
ipe Anniorka : kàglio kàmato ka ' nnèkaton ettupanu . pianni ce o dia ' na fane tunò , na jettù ' liparé n' es fane ka isa ' kecce .
doppu kamposso cerò mapale ipe ' kèccia : ei tosso ka stèome ' ttupanu c " en ìmesta janomena mia ' pittaduce .
ipe Anniorko : ka ' sù es efserin ?
sine , ipe cini .
pianni is èfere i ' taeddha , is prepàrefse otikané , ce u tin èkame . pianni ce ipe : amo ' ce vale fortia poddhà sto ' furno ka cini teli poddhì na jettì .
pianni èvale lumera tuo , pianni ce pire i ' taeddha . tui ipe : ' nnea ce nkàrrati pleo kau i ' taeddha .
enneke tuo panu sti ' plaka ; pianni tui u doke mia ' skatapinta ce on èmpose ' cessu , ' ce sto ' furno .
pianni tui , e kiatera e kèccia , ce pirte stin Anniorka . ipe Agnorka : o pappo skosi ?
ipe : ste vaddhi skupò i ' taeddha .
ipe cini mapale , tui e kèccia : Eh , posso ceròn ei ka steo ' ttupanu ce ' en ime janomena mai a maddhia i ' nanniorka .
pianni is ta tà' lise ce nsìgnase n' i ' ghiurefsi : is irte fatta , vasta ena ' spinguluna puntao sto ' petto , is to kiàvefse sti ' ciofali ce skiòppefse .
in efsikkosa ' tue , ise tris kiatere , in efsikkòsane c ' i ' pelìsane ' cessu ' s emmia ' fossa , i ' klìsane , pelisa ' choma pu panu , krotèddhia , c' ìrtane ' ttos ti ' kàmbara .
pianni ce mbìkane e ' citti kàmbara pu iche olo to kalò .
tui pianni ce ìvrike mia ' kasciddha , mia ' kàscia ce iche us àbitu krusù , matafsotù . pianni tui ce in iche klimmeni itti kasciddha , in iche klimmeni mi ' tis tin dune e derfesti .
mian attes foré , o pedì tu ria , pu este ' s ena ' paisi ' cisimùddhia , doke ena ' nvito : is pu te ' na pai na chorefsi , na diverteftì , na pai ' cì .
ipa ' tue : esù minon ettù , ka emì ènna pame ' cì pu mbìtefse o ' ballo o ria , is ìpane is kèccia .
pianni i ' fìkane ce pìrtane ; ce nsignasa ' na chorèfsune .
fìnnome pu ' ttù ce piànnome atti ' kiatera kèccia pu essu .
e tsia fata is èmbiefse enan ampari , me suliné krusé ce vasta e ' brije fse krusafi , ce o vardeddhi puru fse krusafi , otikané fse krusafi .
pianni c' is pirte panu sto palai t' amparai ; is pirte ' mbrovo ce tui ennòrise ka is to tò' mbiefse e tsia : pianni presta ce ndisi . ' nneke panu st' ampari ce pirte tui , èkame enan giro sto ' ballo ce skàppefse pu ' cessu .
ce pirte panu sto palai . fseputisi presta , tò' mbiefse amparai , ce pirte ' mbrò sti ' lumera mapale .
pianni c' ìrtane e aderfé , e male .
ipe : ' en ìdato mia ' signura anu ' s enan amparin pu jaike pu ' cé sto ' ballo ? ' e kitei den ! ipe .
pianni eplòsane o vrai , fsemèrose addhi mera ; mapale pianni èftase o vrai ce mapale pirta ' tue .
e tsia pianni c' is èmbiefse mapale amparai .
tui tù' de amparai mapale , ce ndisi . ' nneke panu st' ampari ; epirte ecì sto ' ballo ce èkame enan giro mapale ; pianni c' is èpese o ' skarpino .
tui ji ' fuddha on èfike ce pirte essu .
èmbiefse amparai , fseputisi ce mbike kau sto ' kantuna .
sto ' ria pian votonta tinos e ' ciso skarpino sto ' ballo ; c " en brìskato tinò tinos ene .
pianni irta ' to vrai aderfé ; ìpane : arte vrai irte mia ndimeni pleo kajo c' is èpese o ' skarpino ce pian botonta alò tinos ene c " en brika ' makà tinos ene . ' e kitei den , ipe cini .
Eplosa ' to vrai , fsemèrose addhi mera , mapale ; jetti vrai , tue mapale pìrtane sto ' ballo .
pianni mapale e tsia is èmbiefse ampari : tui tù' de mapale ampari , ndisi presta , ' nneke panu st' ampari m' ena skarpino sì ce m' enan de ' .
cine pu stèane ' ce sto ' ballo stean ole ma chèria niftà n' in efsikkòsune mottan èmbenne .
tui mapale pianni èkame mian balava c' in efsikkòsane . ènna dume m' is pai tunì !
is ton bàlane c' is ìbbie cinì .
pianni : i ' telo ja jinèkammu , tui ipe o pedì tu ria .
ermàstisa , kalì kuttenti , kalì kunsulai .
tis pu teli n' u ' di , na pai ti us torì .
o kunto ' e pai pleo , ena ' sordo mereteo .
