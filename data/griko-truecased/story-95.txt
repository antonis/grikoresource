io ' mia forà c' io ' mia ' jineka c' enan andra ; e jineka tò' kanne a cèrata andrù ; olimera ìbbie ' s emmia kappeddha ; ecessu iche on an Bito , ce cini ìbbie quai meri ' cì ston an Bito n' is kami i ' chari . c' ele : san Vitu miu , famme na grazia cu ngurcia lu miu maritu , cu no me viscia cu quiddhu addhu amicu . quai meri ìbbie c' ekanne utti ' funtsiuna , tris forè tin emera .
mian attes foré , diaike ena ' c' in ìgue tui pu jure i ' chari tu ja Vitu .
ipe : ti lei tui ' ttossu pu ste kanni tosso ? ipe , c' ìkuse otikanè .
pirte ston andra : den , ipe , ti tui pai ' cé sti ' kappeddha n' is kami i ' chari na cekefsi , purke na stasì mus addhus , ipe .
esù , fseri ti ènna kamis arte ? u tù' pe u ' ndru cinì , na pais ' cimpì st' artari tu ja Vitu , ipe ; dopu e ' jurata diu tri foré ti ' chari cini , n' is kami i ' charin a ' Biton , ipe , esù respundei ce leis , ipe : fseri poa su kanno i ' chari ?
mottin , ipe , tu kannis ena ' piatto makkarrunu tu ' ndrusu , na fai , ipe , c' enan gapunan ecipanu , ipe ; cino , tappu ste ' ce us troi , ipe , tronta tronta cekei .
cino ius èkame .
presta tui pirte essu , mottan ìkuse iu , pirte essu presta , èkame u ' makkarrunu tui mon gapunan ecipanu .
èftase o vrain o àndratti , ce nsìgnase n' o ' kalopiai : pai poddhì strakko ?
Ekàscio , kàscio , ti sò' kama diu makkarrunu na fais , ipe .
uh , ipe cino , pao tosso strakkon ! ipe .
tappu este na kordosi m' ittu ' makkarrunu : Nah , ipe cino , nafse o ' livnon ! ipe . ' e torì ka stei nammenon ? ipe .
vota st' addho meron ce fili , ti is èkame i ' charin a ' Bito . ' nvece na vali sto ' lemò èvaddhe sti ' mitti , cino .
katsa ! ele cini , cèkefse kalà !
beh , ipe cino , fseri ti kame ? ipe ; prai , prai , de a ' me fseputisi na me valis es to grattin , ipe . pirte , on efsepùtise ce on èvale sto gratti .
èftase o pornò c' ìtele na skosì .
fseri ti kame , prai de a ' me ndisin , ipe ; pàreme ' mbrò sti ' portan , ipe , armenu na su vlefso es òrnise , ipe ; dommu mia ' matsan , ipe .
cini ' mbrò cino èkanne ti klei , depoi vota st' addho mero ce fili ' ttumesa .
èbbie ce kàise ' cisimà sti ' porta .
pianni ce diaike o addho .
ellae , ipe cini .
vu , ipe cino , ka ' cì echi tuo , pos ènnà' rto ? ipe . ' e se torì den , ipe cini , cèkefse , ipe .
ce tuo panta èkanne : Sciù , gallina ! c' èddie mi ' matsa quai tòssonna t' ìgue potimae . mapale tuo ìgue potimae : Sciù , gallina ! mapale , ce os èdie os òrniso .
c " en iche tipo ecisimava .
pianni ce mbike tuo .
tapp' este pròbbio ' mbrò sti ' porta ' cisimà cino , tuo èkanne panta : Sciù gallina ! Zafi mia ' botta ce o ' finni vìttima , m' itti matsa pu vasta , c' ipe : Sciù gallone ! ipe .
presta tuo , c " en io ' makà pleo ' cekao , on èkame mortalo .
vu ! ipe cini , ti mò' kames arte ? tù' pe u ' ndrù .
ka cino iche èmbonta na piai lio ' lumeran , ipe .
via , ipe cino , piatto ce sìreto kau sto grattin , ipe , k' arte vrai , motta stèome oli ce diu , pame ce o ' pèrnome , ipe .
on èvale kau sto gratti , èftase o vrai , piaka ' mia ' stotsa skala ce on balan ecipanu .
ipe tuo : emena o bonànima tu ciurumu , ipe , ìunna m' èmase na povo , ipe : Domine , miserere , trede vàune e unu vene .
c' ipe ja fta fto forén iu , c' icha ' na pa ' lèonta j' oli ti ' stra ' .
tui mai n' i ' pensefsi ka cino ele iu : " trede vàune e unu vene " , jatì ìsele n' is kami i ' festa cinì puru .
prai prai , prai prai , pianni ce ftàsane ' cisimà sto frea : beh , ipe , o ' pelume ' ttossu , ipe o andra .
o ' pelume , ipe cini .
presta o ' trabbukkèfsane ' cessu .
den , ipe , mi ' mas eskobrefsi ce stasì panu panu , is ipe andra ti ' ghineka .
pirte na ' ffacceftì cini : bum ! c' i ' pèjase ' cessu .
cinon ghiùrise essu ce stasi .
o kuntos ' e pai pleo : is pu te ' n' on di , os pai ston an Bito ka o ' torì .
