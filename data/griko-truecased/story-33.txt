io ' mia forava c' isa ' tria pedia ; ena fs ' poddhì rikko , o addho puru io ' rikko , ce addho io ' ftechuddhi. ena ' ìgue Mikelàngelo , addho Vriziu Lenardu ce addho Paolu .
o ftechuddhi iche mia ' màndria pedia .
ce arte èftase mia mera mali ce tuo ' en iche tipo n' o ' doi na fane : Ahn , ipe , ti kànnome ? ipe , ti kànnome ?
io ' tu Kristù ... os ùrtimo .
ipe e jinèkatu : amon , ipe , ston aderfossu , ce kame na su doi tìpoti , ka èchome utta diu làchana , ipe , ce makà krea ; su di lio cinon , ipe , ce marèome utta làchana ce a trome .
arte pu pirte tuo sto ' Mikelàngelo na jurefsi o krea : amo pu ' ttù , k' andé arte se pianno ...
krea su dio ? ipe .
vu ! ipe e jineka u Mikelàngelu , o ' fonàzome n' u dòkome utta diu stèata , ipe , utte diu sciuntèddhe , na pa ' na fai m' utta pedàcia ?
Fonasotto , a te ' n' o ' fonasi , ipe cino , o andra .
o ' fònase , u ta tà' vale ' s ena ' makkaluri c' ipe : na , amo ce fae m' itta pedàcia , ipe .
pirte tuon essu , stin ghineka , c' ipe tui : s' o doke o krean , aderfossu ? ipe .
Eh , ipe cino , mu doke quai stèata .
vu ! ipe e jineka , ce arte t' ènna ta kami tua ? ipe .
Eh , ipe cino , ènna pao n' u ta paro u demoniu , ipe .
prai prai , prai prai , ce u guenni ena ' monekuddhi ambrò : pu pais , ipe , pu pais ? Eh , ipe cino , na pao na paro utta stèata u demoniu , ipe .
ce arte , ipe cino , fseri pos ènna kamin ? ipe .
des , ipe , ti pai ce vriskis ena ' portunan , ipe : mi ' tutsefsis mi ' cheran , ka se cei . epais , ipe , cini se vàddhune ' cessu ce su lèune : Oh , brao ; ti e ' brao tuo !
prai prai , ti teli na su dòkome ?
ti teli ? ' ttù echi rama , ' ttù echi asimi : forta cippu teli .
esù respundei ce leis , ipe , ka ' e teli tipo tipo addhon , ipe ; sulamente la petruddha de lu kumandu .
pirte tuo .
maleppena on ìdane , nsignàsane n' u rotìsune cippu ìtele .
ce cino ghiùrefse panta itto lisari .
ce ' sù ti pais kànnonta ? ipe enan demoni .
Eh , ipe cino , pirta ston aderfommu na jurefso lio krea , ipe , ce cino mu doke utta stèata , ce ' vò ipa ka ènn' os ta paro o ' demonion .
ce ' vò arte sas tà' fera san , ipe . sì sì , sì sì , ipa ' tua .
tuon arte o kremume ' cì pu steu ' t' addha .
ka ' ttù , ipa ' , de posson ei kremammenon : tuon en olo krea tu derfusu pu e ' glemmena .
ce cinon , ipa ' , ènn' o di mian addhi forà tuo .
den a ' ton din , ipa ' , ce pestu nà' rti na fortosi puru turnìscia .
sì sì , ipe cino , penseo ' vò n' u to po , ipe .
u doka ' ta demogna o lisari ; èbbie ce pirte cino .
pirte stin ghinèkattu : vu ! ipe e jineka , posson ei ti mankeis !
evò ' en efsero ti ènna kamo m' utta pedàcia , ipe .
beh , ipe cino , stasu citta , ce amo ce fsunna ta pedia ti ènna fane .
Eh , ipe cini , ka stasu citto !
ka ' vò ' en ìfsera pos icha na kamo na ta ploso , ipe .
evò su leo na pai na ta fsunnisi ti ènna fane .
ce a ' pao na ta fsunniso se trone , ipe cini , ji ' famòtria pu vastune .
amo ce fsùnnata ! ipe o andra .
ius telis , ipe e jineka , iu sia .
pirte ce a fsùnnise .
èbbie ti' petruddha de kumanduc' èsafse mia ' bottan ecipanu . Skobrefti mia tàola òria ce ' cipanu iche poddhà pràmata òria : makkarrunu , krea , fs' otikané .
vu ! ipe e jineka , ka ' sù èfere o kalò tu Kristù ! ipe .
Eh , ipe o andra , utta spìddia pu stèome ènna pulistune , otikanén avri pornòn , ipe . pirte tuo , mes ena ' mina , ce frabbìkefse ena ' palai , kajo pi cino tu ria , mes sti ' mesi .
arte , tuo pu tù' che dòkonta ta stèata , tò' mase ka o ' derfò jetti rikko , ka nsìgnase na vorasi koràfia , otikaneve .
nsomma , mian emera , pianni ce u diaike pu ' mbrò . u diaike pu ' mbrò c' ipe : " tuos ene aderfommu ?
is ene ? " . iche ndisonta cino de gran tenuta c' e ton ennòrize tispo .
ipe tuo ti ' ghinèkattu : k' amo ce skarkagna in ghineka tunù ! ce pesti pos èkame , ipe , na jettì tosso rikko àndratti .
Eh , ipe cini , pos èkame ? itta stèata pu tu dòkato esì , pirte ce os ta pire o ' demonios , ipe , ce cina o fortosa ' turnìscia .
ce ius , ipe , ' mì frabbekèfsamo m' itta turnìscia .
ipe cino : ce cino u pire a stèata ? ipe . evò u perno mian biteddhan , ipe .
èbbie tuo èsfafse mian biteddha , in èkame diu stotse , i ' fòrtose ce os ti ' pire .
prai prai , prai prai ce u guenni ' mbrò ciso monekuddhi mapale c' ipe : pu pais ? tù' pe tunù mo krea .
ènna su po ' sena pu pao ? ipe cino .
Eh , ipe cino , ka ' e su sozo doi kané kosijon ? ipe .
dete ' cina , ipe mapale cino , esena u mustìsciu ènna su po pu pao ? ipe .
meh , ipe , ' e teli pokka , amon , ipe .
pirte c' èftase o ' portuna , ce nsìgnase na tutsefsi ' mpì citto portuna .
ecì pu pirte na tutsefsi u tò' kafse i ' chera , ka o portunan ece . Ah ! èkanne cino , ma ' e ' kite : iche na fortosi cino !
presta cina pirta ' ce nìfsane : Oh , ti e ' brao tuo ! nsignasa ' na pun ola .
o ' piaka ' ce o ' piran essu .
beh ! ipe cino , o derfommu sas èfere a stèata , ipe , ce ' vò sas èfera o krean , ipe .
beh , arte o kremume ' ttù pu ei t' addhon , ipa ' .
ce ' vò irta na fortoso sekundu fortòsato cino . cino ' en iche ponta makà ti èbbie o lisarin de kumandu , ipe ti fòrtose c' irte .
sì sì sì , ipa ' cina , prai prai prai ce forta cippu teli .
ettù stei asimin , ipa ' , ' ttù stei e rama ; cippu teli forta ce piàne .
nsìgnase na fortosi a visàccia , tuo , sordu ; dopu ' e chori pleo ' cé sta visàccia ...
a ' sosin , ipa ' cini , vale possa telin , ìpane .
minon , ipe cino , ka pao c' èrkome mapale , pao c' èrkome ...
dopu gòmose itta visàccia , ebbianni c' ìpane ta demogna : de de t' en òrio tutto gràttina , ìpane .
su piacei , su piacei ?
teli na plosi lion ecipanu ? ìpane ; ka depoi rifriskei ce pai ?
uh , ipe cino , t' en òrion , ipe ; fio na bejastò na rifriskefso lion , ipe .
belisti ' cipanu : pleon ìbbie , itto gratti , pleon èvraze ; iche lumeran ecipanu .
nsìgnase na kami kamposses kulutrùmbule ; èkame kamposse , depoi èmine , pèsane .
o kunto ' e pai pleo , ena ' sordo mereteo .
