io ' mia forà c' ìone ena ' markanto , ce uso markanto iche tris kiatere , pu isan òrie ole ce tri , ma e kèccia io ' però pleon deferenta , ' en iche sìmeli , io ' pròbbio la dea Venere .
mian emera es fònase tuo ce os ipe : evò ènna tarasso ja larga , dunque evò nà' o na sa vriko sekundu sas finno , na min echo kammian dubitìa , k' andé evò ' e pao pùpeti .
cines ìpane : àmone skuscetao , mi ' pensa ' cì ' s emà , ka ' mà , kundu mas finni , ma ' briski .
dunque , ipe cino , petemmu ti tèlete na sas fero . petemmu on ngegno pu èchete .
e mali ipe : emena fèremu enan àbeto matafsotò rodinò .
e menzana ipe : c' emena fèremu ena ' turkini matafsotò .
vòtise e kèccia : evò fseri ti telo ? na mu cheretisi to Gran Biziri , ' e telo tìpoti addho , àmone sto kalò .
lecenziefti o ciuri ce tàrasse kuntento toronta cino tes kiatere m' itti fiducia ce mbarkefti c' e kiatere minan essu .
arte cino nsìgnase na pratisi mes ti ' tàlassa .
prai prai , èftase mia ' cità , ntartènefse o ' bastimento ce mbike na di an ene e cità u Gran Biziri , jatì tuo ' en ìfsere arte .
ecì tispo ìfsere n' u doi noa tu Gran Biziri .
dunque tuo nsìgnase na pratisi mapale c' èftase mian addhi cità .
dopu pràise poddhì cerò , ce kateke pale , pirte ce nsìgnase na rotisi atto Gran Biziri , mi ene e cità u Gran Biziri , ce manku ìvrike noan ecì : tispo ton ìfsere fse nomatisi .
matapale mbike ' s kammino ce nsìgnase na pratisi .
depoi èftase ' s emmian addhi citava ce mapale kateke de kapu ce nsìgnase na rotisi . ecì u dokan noa depoi ti ene e cità u Gran Biziri .
dunque vòtise ce u difsa ' to palai , ma e persona pu ìone ipe : ma ' sù , ipe , kuntei mo ' Gran Bizir ?
Maddonna , ipe , na kuntefsi makà .
cino èkame enan ànimo mea mea ce doke c' enneke panu .
dopu ' nneke panu , frùntefse ena ' giòveno ambrostu ce on ellivèrefse , ce cinos ipe : ti kumandeis ?
cino respùndefse c' ipe : evò o ' Gran Bizir pao vrìskonta .
aftentìasu ? ipe . den , ipe cino ; o derfommu ene o Gran Bizir .
en derfòn dikommu , ipe .
ma ' vò , ipe cino arte , o markanto , evò ìsela na kuntefso pròbbio ma safto .
ma cino , ipe , an ènna kuntefsi ma safton , cino mia ' ' ssuta sulamente kanni in emera : guenni pus tutti porta ce mbenni stin addhi .
an bresì toa pròbbio pronto pronton ene .
ce cino ipe : iu ' mu lei , ce ius kanno .
u tù' pe in ora pu ènna vresì ce cino kàise ecì ce on èmine .
èftase ora pu tù' pe cino ce tuo guike .
osso pu on ide , cino osson irte ecisimà . cino tu belisti sta poja ce tù' pe : na preghiera ! ipe .
cino vòtise ce : ce cumandi ? ipe .
na preghiera .
ce ti kumandei ?
dunque , ipe cino , evò echo mia ' kiatera , c' ipe ti se cheretava , ce sulamente t' ìsele na se di .
cino kàlefse ' ttossu sti ' punga , èguale enan veloni krusovo : na , dòketis tùonna , ipe , ce a ' teli na me di , vaddhi tuo ecé sti ' limba ma lio nerò : kanonì ' cé sti ' limba ce me torì .
dunque cino pirte nà' gui ce frùntefse cino pu iche deskorrèfsonta o pornò , on aderfottu . ipe tuo : ti sù' pe ?
u ti ' cherètisa ipe .
ce ti sù' pe ?
na , mu doke tùonna n' i ' doko , ipe .
fseris ti kame ? ipe , mìnone stin ora pu avri ce pestu ti ' e tin ebbastei tuo tis kiatèrassu ; ti torì tuo ce klei ; na su doi tipo addho . cino èmine pale sti ' stessan ora .
cino , èftase pale ora , ce diaike . ipe : ce cumandi ?
ce cumandu ?
la mia figlia quista no li basta e quandu vide quistu quiddha chiange .
cino kàlefse sti ' punga , èbbie o makkaluri : na , ipe , cu se stuscia le lacrime .
cino tò' bbie pale ce vòtise i ' stra ' na pai . .
pale u guike o ' derfò sto ' stesso topo : ti èkame ? ipe cino .
mu doke itto makkaluri " cu se stuscia le lacrime " , ipe .
mino mian ghià avri , ipe o aderfottu , ce pestu ti tua pu su di , ti ' kiatèrassu ' e ti ' kanune , pe .
cino ftechuddhi pale èssifse u ' nomu c' èmine .
èftase ora pale ce diaike tuo . c' ipe cino pale : la mia figlia , quandu vide quistu , se danna ( tò' le cino , o derfottu , pos ènna pi ) .
cino kàlefse sti ' punga , èbbie ena ' temperino krusò c' ipe : na , dalli quistu , e quandu se danna dilli cu se scanna . Depoi pirte nà' gui ce pale frùntefse tuo ce u tù' pe : ti èkame ?
na , tùonna mu doke .
ce arte , ipe cino , bastei : amo ja pensièriasu .
cino pirte èkame a pensièriatu pu iche na kami . ce jùrise essu .
osso pu èftase essu , mi ' to piàme iu a longu , e kiatere chiùstisa , tappu ida ' to ' bastimento tu ciuruto , ce tappu ekateke on embrafsòsane ce o ' kalopiàkane .
o ciuri , osso pu ide es kiatere , pu es ìvrike sekundu pu es èfike , cheresti .
fònase i ' mali ce i ' doke on dùnotti . fònase i ' menzana ce i ' doke on dikotti .
cine piaka ' ton duno ce nsignàsane na kiakkerisu ' ti ' kèccia : fae ' sù arte , des emà ti mas èfere , ce su minon arte !
cino depoi fònase i ' kèccia ce is ipe : beh , na , tui ine e dùnisu , ipe ; ce is kùntefse cinì .
cini tò' bbie , o ' duno pu is èfere ; ce ' e tis fènato na mini manechiti ; na piai i ' limba cini , na pai manechiti , mi ' tin dune e addhe arte cini ; is irte fatto , èbbie pirte sto ' cipo mi ' limba kau ' s emmian arangea ce èvale o velonin ecessu , ce is ampàrefse cino kau sto nerò .
cini , doppu on ide , ' en iche pleo reggetto . irte addhi mera , ce pirte sto ' ciùritti : Gnore , ipe , evò telo lio moneta , cippu teli na mu doi , ipe , evò telo na tarasso , ti ènna pao na vriko o ' Gran Bizir .
o ciuri arte ' en iche telìsonta ; in èbbie cino ce aderfé ce nsignasa ' na klàfsune : oi , ti lete ? ipe cini , evò ènna tarasso .
dunque tui este na tarassi ce u ciurù ' e tu koddha .
ma in ide iu ' nkapunita , i ' doke tèssare chijae moneta ce cini tàrasse . pirte stin agra tis tàlassa ce èmine na di ena ' bastimento na mbarkeftì .
sti ' poddhin ora skuperefti ena ' bastimento . èguale o makkaluri tui ce tò' kame simai .
ce o bastimento pirte ' rtea safti . dunque ipa ' tui : ti kumandei ?
evò , ipe cini , telo na mbarkeftò .
ipa ' cini : ènna tarassi o bastimento ja ' sena manechì ?
teli poddhì moneta .
ipe cini : evò vastò tèssare chijae . sopu kanisi o turnisi , ipe , sopu de ' mu milite ce ' vò katenno .
dunque mbarkèftisa ce nsignasa ' na pratisu ' mes sti ' tàlassa .
osso pu cini spìccefse o turnisi , ìpane e marinari : emì ' e sòzome pratisi pleo , o turnisi e ' spicciao .
ipe cini : ' en echi pùpeti na me katete ettusimà , ena ' paisi , mia ' cità , pos ènna kamo evò arte ?
skuperèato toa isa isa mia cità . gualetemme ' s tutti cità ce pulisetemme , ipe : mi ' me dòkete menu pi tèssare chijae .
mbìkane sti ' cità tui ce nsignasa ' na fonàsune : ci vole na schià ' , ci vole na schià ' ?
ecì iche ena ' palai tu ria ce vrèsisan e damigelle nfacciae : vu ti en òria ! ti ' piànnome , Maestà , ti ' piànnome ti e ' poddhìn òria .
c' i ' kama ' na ' nnei .
osso pu in ide o ria ce e regina , os piàcefse ce in voràsane .
i ' dòkane tèssare chijae ce cini èmine ma safto ce cini pìrtane sto ' fattotto , e marinari .
dunque cini , stèonta ' cipanu ' s tutto palai , ' e tori addho pi luttu , prikae , sia ka ' en io ' manku palai ria .
dunque mian emera tui vresi mi ' rigina : ènna su po mia , ipe tui : utto palàissa , is ipe , sia ka ' en e ' makà palai ria , ' en ei kané divertimento . luttu ce luttu , ipe .
respùndefse e regina ce ipe : kiaterèddhamu , mi ' telisi na fseri .
jatì ? ipe tui .
evò , ipe tui , ena ' pedì manechò èkama ce en ikosiena chronò ce stei panta panu sto gratti , plonni ce ' en efsunnà mai . ' e troi ce ' e pinni , ipe , ce cino stei sareno pu a ' ton di menis iu , panta plònnonta , nifta ce mera .
makari ti u dia ' , makari ti o pitsìzane , cino ' en efsunna mai , panta ecipanu este .
dunque ipe tui : kàmeme na do utto pedissu .
i ' pire ce in èkame n' on di . osso pu on ide tui , èmine spramentuta : rotinò lìmpido !
nsìgnase n' o pitsisi : ' en efsùnnise ma kammia manera .
vòtise sti ' rigina cini : fseri ti ènna kami ?
o gràttimmu , paretèmmuto ' mpì sti ' pòrtattu .
o pira ' ce is to ftiàsane : ' ttossu este cino , ce o gratti tis to ftiàsane ' mpì sti ' porta .
èftase o vrai ce tui pirte c' èblose ' mpì sti ' porta .
tappu ìsane e pente , pu oli icha ' plòsonta ce ' e kùato tispo pleo , kàlefse mia signa c' enas urso . doppu mbike ecessu , e signa èguale mia ' bakketteddha ce on ènghise , ce cinon efsùnnise .
doppu skosi cino , cini vòtise ston urso ce ipe : Ursu , conza tàola , ipe .
dopu èftiase i ' tàola cino , ma pa ' kalò ecianu , kàise cino ce e signa ce nsignàsane na fane .
dopu spiccèfsane uso fai , pirta ' ce plòsane ; dopu iche plòsonta diu ore , dopu o pronò ìpuno , vòtise e signa ce ipe : Ursu , ce ura ete ?
respùndefse o urso ce ipe : galli no càntanu , campane no sònanu e cani no bàjanu .
e ' segnu ca potimu stare n' addhu picca . Tui arte kontemple i ' bella pu ' mpì sti ' porta .
doppu lion addho e signa fsùnnise pale ce ipe o stesso . vòtise o urso ce ipe : mo tocca ku ni azzamu ca se nvicina l' ora .
cini e signa skosi , u doke mia ' botta m' itti bakketteddha ce pale èpese pesammeno cino , ' e ton efsunna tispo .
skosi tui o pornò pu ' mpì sti ' porta , ce pirte sti ' mànatu . arte ipe cini : fsèrete ti telo ?
ti telis ? ipe cini ; magari na vrìskato kané refrigèrio .
arte fsèrete ti telo ?
arte pu kanni es eftà ora tu niftù na kàmete avisata ti ' gaddhinara na skupagnefsi ole tes òrnise na kantalisu ' . posse aglisies echi , tosse kametetes antese na ndalìsune allarmi stes eftà ore tu niftuvu .
ce possi sciddhi èchete ' ttuanu oli na stasù lesti n' us enkarrefsu ' na bajèfsune cittin ora . dunque tui signa èftase ora , osso pu mbasùnefse olo to jeno , ce tui pirte pale .
on ènghise mi ' bakketteddha , ce fsùnnise . tù' pe t' urso na ftiasi tàola ce fane pale , ce poi pirta ' ce plòsane .
dopu èkame lion ìpuno cini , ipe : Ursu , ce ura ete ? respùndefse o urso ce ipe : galli no càntanu , campane no sònanu e cani no bàjanu .
e ' segnu ca potimu stare n' addhu picca . Dunque cini in èbbie ìpuno pale .
èftase ora , èkame es eftà ce cini èkame o ' terrisbìglio . e signa risbìjefse ce ìkuse u ' gaddhu pu kantalùsane , es kampane , u ' sciddhu .
c' èffie ce scèrrefse n' u doi i ' botta mi ' bakketteddha . tuo , osso pu vresi risbijao , skosi mprimu ce nsìgnase na spassiefsi o palai cinon olo .
osso pu skòsisa e mana ce o ciuri ce on ìdane pu este spassièonta i ' kàmbara , pensa esù possi charà os èftase ! pìrtane ecì safti : Oh , kiaterèddhamu , ka is s' èmbiefse ettupanu ?
Èamo janomena tossa tossa , c " en brìskamo kané refrigerio .
arte , ipe cini , evò nkuruneo ' sena , o ria nkurunei o pedimmu ce s' o dìome j' andra .
dunque vòtise cini c' ipe : evò sas endigrazieo atti ' bontassas , ipe ; ti ' vò ' en ime skarpa jo pedissa , ti u pedìusa ènn' u doi mia ' kiatera riu , ipe .
respùndefse o ria c' ipe : emì ' s esena vastume on angegno ; ka ' sù tu doke ti ' zoì .
evò , ipe cini , ' e telo addho pi i ' libertammu , ipe .
osso pu idan iu , is tin dòkane : na sò ' vloimmeni , ipa ' cini . tui ekateke pu ' cianu ce pirte pale .
kateke , praise ce pirte stin agra tis tàlassa .
stèonta ' cìsimà , skuperefti ena ' bastimento . èguale o makkaluri atti ' punga ce tò' kame simai , ce pirte pale o bastimento e ' safti .
ipa ' : ti kumandeis ? ipe cini : ìsela na pratiso ' mbrò , a ' me sòsato pari .
Respundèfsane e marinari c' ìpane : ènna tarassi o bastimento ja ' sena manechì ?
teli poddhì moneta .
ipe cini : evò vastò tèssare chijae . sopu kanisi o turnisi , ipe , sopu de ' mu milite ce ' vò katenno .
ftàsane ' s emmian addhi cità . tis ìpane : Spiccefti o turnisi , ' e sòzome pratisi pleon ambrò .
respùndefse cini : armenu gualetemme mes ti ' citàn , ipe .
ftasa ' ti ' cità tui ce nsignasa ' na fonàsune : ci vole na schià ' , ci vole na schià ' ?
vresi enan vèkkio , enan vekkiaruddhi .
vresi uso vèkkio , tuo ce e jinèkatu ìone pirte stin jinèkatu c' ipe : pulù ' mia ' kiateran òria ; ' e tin voràzome ?
cini pronà skìfefse lio , poi toronta ti cino i ' teli : esù i ' telis , ipe , c' i ' piànnome pokka a ' mas endiastì , ti ' en èome tinò . kame kundu teli .
cino pirte , doke tèssare chijae c' i ' pire èssutu c' i ' kràise .
usi jineka arte tunù vèkkiu o vrai tù' die o ' nùbbio ce on ennùbbie . cino skioppe sti ' tàola : ènghize n' o ' fseputìsune ce na pa ' n' on vàlune sto gratti , tui kiatera ce tui jinèkatu .
tappu on vaddha ' sto gratti , tui jinèkatu èguenne ce sianònnato sto mero tu pornò pale .
tui kiatera pu iche donta ola tua , mian emera is irte fatta ce tù' pe : Nanni , ti on ele nanni cini , arte vrai , o krasì pu su di e jinèkasu , kame ti o pinni ce kame na su pesi ecé sto ' petto ; ma pese sfammeno ce kame ti plonnis .
ipe cino : Kiatereddhamu , iu ' mu lei ? ce ius kanno .
irte o vrai , èftiase i ' tàola e vèkkia ce tu prepàrefse o ' nùbbio .
tuo , kundu tò' bbie , èkame ti o ' pinni ce to ' pèjase sto ' pèttotu . dopu pu èkame ti o ' perni ìpuno , èpese sa ' sfammeno ; o ' piàkane ce o ' pìrane sto gratti .
dunque tui jineka , kundu èkanne addha vràdia , ius èkame itto vrai .
chasi pale pu èssuti . tui pale , tappu guike cini , pirte ston vèkkio : torì , nanni , ipe , esù avri peseni jo ' nùbbio ti su di fissa fissa cini .
dunque tuo tis ipe : dela ' ttù , tuon iche o ' tresoro c' i ' pire : tuon e ' krusafi , tuon ene asimi , ce tui e ' rama : piao ce forta osso teli .
evò , nanni , ipe cini , ' e telo tipo ; ' e telo addho pi i ' libertammu .
ipe cino : e volontammu ìone na se krateso ettù , ce tuon ion olon dikossu .
ius teli , ius kame , na sò ' koncesso . tui èbbie ce pirte .
guike stin agra tis tàlassa ce èkame o stesso mistieri : èkame nà' rti o bastimento pale ce èkame n' i ' pàrune .
dopu in guàlane sti ' cità , tui èbbie ce ròtise : ti cità e ' tui ?
e cità u gran bisiri , ipa ' cini .
mbike mes sti ' cità tu gran bisiri tui ce nsìgnase na piai nformu pu stei o palàitu. Lumen de lumine !
depoi i ' difsa ' to palai ce retirefti ' cìrtea cini .
pirte ' cìsimà c' ìvrike quai vèkkia , mbike èssuti ce èmine m' ittin vèkkia . esù ènna me kami na me ' nnei panu sto palai tu gran visiri .
respùndefse e vèkkia ce ipe : ecì , ipe , o ' nnei manechò tappu nennun oli ' cipanu ti kaizun ballon ene . dunque , ipe , satti pu en ora se ' nnenno evò , ti toa echi libertàn ghià olus .
cini , osso pu èftase ora na pari na mbièfsune e soni : meh , arte en ora , ipe .
cini depoi vasta mian vesta pleon ordinaria pu anu , pu kau vasta ena ' vestimento de viduta pròbbio to kàglio ce ndisi poddhì vrai . in èbbie e vèkkia c' i ' pire ecipanu .
osso pu ' nneke ecipanu , ecì òria ti ìone òria ti ndisi , osso ti ' kanonusan oli , osso minan oli .
iche arte utto gran visiri pu iche fìkonta a ' mmàddia panu safti : ka io ' l' ùrtima belletsa . ce ' cianu en iche kammian addhi kundu cini .
doppu in guàlane sto ' ballo na chorefsi , osso pu nsìgnase na chorefsi tui , pleo poddhì mènane stupiti oli possi ìsane .
doppu spìccefse fse chorefsi , pu guala ' tin addhi , o gran visiri is èkame simai , ce cini kàise ' cìsimà ' mbrostu . doppu i ' kàise , cino is ipe st' aftia , mi ' to ' kùsune e addhi : esù arte ènna mu pi ' puttes ise ce is ise .
cini respùndefse c' ipe : ' e me norizis ? ipe .
ka pos ènna se noriso ?
ka ' vò ' e s' icha donta mai , ipe .
fsìkkose cini c' èguale o veloni to krusò : tuo sì , ipe cino , to norizo .
beh , ipe cini : a ' te ' na se do , n' o valo sti ' limba na se do ...
o ciùrimmu ipe ti , motti se torò , ti kleo ce ' sù mu doke to makkaluri na sunghiso a dàmmia . Rekordese ?
depoi èguale o ' temperino : osso pu sù' pe o ciùrimmu ti ' vò dannèome , esù mò' mbiefse to ' temperino na skanneftò .
cino , doppu tù' pe cini utta loja , ti depoi cini u kùntefse ola possa iche patuta ( ' en dulei na ta pume pale tua ' mì ) : meh , arte , ipe , ' en ekkatenni pleo pu ' ttuanu , ipe cino .
Nòttefse poi itti niftan ecì , depoi skòsisa to pornò . ipe cino : evò telo na se stafanoso .
ce ' vò tuo pao vrìskonta , ei tosso pu pateo .
depoi in estefànose ce stasi ma cinon ecipanu .
tuos arte , o gran visiri , iche mia ' kàmbara , ce ' céssu citti kàmbara iche enan ghialì ; ce quai chrono ìbbie c' in ènifte citti kàmbara cino .
doppu èftase o chrono , cino ènifse i ' kàmbara ce pirte ce nfaccefti sto jalì ; doppu nfaccefti sto jalì , ensìgnase na jelasi cino .
respùndefse e jineka ce ipe : Oh , pu na ìfsera jatì jelà ! àremo ti ide ecessu pu jelàs , ipe .
depoi ipe cino : teli na su po ?
ka già , ipe cini , ìsela na mu pi .
respùndefse cino ce ipe : i derfisu e mali ste ce stefanonni sìmmeri , ste c' in endìnnune na pai stin aglisia .
respùndefse cini ce ipe : Oh , pu na icha na pao .
ìsele na pais ?
na pao arte .
a ' te ' na pai , ipe cino , amba ' céssu citti kàmbara , ndisu fse gran tenuta , ma kajo rucha , ce piaon enan duno n' is pari tis aderfissu , piaon ena ' prama krusò , kalò n' i ' dois arte pu pai .
ipe cino : beh , àmone ' cisimava , de ti vrìskete enas ànemo pu se perni ce vrìskese ecì , ipe .
dopu vresis ecì , i ' pernu ' stin aglisia tin aderfissu . doppu èrkete essu , ipe , mi ' telisi na kaisi na fai , lecenziese , pai kau sto ' samporto , vrìskete o ànemo ce èrkese ettù .
mi ' telisi na fais , ipe ; a ' kaisis na fai , ' e me vriski pleo , ipe .
iu ' mu leis , ipe cini , ce ' vò ius kanno .
vresi ànemo ce èftase stin aderfitti .
osso pu e aderfé idan cittin aderfì arte m' itti manera , chiùstisa n' i ' filìsune , ce pìrtane stin aglisian depoi .
sto jurisi pu kàmane cines arte telusa ' n' i ' kaìsune . cini ipe : evò ' e sozo kaisi , nghizi na pao .
lecenzièftisa , pirte kau sto ' samporto , vresi ànemo ce vresi èssuti . osso pu in ide andra : Oh , ipe , ben fatta !
in ennòrises kami , ipe .
cheresti cinon depoi .
depoi irte addho chrono . rmasti addhi derfì ce èkame o stesso .
an de ' te ' na ta valis ola ... tui pirte ce irte ' s sarvamento pale . ( stin addhi forà io ' pu in èndese ) .
diaike o chrono ce tuo pirte nfaccefti sto jalì .
nfaccefti sto jalì ce u sburrefsa ' ta dàmmia . e jineka arte , ka o ' kanoni , ton ide na dammisi : pemmu ti echis , esù ste ce kleis ! ipe .
respùndefse cino : es addhes foré sò ' pimena ; ma tunis forà ' e su leo makà , ipe cino .
cini vòtise ce on eskungiùrefse : ènna mu pis , ipe cini . ion de fortsa fatta n' is pi .
meh , ipe cino , o ciùrissu pèsane , ce ste ce o ' klinnu ' sti ' baran , ipe .
vu ! ipe cini , kàmeme na pao sekundu ime janomena es addhes forés , ipe . beh , ipe cino , veramente te ' na pais ?
a ' te ' na pais , ipe , ènna kami sekundu su leo evò , andé ' e me vriski pleon , ipe .
cini ipe : mi pensa makà ' cì .
es addhes foré ' en e ' janomena iu ? ce arte o stesso kanno .
dunque ipe cino : àmone ' cé citti kàmbara ti echi u bestitu de luttu olu matafsotù , vale pleo telis , ipe .
dopu pirte cini sti ' kàmbara , nordinefti ce depoi pirte ' s safto .
arte fseri ti ènna kamis ? ipe ; arte pu pais ecì , e derfessu steu ' ce ssazutte , ipe .
dunque ' sù ftazis , filìs to ciùrissu , ipe , ce depoi pa ' ce kaizi es emmia sèdia sto largo , ipe . satti pu kaizi , o simai pu ènnà' rtis e ' tuo : de ti su battei ena ' prama krusò stin garza , ènna skosìn a la botta ce ènnà' rtis , ipe .
cini tù' pe umme , pirte panu sto palai ce vresi stes aderfesti . osso pu e ' derfé tin ida ' pale : vu derfèddhamu !
cini o fìlise ce pirte ce kàise .
arte nsignàsane e derfesti : Soru , kame tata miu ! uh , ti derfì desamurata !
nsignasa ' na malangòsune kontra stin aderfì .
doppu lio tunì nsìgnase n' is ebbattefsi o prama to krusò na pai . tui respùndefse c' ipe : batta ' sù , dorloci , posso teli , evò ' e sozo kratestì pleo na bejastò !
skosi ce nsìgnase na klafsi sti ' bara . dopu ìrtane e pateri ce o ' pìrane , tui èmine ' cì ma safte .
depoi lecenziefti tui , pirte sto ' topo na petasi na pai . ecì ' en irte de ' anemo de ' tipo ; èmine , ftechì .
dopu dosi ' s tutto stato tui , manku ipe tipo stes aderfesti , èbbie i ' stra ' ce nsìgnase na pratisi . tui ìbbie ètimi puru arte , tui kristianì .
pratonta pratonta , tui , sentsa fai ce sentsa pi , sentsa kané riparo de ' nifta de ' mera . pirte c' èftase mia ' kampagna skura ci no cumparia né celu né terra ce nsìgnase na pratisi mes citti kampagna .
pratonta pratonta , pai ce fruntei ena ' tranieri ; uso tranieri , osso pu in ide manechiti , ìbbie ndimeni ius òria , òria pu ione èmine stupito tuo . is èftase pleo sta kùkkia : Oh , ' e mu lei ti pais kànnonta mes tuttes pianures , ipe , pu ' e pratù ' manku t' animàglia ?
tui respùndefse ce tù' pe : na mi ' telisi na fseri ton destìnommu .
cino tosso tu despiàcefse , tosso tu kòrpiase i ' kardia : fseri ti kame ? nea sto ' traino ti se perno èssumu , ipe .
cini ftecheddha ' en iche kané spettàkulo , ' nneke ce pirte mo ' tranieri .
tuso tranieri este kalà ma ' en iche de ' pedia de ' tinò : cino ce e jinèkatu ìone , ce stea ' manechato .
osso pu in ide e jinèkatu : ti e ' tui pu mò' fere , t' ènna kami ? nsìgnase n' o ' lapatisi , n' u pi loja , ti ' e tin ìsele , ja celosia .
cino respùndefse , is ipe : echi kumpassiuna pu safti , ti ' sù ' en ei tinòn èssusu , ti e ' supèrkio n' i ' kratesi , iu su kanni u ' serviziu .
makà cini , ma kané modo .
leontai leontai , kùntefse ti is kànnane mia ' lettereddha ' cé sti ' remesan ; già i ' kapacìtefse m' itti manera , c' is eftiàsane ena ' pramafsi pu plonnu ' ' cé sti ' remesan cinì .
stèonta sìmmeri , stèonta avri , stèonta mesavri arte tui , is ìrtane e poni na jennisi .
uso kristianò vresi pu mbike ' cé sti ' remesan ja ftinà ce in ide pu skiame manechiti , ti ' en iche kammian avisia .
pirte stin jinèkatu : Auh , ipe , Maria , k' amon bisa itti jineka !
ka ' en e ' kreatura u teù cini puru ?
cini poi sia pu is entenèrefse e kardia , ka io ' kalì kiatera , c' is pire i ' mammana ce in visìsane .
jènnise tui ce èkame ena ' pedai .
doppu ti èkame utto pedai arte tui , in bàlane sto gratti , pàssio mia pirte èssuti ce tui èmine manechiti .
arte fìnnome apu ' ttù ce piànnome atto ' gran bisiri .
tuo ta ìfsere ola tua , ce ìfsere ti ste patei . tuo , doppu plose e jinèkatu , pu i ' pire ìpuno , fsìkkose ce pirte ; dopu pirte , èbbie itto pedai ce o fsefàsciose ce u tò' vale ena ' prama krusò ecessu sti ' fascia ce pirte .
( iche es eftà tevne c' imisi , tuo ! ) . o pornò to kecci egle ; e jineka tunù tranieri on ìkuse ce mbike : sa ' ka i ' despiàcefse tunì , pirte n' is piai tutto kecci , na kami ta guatati cini arte .
ton èbbie o pedì , nsìgnase na to sfasciosi ce is èpese itto prama to krusò ecimesa .
doppu èpese , cino sprandòrefse fse krusafi ecimesa , ma cini èkanne a guatati ce ' e tù' de . e trainara kàlefse ce tò' bbie ce tò' vale sti ' punga .
tui , doppu i ' doke o kecci , pirte tù' de ka io ' prama de valoro ; nsìgnase n' i ' gualisi òrnise na fai , n' is kami brodu ...
arte , tappu tuo gran bisiri skandàjefse ti tui ' e tis èperne na fai pleo , pirte ce is èkame o stesso c' èvale enan addho prama krusò ecé stes fasce tu pediu .
tuo matapale is èglafse , o kecci ; pirte tui trainara ce is tò' bbie . o fsefàsciose , èpese ecimesa o prama to krusò ; ce nsìgnase n' i ' gualisi pale na fai .
arte fìnnome pu ' ttù ce piànnome ti ' cìsimà iche ena ' palai riu , ce tui rigina puru is ìrtane e poni na jennisi .
vresi tui trainara citti rigina pu jènnise . tui , doppu jènnise : arte , ipe o ria , telùsamo mia ' nodrissa .
respùndefse isi trainara ipe : motti ìsele , ti kau sti ' remèssamu echi mia pu kanni gala pufà ! ipe , pu kanì j' alò ce diu .
dunque , kamei na ' nnei , ipe o ria . ' nneke ' cipanu ce os piàcefse .
is to doka ' tutto pedai n' o nastisi , ce cini a krai ma safti sti ' remesan ola ce diu . cini , arte , poan este ' cikau , ce poan este essu u ria .
arte utta pedàcia , vizzànnonta vizzànnonta ce tronta tronta , jèttisa malùddhia ce pratusa ' manechato ; ma o pedì tu ria ' mbrò cinu tu gran visiri iche mali ndeferenza . mia forà , tua pèzane ola ce diu ce tuo tu ria nsìgnase na klafsi .
arte pu dunefti o ria pu cino nsìgnase na klafsi , arte nsignàsane tui : uso mulakkiuna ! àremo ti furman ene !
nsignasa ' n' u pu loja i ' mana ce cinù pedai . uso gran visiri ìfsere otikané ce u despiàcefse ce presentefti ' cì .
èbbie o pedittu stin brafsà : tuo e ' pedìn dikommu , ' en e ' mulakkiunan ! ipe .
fsìkkose itto pedai tu ria atta maddhia , u doke mia ' skatapinta , on èsire ecimesa , tò' ssafse , èbbie o pedittu , èbbie i ' jinèkattu ce chàsisa pu ' cì .
i ' pire depoi sto palàitu ce stàsisa kalì kuttenti , kalì kunsulai .
is ipe : ' en ìkuses emena . ide ti su succèdefse ?
ce stàsisa depoi ...
ce spìccefse .
