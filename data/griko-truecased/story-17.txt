io ' mia forà c' ìone ena ' ciuri , c' iche tria pedia .
utta tria pedia ìsane skuturtsata ce tignusa , ma depoi isan òria fse muso .
ekku ti o ciuri pèsane ce t' àfike ola skunsulata , utta tria pedia .
tapu stàsisa dekapente mere ' tto , dekapente , ikosidiu ipe o mea :
ti stèome essu ce ' e mas ferni tispo na fame !
epame na dume an ebuskèfsome tipo na fame .
ipe iu tuo . taràssane , diu j' ena punto ce cino manechottu j' enan addho :
sto ' chrono menu tris emere na sianosune .
ekku ti a diu kèccia ftàsane es emmia massaria , eftiàstisa sto ' mina ja kumenenzieri ce mìnane .
o mea èftase stamesa i ' Napuli , mes emmia mesi , c' irte fonammeno apus emmia jineka , c' èkanne :
pis pis pis .
es tutta simàdia pu tò' ddie usi jneka : en daveru , ipe , t' ime tignuso ce skuturzao , poka , ma depoi ime òrion atto muso .
èbbie ce vòtise . cini o ' fònase . ipe : dela ' ttù .
dopu pirte panu sto palai , cini èbbie ce ipe :
te ' nà' mbi ja servituri ?
sì , ipe cino , ka j' itton mistierin epao votonta .
ipe cini : dela ka se vaddho ecè ' s emmia kàscia ti arte pu èrkete o àndrammu kanno na se di .
on èvale sti ' kàscia .
irte o andra ce stèane sti ' tàola ce ipe :
Loici , telis ena ' servituri kalò ?
ipe cino : basta ka io ' Leccesi ka on ìtela , ka cini en i ' ladri .
e jineka pirte ce o fònase .
guike cino pu ' cé sti ' kàscia , pirte ce tò' guale asciadi tu padruna , ka ion ikosidiu mere ka cino ' e tori fsomì .
èbbie ipe cino :
putten ise ?
ime Leccesi .
fseri na grafsi ce na meledisi ?
ipe , k' andé amo pu ' ttupanu .
sì , ipe cino , fsero na grafso ce na melediso .
pos kui ?
Rafelin , ipe .
i ' merenda o ' pire sto ' magazzino ce nsìgnase n' u difsi olu tus pretsu atto ' magazzino :
utto rucho tale pretso , utto rucho to tale , olu tus pretsu .
ekku ti dupu dekapente mere o padruna ' en èbbianne kanena mpàccio pleo ;
ìtele na kami kridentsen , èkanne ;
èkanne o ' tale pretso .
o padruna ' en èkanne kané cino pleo .
ekku ti dopu efse minu èpese dìnato o padruna ce dopu diu tris emere pèsane .
pèsane o padruna ce tuon arten èkanne pleon òrion ecé sto ' negozio .
tusi giòveni , e jneka u merkantu , stèonta mian emera , stèonta diu , konsidèrefse c' ipe :
'En e ' kajo n' u to po cinù na me piai ?
K' andè , piànnonta enan addho , ena ' Napuletani , mu troi o rucho ce me finni mes emmia stra ' ;
mu pianni u ' libru attes kridentse ce pai pu ' ttù .
iu e ' kajo n' u to po mprima .
ekku ti tappu pirte sti ' tàola usi jineka u tù' pe :
Rafeli , de ti ' vò pènsefsa ma ena ' modo :
piànnonta enan addho , ekku ti ' vò ' e kunkludeo tipo atto ' negozio .
'En e ' kajo na piaos esena ?
ipe tuo : sì , kame sekundu telis esù .
tuon este kòmodon arte pu rmasti ma tui .
ekku ti tappu tui toristi nkometao èftase o chrono menu tris emere pu a dèrfia icha ' na vresune oli ce tri sto spiti na kutefsun ton affitto .
a diu kèccia sianòsisa , kutefsa ' ton affitto tu spitiu ce rotisan atto ' mea , mi en ertomeno .
den , ìpane , is efseri pu en bresomeno .
ipe o mean atta diu : cino i ' stra ' atti ' Napuli èbbie toa .
pame na dume a ' ton brìkome .
pame , ipe o kecci .
ekku ti tui taràssane ce pirta ' na pa ' n' on brìkune .
ftàzune sti ' Napuli ce diaika ' pròbbio pu ' mbrò sti ' kagnà .
nsìgnase tui n' us fonasi , o stesso simai cinù :
pis pis pis .
ius irta ' fonammeni pu ' cìpanu ce pìrtane .
topu us fònase , us èvale sti ' stessa kàscia .
dopu us èvale sti ' kàscia pai ce ftazi o Rafeli .
cini ekkàntefse lio na pai na nifsi i ' porta . èbbie ipe tuo :
" lei ka echi kané napulitanin ettossu pu ste ' ce i ' botà a maddhia ( pu ste ' ce i ' botà a cèrata , nquasi ) , ce us èglise ecè sti ' kàscia pu èglise emena toan ? " .
ekku ti sekundu mbiken essu , pelìete panu citti kàscia ce toppu pelisti èpese ce stasi fiakko tris emere ce tri nifte .
toppu kamposso e jineka pirte na nifsi i ' kàscia ; enifse ce us ìvrike olu ce diu pesammenu .
fònase i ' serva ce in èmbiefse na fonasi ena ' fakkino :
pestu nà' rti ' ttuna ti ènna parin ena ' prama sti ' tàlassa c' echin akatòn dukau .
dopu pirte o fakkino , iche guàlonta ena fse cinu , ce tù' pe :
'Sù ènna pari tuon es ti ' tàlassa ce nghizi na vali skupò ti tuon jurizi .
ci l' ha criatu , tuo jurizi ?
pao ce o pelò .
doppu jùrise cino , cini iche preparèfsonta on addho kundu cino ce ipe :
Sù' pa ' vò ti arte pu pai esù tuo jurizi !
tosson embelenefti cino !
pirte ce o pèjase pleo panu .
topu pu èftase kau , iche jurìsonta ' stotse : isa ' tris ore niftù topu jùrise cino .
jaike o Rafeli : itto vrai io ' skuro ce vasta i ' linterna cino nammeni .
tappu este sti ' skala , o Rafeli ènnenne pu mpivi ce uso fakkino ènnenne pu ' mbrovo .
votà mpivi cino , pu ìgue ka pia ' pratonta , ce torì tuo mi ' linterna .
èbbie ce pùntefse lio , nsìgnase na kanonisi .
nsìgnase na kanonisi : Mannaggia ci t' ha criatu , èrkese puru mi ' linterna !
u kiafei ena ' tàfaro .
sekundu u to kiafei , cino petti ce peseni .
Nfaccete e signura atti ' porta :
Assassine , ti èkame ?
cinos ìone àndrammu . Mannaggia ci te criau !
'sù ipe ka jurìzune ; cinos io ' sìmilo sìmilo cinòs ! ipe o fakkino .
dopu ìkuse ti pèsane :
na us akatòn dukaus , ipe e signura ; avri pornò lèome ti pèsane de morte improvisan .
èbbie ton èvale sto gratti ce èmine pesammeno .
ce cini èmine manechitti .
