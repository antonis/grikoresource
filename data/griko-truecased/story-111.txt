io ' mia forà c' ion ena ' palai ; c " en ìbbie tispo panu citto palai .
este ' s kampagna .
is pu ìbbie us esfàzane ka iche a demogna .
depoi ' en iche tispo na pai pleo , ka olu tus esfàzane .
depoi iche o pleon àscimo atti ' Kalimera , o mesci ' Achillo ; ipe : arte vrai pao ' vò mi ' bankiddha ce rafto skarpe .
nsìgnase na rafsi skarpe , ce poi ìkuse mia ' fonì pu ele : Kàsciu !
u tò' sbinne o linno ; ce cino vasta us pròsperu c' ele : esù o ' sbinni , c' evò o ' nafto .
èkanne kuràggio .
depoi , doppu ipe pente efse foré : Kàsciu ! èbbie ce bèjase mian anka .
depoi ipe pale : Kàsciu ! ce bèjase in addhi ; depoi bèjase o soma ; celì celì bèjase otikané , quai lò ' ebeja ena ' prama .
depoi jetti enan òrio giòveno ; èpese olo stotse stotse ce jetti enan òrio giòveno .
depoi ipe tuo : arte ' en echis ebesogno na sazo pleo , ka is pu irte ' ttù in oli farimmeni : pesenu ' ji ' paura ma ' vò ' e tos kanno tipo , ce ' sù ' e faristi .
depoi iche o jeno to pornò ce ipe : beh , pame piànnome o' mesci ' Achillo ; ka e kalì e ' pesammeni , o fiakko ' en ènna pesani ?
tuo èkanne on guappo to pornò : arte èbline o ' muso ce sunghìzato mo mandilai ; ce tui pia ' n' o piane mi ' bariddha .
tappu on ida ' cini : Achà , ìpane , ka ' e pèsane makata ?
emì ìchamo fèronta i ' bara .
sì , t' imo ' sekundu us addhu !
c' iu ghiurìsane .
Epèjase i ' bankiddhan atti ' fenestra ce pire in ghinèkattu ' cianu , c' este de signore .
depoi ipe o mesci ' Achillo : fio , fio nà' guo lion es ti ' kàccia .
pirte sti ' kàccia ' s enan daso ; ce nsìgnase na votisi maletiempu .
jetti mia ' skuritava , nifta , ' en ìfsere pu na pai ; èmine ' cé citto daso .
skupèrefse ena ' livno , ce pirte ce jùrefse allòggio ' cessu quai spidai , c' iche ena ' briganton ecessu . iche ena ' briganton ecessu , c' ipe : nce nu pocu d' alloggiu ?
sine .
on èkame nà' mbi ' cessu c' èvale mia ' plaka pu ' mpì na mi ' sosi ègui pleo .
ce arte ipe cino : c' evò putten ènnà' guo ?
Achà , ka ' sù , ti en egui pleo ? ' sù ènna minis ettossu ma mena , ipe uso briganto .
c' iche tossu kristianù kremammenu ecessu .
c' ipe : ti ènn' us kami cìnunna ?
ipe cino : ti ènn' us kamo ?
us tro ; doppu spicceo cinu pianno ' sena .
cino tus èsfaze ce us krema na kratestune sto ' frisko .
arte u mbike mia paura tunù ka io ' mia plaka mali c " e tin ìsoze tuo : cinon io ' gioganto ce tìn ìsoze guali .
depoi ipe : " andé me fèrmefse ena ènna me fermefsi tuo ?
arte ènna do ti ènna kamo " , ipe .
ti pènsefse tuo ?
o gioganto vasta enan ammai : esù , ipe tuo , vastà enan ammai , an vasta diu , fseri t' ison òrio ?
ce ' sù ti tevnin echi ? uso gioganto tù' pe tunù .
ce ' vò ime guasta e conza , ftiazo puru ' màdia ce an icha na su ftiaso ammain esena puru s' o tò' ftiaza : nvece na vastafsin ena na vastafsin diu , ipe .
ce fidese ' sù na mo kami ?
cinon ipe : fidèome .
ce ella ' ' ttù , na di pos ènna se ftiaso .
on èdese ma t' antsàrtia atta poja , atta chèria , atti ' ciofali ce o ' krèmase .
o ' krèmase , ènafse mia ' lumera ce rotìgnase ena ' spido , èbbie itto spido ce u tò' kafse t' ammai .
depoi cino , posse rebattitses èkame , èpese ' cimesa , spetsefse ole tes korde , t' antsàrtia , c' ìbbie votonta n' on ivri ; ma ' e ton ìvriske ka arte io ' cekao .
tuon ìbbie kau sta pròata c' e ton embeste mai , tanta tanta .
depoi tuon bòtise kamposso ce jo ' pono ce strakko pu ìbbie èpese ce o pire ' ìpuno .
depoi o mesci ' Achillo skòrcefse mia ' pratina , in èftise c' in efe .
depoi skosi , èbbie o derma ce o krai .
ce ipe cino , o gioganto : " ' ttossu ènna stasì cino ; ènnà' gui !
cino ton embesteo ; ma cino ènnà' gui !
efse pròata ine " .
èffie i ' plaka ce skànkose ' mbrò sti ' porta na jaù ' ta pròata pu kau stes anke .
o mesci ' Achillo èvale o derma ' ttupanu ce guike ce cino to tàntefse , esumpone ti e ' pròato ce nsìgnase na metrisi : c' en ena , c' in diu , c' i ' tria ...
ce mètrise ris ta fta : Achà , ipe cino , tua ine fta !
ce ' vò ìmone o pronò , ipe cino pu defore .
ce cino depoi raggèato ; t' iche na kami pleo ?
ce poi ?
depoi ' e tus ida ti kama ' pleo !
ce spìccefse o kunto .
