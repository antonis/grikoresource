mia forà ion ena ' c' ìgue Mèsciu Frangisko . iche i ' tenni na ndalisi i ' kitarra tuo .
mia ' nifta u pirte ' s ìpunu : sto ' kumento tu ja Paskali echi mian akkiatura , c' ènna pate diu n' i ' piate ittin akkiatura , ipe .
ènna pate diu : esù na ndalisi ti ' kitarra , ce cino na chorefsi m' ena stenò .
ipe : de ka guennu ' mòneki mali , mi ' tus faristite makà u ' mònekus esì ; a via de kitarra ènn' i ' piate in akkiaturan , ipe .
skosi tuo to pornò , fònase enan addho ce pirta ' tui .
piaka ' to stenò ce pìrtane : tuo ndali ce nsìgnase na chorefsi tuo mo stenò .
pianni ce guennu ' diu monekùddhia : bonasera mesciu Frangiscu .
bonasera , bonasera .
ce tuo ndali i ' kitarra .
pianni ce guikan diu addhi mapale pleo mali : bonasera , mesciu Frangiscu .
bonasera , bonasera !
èbbie ce nsìgnase na ftasi i ' plaka , ce pleon ìbbie pleo mòneki mali guènnane . pianni all' ùrtimu nsignasa ' nà' gune afsilì , mali mòneki , ce tuo ndali i ' kitarra .
èftase i ' plaka , èbbie c' èskose i ' plaka cino .
pianni tuo c' ide itto krusafi p' este ' cikau .
o mesciu Frangisko , na di lio pos ene , èfike i ' kitarra ce mbike na piai lio na di pos ene .
ecì pu pirte n' o piai , klisti e plaka , chàsisa e mòneki pu ' cipanu , otikané .
tui , pai , osso ce vrèsisa manechito tui . pirtan essu .
i ' nifta , u mesciu Frangisku u pirte ' s ipunu o stesso ( enan ajo teleste io ' pu tu pirte ' s ipunu ) ce u tù' pe : mesciu Frangisko , ipe , evò elo ' ka ènna fai panetta , ma ' en io ' makà ja ' sena .
arte mi ' pate pleo ka ' e ti ' pianni de ' sù de tispo pleo , ipe .
