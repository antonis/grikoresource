io ' mia forà c' io ' mia ' mana c' iche tria pedia .
o pronò pirte c " en ghiùrise pleo .
èbbie c' èsire tuo o menzani : mamma , aderfommu pirte : arte ' en ènna pao ' vò n' on vriko ?
ce ti kànnome : ena ' c' enan diu ? ipe tui .
deje , mamma , mi ' pensa ' cì ; ti ' vò pao ce jurizo .
deje !
vloisomme ti ènna pao .
epirte .
pirte tuo , ena ' c' enan diu , c " en ghiurisa ' pleo .
fìnnome pu cinu pu ' en ghiurisa ' pleo ; piànnome atto keccin arte .
epirte : mamma , ecì pu pirta ' t' adèrfiamu telo na pao ' vòvo .
Auh , ti mu kanni pedàimmu !
arte pais esù ce spicceo .
mi ' pensa ' cì , mamma , ti ' vò jurizo .
èbbie i ' strava tuo ce pirte na pai na vriki adèrfiatu . Oh !
epirte , èmine ena ' mina , èmine diu ...
e mana , mini mini , èchase e ' sperantse pus ola ce tria .
o pedìn arte nsìgnase na pratisi , na di an ìvriske a ' derfia ta mala .
sto pratisi pu èkanne ìvrike enan daso c' ipe : mbenno ' ttossu o ' en embenno ?
èkame ena ' koràggio mea mea c' embike .
mbike ' cì sto daso ; arte pu toristi ' céssu , tuo nsìgnase na faristì ti este mes te ' makke .
tappu pràise posson ena ' mili , epianni ce pai ce vriski ena ' liuna , mian àkula ce mia ' furmìkula pu ste kontraferèatto na meràsune enan vui fsofimmeno . èbbie c' ipe tuo : bongiornu !
bongiornu ! ce diaike .
dopu iche kàmonta diu tris passu o fonàsane ; c' ipe tuo pu ìone o liuna : dela ettù , kàmemas ena ' piaciri : merasomma lio tutto vui , ti ' mì , ipe , ' en eftiazomesta .
beh , ' sù pu ise àkula , su dio a stèata , ti vastà to ' pitso ce a kaserni ; esù pu ise o liuna , su dio i ' muddhika ; ce ' sù pu ise e furmìkula , su dio i ' ciofali ka sozi embi c' ègui pu ' céssu na fai otikané pus pa ' tripi .
Estete kuttenti ? ipe tuo .
sì ! kajo pi mas èkames ' e soggeste , ipe o liuna , na mas kami .
stasiste kalì .
stasu kalò .
ce pirte .
tappu iche kàmonta fta fto passus , piaka ' ce o fonasa ' mapale .
ipe tuo : ti tèlete ? Elae , ipe o liuna , mi ' faristu !
tuo farìato , tappu o fònase .
mi ' faristu , ipe o liuna . dopu jùrise tuo , ipe : emìs ' en ìchamo tipo na su dòkome , ipe o liuna .
evò , ipe cino , ' e telo tipo , sa ' ringrazieo , ipe , c " e telo tipoi .
de ' , ipe o liuna , sentsa tipo ' e se fìnnome .
evò su dio : motta teli na jurisi liuna , lei : " lione me fazzu " .
ce ' vò , ipe e àkula , puru o stesso : tappu vrìskese ' s kanèn abesogno lei : " aquila me fazzu " .
evò o stesso , ipe e furmikuleddha .
meh , stasiste kalì .
stasu kalò .
ce pirte .
tappu làrghefse posson ena ' mili pu cinu : ènna dume , ipe , an en jalìssio cippu mù' pe uso liuna ! èbbie c' ipe : Leone me fazzu .
ce jetti liuna .
uh , t' ime àscimon ! ipe . cristianu me farò , ka ' vò ime àscimo sentsa finan , ipe .
beh , ipe tuo , ènna jettò furmìkula : furmìcula me fazzu ! jetti furmìkula .
ka ' vò ime poddhì keccia ! fio fio ! ' e telo pleo .
epianni ce jùrise kristianò . cristianu me fazzu .
beh , ènna dume mapale : aquila me fazzu , ipe .
jùrise àkula ce pètase . guike defore atto daso ce èftase ena ' paisi .
kui e ' males kampane pu ndalùsane a mortòrio . eftazi , èbbie tuo ce kàlefse kau , c' ipe : cristianu mi farò .
jetti kristianò ce nsìgnase na pratisi .
ìvrike mian bèkkia c' ipe : ' e mu lei jatì ndalù ' tuse kampane tosson a mortòrio ?
ti e ' , pedaimmu ? ipe . èchase i ' kiatera , u piaka ' ti ' kiatera tu ria .
dopu ipe : ce fseri makà pu i ' piran ? ipe tuo .
Eh , pedaimmu , ipe cini , ka ti fsero ? !
prai pleon ambrò ce vriski mia ' pleon bèkkia oi ena ' pleon bèkkio pi ' mena ce su fseri doi noa .
prai prai , prai prai tuo , ce pai ce vriski enan bekkiaruddhi . mu lei tuse kampane jatì ndalù ' tosson a mortorio ? ipe o stesso .
u klefsa ' mia ' kiatera u ria , ipe , c " en efsèrome de ' noan de ' vèkkian , ipe .
guike pus tutto paisi ce vresi st' addho . ce mapale kùatto to stesson e kampane .
ìvrike mia ' kristianì : mu leis , ipe , pu i ' pìrane utti kiatera , pu in espatrièfsane ?
dommu noan , ipe .
Ah , pedaimmu , ipe cini , madonna t' i ' pìrane ' cìrtea sti ' tàlassa .
tuo pianni c' ipe : aquila me fazzu !
èbbie volo ce pètase .
Epètase ce nsìgnase na ngirefsi ' mbrò sti ' tàlassa .
petonta petonta , pai ce vriski ena ' palai stamesa is tàlassa .
mpòggefse panu sto kornici tu paladiu c' èmine . " evò ènna do , ipe tuo , m' in ei ' ttossu " .
dalli e dalli , pai ce torì utti gioveni . " evò ènna kamo de tuttu na kalefso " , ipe tuo ; fs' àkula panta este . motti pu este panu ' s tutto ticho tuo , pu nkora iche na kalefsi , etorì o ' Nanni Orku pu este c' èguenne defore na kacciefsi .
ipe tuo : Oh , katsas , ipe , ce pos kanno na kalefso ? doppu tuo pirte larga pu ' cisimà , epianni ce kàlefse mes tutto ghiardino tuo t' iche ghiardino ce nsìgnase na piai es patte na di putten ènnà' mbi ' ttù ' s tui .
proppi nà' rti o Nanni Orku , pianni tuo ce mbenni sti ' kàmbara pu este cini : iche jettonta àntrepo arte tuo .
epianni ce mbike tuo motti cini este tisa mes to spiti , este c' èkanne serviziu .
guike tuo ce in efsìkkose pu ' mpì . pappo , pappo ! ntrikefti na fonasi tui .
cino , pu ìone ora pu iche na sianosì atti ' kàccia , pai ' cì cini pu fònase : ti praman e ' , kiaterèddhamu ? ipe .
malepena fònase cini , cinos ipe : Furmìcula me fazzu ! jetti furmìkula c' is embike sto ' petto .
c' is este sto ' petto tuo , ce cittu ' sù ce citta ' vò .
èbbie ce nsìgnase na votisi o Nanni Orku : kiaterèddhamu , ettù ' e sozi ennei tispon ettuanu !
tuon vòtise vòtise c " en ìvrike tipo : kiaterèddhamu , evò ' ttù ' en vrisko tìpoti .
mi ' faristu , ipe , ti ' ttupanu ' e sozi ' nnei tispo , ti an ennèune us tro . èbbie i ' strava : kiaterèddhamu , stasu skuscetata ti ' vò pao .
tuo mapale , motti pu skandàgliefse ti tuo iche ftàsonta kau , èbbie c' ipe tuo : cristianu mi farò , c' in efsìkkose pu ' mpì ; c' is efsìkkose o ' lemò c' is to ' kràise : citta , ipe , posson ei ti pateo ja sena . pappo , pappo ! fònase cini mapale .
ce o pappo pianni ce kui mapale .
cino pianni ce jènete mapale furmìkula : " Ah , ti mò' kame ! mapale èkame na jurisi " , ipe ecé safto .
èbbie cino ce jùrise ; èbbie c' èkame o stesso ; votise : ma ' vò ' e s' o tò ' ' pimena ti ' ttupanu ' e sozi ' nnei tispo ti ' vò us trovo ? ipe .
èbbie ce pirte : mi ' me fonasi pleo ! ipe .
ce pirte .
dopu pirte , èbbie tuo mapale c' ipe : cristianu mi farò .
èbbie c' is efsìkkose o ' lemò : cittu , cittu , posson ei ti me kanni na patefso !
vu ! ipe cini , ce po ' bresis ettupanu ?
Ah , ja ' sena vresi ettupanu . ka emasa ti s' èglefse uso Nanni Orku ; se glefsa ' ce se fèrane ' ttù ; ce ' vò tò' masa , ce irta na dume a ' se soso piai na se paro .
mai , kané cerò me sozi pari , ipe cini . ma pròbbio , pos èkame na ' nneis ettupanu ? ipe cini cinù .
pos èkama ?
ènna di pos èkama ?
lione me fazzu ! ce jetti liuna .
vu , t' is ' àscimo , ipe cini ; jettu , jettu kristianò !
ce liuna jetti nà' rtis ettù ? ipe cini .
de ' , ipe cino , arte torì .
Oh , benes , èbbie ce jetti furmìkula ; jetti furmìkula : Oh , jettu kristianò , ti arte ise poddhì kèccia ce se patume , ipe cini .
èbbie , amés tutto mentre , kùstisa patimae . vu vu , ste c' èrkete o pappos , ipe cini ; presta , klisu ! ipe .
toa stesso iche jettonta kristianò ce poi ènghise na jettì furmìkula n' is embi sto ' petto ; ce tù' fsere cini . 'Nneke o pappo mapale : e ' domena tipo pleo , kiaterèddhamu ?
deje , pàppomu , ' en e ' domena tipo pleo .
èbbie i ' strava , doppu efe , èbbie o pappo ; kateke ce pirte .
èbbie c' ipe , dopu este ste ' skale : kiaterèddhamu , mi ' faristivi , stasu skuscetata ce mi ' pensa pupei .
prai , ekatea ' sù pu ' ttupanu , ipe cini , ce jettu kristianò , ti ènna fais , ipe .
èbbie , on èkame na fai , on èkame na pi , o ' kòrdose .
plòsane . stin ora pu skandàglie nà' rti o pappo : aska , òriommu , ti art' èrkete cino ce mas troi olu ce diu , na !
èbbie c' ipe : esù ènna kami addhi mian , ipe tuo ; esù ènna stasì prikì ; tapu èrkete cino , cino se rotà : " ti echi , kiaterèddhamu , ti stei prikì ? " .
esù u respundei ce lei : " esteo prikì ka , arte pu pesenis esù , evò meno manechimmu ' ttupanu " .
ce u leis ola tutta loja , ipe o paddhikarin arte . beh ' en en ora pleo , jatì arte pale ftazi o pappo .
èbbie ce jetti mapale furmìkula tuo .
èbbie ce ' nneke tuo , ce mapale in ìvrike pleo prikì : ma kiaterèddhamu , jatì stei prikì ?
jatì ' sù peseni ce ' vò meno manechimu !
Auh , kiaterèddhamu ! evò sù' pa ka ' e peseno mai .
efe , èbbie ce ekateke ce pirte .
jetti kristianò cino . beh , prai , prai na fai , ti cino kateke , ipe .
dunque èbbie i ' stra ' doppu efe ; e ora èftase pu iche nà' rti o pappo .
fseri ti ènn' u pis ? ipe tuo ; ènn' u pi : poa peseni ?
dopu su pi otikané , esù pianni ce stei citta .
vale skupò cippu su lei ce kraita stannù .
fìnnome pus tuo ce piànnome atto ' pappo , ti èftase ora .
epirte . ipe : kiaterèddhamu , pos ise stammeni ?
e ' domena tinò pleo ?
deje , ma steo prikì ti arte pu ' sù peseni , evò meno manechimu mes ti ' tàlassa .
ellae ' ttù ma ' mena , rispùndefse o pappo , kanòscio larga larga ' rtea sti ' tàlassa : ecé citti tàlassa echi ena ' palai frabekao ; ecessu citto palai echi mian ursa , cini ursa vastà mian aquila , ènn' in esfàfsune , cini vastà tria ' guà , itta ' guà ènna ma skiattarìsune sto frontili ' ccena ' ccena : toa peseno ' vò .
torì , kiaterèddhamu ?
stasu skuscetata , ti ' vò ' e peseno makata .
efe ce pirte o pappo .
tui , tapu pirte , u ta ta ipe tunù pu jetti kristianò . tappu u ta tà' le , este prikì tui ti u ta tà' le tunù u paddhikari : Ah , ipe , amo ce kame utto praman arte !
ka ti e ' pu sù' pe ?
mù' pe iu c' iu . ma utto prama soggeste arte pu ipe iu cino .
Ich ! ipe cino , ka tuo e ' tosso prama fàcelo !
respùndefse cini : daveru lei ?
ka pleon daverun ' e soggesten , ipe cino .
tui arte , uttin addhi virtù pu iche tuo , fse aquila , ' e tù' fsere tui . sì ? respùndefse cini , eis addhe virtù ?
kame kame na dume ti ene !
aquila me fazzu , ipe cino , ce jetti aquila .
vu vu , ti cherèome , ipe cini ; vu ti cherèome !
meh , meh , allora ìmesta certi ti pame pu ' ttupanu ?
sì , sì , mi ' pensa ' cì , ipe cino .
èbbie i ' strava pale , fane ce pìane . beh , arte tris emere cerò ènna mu dois , ipe o paddhikari ; an es tutte tris emeres ' en ime ' rtomeno , na mi ' me mini pleo , ti ' vò irta sfammeno attin ursa .
èbbie tuo : stasu kalì , ce pensa ' s emena tappu ' e me torivi .
licenziefti , ipe : aquila mi farò ! is pètase pu ' mbrò ce pirte .
pirte panu sto kornici tu paladiu c' èkame i ' spia tuo , na di mi tò' rkete fatta na di an esfafsi in ursa : c' este d' àquila . kanòscio sìmmeri , kanòscion avri , all' ùrtimu èbbie c' in ide uttin ursa .
in ide pu stei , epelisti tuo ecessu . belisti ecessu sto palai ce jetti liuna .
nsignà na fsikkosì e ursa ce o liuna ; èbbie c' in èsfafse . dopu in èsfafse , is èbbie ittin àquila pu ' céssu c' in èsfafse c' is èbbie utta tria ' guà ; èbbie ena ' ghià ranfa sta diu poja , c' ena sto ' pitso ce pètase .
tappu pirte ' cì , cini este panu sti ' làmmia ce kanoni fissa ; maleppena on ide tui , is irte i ' fsichì sti ' mitti : Oh , ipe , grazia de diu , ste c' èrkete o ' nnamuràomu ! ipe . maleppena èftase , pensa ' sù arte , mbike ' cé sto ' paradiso cini arte ji ' charava .
u doke na fai ce ntrikefti na rotisi : ka pos èkame , pos èkame na feri tutta ' guà ?
fsero ' vò posso pàtefsa n' is kamo i ' spia cinìs ursa !
dunque efe , èbbie ce jetti furmikuleddha , ti oramai èftase o cerò .
me ' citto momento , ekusti o pappo ce ipe : prai nìfsemu , ipe , kiaterèddhamu , ti ' e sozo na nifso o ' portuna .
vu vu , furtùnamu , ipe tui ; ka poka , pai ce me finni ?
utta tria ' guà , tui , a vasta sto ' petto .
Pah ! tu skiattàrise o pronò sto frontili .
Ohimé ! ipe tuo , ime pesammeno .
èbbie ce u tò' sire t' addho , ' mpì cino t' addho ce pèsane .
piàkane , u tò' kofse i ' ciofali , o soma o pègliase sti ' tàlassa c' i ' ciofali ti ' krèmase ' cì sto palai , es ena ' ravdì tin èvale .
beh , pos kànnome arte ? ipe tui . evò on èsfafsa cino , ipe ; pensa ' sù arte pos ènna kàmome na pame pu ' ttù , ipe cini .
ipe cino : arte ' nnenno panu citti làmmia ce a ' toriso kané bastimento u kanno simai mo makkaluri nà' rti ' tturtea .
èbbie ' nneke panu sti ' làmmia cini ce nsìgnase na kami simai , ka ide o ' bastimento .
kànnonta simai , ekkùkkefse . ekkùkkefse c' ipe : ti teli ?
cini : mu kannis ena ' piacirin ? ipe cini .
ti telis ? ipe o kapitano .
ènna mas kami to piaciri na mas pari pu ' ttupanu .
gnorsì , ipe cino .
su dio tosso ce mas perni ; mas perni puru àrtena .
motti teli , ipe .
Sianosa ' ' tikaneve ce vàlane cipanu sto ' bastimento .
piaka ' ce pìrtane ce e ciofali u pappu èmine kremammeni .
pìrtane stamesa i ' tàlassa : itto pedì on efsìkkose e mamma Serena .
èmine òria cini ! èbbie ce ipe cini , to korasi : vu vu ! tuo mas ton èbbie ce mas ton èvale kau sti ' tàlassa mia vèkkia , ipe .
respùndefse o kapitano c' ipe : beh , prai na pame .
ipe cini : ' en èrkome makà , ipe cini , ti cino me libèrefse pu ' cìanu .
ce dunque pos kànnome ? Menome ' ttusimà ?
sì , ' ttusumuddhia ; kame na mini o ' bastimento ti ènnà' rti àndrammu , ipe cini . ' Ttusumùddhia meno ' vò ; ce ' sù àmone ' cì citto paisi ce rota pos ènna kàmome na guàlome pu kau sto nerò tuo .
àmone .
pirte , fatsamu san e ' Martignana , ce ròtise c' ipe : e Serena mas èbbie quai pedì ce tò' vale kau sti ' tàlassa ; anì bèkkia o kùntefse , ti teli ?
ipe cini : ti teli ?
teli poddhù sonu , ka cini en gapimeni attu ' sonu .
tuo ius èkame . ìvrike u ' malu sonu ce us pire ' cisimà citto punto pu iche cino .
nsignasa ' na ndalisun arte oli tui soni pu pire . e vèkkia pu iche panta na pari u ' sonu ìbbie puru ma tutu .
tappu is endalùsane , èbbie c' effaccefti cini e Serena . beh , ipe tui , beh , cittìsete , min endalìsete pleo , ipe e vèkkia , ' ttusumuddhia stei .
ipe tui , e Serena : Ah , vèkkia maledetta , esù iso ' pu èkame na mussèfsune e soni ? ipe .
tappu ' ffaccei cino pu kratì kau sto nerò , toa kordonni sonu , ipe .
efsikkonni mapale ce nsignasa ' na ndalìsune : meh , ndalìsete , ipe e vèkkia .
èbbie on effàccefse ìmiso . respùndefse cino , tappu on effàccefse : kàmete na ndalisi , iu forsi me ' ffaccei ros ta kònata ce ' vò petò .
na nkurtèfsome o ' kunto , tappu on enfàccefse ros ta kònata , èbbie ce pètase tuo . es to bastimento ' en embike pleo , pètase , pirte ' mbrò .
ftàsane stin agra is tàlassa ce katekan oli . iche posson enan addho ìmiso mili stra ' c' i ' kàmane allampedi ji ' charà .
ftàsane sto paisi : vu ! ntrikèftisa , ste c' èrkete e kiatera u ria : vresi !
ce ntrikèftisa na ndalìsune e kampane arte all' armi , makà pleo mortorio . o jeno : vu vu , irte e kiatera u ria ; is efseri pu este !
i ' pìrane sto palai , nsignasa ' na preparefsu ' tossa mala pràmata .
pai , èbbie i ' strava , doppu fane , doppu pìane , iche enan ambrosti ce on addho pu ' cì , cino pu in èffie pu ' cìvi , st' addho mero , dekoste . èbbie i ' stra ' c' ipe : arte ti teli , kiaterèddhamu , na rmastìs ? ipe o ciuri .
ce tùonna o teli , pu stei ' ttusumà ' mbrossu ?
na mu stasì kalò , ipe cini , ' e to telo makà tuo .
tuo ka scendei fse ria , kundu ìmesta ' mì , c " e to teli ?
o mu di tuo pu m' èffie pus ta chèria u Nanni Orku andé n' armastò ' e telo .
ce meh , kiaterèddhamu , tuo s' èffie pus ta chèria u Nanni Orku , tuo piao .
èbbie , ' cì citto stesso fai pu stèane , stefanòsane ce stàsisa kalì kuttenti ce kunsulai .
is pu te ' n' u ' di , na pai sti ' Napuli ka us torì .
