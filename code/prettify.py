import codecs
import sys

with codecs.open(sys.argv[1], 'r', 'utf-8') as inp:
	lines = inp.readlines()


outf = codecs.open(sys.argv[2], 'w', 'utf-8')
N = sys.argv[3]
for l in lines:
	l = l.strip().split()
	c = 0
	words = u''
	tags = u''
	wi = 0
	ti = 0
	for k in l:
		k = k.split('|')
		w,t = k[0], k[1]

		if len(w) + wi < N and len(t) + ti < N :
			words += w + ' '
			wi += len(w)+1
			tags += t + ' '
			ti += len(t) + 1
			if wi >= ti:
				for _ in range(wi-ti):
					tags += ' '
			else:
				for _ in range(ti-wi):
					words += ' '
					wi += 1
			ti = len(tags)
			wi = len(words)
		else:
			outf.write(words+'\n')
			outf.write(tags+'\n\n')
			words = w + ' '
			tags = t + ' '
			wi = len(w) + 1
			ti = len(t) + 1
			if wi >= ti:
				for _ in range(wi- ti):
					tags += ' '
			else:
				for _ in range(ti-wi):
					words += ' '
					wi += 1
			ti = len(tags)
			wi = len(words)
		print words, len(words), wi
		print tags, len(tags), ti
	outf.write(words+'\n')
	outf.write(tags+'\n\n')
	words = u''
	tags = u''
	wi = 0
	ti = 0
	for _ in range(N):
		outf.write('-')
	outf.write('\n\n')

outf.close()


