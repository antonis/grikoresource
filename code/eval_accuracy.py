import codecs
import sys

with codecs.open(sys.argv[1], 'r', 'utf-8') as inp:
	ref = inp.readlines()

with codecs.open(sys.argv[2], 'r', 'utf-8') as inp:
	hyp = inp.readlines()

h = [w for h in hyp for w in h.strip().split()]
r = [w for r in ref for w in r.strip().split()]

assert(len(r) == len(h))
correct = 0.0
allt = 0.0
for i,p in enumerate(r):
	p = p.split('|')
	gt = p[1]
	t = h[i].split('|')[1]
	if t == gt:
		correct += 1
allt = len(h)
print ("%.4f" % (correct/allt))






